package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.dto.TipNaZapasDto;

import java.util.List;

/**
 * Service pre pracu s mailom.
 *
 * User: Dano
 * Date: 27.2.2014
 */
public interface MailService {

    /**
     * Posle mail z preddefinovaneho gmail konta (podla username).
     *
     * @param mail            mailova adresa
     * @param predmet         predmet mailu
     * @param text            text mailu
     * @param priloha         priloha
     * @param prilohaFileName nazov suboru, pod ktorym sa ma priloha prilozit do mailu
     * @param prilohaMimeType mime type prilohy
     */
    void sendMail(String mail,
                  String predmet,
                  String text,
                  byte[] priloha,
                  String prilohaFileName,
                  String prilohaMimeType);

    /**
     * Prebehne neprecitane maily a ulozi zadane tipy pouzivatela
     */
    List<TipNaZapasDto> getNeprecitaneTipyNaZapas() throws Exception;
}
