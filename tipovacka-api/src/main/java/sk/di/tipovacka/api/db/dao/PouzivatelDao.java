package sk.di.tipovacka.api.db.dao;

import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.exception.EntityUniqueExeption;

/**
 * Dao objekt pre komunikaciu s DB pre pouzivatela
 * <p/>
 * User: Dano
 * Date: 9.5.2013
 */
public interface PouzivatelDao extends EntityDao<Pouzivatel> {

    /**
     * Ziska pouzivatela podla loginu.
     *
     * @param login zadany login pouzivatela
     * @return najdeny pouzivatel, alebo null
     * @throws sk.di.tipovacka.api.exception.EntityUniqueExeption ked pre jeden login je dohladanych viacej pouzivatelov
     */
    Pouzivatel getPouzivatelPodlaLoginu(String login) throws EntityUniqueExeption;

    /**
     * Dohlada pouzivatela podla mailu.
     *
     * @param mail mail pouzivatela
     * @return najdeny pouzivatel
     * @throws EntityUniqueExeption ked pre jeden login je dohladanych viacej pouzivatelov
     */
    Pouzivatel getPouzivatelPodlaMailu(String mail) throws EntityUniqueExeption;
}
