package sk.di.tipovacka.api.db;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import sk.di.tipovacka.api.enums.KoniecZapasuTypEnum;
import sk.di.tipovacka.api.enums.ZapasTypEnum;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Databazovy objekt pre zapas.
 *
 * User: Dano
 * Date: 10.5.2013
 */
public class Zapas extends Entity{

    private static final long serialVersionUID = 7548061750681870192L;

    private final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private Long sutazRocnikId;
    private SutazRocnik sutazRocnik;
    // datum, kedy sa zapas hra
    private Date datum;
    // skupina
    private String skupina;
    // domaci
    private Long domaciId;
    private Muzstvo domaci;
    // hostia
    private Long hostiaId;
    private Muzstvo hostia;
    // skore
    private Integer golyDomaci;
    private Integer golyHostia;
    private String priebeh;
    // typ zapasu, v akej casti sutaze sa hra
    private ZapasTypEnum typZapasu;
    // typ, ako zapas skoncil
    private KoniecZapasuTypEnum koniecZapasuTyp;
    // highlights
    private String highlightsUrl;

    public Long getSutazRocnikId() {
        return sutazRocnikId;
    }

    public void setSutazRocnikId(Long sutazRocnikId) {
        this.sutazRocnikId = sutazRocnikId;
    }

    public SutazRocnik getSutazRocnik() {
        return sutazRocnik;
    }

    public void setSutazRocnik(SutazRocnik sutazRocnik) {
        this.sutazRocnik = sutazRocnik;
    }

    public synchronized String getNaformatovanyDatum() {
        if (getDatum() == null) {
            return "";
        }

        return sdf.format(getDatum());
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getSkupina() {
        return skupina;
    }

    public void setSkupina(String skupina) {
        this.skupina = skupina;
    }

    public Long getDomaciId() {
        return domaciId;
    }

    public void setDomaciId(Long domaciId) {
        this.domaciId = domaciId;
    }

    public Muzstvo getDomaci() {
        return domaci;
    }

    public void setDomaci(Muzstvo domaci) {
        this.domaci = domaci;
    }

    public Long getHostiaId() {
        return hostiaId;
    }

    public void setHostiaId(Long hostiaId) {
        this.hostiaId = hostiaId;
    }

    public Muzstvo getHostia() {
        return hostia;
    }

    public void setHostia(Muzstvo hostia) {
        this.hostia = hostia;
    }

    public Integer getGolyDomaci() {
        return golyDomaci;
    }

    public void setGolyDomaci(Integer golyDomaci) {
        this.golyDomaci = golyDomaci;
    }

    public Integer getGolyHostia() {
        return golyHostia;
    }

    public void setGolyHostia(Integer golyHostia) {
        this.golyHostia = golyHostia;
    }

    public String getPriebeh() {
        return priebeh;
    }

    public void setPriebeh(String priebeh) {
        this.priebeh = priebeh;
    }

    public ZapasTypEnum getTypZapasu() {
        return typZapasu;
    }

    public void setTypZapasu(ZapasTypEnum typZapasu) {
        this.typZapasu = typZapasu;
    }

    public KoniecZapasuTypEnum getKoniecZapasuTyp() {
        return koniecZapasuTyp;
    }

    public void setKoniecZapasuTyp(KoniecZapasuTypEnum koniecZapasuTyp) {
        this.koniecZapasuTyp = koniecZapasuTyp;
    }

    public String getHighlightsUrl() {
        return highlightsUrl;
    }

    public void setHighlightsUrl(String highlightsUrl) {
        this.highlightsUrl = highlightsUrl;
    }

    /**
     * Vrati vitazne muzstvo zo zapasu.
     *
     * @return muzstvo
     */
    public Muzstvo getVitaz() {
        if (getKoniecZapasuTyp() != null && getKoniecZapasuTyp().isKoniecZapasu()) {
            return getGolyDomaci() > getGolyHostia()
                    ? getDomaci()
                    : getHostia();
        }

        return null;
    }

    public String toText() {
        return "Zapas{" +
                "id=" + getId() +
                ", datum=" + datum +
                ", superi=" + (domaci == null ? "TBD" : domaci.getKod()) + ":" + (hostia == null ? "TBD" : hostia.getKod())
                + "}";
    }

    @Override
    public String toString() {
        return "Zapas{" +
                "sutazRocnikId=" + sutazRocnikId +
                ", datum=" + datum +
                ", skupina='" + skupina + '\'' +
                ", domaci=" + domaci +
                ", hostia=" + hostia +
                ", golyDomaci=" + golyDomaci +
                ", golyHostia=" + golyHostia +
                ", priebeh='" + priebeh + '\'' +
                ", typZapasu=" + typZapasu +
                ", koniecZapasuTyp=" + koniecZapasuTyp +
                ", highlightsUrl='" + highlightsUrl + '\'' +
                '}';
    }
}
