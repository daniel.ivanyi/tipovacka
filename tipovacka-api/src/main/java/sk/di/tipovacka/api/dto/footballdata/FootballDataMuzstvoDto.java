package sk.di.tipovacka.api.dto.footballdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * DTOcko pre nacitanie muzstiev z REST sluzby http://api.football-data.org
 *
 * Created by divanyi on 7. 6. 2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FootballDataMuzstvoDto {

    private String code;
    private String name;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "FootballDataZapasDto{" +
                "code='" + code + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
