package sk.di.tipovacka.api.dto;

import sk.di.tipovacka.api.enums.EntityStatusEnum;

import java.io.Serializable;

/**
 * User: Dano
 * Date: 16.5.2013
 */
public abstract class AbstractDto implements Serializable {

    private static final long serialVersionUID = -737335961015365843L;

    private Long id;
    private EntityStatusEnum status;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public EntityStatusEnum getStatus() {
        return status;
    }

    public void setStatus(EntityStatusEnum status) {
        this.status = status;
    }

    public boolean isAktivny() {
        return (getStatus() != null && EntityStatusEnum.AKTIVNY.equals(getStatus()));
    }
}
