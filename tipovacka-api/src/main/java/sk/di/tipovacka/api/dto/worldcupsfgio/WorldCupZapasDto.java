package sk.di.tipovacka.api.dto.worldcupsfgio;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * Mapovanie DTOcka z http://worldcup.sfg.io
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorldCupZapasDto {

    private String status;
    private String time;
    private String datetime;
    @JsonProperty(value = "home_team")
    private WorldCupMuzstvoDto homeTeam;
    @JsonProperty(value = "away_team")
    private WorldCupMuzstvoDto awayTeam;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public WorldCupMuzstvoDto getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(WorldCupMuzstvoDto homeTeam) {
        this.homeTeam = homeTeam;
    }

    public WorldCupMuzstvoDto getAwayTeam() {
        return awayTeam;
    }

    public void setAwayTeam(WorldCupMuzstvoDto awayTeam) {
        this.awayTeam = awayTeam;
    }

    @Override
    public String toString() {
        return "WorldCupZapasDto{" +
                "status='" + status + '\'' +
                ", time='" + time + '\'' +
                ", datetime='" + datetime + '\'' +
                ", homeTeam=" + homeTeam +
                ", awayTeam=" + awayTeam +
                '}';
    }
}
