package sk.di.tipovacka.api.db;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.enums.RolaEnum;

import java.util.HashSet;
import java.util.Set;

/**
 * Objekt pre prenos udajov do db pre pouzivatela
 *
 * User: Dano
 * Date: 9.5.2013
 */
public class Pouzivatel extends Entity {

    private static final long serialVersionUID = 7307637861836437024L;

    private String login;
    private String heslo;
    private String priezvisko;
    private String meno;
    private String mail;
    private String roly;
    private Set<RolaEnum> rolyEnum;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHeslo() {
        return heslo;
    }

    public void setHeslo(String heslo) {
        this.heslo = heslo;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    public String getMeno() {
        return meno;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public String getRoly() {
        return roly;
    }

    public void setRoly(String roly) {
        this.roly = roly;
    }

    public Set<RolaEnum> getRolyEnum() {
        Set<RolaEnum> roly = new HashSet<>();

        if (StringUtils.isNotEmpty(getRoly())) {
            for (String rola : getRoly().split(" ")) {
               roly.add(RolaEnum.valueOf(rola));
            }
        }

        return roly;
    }

    public void setRolyEnum(Set<RolaEnum> rolyEnum) {
        this.rolyEnum = rolyEnum;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
