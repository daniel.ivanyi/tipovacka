package sk.di.tipovacka.api.comparator;

import sk.di.tipovacka.api.dto.ZapasDto;

import java.util.Comparator;

/**
 * Zoradi zapasy podla typu a datumu
 *
 * User: filusik
 * Date: 17.6.2013
 */
public class ZapasTypComparator implements Comparator<ZapasDto> {

    @Override
    public int compare(ZapasDto z1, ZapasDto z2) {
        if (z1.getTypZapasu().getPoradie() < z2.getTypZapasu().getPoradie()) {
            return -1;
        } else if (z1.getTypZapasu().getPoradie() > z2.getTypZapasu().getPoradie()) {
            return 1;
        } else {
            if (z1.getDatum().after(z2.getDatum())) {
                return 1;
            } else if (z1.getDatum().before(z2.getDatum())) {
                return -1;
            }
        }
        return 0;
    }
}
