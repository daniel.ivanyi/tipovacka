package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.dto.MuzstvoDto;

import java.util.List;

/**
 * Service pre pracu s muzstvom.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface MuzstvoService {

    /**
     * Prida muzstvo.
     *
     * @param autor   autor zmeny
     * @param muzstvo pridavane muzstvo
     * @return identifikator pridaneho muzstva
     */
    Long addMuzstvo(String autor, MuzstvoDto muzstvo);

    /**
     * Zedituje muzstvo.
     *
     * @param autor   autor zmeny
     * @param muzstvo muzstvo zo zmenenymi udajmi
     */
    void editMuzstvo(String autor, MuzstvoDto muzstvo);

    /**
     * Vrati muzstvo podla identifikatora.
     *
     * @param muzstvoId identifikator muzstva
     * @return najdene muzstvo
     */
    MuzstvoDto getMuzstvo(Long muzstvoId);

    /**
     * Vrati zoznam nadefinovanych muzstiev.
     *
     * @return zoznam muzstiev
     */
    List<MuzstvoDto> getMuzstva();

    /**
     * Dohlada muzstvo podla kodu krajiny.
     *
     * @param kod kod krajiny, za ktora muzstvo hra
     * @return najdene muzstvo
     */
    MuzstvoDto getMuzstvoPodlaKodu(String kod);


    /**
     * Deaktivuje muzstvo
     *
     * @param autor     autor zmeny
     * @param muzstvoId identifikator muzstva
     */
    void removeMuzstvo(String autor, Long muzstvoId);
}
