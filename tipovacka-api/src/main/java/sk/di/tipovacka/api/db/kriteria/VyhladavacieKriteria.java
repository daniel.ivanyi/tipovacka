package sk.di.tipovacka.api.db.kriteria;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.joda.time.DateMidnight;
import sk.di.tipovacka.api.enums.OrderDirectionEnum;

import java.io.Serializable;
import java.util.Date;

/**
 * Abstraktne vyhladavacie kriteria, ktore dedia vsetky vyhladavacie kriteria.
 * <p/>
 * User: Dano
 * Date: 15.5.2013
 */
public class VyhladavacieKriteria implements Serializable {

    private static final long serialVersionUID = -7429343255286081364L;

    private Long sutazRocnikId;
    private Date datumOd;
    private Date datumDo;
    // defaultne sa orderuje podla datum desc
    private String orderBy = "datum";
    private OrderDirectionEnum orderDirection = OrderDirectionEnum.DESC;

    public Long getSutazRocnikId() {
        return sutazRocnikId;
    }

    public void setSutazRocnikId(Long sutazRocnikId) {
        this.sutazRocnikId = sutazRocnikId;
    }

    public Date getDatumOd() {
        return datumOd;
    }

    public void setDatumOd(Date datumOd) {
        // pri setovani nastavime na prvu sekundu dna
        if (datumOd != null) {
            this.datumOd = new Date(new DateMidnight(datumOd).toDateTime().getMillis());
        } else {
            this.datumOd = null;
        }
    }

    public Date getDatumDo() {
        return datumDo;
    }

    public void setDatumDo(Date datumDo) {
        // pri setovani nastavime na poslednu sekundu dna
        if (datumDo != null) {
            this.datumDo = new Date(new DateMidnight(datumDo)
                    .toDateTime()
                    .plusDays(1)
                    .minusSeconds(1)
                    .getMillis());
        }  else {
            this.datumDo = null;
        }
    }

    /**
     * Ked nechceme aby sa datumOd nastavil na zaciatok dna
     */
    public void setDatumOdPresne(Date datumOd) {
        this.datumOd = datumOd;
    }

    /**
     * Ked nechceme aby sa datumDo nastavil na koniec dna
     */
    public void setDatumDoPresne(Date datumDo) {
        this.datumDo = datumDo;
    }

    public String getOrderBy() {
        return orderBy;
    }

    public void setOrderBy(String orderBy) {
        this.orderBy = orderBy;
    }

    public OrderDirectionEnum getOrderDirection() {
        return orderDirection;
    }

    public void setOrderDirection(OrderDirectionEnum orderDirection) {
        this.orderDirection = orderDirection;
    }

    @Override
    public String toString() {
        return "VyhladavacieKriteria{" +
                "sutazRocnikId=" + sutazRocnikId +
                ", datumOd=" + datumOd +
                ", datumDo=" + datumDo +
                ", orderBy='" + orderBy + '\'' +
                ", orderDirection=" + orderDirection +
                '}';
    }
}
