package sk.di.tipovacka.api.comparator;

import org.apache.commons.lang.StringUtils;
import sk.di.tipovacka.api.dto.MuzstvoDto;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * Comparator muzstva podla nazvu muzstva.
 */
public class MuzstvoNazovComparator implements Comparator<MuzstvoDto> {

    private Collator collator = Collator.getInstance(new Locale("sk", "SK"));

    @Override
    public int compare(MuzstvoDto m1, MuzstvoDto m2) {
        if (StringUtils.isNotBlank(m1.getNazov()) && StringUtils.isNotBlank(m2.getNazov())) {
            return collator.compare(m1.getNazov(), m2.getNazov());
        }

        return m1.getKod().compareTo(m2.getKod());
    }
}
