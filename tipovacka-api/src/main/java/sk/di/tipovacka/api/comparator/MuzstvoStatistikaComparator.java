package sk.di.tipovacka.api.comparator;

import sk.di.tipovacka.api.dto.MuzstvoStatistikaDto;

import java.util.Comparator;

/**
 * Comparator pre zotriedenie muzstiev vo vysledkovej tabulke.
 *
 * User: Dano
 * Date: 13.6.2013
 */
public class MuzstvoStatistikaComparator implements Comparator<MuzstvoStatistikaDto> {

    @Override
    public int compare(MuzstvoStatistikaDto m1, MuzstvoStatistikaDto m2) {
        // najskor sa zoraduje podla bodov
        if (m1.getBody() > m2.getBody()) {
            return -1;
        } else if (m1.getBody() < m2.getBody()) {
            return 1;
        } else {
            // nasledne zoradujeme podla poctu vitazstiev
            if (m1.getPocetVitazstiev() > m2.getPocetVitazstiev()) {
                return -1;
            } else if (m1.getPocetVitazstiev() < m2.getPocetVitazstiev()) {
                return 1;
            } else {
                // nasledne podla vitazstiev v predlzeni
                if (m1.getPocetVitazstievPredlzenie() > m2.getPocetVitazstievPredlzenie()) {
                    return -1;
                } else
                if (m1.getPocetVitazstievPredlzenie() < m2.getPocetVitazstievPredlzenie()) {
                    return 1;
                } else {
                    // nasledne podla kladneho skore
                    if (m1.getRozdielGolov() > m2.getRozdielGolov()) {
                        return -1;
                    } else if (m1.getRozdielGolov() < m2.getRozdielGolov()) {
                        return 1;
                    } else {
                        // nasledne podla poctu strelenych golov
                        if (m1.getPocetGolovStrelenych() > m2.getPocetGolovStrelenych()) {
                            return -1;
                        } else if (m1.getPocetGolovStrelenych() < m2.getPocetGolovStrelenych()) {
                            return 1;
                        }
                    }
                }
            }
        }

        return 0;
    }
}
