package sk.di.tipovacka.api.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import sk.di.tipovacka.api.db.SutazRocnik;

/**
 * Zakladne udaje charakterizujuce rocnik sutaze.
 *
 * User: Dano
 * Date: 10.11.2013
 */
public class RocnikSutazeHlavickaDto extends AbstractDto {

    private static final long serialVersionUID = -1209613088554037213L;

    private int rok;
    private SutazDto sutaz;

    public static RocnikSutazeHlavickaDto create(SutazRocnik rocnik) {
        RocnikSutazeHlavickaDto r = new RocnikSutazeHlavickaDto();
        r.setId(rocnik.getId());
        r.setSutaz(SutazDto.create(rocnik.getSutaz()));
        r.setRok(rocnik.getRok());

        return r;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    public SutazDto getSutaz() {
        return sutaz;
    }

    public void setSutaz(SutazDto sutaz) {
        this.sutaz = sutaz;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
