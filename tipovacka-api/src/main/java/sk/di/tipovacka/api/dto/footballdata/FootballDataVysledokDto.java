package sk.di.tipovacka.api.dto.footballdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * DTOcko pre nacitanie vysledku zapasu z REST sluzby http://api.football-data.org
 *
 * Created by divanyi on 7. 6. 2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FootballDataVysledokDto {

    private Integer goalsHomeTeam;
    private Integer goalsAwayTeam;

    public Integer getGoalsHomeTeam() {
        return goalsHomeTeam;
    }

    public void setGoalsHomeTeam(Integer goalsHomeTeam) {
        this.goalsHomeTeam = goalsHomeTeam;
    }

    public Integer getGoalsAwayTeam() {
        return goalsAwayTeam;
    }

    public void setGoalsAwayTeam(Integer goalsAwayTeam) {
        this.goalsAwayTeam = goalsAwayTeam;
    }

    @Override
    public String toString() {
        return "FootballDataVysledokDto{" +
                "goalsHomeTeam=" + goalsHomeTeam +
                ", goalsAwayTeam=" + goalsAwayTeam +
                '}';
    }
}
