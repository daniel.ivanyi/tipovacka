package sk.di.tipovacka.api.db.dao;

import sk.di.tipovacka.api.db.SutazRocnikSuhlas;

import java.util.List;

/**
 * Dao pre suhlas s ucato na danom rocniku sutaze.
 *
 * User: Dano
 * Date: 16.2.2014
 */
public interface SutazRocnikSuhlasDao extends EntityDao<SutazRocnikSuhlas> {

    /**
     * Priznak, ci pouzivatel pre dany roncik suhlasil s ucastou.
     *
     * @param pouzivatelId identifikator pouzivatela
     * @param rocnikId     identifikator rocnika sutaze
     * @return             priznak, ci dany pouzivatel suhlasil s podmienkami tipovania pre dany rocnik
     */
    boolean isPouzivatelSuhlas(Long pouzivatelId, Long rocnikId);

    /**
     * Vrati vsetky zaznamenane suhlasy pre dany rocnik.
     *
     * @param rocnikId identifikator rocnika sutaze
     * @return zoznam
     */
    List<SutazRocnikSuhlas> findAllByRocnik(Long rocnikId);
}
