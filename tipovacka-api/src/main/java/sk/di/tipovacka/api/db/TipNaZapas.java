package sk.di.tipovacka.api.db;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Date;

/**
 * Databazovy objekt pre tip
 *
 * User: Dano
 * Date: 10.5.2013
 */
public class TipNaZapas extends Entity {
    private static final long serialVersionUID = 4288742808014071525L;

    private Date datum;
    // zapas, na ktory sa tipuje
    private Long zapasId;
    private Zapas zapas;
    // pouzivatel, ktory zadava tip
    private Long pouzivatelId;
    private Pouzivatel pouzivatel;
    // tipovane skore, z neho sa odvodzuje aj kto vyhral
    private int golyDomaci;
    private int golyHostia;
    private boolean nahodnyTip;
    // ziskane body
    private int bodyZaVitaza;
    private int bodyZaGolyDomaci;
    private int bodyZaGolyHostia;

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Long getZapasId() {
        return zapasId;
    }

    public void setZapasId(Long zapasId) {
        this.zapasId = zapasId;
    }

    public Zapas getZapas() {
        return zapas;
    }

    public void setZapas(Zapas zapas) {
        this.zapas = zapas;
    }

    public Long getPouzivatelId() {
        return pouzivatelId;
    }

    public void setPouzivatelId(Long pouzivatelId) {
        this.pouzivatelId = pouzivatelId;
    }

    public Pouzivatel getPouzivatel() {
        return pouzivatel;
    }

    public void setPouzivatel(Pouzivatel pouzivatel) {
        this.pouzivatel = pouzivatel;
    }

    public int getGolyDomaci() {
        return golyDomaci;
    }

    public void setGolyDomaci(int golyDomaci) {
        this.golyDomaci = golyDomaci;
    }

    public int getGolyHostia() {
        return golyHostia;
    }

    public void setGolyHostia(int golyHostia) {
        this.golyHostia = golyHostia;
    }

    public boolean isNahodnyTip() {
        return nahodnyTip;
    }

    public void setNahodnyTip(boolean nahodnyTip) {
        this.nahodnyTip = nahodnyTip;
    }

    public int getBodyZaVitaza() {
        return bodyZaVitaza;
    }

    public void setBodyZaVitaza(int bodyZaVitaza) {
        this.bodyZaVitaza = bodyZaVitaza;
    }

    public int getBodyZaGolyDomaci() {
        return bodyZaGolyDomaci;
    }

    public void setBodyZaGolyDomaci(int bodyZaGolyDomaci) {
        this.bodyZaGolyDomaci = bodyZaGolyDomaci;
    }

    public int getBodyZaGolyHostia() {
        return bodyZaGolyHostia;
    }

    public void setBodyZaGolyHostia(int bodyZaGolyHostia) {
        this.bodyZaGolyHostia = bodyZaGolyHostia;
    }

    public Muzstvo getVitaz() {
        if (getGolyDomaci() == getGolyHostia()) {
            return new Muzstvo();
        }

        return getGolyDomaci() > getGolyHostia()
                ? getZapas().getDomaci()
                : getZapas().getHostia();
    }

    @Override
    public String toString() {
        return "TipNaZapas{" +
                "datum=" + datum +
                ", pouzivatel=" + (pouzivatel == null ? "null" : pouzivatel.getLogin()) +
                ", zapas=" + (zapas == null ? "null" : zapas.toText()) +
                ", goly={" + golyDomaci +":" + golyHostia + "}" +
                ", nahodnyTip=" + nahodnyTip +
                ", body={" + bodyZaVitaza + "+" + bodyZaGolyDomaci + "+" + bodyZaGolyHostia + "}" +
                "}";
    }
}
