package sk.di.tipovacka.api.dto;

/**
 * DTOcko pre zobrazenie statistiky daneho muzstva v rocniku sutaze.
 *
 * User: Dano
 * Date: 13.6.2013
 */
public class MuzstvoStatistikaDto extends AbstractDto {
    private static final long serialVersionUID = 1577851202686282967L;

    private MuzstvoDto muzstvo;
    private String mesto;
    private int pocetZapasov;
    private int pocetVitazstiev;
    private int pocetVitazstievPredlzenie;
    private int pocetPrehier;
    private int pocetPrehierPredlzenie;
    private int pocetGolovStrelenych;
    private int pocetGolovInkasovanych;
    private int body;

    public MuzstvoDto getMuzstvo() {
        return muzstvo;
    }

    public MuzstvoStatistikaDto setMuzstvo(MuzstvoDto muzstvo) {
        this.muzstvo = muzstvo;
        return this;
    }

    public String getMesto() {
        return mesto;
    }

    public MuzstvoStatistikaDto setMesto(String mesto) {
        this.mesto = mesto;
        return this;
    }

    public int getPocetZapasov() {
        return pocetZapasov;
    }

    public MuzstvoStatistikaDto  addPocetZapasov() {
        this.pocetZapasov += 1;
        return this;
    }

    public int getPocetVitazstiev() {
        return pocetVitazstiev;
    }

    public MuzstvoStatistikaDto  addPocetVitazstiev() {
        this.pocetVitazstiev += 1;
        return this;
    }

    public int getPocetVitazstievPredlzenie() {
        return pocetVitazstievPredlzenie;
    }

    public MuzstvoStatistikaDto  addPocetVitazstievPredlzenie() {
        this.pocetVitazstievPredlzenie += 1;
        return this;
    }

    public int getPocetPrehier() {
        return pocetPrehier;
    }

    public MuzstvoStatistikaDto  addPocetPrehier() {
        this.pocetPrehier += 1;
        return this;
    }

    public int getPocetPrehierPredlzenie() {
        return pocetPrehierPredlzenie;
    }

    public MuzstvoStatistikaDto addPocetPrehierPredlzenie() {
        this.pocetPrehierPredlzenie += 1;
        return this;
    }

    public int getPocetGolovStrelenych() {
        return pocetGolovStrelenych;
    }

    public MuzstvoStatistikaDto addPocetGolovStrelenych(int pocetGolovStrelenych) {
        this.pocetGolovStrelenych += pocetGolovStrelenych;
        return this;
    }

    public int getPocetGolovInkasovanych() {
        return pocetGolovInkasovanych;
    }

    public MuzstvoStatistikaDto addPocetGolovInkasovanych(int pocetGolovInkasovanych) {
        this.pocetGolovInkasovanych += pocetGolovInkasovanych;
        return this;
    }

    public int getBody() {
        return body;
    }

    public MuzstvoStatistikaDto addBody(int body) {
        this.body += body;
        return this;
    }

    /**
     * Textova prezentacia poctu golov strelenych a dostanych.
     *
     * @return text
     */
    public String getSkore() {
        return getPocetGolovStrelenych() + ":" + getPocetGolovInkasovanych();
    }

    /**
     * Rozdiel v poctu golov strelenych a dostanych.
     *
     * @return cislo
     */
    public int getRozdielGolov() {
        return getPocetGolovStrelenych() - getPocetGolovInkasovanych();
    }
}
