package sk.di.tipovacka.api.dto.footballdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * DTOcko pre nacitanie ucastnnikov z REST sluzby http://api.football-data.org
 *
 * Created by divanyi on 7. 6. 2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FootballDataUcastniciDto {

    private List<FootballDataMuzstvoDto> teams;

    public List<FootballDataMuzstvoDto> getTeams() {
        return teams;
    }

    public void setTeams(List<FootballDataMuzstvoDto> teams) {
        this.teams = teams;
    }

    @Override
    public String toString() {
        return "FootballDataUcastniciDto{" +
                "teams=" + teams +
                '}';
    }
}
