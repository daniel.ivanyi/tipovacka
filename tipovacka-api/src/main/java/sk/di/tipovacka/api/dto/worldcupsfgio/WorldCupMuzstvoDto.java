package sk.di.tipovacka.api.dto.worldcupsfgio;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Mapovanie DTOcka z http://worldcup.sfg.io
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class WorldCupMuzstvoDto {

    private String country;
    private String code;
    private Integer goals;
    private Integer penalties;

    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public Integer getGoals() {
        return goals;
    }

    public void setGoals(Integer goals) {
        this.goals = goals;
    }

    public Integer getPenalties() {
        return penalties;
    }

    public void setPenalties(Integer penalties) {
        this.penalties = penalties;
    }

    @Override
    public String toString() {
        return "WorldCupMuzstvoDto{" +
                "country='" + country + '\'' +
                ", code='" + code + '\'' +
                ", goals=" + goals +
                ", penalties=" + penalties +
                '}';
    }
}
