package sk.di.tipovacka.api.db;

import sk.di.tipovacka.api.enums.SportTypEnum;
import sk.di.tipovacka.api.enums.SutazTypEnum;

import java.util.Set;

/**
 * Databazovy objekt pre sutaz
 *
 * User: Dano
 * Date: 10.5.2013
 */
public class Sutaz extends Entity {

    private static final long serialVersionUID = -7154620191963642859L;

    private String nazov;
    private SportTypEnum sportTyp;
    private SutazTypEnum sutazTyp;
    private Set<SutazRocnik> rocniky;

    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public SportTypEnum getSportTyp() {
        return sportTyp;
    }

    public void setSportTyp(SportTypEnum sportTyp) {
        this.sportTyp = sportTyp;
    }

    public SutazTypEnum getSutazTyp() {
        return sutazTyp;
    }

    public void setSutazTyp(SutazTypEnum sutazTyp) {
        this.sutazTyp = sutazTyp;
    }

    public Set<SutazRocnik> getRocniky() {
        return rocniky;
    }

    public void setRocniky(Set<SutazRocnik> rocniky) {
        this.rocniky = rocniky;
    }
}
