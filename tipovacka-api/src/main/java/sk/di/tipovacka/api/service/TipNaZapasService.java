package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Service pre pracu s tipom na zapas.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface TipNaZapasService {

    /**
     * Prida tip na zapas. Ak sa uz neda pridavat na dany zapas tip,
     * vyhody exception.
     *
     * @param autor      autor zmeny
     * @param tipNaZapas tip na zapas
     * @return identifikator tipu na zapas
     * @throws sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException ked sa pokusame zadat tip na zapas po
     *                                        vyprsani casu na tipovanie
     */
    Long addTipNaZapas(String autor, TipNaZapasDto tipNaZapas) throws CasNaTipovanieVyprsalException;

    /**
     * Prida tip na zapas. Ak sa uz neda pridavat na dany zapas tip,
     * vyhody exception.
     *
     * @param autor      autor zmeny
     * @param tipNaZapas tip na zapas
     * @param admin      priznam, ci tip pridava admin
     * @return identifikator tipu na zapas
     * @throws sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException ked sa pokusame zadat tip na zapas po
     *                                        vyprsani casu na tipovanie
     */
    Long addTipNaZapas(String autor, TipNaZapasDto tipNaZapas, boolean admin) throws CasNaTipovanieVyprsalException, EntityNotFoundException;

    /**
     * Zedituje tip na zapas.
     *
     * @param autor      autor zmeny
     * @param tipNaZapas typ na zapas so zmenami
     * @throws CasNaTipovanieVyprsalException ked sa pokusame zadat tip na zapas po
     *                                        vyprsani casu na tipovanie
     * @throws EntityNotFoundException        ked sa pokusame editovat entitu, ktora este nie je ulozena
     */
    void editTipNaZapas(String autor, TipNaZapasDto tipNaZapas) throws CasNaTipovanieVyprsalException, EntityNotFoundException;

    /**
     * Zedituje tip na zapas, ked je priznak admin TRUE, nekontroluje sa, ci sa edituje tip na uz hrany zapas.
     *
     * @param autor      autor zmeny
     * @param tipNaZapas typ na zapas so zmenami
     * @param admin      priznam, ci tip edituje admin
     * @throws CasNaTipovanieVyprsalException ked sa pokusame zadat tip na zapas po
     *                                        vyprsani casu na tipovanie
     * @throws EntityNotFoundException        ked sa pokusame editovat entitu, ktora este nie je ulozena
     */
    void editTipNaZapas(String autor, TipNaZapasDto tipNaZapas, boolean admin) throws CasNaTipovanieVyprsalException, EntityNotFoundException;

    /**
     * Vrati tip na zapas podla identifikatora.
     *
     * @param tipNaZapasId identifikator na zapas
     * @return najdeny zapas
     */
    TipNaZapasDto getTipNaZapas(Long tipNaZapasId);

    /**
     * Vrati zoznam vsetkych tipov na dany(e) zapas(i).
     *
     *
     * @param zapasIds zoznam identifikatrov zapasov
     * @return zoznam tipov
     */
    List<TipNaZapasDto> getTipyNaZapasPodlaZapasId(List<Long> zapasIds);

    /**
     * Vrati zoznam vsetkych tipov na zapasy.
     *
     * @return zoznam tipov
     */
    List<TipNaZapasDto> getTipyNaZapas();

    /**
     * Dohlada tipy na zapas, kore vyhovuju vyhladavacim kriteriam.
     *
     * @param kriteria vyhladvacie kriteria
     * @return  zoznam najdenych typov
     */
    List<TipNaZapasDto> findTipyNaZapas(TipNaZapasKriteria kriteria);

    /**
     * Vrati distinct zoznama pouzivatelov, ktori tipovali minimalne na jeden zapas rocnika.
     *
     * @param rocnikId identifikator rocnika
     * @return zoznam tiperov
     */
    List<PouzivatelDto> getTipujucichNaZapas(Long rocnikId);

    /**
     * Vrati najlepsie tipujuceho na vitaza zapasu.
     *
     * @param rocnikId identifikator rocnika
     * @return zoznam tiperov
     */
    List<String> getNajlepsieTipujuciVitazaZapasu(Long rocnikId);

    /**
     * Vrati najlepsie tipujuceho skore zapasu.
     *
     * @param rocnikId identifikator rocnika
     * @return zoznam tiperov
     */
    List<String> getNajlepsieTipujuciSkoreZapasu(Long rocnikId);

    /**
     * Vrati najlepsie tipujuceho pomocou nahodnych tipov.
     *
     * @param rocnikId identifikator rocnika
     * @return zoznam tiperov
     */
    List<String> getNajlepsieNahodneTipujuci(Long rocnikId);
}
