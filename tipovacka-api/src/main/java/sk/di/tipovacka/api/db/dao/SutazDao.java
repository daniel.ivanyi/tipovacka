package sk.di.tipovacka.api.db.dao;

import sk.di.tipovacka.api.db.Sutaz;
import sk.di.tipovacka.api.enums.SportTypEnum;

import java.util.List;

/**
 * Dao pre pracu s databazovym objektom sutaz
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface SutazDao extends EntityDao<Sutaz> {

    /**
     * Vrati sutaze podla nazvu.
     *
     * @param nazovSutaze nazov sutaze
     * @param strict      priznak, ci sa vyhladava striktne podla zadaneho nazvu,
     *                    alebo zadany nazov sa obali *
     * @param typ         typ sportu (napr. hokej, futbal)
     * @return najdene sutaze
     */
    List<Sutaz> getSutazPodlaNazvu(String nazovSutaze, SportTypEnum typ, boolean strict);
}
