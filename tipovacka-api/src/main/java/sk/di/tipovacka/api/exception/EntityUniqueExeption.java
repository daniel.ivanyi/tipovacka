package sk.di.tipovacka.api.exception;

/**
 * Vynimka, ked nie je dodrzana jednoznacnost. Napriklad sa pokusame zapisat nieco co musi byt unigue,
 * alebo ked viacej entit ma ten isty unique kluc.
 *
 * User: Dano
 * Date: 9.5.2013
 */
public class EntityUniqueExeption extends RuntimeException {

    public EntityUniqueExeption() {
    }

    public EntityUniqueExeption(String message) {
        super(message);
    }

    public EntityUniqueExeption(String message, Throwable cause) {
        super(message, cause);
    }
}
