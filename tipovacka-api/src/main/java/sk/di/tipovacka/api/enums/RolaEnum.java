package sk.di.tipovacka.api.enums;

import org.springframework.security.core.GrantedAuthority;

/**
 * Rola pre uzivatela
 *
 * User: Dano
 * Date: 16.5.2013
 */
public enum RolaEnum implements GrantedAuthority {

    ADMIN("Administrátor"),
    POKLADNIK("Pokladník"),
    TIPER("Tipér");

    private String nazov;

    private RolaEnum(String nazov) {
        this.nazov = nazov;
    }

    public String getNazov() {
        return nazov;
    }

    @Override
    public String getAuthority() {
        return name();
    }
}
