package sk.di.tipovacka.api.dto;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.text.StrBuilder;
import sk.di.tipovacka.api.db.Muzstvo;

/**
 * Portalovy objekt pre muzstvo.
 *
 * User: Dano
 * Date: 26.5.2013
 */
public class MuzstvoDto extends AbstractDto {

    private static final long serialVersionUID = -8262945883524629270L;

    private String kod;
    private String nazov;
    private String nazovKod;

    public static MuzstvoDto create(Muzstvo muzstvo) {
        if (muzstvo == null) {
            return null;
        }

        MuzstvoDto m = new MuzstvoDto();
        m.setId(muzstvo.getId());
        m.setStatus(muzstvo.getStatus());
        m.setKod(muzstvo.getKod());
        m.setNazov(muzstvo.getNazov());
        m.setNazovKod(muzstvo.getNazovKod());

        return m;
    }

    public String getKod() {
        return kod;
    }

    /**
     * Metodka pre vratenie kodu muzstva, je v nej osetrene, ci sa je zadany kod statu, alebo klubu.
     *
     * @return kod
     */
    public String getKodMuzstva() {
        return StringUtils.isEmpty(getKod())
                ? getNazovKod()
                : getKod();
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public String getNazovKod() {
        return nazovKod;
    }

    public void setNazovKod(String nazovKod) {
        this.nazovKod = nazovKod;
    }

    @Override
    public String toString() {
        return "MuzstvoDto{" +
                "kod='" + kod + '\'' +
                ", nazov='" + nazov + '\'' +
                "} " + super.toString();
    }
}
