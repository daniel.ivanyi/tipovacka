package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.dto.MuzstvoStatistikaDto;
import sk.di.tipovacka.api.dto.PouzivatelovTipStatistikaDto;

import java.util.List;
import java.util.Map;

/**
 * Service pre statisticke zobrazenie udajov.
 *
 * User: Dano
 * Date: 13.6.2013
 */
public interface StatistikaService {

    /**
     * Vrati zosortovany zoznam statistiky/tabulku tiperov.
     *
     * @param rocnikId identifikator rocnika, za ktory sa ma vyskladat tabulka
     * @return zoznam tiperov
     */
    List<PouzivatelovTipStatistikaDto> getStatistikaTiperov(Long rocnikId);
}
