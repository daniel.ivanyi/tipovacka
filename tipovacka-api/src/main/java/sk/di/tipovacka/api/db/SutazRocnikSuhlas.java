package sk.di.tipovacka.api.db;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.sql.Timestamp;
import java.util.Date;

/**
 * Suhlas s podmienkami tipovania pre dany rocnik.
 *
 * User: Dano
 * Date: 16.2.2014
 */
public class SutazRocnikSuhlas extends Entity {

    private static final long serialVersionUID = -1380330918943065994L;

    private Date datum;
    private Long sutazRocnikId;
    private SutazRocnik sutazRocnik;
    private Long pouzivatelId;
    private Pouzivatel pouzivatel;
    private boolean suhlas;
    private Timestamp zaplatene;

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Long getSutazRocnikId() {
        return sutazRocnikId;
    }

    public void setSutazRocnikId(Long sutazRocnikId) {
        this.sutazRocnikId = sutazRocnikId;
    }

    public SutazRocnik getSutazRocnik() {
        return sutazRocnik;
    }

    public void setSutazRocnik(SutazRocnik sutazRocnik) {
        this.sutazRocnik = sutazRocnik;
    }

    public Long getPouzivatelId() {
        return pouzivatelId;
    }

    public void setPouzivatelId(Long pouzivatelId) {
        this.pouzivatelId = pouzivatelId;
    }

    public Pouzivatel getPouzivatel() {
        return pouzivatel;
    }

    public void setPouzivatel(Pouzivatel pouzivatel) {
        this.pouzivatel = pouzivatel;
    }

    public boolean isSuhlas() {
        return suhlas;
    }

    public void setSuhlas(boolean suhlas) {
        this.suhlas = suhlas;
    }

    public Timestamp getZaplatene() {
        return zaplatene;
    }

    public void setZaplatene(Timestamp zaplatene) {
        this.zaplatene = zaplatene;
    }

    @Override
    public String toString() {
        return "SutazRocnikSuhlas{" +
                "datum=" + datum +
                ", sutazRocnikId=" + sutazRocnikId +
                ", pouzivatelId=" + pouzivatelId +
                ", suhlas=" + suhlas +
                ", zaplatene=" + zaplatene +
                '}';
    }
}
