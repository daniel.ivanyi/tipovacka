package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.dto.RocnikSutazeHlavickaDto;

import java.io.IOException;
import java.util.List;

/**
 * Service pre pracu s rocnikom sutaze.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface SutazRocnikService {

    /**
     * Prida novy rocnik do sutaze.
     *
     * @param autor       autor zmeny
     * @param sutazRocnik pridavany rocnik sutaze
     * @return identifikator pridaneho rocnika
     */
    Long addSutazRocnik(String autor, RocnikSutazeDto sutazRocnik);

    /**
     * Zedituje rocnik sutaze
     *
     * @param autor       autor zmeny
     * @param sutazRocnik rocnik sutaze so zmenenymi udajmi
     */
    void editSutazRocnik(String autor, RocnikSutazeDto sutazRocnik);

    /**
     * Vrati rocnik sutaze podla id.
     *
     *
     * @param sutazRocnikId identifikator rocnika sutaze
     * @return najdeny rocnik, alebo null
     */
    RocnikSutazeDto getRocnikSutaze(Long sutazRocnikId);

    /**
     * Vrati aktualne prebiehajuce rocniky sutaze, na ktore sa da tipovat.
     *
     * @return aktualny rocnik sutaze
     */
    List<RocnikSutazeDto> getAktualnyRocnikSutaze();

    /**
     * Vrati zoznam vsetkych rocnikov sutazi
     *
     * @return zozna rocnikov
     */
    List<RocnikSutazeDto> getRocnikySutaze();

    /**
     * Vymaze rocnik sutaze, inymi slovami, nastavi rocnik ako neaktivny.
     *
     * @param autor         autor zmeny
     * @param sutazRocnikId identifikator rocnika sutaze, ktora sa ma vymazat
     */
    void removeRocnik(String autor, Long sutazRocnikId);

    /**
     * Inicializacia rocnika sutaze je na zaklade zadanej url statistiky.
     * Statistika je zostavena tak, ze sa daju z nej stiahnut udaje o skupinach
     * a zapasoch. Preto na zaklade zadaneho ID rocnika sutaze, je dohladana
     * urlka statistiky, ktora je nasledne parsovana a spracovana.
     *
     * @param autor autor zmeny
     * @param sutazRocnikId identifikator rocnika sutaze
     * @throws IOException vynimka, ked sa nepodarilo napojit na zadanu URL
     */
    void inicializujRocnikSutaze(String autor, Long sutazRocnikId) throws IOException;

    /**
     * Updatene priebeh rocnika podla udajov zo stranky statistiky. Doplni vysledky
     * a vyradovacie zapasy ak su zname.
     *
     * @param autor autor zmeny
     * @param sutazRocnikId identifikator rocnika sutaze
     * @throws IOException vynimka, ked sa nepodarilo napojit na zadanu URL
     */
    void updatePriebehRocnikaSutaze(String autor, Long sutazRocnikId) throws IOException;

    /**
     * Nasetuje vitaza rocnika, tym padom nasetuje aj body za tip na vitza.
     *
     * @param autor    autor, kto nastavil vitaza
     * @param rocnikId identifikator rocnika, pre ktory setujeme vitaza
     * @param vitazId  identifikator muzstva, ktore sa ma nastavit ako vitaz
     */
    void setVitazRocnika(String autor, Long rocnikId, Long vitazId);

    /**
     * Vrati zoznam ukoncenych rocnikov. To znamena, ze rocniky, ktore poznaju
     * vitazne muzstvo.
     *
     * @return zoznam ukoncenych rocnikov
     */
    List<RocnikSutazeHlavickaDto> getUkonceneRocniky();
}
