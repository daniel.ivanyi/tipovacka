package sk.di.tipovacka.api.db;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.Date;

/**
 * Tip na celkoveho vitaza rocnika sutaze.
 *
 * User: Dano
 * Date: 16.5.2013
 */
public class TipNaVitaza extends Entity {
    private static final long serialVersionUID = 203422376366896869L;

    private Date datum;
    private Long pouzivatelId;
    private Pouzivatel pouzivatel;
    private Long sutazRocnikId;
    private SutazRocnik sutazRocnik;
    private Long muzstvoId;
    private Muzstvo muzstvo;
    private boolean nahodnyTip;
    private int body;

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Long getPouzivatelId() {
        return pouzivatelId;
    }

    public void setPouzivatelId(Long pouzivatelId) {
        this.pouzivatelId = pouzivatelId;
    }

    public Pouzivatel getPouzivatel() {
        return pouzivatel;
    }

    public void setPouzivatel(Pouzivatel pouzivatel) {
        this.pouzivatel = pouzivatel;
    }

    public Long getSutazRocnikId() {
        return sutazRocnikId;
    }

    public void setSutazRocnikId(Long sutazRocnikId) {
        this.sutazRocnikId = sutazRocnikId;
    }

    public SutazRocnik getSutazRocnik() {
        return sutazRocnik;
    }

    public void setSutazRocnik(SutazRocnik sutazRocnik) {
        this.sutazRocnik = sutazRocnik;
    }

    public Long getMuzstvoId() {
        return muzstvoId;
    }

    public void setMuzstvoId(Long muzstvoId) {
        this.muzstvoId = muzstvoId;
    }

    public Muzstvo getMuzstvo() {
        return muzstvo;
    }

    public void setMuzstvo(Muzstvo muzstvo) {
        this.muzstvo = muzstvo;
    }

    public boolean isNahodnyTip() {
        return nahodnyTip;
    }

    public void setNahodnyTip(boolean nahodnyTip) {
        this.nahodnyTip = nahodnyTip;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "TipNaVitaza{" +
                "datum=" + datum +
                ", pouzivatel.login=" + (pouzivatel == null ? "null" : pouzivatel.getLogin()) +
                ", sutazRocnikId=" + sutazRocnikId +
                ", muzstvo.kod=" + (muzstvo == null ? "null" : muzstvo.getKod()) +
                ", nahodnyTip=" + nahodnyTip +
                ", body=" + body +
                "} " + super.toString();
    }
}
