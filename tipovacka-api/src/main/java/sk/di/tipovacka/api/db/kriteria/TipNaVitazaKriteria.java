package sk.di.tipovacka.api.db.kriteria;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.List;

/**
 * Vyhladavacie kriteria pre dohladanie tipov na vitaza.
 *
 * User: Dano
 * Date: 16.5.2013
 */
public class TipNaVitazaKriteria extends VyhladavacieKriteria {

    private List<Long> pouzivatelIds;

    public List<Long> getPouzivatelIds() {
        return pouzivatelIds;
    }

    public void setPouzivatelIds(List<Long> pouzivatelIds) {
        this.pouzivatelIds = pouzivatelIds;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
