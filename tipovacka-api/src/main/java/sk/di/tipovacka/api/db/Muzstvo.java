package sk.di.tipovacka.api.db;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.commons.lang.text.StrBuilder;

/**
 * Databazovy objekt pre muzstvo.
 *
 * User: Dano
 * Date: 10.5.2013
 */
public class Muzstvo extends Entity {

    private static final long serialVersionUID = 8687921328176489027L;

    private String kod;
    private String nazov;
    private String nazovKod;

    public String getKod() {
        return kod;
    }

    /**
     * Metodka pre vratenie kodu muzstva, je v nej osetrene, ci sa je zadany kod statu, alebo klubu.
     *
     * @return kod
     */
    public String getKodMuzstva() {
        return StringUtils.isEmpty(getKod())
                ? getNazovKod()
                : getKod();
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public String getNazovKod() {
        return nazovKod;
    }

    public void setNazovKod(String nazovKod) {
        this.nazovKod = nazovKod;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
