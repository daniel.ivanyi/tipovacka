package sk.di.tipovacka.api.db.kriteria;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Vyhladavacie kriteria pre dohladanie zapasov.
 * <p/>
 * User: Dano
 * Date: 15.5.2013
 */
public class ZapasKriteria extends VyhladavacieKriteria {

    private static final long serialVersionUID = -4910420803934217497L;

    private final Logger logger = Logger.getLogger(getClass());

    private List<Long> muzstvoIds;

    /**
     * Vratime nemenitelny list.
     *
     * @return nemenitelny list
     */
    public List<Long> getMuzstvoIds() {
        if (muzstvoIds == null) {
            muzstvoIds = new ArrayList<>();
        }

        return Collections.unmodifiableList(muzstvoIds);
    }

    /**
     * Prida identifikator muzstva do zoznamu vyhladavanych muzstiev.
     *
     * @param muzstvoId pridavany identifikator muzstva
     * @return  priznak pridania
     */
    public boolean addMuzstvoIds(Long muzstvoId) {
        if (getMuzstvoIds().size() > 2) {
            logger.warn("Maximalne mozu byt pridane 2 druzstva.");

            return false;
        }

        if (muzstvoIds == null) {
            muzstvoIds = new ArrayList<>();
        }

        return muzstvoIds.add(muzstvoId);
    }

    /**
     * Vycisti zoznam vyhladavanych muzstiev.
     */
    public void removeMuzstvoIds() {
        if (muzstvoIds != null) {
            muzstvoIds.clear();
        }
    }

    @Override
    public String toString() {
        return "ZapasKriteria{" +
                "muzstvoIds=" + muzstvoIds +
                "} " + super.toString();
    }
}
