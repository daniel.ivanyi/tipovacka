package sk.di.tipovacka.api.dto;

import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.enums.KoniecZapasuTypEnum;
import sk.di.tipovacka.api.enums.ZapasTypEnum;

import java.util.Date;

/**
 * Portalovy objekt pre zapas.
 *
 * User: Dano
 * Date: 26.5.2013
 */
public class ZapasDto extends AbstractDto {

    private static final long serialVersionUID = 8709238979807363405L;

    private Long rocnikSutazeId;
    private RocnikSutazeDto rocnikSutaze;
    private Date datum;
    private String skupina;
    private Long domaciId;
    private MuzstvoDto domaci;
    private Long hostiaId;
    private MuzstvoDto hostia;
    private Integer golyDomaci;
    private Integer golyHostia;
    private String priebeh;
    private ZapasTypEnum typZapasu;
    private KoniecZapasuTypEnum koniecZapasuTyp;
    private String highlightsUrl;

    public static ZapasDto create(Zapas zapas) {
        if (zapas == null) {
            return null;
        }

        ZapasDto z = new ZapasDto();
        z.setId(zapas.getId());
        z.setRocnikSutazeId(zapas.getSutazRocnikId());

        SutazRocnik rocnik = zapas.getSutazRocnik();
        RocnikSutazeDto r = new RocnikSutazeDto();
        r.setId(rocnik.getId());
        r.setRok(rocnik.getRok());
        r.setSutaz(SutazDto.create(rocnik.getSutaz()));
        r.setSutazId(rocnik.getSutaz().getId());
        r.setStatistikaUrl(rocnik.getStatistikaUrl());
        r.setStatus(rocnik.getStatus());
        r.setInit(rocnik.isInit());
        z.setRocnikSutaze(r);

        z.setDatum(zapas.getDatum());
        z.setSkupina(zapas.getSkupina());
        z.setDomaciId(zapas.getDomaciId());
        z.setDomaci(MuzstvoDto.create(zapas.getDomaci()));
        z.setHostiaId(zapas.getHostiaId());
        z.setHostia(MuzstvoDto.create(zapas.getHostia()));
        z.setGolyDomaci(zapas.getGolyDomaci());
        z.setGolyHostia(zapas.getGolyHostia());
        z.setPriebeh(zapas.getPriebeh());
        z.setTypZapasu(zapas.getTypZapasu());
        z.setKoniecZapasuTyp(zapas.getKoniecZapasuTyp());

        z.setHighlightsUrl(zapas.getHighlightsUrl());

        return z;
    }

    public Long getRocnikSutazeId() {
        return rocnikSutazeId;
    }

    public void setRocnikSutazeId(Long rocnikSutazeId) {
        this.rocnikSutazeId = rocnikSutazeId;
    }

    public RocnikSutazeDto getRocnikSutaze() {
        return rocnikSutaze;
    }

    public void setRocnikSutaze(RocnikSutazeDto rocnikSutaze) {
        this.rocnikSutaze = rocnikSutaze;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getSkupina() {
        return skupina;
    }

    public void setSkupina(String skupina) {
        this.skupina = skupina;
    }

    public Long getDomaciId() {
        return domaciId;
    }

    public void setDomaciId(Long domaciId) {
        this.domaciId = domaciId;
    }

    public MuzstvoDto getDomaci() {
        return domaci;
    }

    public void setDomaci(MuzstvoDto domaci) {
        this.domaci = domaci;
    }

    public Long getHostiaId() {
        return hostiaId;
    }

    public void setHostiaId(Long hostiaId) {
        this.hostiaId = hostiaId;
    }

    public MuzstvoDto getHostia() {
        return hostia;
    }

    public void setHostia(MuzstvoDto hostia) {
        this.hostia = hostia;
    }

    public Integer getGolyDomaci() {
        return golyDomaci;
    }

    public void setGolyDomaci(Integer golyDomaci) {
        this.golyDomaci = golyDomaci;
    }

    public Integer getGolyHostia() {
        return golyHostia;
    }

    public void setGolyHostia(Integer golyHostia) {
        this.golyHostia = golyHostia;
    }

    public String getPriebeh() {
        return priebeh;
    }

    public void setPriebeh(String priebeh) {
        this.priebeh = priebeh;
    }

    public ZapasTypEnum getTypZapasu() {
        return typZapasu;
    }

    public void setTypZapasu(ZapasTypEnum typZapasu) {
        this.typZapasu = typZapasu;
    }

    public KoniecZapasuTypEnum getKoniecZapasuTyp() {
        return koniecZapasuTyp;
    }

    public void setKoniecZapasuTyp(KoniecZapasuTypEnum koniecZapasuTyp) {
        this.koniecZapasuTyp = koniecZapasuTyp;
    }

    public String getHighlightsUrl() {
        return highlightsUrl;
    }

    public void setHighlightsUrl(String highlightsUrl) {
        this.highlightsUrl = highlightsUrl;
    }

    /**
     * Textova prezentacia skore.
     *
     * @return skore
     */
    public String getSkore() {
        if (getGolyDomaci() == null && getGolyHostia() == null) {
            return " vs ";
        }
        return getGolyDomaci() + " : " + getGolyHostia();
    }

    /**
     * Priznak, ci mozem tipovat na zapas.
     *
     * @return priznak
     */
    public boolean isMozemTipovatZapas() {
        // termin hrania musi byt v buducnosti
        boolean nezacaloSaHrat = getDatum() != null && getDatum().after(new Date());
        // a zapas musi mat nadefinovanych superov
        String pattern = "TBD|W\\d{2}|L\\d{2}";
        boolean maSuperov = !(getDomaci().getKod().matches(pattern) || getHostia().getKod().matches(pattern));

        return nezacaloSaHrat && maSuperov;
    }

    /**
     * Vrati vitazne muzstvo zo zapasu.
     *
     * @return muzstvo
     */
    public MuzstvoDto getVitaz() {
        if (getGolyDomaci() != null
                && getGolyHostia() != null
                && getKoniecZapasuTyp() != null
                && getKoniecZapasuTyp().isKoniecZapasu()) {
            return getGolyDomaci() > getGolyHostia()
                    ? getDomaci()
                    : getHostia();
        }

        return null;
    }

    /**
     * Vrati muzstvo, ktore prehralo.
     *
     * @return muzstvo
     */
    public MuzstvoDto getPrehra() {
        return getVitaz() == null
                ? null
                : (getVitaz().getId().equals(getDomaci().getId())
                        ? getHostia()
                        : getDomaci());
    }

    /**
     * Vrati priznak, ci zapas skoncil remizou
     *
     * @return priznak
     */
    public boolean isRemiza() {
        if (getKoniecZapasuTyp() != null && getKoniecZapasuTyp().isKoniecZapasu()) {
            return getGolyDomaci().equals(getGolyHostia());
        }

        return false;
    }

    /**
     * Podla stavu tipu na zapas vrati iconu.
     *
     * @param tip tip na zapas
     * @return classa icony
     */
    public String getTipNaZapasStavIcon(TipNaZapasDto tip) {
        // ak nemozem tipovat
        if (!isMozemTipovatZapas()) {
            return "fa fa-fw fa-lock";
        }
        // ak mozem tipovat, ale este som netipoval
        if (tip == null || tip.getDatum() == null) {
            return "fa fa-fw fa-question";
        } else {
            return tip.isNahodnyTip() ? "fa fa-fw fa-thumbs-up" : "fa fa-fw fa-pencil";
        }



//        if (getKoniecZapasuTyp() == null) {
//            return "fa fa-fw fa-question";
//        }
//
//        if (tip == null || tip.getDatum() == null) {
//            switch (getKoniecZapasuTyp()) {
//                case NEZACAL:
//                    return "fa fa-fw fa-exclamation";
//                default:
//                    return "fa fa-fw fa-lock";
//            }
//        }
//
//        switch (getKoniecZapasuTyp()) {
//            case NEZACAL:
//                return "fa fa-fw fa-star-empty";
//            case HRA_SA:
//                return "fa fa-fw fa-spinner fa fa-fw fa-spin"; //fa fa-fw fa-repeat fa fa-fw fa-spin
//            case RIADNY_CAS:
//            case PREDLZENIE:
//            case ROZSTREL:
//                // TODO prerobit na moznost vyuzivania properties
//                switch (tip.getBody()) {
//                    case 0:
//                    case 2:
//                        return "fa fa-fw fa-frown";
//                    case 6:
//                    case 8:
//                        return "fa fa-fw fa-meh";
//                    case 10:
//                        return "fa fa-fw fa-smile";
//                }
//            default:
//                return "fa fa-fw fa-question";
//        }
    }

    public String toText() {
        return "ZapasDto{" +
                "id=" + getId() +
                ", datum=" + datum +
                ", superi=" + (domaci == null ? "TBD" : domaci.getKod()) + ":" + (hostia == null ? "TBD" : hostia.getKod())
                + "}";
    }

    @Override
    public String toString() {
        return "ZapasDto{" +
                "rocnikSutazeId=" + rocnikSutazeId +
                ", datum=" + datum +
                ", skupina='" + skupina + '\'' +
                ", superi={" + (domaci == null ? "null" : domaci.getKod()) + ":" + (hostia == null ? "null" : hostia.getKod()) + "}" +
                ", goly={" + golyDomaci + ":" + golyHostia + "}" +
                ", priebeh='" + priebeh + '\'' +
                ", typZapasu=" + typZapasu +
                ", koniecZapasuTyp=" + koniecZapasuTyp +
                ", highlightsUrl='" + highlightsUrl + '\'' +
                "}";
    }
}
