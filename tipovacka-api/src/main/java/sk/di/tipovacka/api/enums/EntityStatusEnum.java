package sk.di.tipovacka.api.enums;

/**
 * Status entity.
 *
 * User: Dano
 * Date: 9.5.2013
 */
public enum EntityStatusEnum {

    AKTIVNY,
    NEAKTIVNY

}
