package sk.di.tipovacka.api.db.dao;

import java.util.List;
import java.util.Map;
import org.hibernate.FetchMode;
import sk.di.tipovacka.api.db.Entity;

/**
 * Genericky interface pre dao vrstvu.
 *
 * User: Dano
 * Date: 9.5.2013
 */
public interface EntityDao<T extends Entity> {

    /**
     * Nacita entitu podla id. Ak entita so zadanym id neexistuje, vrati null.
     *
     * @param id identifikator entitiy
     */
    T get(long id);
    
    /**
     * Nacita entitu podla id. + nacita vybrane asociacie s nim aj keby bolo lazy=true
     *
     * - alternativa je vytovrit v konkretneom DAO konkretnu metodu
     * - ale vzdy ked budme chceiet nieco zmenit 
     *   alebo pridat musime spravit novu metodu alebo upravit existujucu, takto nam staci zmenit len assosiaciu
     *
     * !! implementacia:
     *
     * Map<String, FetchMode> associationFetchModeMap = new HashMap<String, FetchMode>();
     *
     *  DetachedCriteria dc = DetachedCriteria.forClass(Hrac.class);
     *  dc.add(Restrictions.eq("id", id));
     *
     *  for (String a : associationFetchModeMap.keySet())  {
     *       FetchMode fm = associationFetchModeMap.get(a);
     *      dc.setFetchMode(a, fm);
     *  }
     *
     * !! pozor pouzivat o search podla ID, takze pouzivat len ak fakt vieme na co
     *
     * @param id identifikator entitiy
     * @param associationFetchModeMap - mapa asosiacii a fetch modov ktore maju byt fetchnute pri gete, kedze vsetko moze byt defatult lazy=true
     *
     */
    T get(long id, Map<String, FetchMode> associationFetchModeMap);

    /**
     * Nacita entity podla ids. Ak entity so zadanym id neexistuju, na ich
     * mieste v zozname bude null.
     *
     * @param ids zoznam identifikatorov hladanych entit
     */
    List<T> get(List<Long> ids);

    /**
     * Nacita entitu podla id. Tato metoda moze vratit proxy na danu entitu. Ak
     * entita so zadanym id neexistuje, vyhodi vynimku, alebo vrati neplatnu
     * proxy.
     *
     * @param id identifikator entitiy
     */
    T load(long id);

    /**
     * Uzamkne entitu na citanie.
     * 
     * @param entity entita ktora ma byt locknuta
     */
    void lock(T entity);
    
    /**
     * Perzistuje zadanu entitu.
     *
     * @param entity entita ktora ma byt perzistentna
     */
    public Long save(T entity);
            
    /**
     * Perzistuje zadanu entitu.
     *
     * @param entity entita ktora ma byt perzistentna
     */
    void saveOrUpdate(T entity);

    /**
     * Zmerguje entitu.
     *
     * @param entity entita ktora ma byt mergnuta
     */
    T merge(T entity);

    /**
     * Vymaze entitu.
     *
     * @param entity entita ktora ma byt vymazana
     */
    void delete(T entity);

    /**
     * Vymaze entitu podla jej id.
     *
     * @param id identifikator mazanej entity
     */
    void delete(long id);

    /**
     * Vykona vsetky cakajuce operacie.
     */
    void flush();
    
    /**
     * Dotiahnutie vsetkych zaznamov tohto typu.
     */
    List<T> findAll();
}
