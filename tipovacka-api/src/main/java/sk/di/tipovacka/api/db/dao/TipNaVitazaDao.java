package sk.di.tipovacka.api.db.dao;

import sk.di.tipovacka.api.db.TipNaVitaza;

import java.util.List;

/**
 * Dao pre pracu s databazovym objektom tip na vitaza.
 * <p/>
 * User: Dano
 * Date: 16.5.2013
 */
public interface TipNaVitazaDao extends EntityDao<TipNaVitaza> {

    /**
     * Dohlada vsetky tipy pre vitaza rocnika sutaze.
     *
     * @param sutazRocnikId identifikator rocnika sutaze
     * @return zoznam tipov
     */
    List<TipNaVitaza> getTipyNaVitaza(Long sutazRocnikId);

    /**
     * Dohlada pre dany rocnik a pouzivatela tip na vitaza.
     *
     * @param pouzivatelId identifikator pouzivatela
     * @param rocnikId identifikator rocnika sutaze
     * @return najdeny tip
     */
    TipNaVitaza getTipyNaVitazaRocnikaPodlaLogin(Long pouzivatelId, Long rocnikId);

    /**
     * Tipy na vitaza pre daneho pouzivatela.
     *
     * @param pouzivatelId identifikator pouzivatela
     * @return zoznam tipov
     */
    List<TipNaVitaza> getTipyNaVitazaPodlaLogin(Long pouzivatelId);
}
