package sk.di.tipovacka.api.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import sk.di.tipovacka.api.comparator.ZapasDatumComparator;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.enums.ZapasTypEnum;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * DTOcko pre zobrazenie rocnika sutaze.
 * <p/>
 * User: Dano
 * Date: 23.5.2013
 */
public class RocnikSutazeDto extends AbstractDto {

    private static final long serialVersionUID = 1408101530172930948L;

    private int rok;
    private Long sutazId;
    private SutazDto sutaz;
    private String statistikaUrl;
    private boolean init;
    private boolean aktualny;
    private List<ZapasDto> zapasy;
    // vitaz rocnika
    private Long vitazId;
    private MuzstvoDto vitaz;
    private Date aktualizacia;
    private boolean test;
    private Date zaciatokRocnikaTest;
    // priznak na FE, ci si dany rocnik zvolil pouzivatel
    private Boolean selected = false;

    /**
     * Vytvori z databazoveho objektu portalovy.
     *
     * @param rocnik databazovy objekt
     * @return portalovy objekt
     */
    public static RocnikSutazeDto create(SutazRocnik rocnik) {
        if (rocnik == null) {
            return null;
        }

        RocnikSutazeDto r = new RocnikSutazeDto();
        r.setId(rocnik.getId());
        r.setRok(rocnik.getRok());
        r.setSutaz(SutazDto.create(rocnik.getSutaz()));
        r.setSutazId(rocnik.getSutaz().getId());
        r.setStatistikaUrl(rocnik.getStatistikaUrl());
        r.setStatus(rocnik.getStatus());
        r.setInit(rocnik.isInit());
        r.setAktualny(rocnik.isAktualny());
        for (Zapas z : rocnik.getZapasy()) {
            r.getZapasy().add(ZapasDto.create(z));
        }
        r.setVitazId(rocnik.getVitazId());
        r.setVitaz(MuzstvoDto.create(rocnik.getVitaz()));
        r.setAktualizacia(rocnik.getAktualizacia());
        r.setTest(rocnik.isTest());
        r.setZaciatokRocnikaTest(rocnik.getZaciatokRocnikaTest());

        return r;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    public Long getSutazId() {
        return sutazId;
    }

    public void setSutazId(Long sutazId) {
        this.sutazId = sutazId;
    }

    public SutazDto getSutaz() {
        return sutaz;
    }

    public void setSutaz(SutazDto sutaz) {
        this.sutaz = sutaz;
    }

    public String getStatistikaUrl() {
        return statistikaUrl;
    }

    public void setStatistikaUrl(String statistikaUrl) {
        this.statistikaUrl = statistikaUrl;
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }

    public boolean isAktualny() {
        return aktualny;
    }

    public void setAktualny(boolean aktualny) {
        this.aktualny = aktualny;
    }

    public List<ZapasDto> getZapasy() {
        if (zapasy == null) {
            zapasy = new ArrayList<>();
        }
        return zapasy;
    }

    public void setZapasy(List<ZapasDto> zapasy) {
        this.zapasy = zapasy;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }

    public Date getZaciatokRocnikaTest() {
        return zaciatokRocnikaTest;
    }

    public void setZaciatokRocnikaTest(Date zaciatokRocnikaTest) {
        this.zaciatokRocnikaTest = zaciatokRocnikaTest;
    }

    public Long getVitazId() {
        return vitazId;
    }

    public void setVitazId(Long vitazId) {
        this.vitazId = vitazId;
    }

    public MuzstvoDto getVitaz() {
        return vitaz;
    }

    public void setVitaz(MuzstvoDto vitaz) {
        this.vitaz = vitaz;
    }

    public Date getAktualizacia() {
        return aktualizacia;
    }

    public void setAktualizacia(Date aktualizacia) {
        this.aktualizacia = aktualizacia;
    }

    public Boolean getSelected() {
        return selected;
    }

    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    /**
     * Vrati zaciatok prveho zapasu.
     *
     * @return datum
     */
    public Date getZaciatokRocnika() {
        if (!getZapasy().isEmpty()) {
            Collections.sort(getZapasy(), new ZapasDatumComparator());
            ZapasDto prvyZapas = getZapasy().get(0);
            if (prvyZapas != null) {
                return prvyZapas.getDatum();
            }
        }

        return null;
    }

    /**
     * Vrati zaciatok posledneho zapasu.
     *
     * @return datum
     */
    public Date getKoniecRocnika() {
        if (!getZapasy().isEmpty()) {
            Collections.sort(getZapasy(), new ZapasDatumComparator());
            ZapasDto poslednyZapas = getZapasy().get(getZapasy().size() - 1);
            if (poslednyZapas != null) {
                return poslednyZapas.getDatum();
            }
        }

        return null;
    }

    /**
     * Vrati prvy zapas rocnika
     *
     * @return zapas
     */
    public ZapasDto getPrvyZapas() {
        if (!getZapasy().isEmpty()) {
            Collections.sort(getZapasy(), new ZapasDatumComparator());
            return getZapasy().get(0);
        }

        return null;
    }

    /**
     * Vrati posledny zapas.
     *
     * @return zapas
     */
    public ZapasDto getPoslednyZapas() {
        if (!getZapasy().isEmpty()) {
            Collections.sort(getZapasy(), new ZapasDatumComparator());
            return getZapasy().get(getZapasy().size() - 1);
        }

        return null;
    }

    /**
     * Vrati priznak, ci rocnik uz skoncil. To znamena, ze bol odohraty aj posledny zapas.
     *
     * @return priznak
     */
    public boolean isKoniecRocnika() {
        ZapasDto poslednyZapas = getPoslednyZapas();

        return poslednyZapas != null && poslednyZapas.getKoniecZapasuTyp().isKoniecZapasu();

    }

    /**
     * Vrati zaciatok castu sutaze.
     *
     * @param typ cast sutaze, pre ktoru hladame zaciatok
     * @return zaciatok hladanej casti sutaze
     */
    public Date getZaciatokCasti(ZapasTypEnum typ) {
        if (typ == null) {
            typ = ZapasTypEnum.PRE;
        }
        Date zaciatok = null;
        for (ZapasDto z : getZapasy()) {
            if (z.getTypZapasu() != null && typ.equals(z.getTypZapasu())) {
                if (zaciatok == null || zaciatok.after(z.getDatum())) {
                    zaciatok = z.getDatum();
                }
            }
        }

        return zaciatok;
    }

    /**
     * Vrati koniec casti sutaze.
     *
     * @param typ cast sutaze, pre ktoru hladame koniec
     * @return koniec hladanej casti sutaze
     */
    public Date getKoniecCasti(ZapasTypEnum typ) {
        if (typ == null) {
            typ = ZapasTypEnum.PRE;
        }
        Date koniec = null;
        for (ZapasDto z : getZapasy()) {
            if (z.getTypZapasu() != null && typ.equals(z.getTypZapasu())) {
                if (koniec == null || koniec.before(z.getDatum())) {
                    koniec = z.getDatum();
                }
            }
        }

        return koniec;
    }

    /**
     * Vrati zoznam muzstiev, ktore sa zucastnuju danneho rocnika.
     *
     * @return  zoznam muzstiev
     */
    public List<MuzstvoDto> getMuzstva() {

        List<MuzstvoDto> muzstva = new ArrayList<>();

        if (!getZapasy().isEmpty()) {
            Set<Long> muzstvaSet = new HashSet<>();
            for (ZapasDto z : getZapasy()) {
                if (z.getDomaci() != null && !muzstvaSet.contains(z.getDomaci().getId())) {
                    muzstvaSet.add(z.getDomaci().getId());
                    muzstva.add(z.getDomaci());
                }
                if (z.getHostia() != null && !muzstvaSet.contains(z.getHostia().getId())) {
                    muzstvaSet.add(z.getHostia().getId());
                    muzstva.add(z.getHostia());
                }
            }
        }

        return muzstva;
    }

    /**
     * Priznak, ci v danom rocniku su este zapasy, na ktore sa da tipovat.
     *
     * @return priznak tipovatelnosti
     */
    public boolean isMozemTipovatZapasy() {
        return getPoslednyZapas().getDatum() != null && getPoslednyZapas().getDatum().after(new Date());
    }

    @Override
    public String toString() {
        return "RocnikSutazeDto{" +
                "rok=" + rok +
                ", sutazId=" + sutazId +
                ", sutaz=" + (sutaz == null ? "null" : sutaz) +
                ", statistikaUrl='" + statistikaUrl + '\'' +
                ", init=" + init +
                ", aktualny=" + aktualny +
                ", zapasy=" + (zapasy == null ? "null" : zapasy.size()) +
                ", vitaz=" + (vitaz == null ? "null" : vitaz.getKod()) +
                ", aktualizacia=" + aktualizacia +
                ", test=" + test +
                ", zaciatokRocnikaTest=" + zaciatokRocnikaTest +
                ", selected=" + selected +
                "}";
    }
}
