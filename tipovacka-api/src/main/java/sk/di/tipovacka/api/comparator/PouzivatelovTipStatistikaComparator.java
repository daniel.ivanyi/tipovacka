package sk.di.tipovacka.api.comparator;

import sk.di.tipovacka.api.dto.PouzivatelovTipStatistikaDto;

import java.text.Collator;
import java.util.Comparator;
import java.util.Locale;

/**
 * Zotriedi tiperov podla ziskanych boodv.
 *
 * User: filusik
 * Date: 19.6.2013
 */
public class PouzivatelovTipStatistikaComparator implements Comparator<PouzivatelovTipStatistikaDto> {

    @Override
    public int compare(PouzivatelovTipStatistikaDto p1, PouzivatelovTipStatistikaDto p2) {
        // zotriedime podla bodov
        if (p1.getBody() > p2.getBody()) {
            return -1;
        } else if (p1.getBody() < p2.getBody()) {
            return 1;
        // pri rovnosti bodov sortujeme podla poctu vyssich tipov
        } else {
            // podla vysledku (10b)
            if (p1.getPocetTipovVysledok() > p2.getPocetTipovVysledok()) {
                return -1;
            } else if (p1.getPocetTipovVysledok() < p2.getPocetTipovVysledok()) {
                return 1;
            } else {
                // podla vitaza a golu (8b)
                if (p1.getPocetTipovVitazGol() > p2.getPocetTipovVitazGol()) {
                    return -1;
                } else if (p1.getPocetTipovVitazGol() < p2.getPocetTipovVitazGol()) {
                    return 1;
                } else {
                    // podla vitaza (6b)
                    if (p1.getPocetTipovVitaz() > p2.getPocetTipovVitaz()) {
                        return -1;
                    } else if (p1.getPocetTipovVitaz() < p2.getPocetTipovVitaz()) {
                        return 1;
                    } else {
                        // podla golu (2b)
                        if (p1.getPocetTipovGoly() > p2.getPocetTipovGoly()) {
                            return -1;
                        } else if (p1.getPocetTipovGoly() < p2.getPocetTipovGoly()) {
                            return 1;
                        // podla priezviska
                        } else {
                            Collator collator = Collator.getInstance(new Locale("sk", "SK"));
                            collator.setStrength(Collator.PRIMARY);
                            return p1.getPouzivatel().getLogin().toLowerCase().compareTo(p2.getPouzivatel().getLogin().toLowerCase());
                        }
                    }
                }
            }
        }
    }
}
