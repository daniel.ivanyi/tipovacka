package sk.di.tipovacka.api.enums;

import org.apache.commons.lang.StringUtils;

import java.util.*;

/**
 * Typ zapasu podla umiestnenie v ramci sutaze
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public enum ZapasTypEnum {
    QR(0),      // Qualifying Round - predkolo zakladnej sutaze
    PRE(1),     // Preliminary Round - zakladna skupina
    PR(1),      // Preliminary Round - zakladna skupina
    EF(2),      // Eighthfinals (Round of 16) - osemfinale
    QF(3),      // Quarterfinals - stvrtfinale
    SF(4),      // Semifinals - semifinale
    BMG(5),     // Bronze Medal Game - zapas o tretie miesto
    GMG(6),     // Gold Medal Game - zapas o zlato
    UNKNOWN(99); // Neznamy

    private int poradie;

    private ZapasTypEnum(int poradie) {
        this.poradie = poradie;
    }

    public int getPoradie() {
        return poradie;
    }

    public static List<ZapasTypEnum> getZapasTypy() {
        List<ZapasTypEnum> typy = new ArrayList<>();
        typy.addAll(Arrays.asList(ZapasTypEnum.values()));
        typy.remove(UNKNOWN);
        Collections.sort(typy, new Comparator<ZapasTypEnum>() {
            @Override
            public int compare(ZapasTypEnum o1, ZapasTypEnum o2) {
                return Integer.valueOf(o1.getPoradie()).compareTo(o2.getPoradie());
            }
        });

        return typy;
    }

    /**
     * Vyparsujem podla textacie enum hodnotu.
     *
     * @param zapasTypText textova prezentacia
     * @return enum hodnota
     */
    public static ZapasTypEnum parseZapasTyp(String zapasTypText) {
        if (StringUtils.isEmpty(zapasTypText)) {
            return null;
        }

        if (zapasTypText.contains("Skupina")) {
            return PR;
        } else if ("osemfinále".equalsIgnoreCase(zapasTypText)) {
            return EF;
        } else if ("štvrťfinále".equalsIgnoreCase(zapasTypText)) {
            return QF;
        } else if ("semifinále".equalsIgnoreCase(zapasTypText)) {
            return SF;
        } else if (zapasTypText.contains("miesto")) {
            return BMG;
        } else if ("finále".equalsIgnoreCase(zapasTypText)) {
            return GMG;
        } else {
            return UNKNOWN;
        }
    }
}
