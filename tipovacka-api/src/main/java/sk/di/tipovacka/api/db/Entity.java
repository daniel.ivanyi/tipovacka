package sk.di.tipovacka.api.db;

import sk.di.tipovacka.api.enums.EntityStatusEnum;

import java.io.Serializable;

/**
 * Abstraktna entita obsahujuca id a bezparametricky konstruktor ako
 * zaklad pre genericke Dao. Vsetky perzistentne triedy z nej musia dedit.
 *
 * User: Dano
 * Date: 9.5.2013
 */
public abstract class Entity implements Serializable {

    private static final long serialVersionUID = 466466640771343728L;

    private Long id;
    private EntityStatusEnum status = EntityStatusEnum.AKTIVNY;

    public Entity() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    protected Entity(Long id) {
        this.id = id;
    }

    public EntityStatusEnum getStatus() {
        return status;
    }

    public void setStatus(EntityStatusEnum status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Entity[" + "id=" + id + ", status=" + status + "]";
    }
	
	/**
	 * Porovnava Entity - rovnake su, ak maju rovnaku triedu a rovnake identifikatory.
	 */
	@Override
	public boolean equals(Object o) {
		if (!this.getClass().equals(o.getClass())) {
			return false;
		}
		Entity e = (Entity) o; //mozem castovat vdaka predchadzajucej podmienke, ktora konci spracovanie ak su triedy odlisne
		
		if (id == null && e.getId() == null) { //ak maju obe entity id == null, tak chyba
			throw new IllegalStateException("nemozem posudzovat cez equals entity, ktore obe maju id rovne null");
		}
		if (id == null && e.getId() != null) {
			return false;
		}
		
		return id.equals(e.getId());
	}
	
	/**
	 * Hash kod sa generuje na zaklade id.
	 *
	 * @return -1 ak id je null, inak id.hashCode()
	 */
	@Override
	public int hashCode() {
		if (id == null) {
			return -1;
		}
		
		return id.hashCode();
	}
}
