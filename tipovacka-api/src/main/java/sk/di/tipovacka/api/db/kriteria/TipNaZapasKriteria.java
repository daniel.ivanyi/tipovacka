package sk.di.tipovacka.api.db.kriteria;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;

import java.util.ArrayList;
import java.util.List;

/**
 * Vyhladavacie kriteria pre dohladanie tipov na zapas.
 * <p/>
 * User: Dano
 * Date: 15.5.2013
 */
public class TipNaZapasKriteria extends VyhladavacieKriteria {
    private static final long serialVersionUID = -2614847322497535820L;

    private List<Long> pouzivatelIds;
    private List<Long> zapasIds;

    public List<Long> getPouzivatelIds() {
        if (pouzivatelIds == null) {
            pouzivatelIds = new ArrayList<>();
        }

        return pouzivatelIds;
    }

    public void setPouzivatelIds(List<Long> pouzivatelIds) {
        this.pouzivatelIds = pouzivatelIds;
    }

    public List<Long> getZapasIds() {
        if (zapasIds == null) {
            zapasIds = new ArrayList<>();
        }
        return zapasIds;
    }

    public void setZapasIds(List<Long> zapasIds) {
        this.zapasIds = zapasIds;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
