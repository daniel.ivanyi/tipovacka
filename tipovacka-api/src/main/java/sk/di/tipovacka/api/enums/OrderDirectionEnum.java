package sk.di.tipovacka.api.enums;

/**
 * Enum smeru sortovani
 *
 * User: Dano
 * Date: 10.6.2013
 */
public enum OrderDirectionEnum {
    ASC,
    DESC
}
