package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.SportTypEnum;
import sk.di.tipovacka.api.enums.SutazTypEnum;

import javax.persistence.EntityNotFoundException;
import java.util.Date;
import java.util.List;

/**
 * Service pre pracu so zapasom.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface ZapasService {

    /**
     * Zalozi zapas do systemu.
     *
     * @param autor autor zmeny
     * @param zapas zapas, ktory sa uklada
     * @return identifikator zalozeneho zapasu
     */
    Long addZapas(String autor, ZapasDto zapas);

    /**
     * Editacia zapasu.
     *
     * @param autor autor zmeny
     * @param zapas zmeneny zapas
     * @throws EntityNotFoundException ked nie je dohladany povodny zapas
     */
    void editZapas(String autor, ZapasDto zapas) throws EntityNotFoundException;

    /**
     * Vrati zapas podla identifikatora.
     *
     * @param zapasId identifikator zapasu
     * @return najdeny zapas, alebo null
     */
    ZapasDto getZapas(Long zapasId);

    /**
     * Vrati zoznam zapasov pre dany rocnik sutaze.
     *
     * @param rocnikSutazeId identifikator rocnika sutaze
     * @return zoznam zapasov
     */
    List<ZapasDto> getZapasyPodlaRocnikaSutaze(Long rocnikSutazeId);

    /**
     * Vrati vsetky zapasy, pre ktore vyhovuju vyhladavacie kriteria.
     *
     * @param kriteria vyhladavacie kriteria
     * @return zoznam najdenych zapasov
     */
    List<ZapasDto> findZapasy(ZapasKriteria kriteria);

    /**
     * Vrati zapasy, ktore nemaju superov. Napriklad zapas stvrt-, semi-, male-, velkefinale
     *
     * @param sutazRocnikId identifikator rocnika sutaze
     * @return zoznam zapasov
     */
    List<ZapasDto> getZapasyBezSuperov(Long sutazRocnikId);

    /**
     * Stiahne udaje o vysledkoch zo zadanej url statistiky daneho rocnika.
     *
     * @param url      urlka, podla ktorej sa ma docitat statistika zapasov
     * @param sportTyp typ sporku
     * @param sutazTyp typ sutaze
     * @param init     priznak, ci sa inicializuje dana sutaz  @return zoznam reportu o zapase
     */
    List<ZapasDto> getZapasReport(String url, SportTypEnum sportTyp, SutazTypEnum sutazTyp, boolean init);

    /**
     * Posunie vsetky zapasy o rovnaky interval, ako je medzi zadanym zaciatkom a realnym zaciatkom
     * prveho zapasu daneho rocnika sutaze a nastavi zapasy na testovanie.
     *
     * @param rocnikSutazeId identifikator rocnika sutaze
     * @param zaciatok       zaciatok prveho zapasu pre testovania
     */
    void initZapasyNaTest(Long rocnikSutazeId, Date zaciatok);

    /**
     * Posunie vsetky zapasy o rovnaky interval, ako je medzi zadanym zaciatkom a realnym zaciatkom
     * prveho zapasu daneho rocnika sutaze.
     *
     * @param zapasy   identifikator rocnika sutaze, ktory je v test mode
     * @param zaciatok testovany zaciatok rocnika
     * @return zoznam zapasov s posunutym datum zaciatku hrania
     */
    List<ZapasDto> rollDatumZapasov(List<ZapasDto> zapasy, Date zaciatok);
}
