package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.dto.SutazDto;

import java.util.List;

/**
 * Service pre pracu so sutazou.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface SutazService {

    /**
     * Vytvori novu sutaz.
     *
     *
     * @param autor autor zmeny
     * @param sutaz vytvarana sutaz
     * @return identifikator pridanej sutaze
     */
    Long addSutaz(String autor, SutazDto sutaz);

    /**
     * Zedituje existujucu sutaz.
     *
     *
     * @param autor autor zmeny
     * @param sutaz sutaz zo zmenenymi hodnotami
     */
    void editSutaz(String autor, SutazDto sutaz);

    /**
     * Vymazanie sutaze, deaktivovanie sutaze
     *
     * @param autor autor zmeny
     * @param sutazId identifikator sutaze
     */
    void removeSutaz(String autor, Long sutazId);

    /**
     * Dohlada sutaz podla identifikatora.
     *
     * @param sutazId identifikator sutaze
     * @return najdena sutaz, alebo null
     */
    SutazDto getSutaz(Long sutazId);

    /**
     * Vrati vsetky nadefinovane sutaze
     *
     * @return zoznam sutazi
     */
    List<SutazDto> getSutaze();
}
