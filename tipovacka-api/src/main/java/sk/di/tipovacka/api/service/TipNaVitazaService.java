package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.dto.TipNaVitazaDto;
import sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * User: Dano
 * Date: 16.5.2013
 */
public interface TipNaVitazaService {

    /**
     * Prida tip na vitaza rocnika sutaze do systemu.
     * Tipovat sa da do zaciatku prveho zapasu daneho rocnika sutaze.
     *
     * @param autor       autor zmeny
     * @param tipNaVitaza tip na vitaza
     * @return identifikator zalozeneho tipu
     * @throws sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException ked vyprsi cas na tipovanie
     */
    Long addTipNaVitaza(String autor, TipNaVitazaDto tipNaVitaza) throws CasNaTipovanieVyprsalException;

    /**
     * Zmeni tip na vitaza.
     * Tipovat sa da do zaciatku prveho zapasu daneho rocnika sutaze.
     *
     * @param autor       autor zmeny
     * @param tipNaVitaza zmeneny tip na vitaza
     * @throws CasNaTipovanieVyprsalException ked vyprsi cas na tipovanie
     * @throws EntityNotFoundException ked nebol najdeny pozadovany tip
     */
    void editTipNaVitaza(String autor, TipNaVitazaDto tipNaVitaza) throws CasNaTipovanieVyprsalException, EntityNotFoundException;

    /**
     * Vrati tip na vitaza podla zadaneho identifikatora.
     *
     * @param tipNaVitazaId identifikator tipu na vitaza
     * @return najdeny tip
     */
    TipNaVitazaDto getTipNaVitaza(Long tipNaVitazaId);

    /**
     * Vrati tip daneho pouzivatela pre dany rocnik sutaze.
     *
     * @param pouzivatelId identifkator pouzivatela
     * @param rocnikId identifikator rocnika sutaze
     * @return najdeny tip
     */
    TipNaVitazaDto getPouzivatelovTipNaVitaza(Long pouzivatelId, Long rocnikId);

    /**
     * Vrati vsetky tipy na vitaza za dany rocnik sutaze.
     *
     * @param sutazRocnikaId identifikator rocnika sutaze, pre ktore chceme vratit zoznam tipov na vitaza
     * @return zoznam tipov
     */
    List<TipNaVitazaDto> getTipyNaVitaza(Long sutazRocnikaId);

    /**
     * Vrati vsetky tipy na vitaza daneho pouzivatela
     *
     * @param pouzivatelId identifikator pouzivatela
     * @return najdene tipy
     */
    List<TipNaVitazaDto> getPouzivateloveTipyNaVitaza(Long pouzivatelId);
}
