package sk.di.tipovacka.api.db.dao;

import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;

import java.util.List;

/**
 * Dao pre pracu s databazovym objekom zapas
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface ZapasDao extends EntityDao<Zapas> {

    /**
     * Dohladanie vyhovujucich zapasov podla vyhladavacich kriterii.
     *
     * @param kriteria vyhladavacie kriteria
     * @return vyhovujuce zapasy
     */
    List<Zapas> findByKriteria(ZapasKriteria kriteria);

    /**
     * Vrati zapas, ktory je prvy v danom rocniku sutaze.
     *
     * @param sutazRocnikId identifikator rocnika sutaze
     * @return najdeny zapas, alebo null
     */
    Zapas getPvyZapasRocnikaSutaze(Long sutazRocnikId);

    /**
     * Vrati zoznam zapasov, ktore nemaju nastavenych superov.
     *
     * @param sutazRocnikId identifikator rocnika sutaze
     * @return zoznam zapasov
     */
    List<Zapas> getZapasyBezSuperov(Long sutazRocnikId);
}
