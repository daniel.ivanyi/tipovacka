package sk.di.tipovacka.api.dto.footballdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import java.util.List;

/**
 * Created by divanyi on 7. 6. 2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FootballDataProgramDto {

    private List<FootballDataZapasDto> fixtures;

    public List<FootballDataZapasDto> getFixtures() {
        return fixtures;
    }

    public void setFixtures(List<FootballDataZapasDto> fixtures) {
        this.fixtures = fixtures;
    }

    @Override
    public String toString() {
        return "FootballDataProgramDto{" +
                "fixtures=" + fixtures +
                '}';
    }
}
