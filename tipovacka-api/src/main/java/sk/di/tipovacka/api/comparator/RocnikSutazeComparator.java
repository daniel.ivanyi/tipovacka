package sk.di.tipovacka.api.comparator;

import sk.di.tipovacka.api.dto.RocnikSutazeDto;

import java.util.Comparator;

/**
 * Zoradi rocniky sutaze podla roku
 *
 * User: Dano
 * Date: 2.6.2013
 */
public class RocnikSutazeComparator implements Comparator<RocnikSutazeDto> {

    @Override
    public int compare(RocnikSutazeDto r1, RocnikSutazeDto r2) {
        if (r1.getRok() > r2.getRok()) {
            return -1;
        } else if (r1.getRok() < r2.getRok()) {
            return 1;
        } else {
            return 0;
        }
    }
}
