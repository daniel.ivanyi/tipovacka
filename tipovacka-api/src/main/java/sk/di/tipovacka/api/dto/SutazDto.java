package sk.di.tipovacka.api.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import sk.di.tipovacka.api.db.Sutaz;
import sk.di.tipovacka.api.enums.SportTypEnum;
import sk.di.tipovacka.api.enums.SutazTypEnum;

import java.util.Arrays;
import java.util.List;

/**
 * Dto pre zobrazenie na stranke.
 *
 * User: Dano
 * Date: 20.5.2013
 */
public class SutazDto extends AbstractDto {

    private static final long serialVersionUID = 900125590754043880L;

    private String nazov;
    private SportTypEnum sportTyp;
    private SutazTypEnum sutazTyp;

    public static SutazDto create(Sutaz sutaz) {
        if (sutaz == null) {
            return null;
        }

        SutazDto s = new SutazDto();
        s.setId(sutaz.getId());
        s.setNazov(sutaz.getNazov());
        s.setSportTyp(sutaz.getSportTyp());
        s.setSutazTyp(sutaz.getSutazTyp());
        s.setStatus(sutaz.getStatus());

        return s;
    }

    public String getNazov() {
        return nazov;
    }

    public void setNazov(String nazov) {
        this.nazov = nazov;
    }

    public SportTypEnum getSportTyp() {
        return sportTyp;
    }

    public void setSportTyp(SportTypEnum sportTyp) {
        this.sportTyp = sportTyp;
    }

    public SutazTypEnum getSutazTyp() {
        return sutazTyp;
    }

    public void setSutazTyp(SutazTypEnum sutazTyp) {
        this.sutazTyp = sutazTyp;
    }

    /**
     * Priznak, ci sa jedna o klubovu sutaz.
     *
     * @return priznak
     */
    public boolean isKlub() {
        // zoznam typov sutaze, ktore su klubove
        List<SutazTypEnum> klubovaSutaz = Arrays.asList(SutazTypEnum.LIGA_MAJSTROV);

        return klubovaSutaz.contains(getSutazTyp());
    }

    @Override
    public String toString() {
        return "SutazDto{" +
                "nazov='" + nazov + '\'' +
                ", sportTyp=" + sportTyp +
                ", sutazTyp=" + sutazTyp +
                "}";
    }
}
