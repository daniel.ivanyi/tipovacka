package sk.di.tipovacka.api.enums;

/**
 * Typ sutaze.
 *
 * User: Dano
 * Date: 10.11.2013
 */
public enum SutazTypEnum {

    MAJSTROVSTVA_SVETA("MS"),
    MAJSTROVSTVA_EUROPY("ME"),
    OLYMPIJSKE_HRY("OH"),
    NEMECKY_POHAR("NP"),
    LIGA_MAJSTROV("LM");

    private String skratka;

    private SutazTypEnum(String skratka) {
        this.skratka = skratka;
    }

    public String getSkratka() {
        return skratka;
    }
}
