package sk.di.tipovacka.api.dto;

import sk.di.tipovacka.api.db.SutazRocnikSuhlas;

import java.sql.Timestamp;
import java.util.Date;

public class RocnikSutazeSuhlasDto extends AbstractDto {

    private static final long serialVersionUID = -731005961015365843L;

    private Date datum;
    private Long sutazRocnikId;
    private PouzivatelDto pouzivatel;
    private boolean suhlas;
    private Timestamp zaplatene;

    public static RocnikSutazeSuhlasDto create(SutazRocnikSuhlas suhlas) {
        if (suhlas == null) {
            return null;
        }

        RocnikSutazeSuhlasDto dto = new RocnikSutazeSuhlasDto();
        dto.setId(suhlas.getId());
        dto.setDatum(suhlas.getDatum());
        dto.setPouzivatel(PouzivatelDto.create(suhlas.getPouzivatel()));
        dto.setSutazRocnikId(suhlas.getSutazRocnikId());
        dto.setSuhlas(suhlas.isSuhlas());
        dto.setZaplatene(suhlas.getZaplatene());

        return dto;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public Long getSutazRocnikId() {
        return sutazRocnikId;
    }

    public void setSutazRocnikId(Long sutazRocnikId) {
        this.sutazRocnikId = sutazRocnikId;
    }

    public PouzivatelDto getPouzivatel() {
        return pouzivatel;
    }

    public void setPouzivatel(PouzivatelDto pouzivatel) {
        this.pouzivatel = pouzivatel;
    }

    public boolean isSuhlas() {
        return suhlas;
    }

    public void setSuhlas(boolean suhlas) {
        this.suhlas = suhlas;
    }

    public Timestamp getZaplatene() {
        return zaplatene;
    }

    public void setZaplatene(Timestamp zaplatene) {
        this.zaplatene = zaplatene;
    }

    public boolean isZaplatene() {
        return zaplatene != null;
    }

    @Override
    public String toString() {
        return "RocnikSutazeSuhlasDto{" +
                "datum=" + datum +
                ", sutazRocnikId=" + sutazRocnikId +
                ", pouzivatel=" + pouzivatel +
                ", suhlas=" + suhlas +
                ", zaplatene=" + zaplatene +
                '}';
    }
}
