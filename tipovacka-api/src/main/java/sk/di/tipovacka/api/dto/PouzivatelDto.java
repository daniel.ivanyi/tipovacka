package sk.di.tipovacka.api.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.enums.RolaEnum;

import java.util.*;

/**
 * Portalovy objekt pouzivatel
 * <p/>
 * User: Dano
 * Date: 9.5.2013
 */
public class PouzivatelDto extends AbstractDto implements UserDetails {

    private static final long serialVersionUID = -5783645126512076855L;

    private String login;
    private String heslo;
    private String priezvisko;
    private String meno;
    private String mail;
    private List<Long> aktivneSutazeId;
    private RocnikSutazeDto vybranyRocnik;
    private Set<RolaEnum> roly;

    /**
     * Preklopenie databazoveh pouzivatela na portaloveho.
     *
     * @param pouzivatel databazovy pouzivatel
     * @return portalovy pouzivatel
     */
    public static PouzivatelDto create(Pouzivatel pouzivatel) {
        if (pouzivatel == null) {
            return null;
        }

        PouzivatelDto p = new PouzivatelDto();
        p.setId(pouzivatel.getId());
        p.setLogin(pouzivatel.getLogin());
        p.setHeslo(pouzivatel.getHeslo());
        p.setMeno(pouzivatel.getMeno());
        p.setPriezvisko(pouzivatel.getPriezvisko());
        p.setMail(pouzivatel.getMail());
        p.setStatus(pouzivatel.getStatus());

        for (RolaEnum rola : pouzivatel.getRolyEnum()) {
            p.getRoly().add(rola);
        }

        return p;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getHeslo() {
        return heslo;
    }

    public void setHeslo(String heslo) {
        this.heslo = heslo;
    }

    public String getPriezvisko() {
        return priezvisko;
    }

    public void setPriezvisko(String priezvisko) {
        this.priezvisko = priezvisko;
    }

    public String getMeno() {
        return meno;
    }

    public void setMeno(String meno) {
        this.meno = meno;
    }

    public String getMenoPriezvisko() {
        return getMeno() + " " + getPriezvisko();
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public boolean isSuhlas() {
        return !getAktivneSutazeId().isEmpty();
    }
    public boolean isSuhlas(Long rocnikId) {
        if (rocnikId == null) {
            return false;
        }
        if (getAktivneSutazeId().isEmpty()) {
            return false;
        }

        return getAktivneSutazeId().contains(rocnikId);
    }

    public List<Long> getAktivneSutazeId() {
        if (aktivneSutazeId == null) {
            aktivneSutazeId = new ArrayList<>();
        }

        return aktivneSutazeId;
    }

    public RocnikSutazeDto getVybranyRocnik() {
        return vybranyRocnik;
    }

    public void setVybranyRocnik(RocnikSutazeDto vybranyRocnik) {
        this.vybranyRocnik = vybranyRocnik;
    }

    public Set<RolaEnum> getRoly() {
        if (roly == null) {
            roly = new HashSet<>();
        }

        return roly;
    }

    public void setRoly(Set<RolaEnum> roly) {
        this.roly = roly;
    }

    /**
     * Priznak, ci je pouzivatel admin.
     *
     * @return priznak
     */
    public boolean isAdmin() {
        return getRoly().contains(RolaEnum.ADMIN);
    }

    /**
     * Priznak, ci je pouzivatel pokladnik.
     *
     * @return priznak
     */
    public boolean isPokladnik() {
        return getRoly().contains(RolaEnum.POKLADNIK);
    }

    /**
     * Priznak, ci je pouzivatel tiper.
     *
     * @return priznak
     */
    public boolean isTiper() {
        return getRoly().contains(RolaEnum.TIPER);
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return getRoly();
    }

    @Override
    public String getPassword() {
        return getHeslo();
    }

    @Override
    public String getUsername() {
        return getLogin();
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
