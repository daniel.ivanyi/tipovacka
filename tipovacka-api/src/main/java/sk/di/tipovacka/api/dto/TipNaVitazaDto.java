package sk.di.tipovacka.api.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import sk.di.tipovacka.api.db.TipNaVitaza;

import java.util.Date;

/**
 * Portalovy objekt pre tip na vitaza.
 *
 * User: Dano
 * Date: 3.6.2013
 */
public class TipNaVitazaDto extends AbstractDto {
    private static final long serialVersionUID = 4299433393886754484L;

    private Date datum;
    private PouzivatelDto pouzivatel;
    private RocnikSutazeDto rocnikSutaze;
    private MuzstvoDto muzstvo;
    private boolean nahodnyTip;
    private int body;

    public static TipNaVitazaDto create(TipNaVitaza tip) {
        if (tip == null) {
            return null;
        }
        TipNaVitazaDto t = new TipNaVitazaDto();
        t.setId(tip.getId());
        t.setDatum(tip.getDatum());
        t.setPouzivatel(PouzivatelDto.create(tip.getPouzivatel()));
        t.setRocnikSutaze(RocnikSutazeDto.create(tip.getSutazRocnik()));
        t.setMuzstvo(MuzstvoDto.create(tip.getMuzstvo()));
        t.setNahodnyTip(tip.isNahodnyTip());
        t.setBody(tip.getBody());

        return t;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public PouzivatelDto getPouzivatel() {
        return pouzivatel;
    }

    public void setPouzivatel(PouzivatelDto pouzivatel) {
        this.pouzivatel = pouzivatel;
    }

    public RocnikSutazeDto getRocnikSutaze() {
        return rocnikSutaze;
    }

    public void setRocnikSutaze(RocnikSutazeDto rocnikSutaze) {
        this.rocnikSutaze = rocnikSutaze;
    }

    public MuzstvoDto getMuzstvo() {
        return muzstvo;
    }

    public void setMuzstvo(MuzstvoDto muzstvo) {
        this.muzstvo = muzstvo;
    }

    public boolean isNahodnyTip() {
        return nahodnyTip;
    }

    public void setNahodnyTip(boolean nahodnyTip) {
        this.nahodnyTip = nahodnyTip;
    }

    public int getBody() {
        return body;
    }

    public void setBody(int body) {
        this.body = body;
    }

    @Override
    public String toString() {
        return "TipNaVitazaDto{" +
                "datum=" + datum +
                ", pouzivatel=" + (pouzivatel == null ? "null" : pouzivatel.getLogin()) +
                ", rocnikSutaze=" + (rocnikSutaze == null ? "null" : rocnikSutaze.getId()) +
                ", muzstvo=" + (muzstvo == null ? "null" : muzstvo.getKod()) +
                ", nahodnyTip=" + nahodnyTip +
                ", body=" + body +
                "}";
    }
}
