package sk.di.tipovacka.api.service;

import sk.di.tipovacka.api.dto.RocnikSutazeSuhlasDto;

import java.util.List;

/**
 * Service pre pracu so suhlasom ucasti na tipovacke pre dany rocnik.
 * <p>
 * User: Dano
 * Date: 16.2.2014
 */
public interface SutazRocnikSuhlasService {

    /**
     * Prida suhlas danneho pouzivatela s ucastou na tippovacke pre dany rocnik
     *
     * @param autor        autor zmeny
     * @param pouzivatelId identifikator pouzivatela, ktory suhlasi
     * @param rocnikId     identifikator rocnika sutaze, na ktory je suhlas
     * @return identifikator suhlasu
     */
    Long addSuhlas(String autor, Long pouzivatelId, Long rocnikId);

    /**
     * Prida priznak zaplatenia pouzivatela prispevku do tipovacky.
     *
     * @param autor    autor zmeny
     * @param suhlasId identifikator suhlasu, na ktory sa ma pridat priznak, ze je zaplatene
     */
    void addZaplatene(String autor, Long suhlasId);

    /**
     * Zrusenie priznaku zaplatenia pouzivatela prispevku do tipovacky.
     *
     * @param autor    autor zmeny
     * @param suhlasId identifikator suhlasu, na ktory sa ma pridat priznak, ze je zaplatene
     */
    void removeZaplatene(String autor, Long suhlasId);

    /**
     * Vrati vsetkych, co pre dany rocnik odsuhlasili ucast, ale nesplnili podmienky ucasti.
     *
     * @param rocnikId identifikator rocnika
     * @return zoznam
     */
    List<RocnikSutazeSuhlasDto> findAllCoNezaplatili(Long rocnikId);

    /**
     * Vrati loginy, tych co nesplnili podmienky ucasti.
     *
     * @param rocnikId identifikator rocnika
     * @return zoznam
     */
    List<String> findAllLoginCoNezaplatili(Long rocnikId);

    /**
     * Vrati vsetky zaznamenane suhlasy pre dany rocnik.
     *
     * @param rocnikId identifikator rocnika sutaze, pre ktory sa ziskavaju suhlasy
     * @return zoznam
     */
    List<RocnikSutazeSuhlasDto> findAllByRocnik(Long rocnikId);

    /**
     * Priznak, ci dany pouzivatel splnil podmienky ucasti.
     *
     * @param login    login pouzivatela
     * @param rocnikId id rocnika, pre ktory overujeme splnene podmienky
     * @return
     */
    boolean isSplnenePodmienky(String login, Long rocnikId);
}
