package sk.di.tipovacka.api.dto;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import sk.di.tipovacka.api.enums.KoniecZapasuTypEnum;
import sk.di.tipovacka.api.enums.ZapasTypEnum;

import java.util.Date;

/**
 * Report zo zapasu
 *
 * User: Dano
 * Date: 12.5.2013
 */
public class ZapasReportIihfDto {

    private Date datum;
    private String mesto;
    private String stadion;
    private int poradie;
    private ZapasTypEnum typZapasu;
    private MuzstvoDto domaci;
    private MuzstvoDto hostia;
    private int golyDomaci;
    private int golyHostia;
    private String tretiny;
    private KoniecZapasuTypEnum koniecZapasuTyp;

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public String getMesto() {
        return mesto;
    }

    public void setMesto(String mesto) {
        this.mesto = mesto;
    }

    public String getStadion() {
        return stadion;
    }

    public void setStadion(String stadion) {
        this.stadion = stadion;
    }

    public int getPoradie() {
        return poradie;
    }

    public void setPoradie(int poradie) {
        this.poradie = poradie;
    }

    public ZapasTypEnum getTypZapasu() {
        return typZapasu;
    }

    public void setTypZapasu(ZapasTypEnum typZapasu) {
        this.typZapasu = typZapasu;
    }

    public MuzstvoDto getDomaci() {
        return domaci;
    }

    public void setDomaci(MuzstvoDto domaci) {
        this.domaci = domaci;
    }

    public MuzstvoDto getHostia() {
        return hostia;
    }

    public void setHostia(MuzstvoDto hostia) {
        this.hostia = hostia;
    }

    public int getGolyDomaci() {
        return golyDomaci;
    }

    public void setGolyDomaci(int golyDomaci) {
        this.golyDomaci = golyDomaci;
    }

    public int getGolyHostia() {
        return golyHostia;
    }

    public void setGolyHostia(int golyHostia) {
        this.golyHostia = golyHostia;
    }

    public String getTretiny() {
        return tretiny;
    }

    public void setTretiny(String tretiny) {
        this.tretiny = tretiny;
    }

    public KoniecZapasuTypEnum getKoniecZapasuTyp() {
        return koniecZapasuTyp;
    }

    public void setKoniecZapasuTyp(KoniecZapasuTypEnum koniecZapasuTyp) {
        this.koniecZapasuTyp = koniecZapasuTyp;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
