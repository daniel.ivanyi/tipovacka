package sk.di.tipovacka.api.exception;

/**
 * Ked sa pokusame pridat/upravit tip na zapas po vyprsani casu na tipovanie.
 *
 * User: Dano
 * Date: 11.5.2013
 */
public class CasNaTipovanieVyprsalException extends RuntimeException {

    public CasNaTipovanieVyprsalException() {
    }

    public CasNaTipovanieVyprsalException(String message) {
        super(message);
    }

    public CasNaTipovanieVyprsalException(String message, Throwable cause) {
        super(message, cause);
    }
}
