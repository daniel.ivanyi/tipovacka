package sk.di.tipovacka.api.dto.iihf;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Turnaj z IIHF Rest sluzby.
 * <p>
 * Created by divanyi on 13. 4. 2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IihfTurnajDto {

    private int tournamentID;
    private String longName;

    public int getTournamentID() {
        return tournamentID;
    }

    public void setTournamentID(int tournamentID) {
        this.tournamentID = tournamentID;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    @Override
    public String toString() {
        return "IihfTurnajDto{" +
                "tournamentID=" + tournamentID +
                ", longName='" + longName + '\'' +
                '}';
    }
}
