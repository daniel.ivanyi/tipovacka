package sk.di.tipovacka.api.enums;

/**
 * Typ, ako skoncil zapas.
 *
 * User: Dano
 * Date: 10.5.2013
 */
public enum KoniecZapasuTypEnum {

    NEZACAL(false),
    HRA_SA(false),
    RIADNY_CAS(true),
    PREDLZENIE(true),
    ROZSTREL(true);

    private boolean koniecZapasu;

    private KoniecZapasuTypEnum(boolean koniecZapasu) {
        this.koniecZapasu = koniecZapasu;
    }

    public boolean isKoniecZapasu() {
        return koniecZapasu;
    }
}
