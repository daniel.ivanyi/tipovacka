package sk.di.tipovacka.api.dto;

import sk.di.tipovacka.api.enums.SpravnyTipNaEnum;

import java.math.BigInteger;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

/**
 * DTOcko statistiky daneho tipera.
 *
 * User: Dano
 * Date: 13.6.2013
 */
public class PouzivatelovTipStatistikaDto extends AbstractDto {
    private static final long serialVersionUID = 3504341194937110230L;

    private static final ResourceBundle rb = ResourceBundle.getBundle("configuration");
    public static final int BODY_TIP_GOL = Integer.parseInt(rb.getString("tip.zapas.body.goly"));
    public static final int BODY_TIP_VITAZ = Integer.parseInt(rb.getString("tip.zapas.body.vitaz"));
    public static final int BODY_TIP_VITAZ_ROCNIK = Integer.parseInt(rb.getString("tip.rocnik.body.vitaz"));

    private PouzivatelDto pouzivatel;
    private Map<SpravnyTipNaEnum, Integer> mapaSpravnychTipov;
    private int pocetTipovNaVitazaZapasu;
    private int pocetTipovNaGolDomaci;
    private int pocetTipovNaGolHostia;
    private boolean tipnutyVitaz;
    private int pocetZadanychTipov;
    private int body;

    public PouzivatelDto getPouzivatel() {
        return pouzivatel;
    }

    public void setPouzivatel(PouzivatelDto pouzivatel) {
        this.pouzivatel = pouzivatel;
    }

    public Map<SpravnyTipNaEnum, Integer> getMapaSpravnychTipov() {
        if (mapaSpravnychTipov == null) {
            mapaSpravnychTipov = new HashMap<>();

            mapaSpravnychTipov.put(SpravnyTipNaEnum.NIC, 0);
            mapaSpravnychTipov.put(SpravnyTipNaEnum.GOL, 0);
            mapaSpravnychTipov.put(SpravnyTipNaEnum.VITAZ, 0);
            mapaSpravnychTipov.put(SpravnyTipNaEnum.VITAZGOL, 0);
            mapaSpravnychTipov.put(SpravnyTipNaEnum.VITAZGOLGOL, 0);
        }

        return mapaSpravnychTipov;
    }

    public int getPocetTipov() {
        return getMapaSpravnychTipov().get(SpravnyTipNaEnum.NIC)
                + getMapaSpravnychTipov().get(SpravnyTipNaEnum.GOL)
                + getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZ)
                + getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZGOL)
                + getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZGOLGOL);
    }

    public int getPocetTipovNic() {
        return getMapaSpravnychTipov().get(SpravnyTipNaEnum.NIC);
    }

    public int getPocetTipovGoly() {
        return getMapaSpravnychTipov().get(SpravnyTipNaEnum.GOL);
    }

    public int getPocetTipovVitaz() {
        return getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZ);
    }

    public int getPocetTipovVitazGol() {
        return getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZGOL);
    }

    public int getPocetTipovVysledok() {
        return getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZGOLGOL);
    }

    public int getPocetTipovNaVitazaZapasu() {
        return pocetTipovNaVitazaZapasu;
    }

    public PouzivatelovTipStatistikaDto addPocetTipovNaVitazaZapasu() {
        this.pocetTipovNaVitazaZapasu += 1;
        return this;
    }

    public int getPocetTipovNaGolDomaci() {
        return pocetTipovNaGolDomaci;
    }

    public PouzivatelovTipStatistikaDto addPocetTipovNaGolDomaci() {
        this.pocetTipovNaGolDomaci += 1;
        return this;
    }

    public int getPocetTipovNaGolHostia() {
        return pocetTipovNaGolHostia;
    }

    public PouzivatelovTipStatistikaDto addPocetTipovNaGolHostia() {
        this.pocetTipovNaGolHostia += 1;
        return this;
    }

    public boolean isTipnutyVitaz() {
        return tipnutyVitaz;
    }

    public void setTipnutyVitaz(boolean tipnutyVitaz) {
        this.tipnutyVitaz = tipnutyVitaz;
    }

    public int getPocetZadanychTipov() {
        return pocetZadanychTipov;
    }

    public void setPocetZadanychTipov(int pocetZadanychTipov) {
        this.pocetZadanychTipov = pocetZadanychTipov;
    }

    public void setBody(int body) {
        this.body = body;
    }

    public int getBody() {
//        int body = getMapaSpravnychTipov().get(SpravnyTipNaEnum.GOL) * BODY_TIP_GOL
//                + getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZ) * BODY_TIP_VITAZ
//                + getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZGOL) * (BODY_TIP_VITAZ + BODY_TIP_GOL)
//                + getMapaSpravnychTipov().get(SpravnyTipNaEnum.VITAZGOLGOL) * (BODY_TIP_VITAZ + BODY_TIP_GOL + BODY_TIP_GOL);
//        if (isTipnutyVitaz()) {
//            body += BODY_TIP_VITAZ_ROCNIK;
//        }

        return body;
    }

    /**
     * Aby sme vedeli vo vyslednej tabulke tiperov spravne pridelit rovnake poradove cislo
     * pri rovnosti bodov, zavedieme pre kazdeho tipera hodnotu.
     *
     * @return spojenie pocet jednotlivych typov dokopy
     */
    public String getHodnota() {
        return String.format("%02d%02d%02d%02d%02d",
                getBody(),
                getPocetTipovVysledok(),
                getPocetTipovVitazGol(),
                getPocetTipovVitaz(),
                getPocetTipovGoly());
    }
}
