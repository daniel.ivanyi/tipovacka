package sk.di.tipovacka.api.comparator;

import sk.di.tipovacka.api.dto.MuzstvoDto;

import java.util.Comparator;

/**
 * Zoradi muzstva podla kodu.
 *
 * User: Dano
 * Date: 2.6.2013
 */
public class MuzstvoKodComparator implements Comparator<MuzstvoDto> {

    @Override
    public int compare(MuzstvoDto o1, MuzstvoDto o2) {
        return o1.getKod().compareTo(o2.getKod());
    }
}
