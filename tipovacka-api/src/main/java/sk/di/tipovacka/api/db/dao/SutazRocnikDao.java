package sk.di.tipovacka.api.db.dao;

import sk.di.tipovacka.api.db.Muzstvo;
import sk.di.tipovacka.api.db.SutazRocnik;

import java.util.List;

/**
 * Dao pre pracu s databazovym objekom rocnik sutaze
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface SutazRocnikDao extends EntityDao<SutazRocnik> {

    /**
     * Vrati nasetovanu URLku, kde je statistika k danemu rocniku sutaze.
     *
     * @param sutazRocnikId identifikator rocnika sutaze
     * @return URLka statistika
     */
    String getRocnikSutazeStatistikaUrl(Long sutazRocnikId);

    /**
     * Vrati rocnik sutaz podla sutaze a roka.
     *
     * @param sutazId identifikator sutaze
     * @param rok     rok
     * @return najdeny rocnik
     */
    SutazRocnik getRocnikSutazePodlaSutazeRoka(Long sutazId, int rok);

    /**
     * Vrati aktualne prebiehajuce rocnikz sutaze, na ktore sa da tipovat.
     *
     * @return aktualny rocnik sutaze
     */
    List<SutazRocnik> getAktualnyRocnikSutaze();

    /**
     * Odstrani vsetky tipy pre dany rocnik.
     *
     * @param sutazRocnikId identifikator sutaze
     */
    void clearTipy(Long sutazRocnikId);

    /**
     * Vrati zoznam ukoncenych rocnikov sutaze.
     *
     * @return zoznam ukoncenych rocnikov
     */
    List<SutazRocnik> getUkonceneRocniky();
}
