package sk.di.tipovacka.api.dto.footballdata;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * DTOcko pre nacitanie zapasov z REST sluzby http://api.football-data.org
 *
 * Created by divanyi on 7. 6. 2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class FootballDataZapasDto {

    private String date;
    private String status;
    private String homeTeamName;
    private String awayTeamName;
    private FootballDataVysledokDto result;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getHomeTeamName() {
        return homeTeamName;
    }

    public void setHomeTeamName(String homeTeamName) {
        this.homeTeamName = homeTeamName;
    }

    public String getAwayTeamName() {
        return awayTeamName;
    }

    public void setAwayTeamName(String awayTeamName) {
        this.awayTeamName = awayTeamName;
    }

    public FootballDataVysledokDto getResult() {
        return result;
    }

    public void setResult(FootballDataVysledokDto result) {
        this.result = result;
    }

    @Override
    public String toString() {
        return "FootballDataZapasDto{" +
                "date=" + date +
                ", status='" + status + '\'' +
                ", homeTeamName='" + homeTeamName + '\'' +
                ", awayTeamName='" + awayTeamName + '\'' +
                ", result=" + result +
                '}';
    }
}
