package sk.di.tipovacka.api.comparator;

import sk.di.tipovacka.api.dto.TipNaZapasDto;

import java.util.Comparator;

/**
 * Comparator pre zotriedenie tipu na zapas podla datumu hrania zaapasu, na ktory sa tipuje.
 * User: Dano
 * Date: 17.2.2014
 */
public class TipNaZapasDatumComparator implements Comparator<TipNaZapasDto> {
    @Override
    public int compare(TipNaZapasDto o1, TipNaZapasDto o2) {
        // ked nepoznam tip, alebo zapas, nedokazem sortovat
        if (o1 == null || o2 == null) {
            return 0;
        }
        if (o1.getZapas() == null || o2.getZapas() == null) {
            return 0;
        }

        return o1.getZapas().getDatum().compareTo(o2.getZapas().getDatum());
    }
}
