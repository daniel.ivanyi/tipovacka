package sk.di.tipovacka.api.db;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import sk.di.tipovacka.api.enums.EntityStatusEnum;

import java.util.Date;
import java.util.Set;

/**
 * Databazovy objekt pre rocnik sutaze.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public class SutazRocnik extends Entity {

    private static final long serialVersionUID = -3748343802082618191L;

    private int rok;
    private Long sutazId;
    private Sutaz sutaz;
    // urlka, kde sa daju najst vysledky, napriklad pre stats.iihf.com, www.fifa.com/worldcup/preliminaries/matches/fixtures.html
    private String statistikaUrl;
    // priznak, ze rocnik sutaze bol inicializovany
    private boolean init;
    // priznak, ze dany rocnik je aktualny
    private boolean aktualny;
    private Set<Zapas> zapasy;
    // vitaz rocnika
    private Long vitazId;
    private Muzstvo vitaz;
    private Date aktualizacia;
    // priznak, ze je v test mode
    private Boolean test;
    // fiktivny zaciatok rocnika (prveho zapasu)
    private Date zaciatokRocnikaTest;

    public Set<Zapas> getZapasy() {
        return zapasy;
    }

    public void setZapasy(Set<Zapas> zapasy) {
        this.zapasy = zapasy;
    }

    public int getRok() {
        return rok;
    }

    public void setRok(int rok) {
        this.rok = rok;
    }

    public Long getSutazId() {
        return sutazId;
    }

    public void setSutazId(Long sutazId) {
        this.sutazId = sutazId;
    }

    public Sutaz getSutaz() {
        return sutaz;
    }

    public void setSutaz(Sutaz sutaz) {
        this.sutaz = sutaz;
    }

    public String getStatistikaUrl() {
        return statistikaUrl;
    }

    public void setStatistikaUrl(String statistikaUrl) {
        this.statistikaUrl = statistikaUrl;
    }

    public boolean isInit() {
        return init;
    }

    public void setInit(boolean init) {
        this.init = init;
    }

    public boolean isAktualny() {
        return aktualny;
    }

    public void setAktualny(boolean aktualny) {
        this.aktualny = aktualny;
    }

    public Boolean isTest() {
        return test == null ? false : test;
    }

    public void setTest(Boolean test) {
        this.test = test;
    }

    public Date getZaciatokRocnikaTest() {
        return zaciatokRocnikaTest;
    }

    public void setZaciatokRocnikaTest(Date zaciatokRocnikaTest) {
        this.zaciatokRocnikaTest = zaciatokRocnikaTest;
    }

    public Long getVitazId() {
        return vitazId;
    }

    public void setVitazId(Long vitazId) {
        this.vitazId = vitazId;
    }

    public Muzstvo getVitaz() {
        return vitaz;
    }

    public void setVitaz(Muzstvo vitaz) {
        this.vitaz = vitaz;
    }

    public Date getAktualizacia() {
        return aktualizacia;
    }

    public void setAktualizacia(Date aktualizacia) {
        this.aktualizacia = aktualizacia;
    }

    @Override
    public String toString() {
        return ToStringBuilder.reflectionToString(this, ToStringStyle.SHORT_PREFIX_STYLE);
    }
}
