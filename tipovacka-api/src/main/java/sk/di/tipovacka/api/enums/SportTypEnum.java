package sk.di.tipovacka.api.enums;

/**
 * Typ sportu.
 *
 * User: Dano
 * Date: 10.5.2013
 */
public enum SportTypEnum {

    HOKEJ,
    FUTBAL
}
