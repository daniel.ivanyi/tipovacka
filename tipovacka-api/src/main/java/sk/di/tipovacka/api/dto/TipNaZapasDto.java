package sk.di.tipovacka.api.dto;

import sk.di.tipovacka.api.db.TipNaZapas;

import java.util.Date;

/**
 * Portalovy objekt pre tipovanie na zapas.
 *
 * User: Dano
 * Date: 4.6.2013
 */
public class TipNaZapasDto extends AbstractDto {
    private static final long serialVersionUID = 9168447267744379436L;

    private Date datum;
    private ZapasDto zapas;
    private PouzivatelDto pouzivatel;
    private Integer golyDomaci;
    private Integer golyHostia;
    private boolean nahodnyTip;
    private int bodyZaVitaza;
    private int bodyZaGolyDomaci;
    private int bodyZaGolyHostia;

    public static TipNaZapasDto create(TipNaZapas tip) {
        if (tip == null) {
            return null;
        }

        TipNaZapasDto t = new TipNaZapasDto();
        t.setId(tip.getId());
        t.setDatum(tip.getDatum());
        t.setZapas(ZapasDto.create(tip.getZapas()));
        t.setPouzivatel(PouzivatelDto.create(tip.getPouzivatel()));
        t.setGolyDomaci(tip.getGolyDomaci());
        t.setGolyHostia(tip.getGolyHostia());
        t.setNahodnyTip(tip.isNahodnyTip());
        t.setBodyZaVitaza(tip.getBodyZaVitaza());
        t.setBodyZaGolyDomaci(tip.getBodyZaGolyDomaci());
        t.setBodyZaGolyHostia(tip.getBodyZaGolyHostia());

        return t;
    }

    public Date getDatum() {
        return datum;
    }

    public void setDatum(Date datum) {
        this.datum = datum;
    }

    public ZapasDto getZapas() {
        return zapas;
    }

    public void setZapas(ZapasDto zapas) {
        this.zapas = zapas;
    }

    public PouzivatelDto getPouzivatel() {
        return pouzivatel;
    }

    public void setPouzivatel(PouzivatelDto pouzivatel) {
        this.pouzivatel = pouzivatel;
    }

    public Integer getGolyDomaci() {
        return golyDomaci;
    }

    public void setGolyDomaci(Integer golyDomaci) {
        this.golyDomaci = golyDomaci;
    }

    public Integer getGolyHostia() {
        return golyHostia;
    }

    public void setGolyHostia(Integer golyHostia) {
        this.golyHostia = golyHostia;
    }

    public boolean isNahodnyTip() {
        return nahodnyTip;
    }

    public void setNahodnyTip(boolean nahodnyTip) {
        this.nahodnyTip = nahodnyTip;
    }

    public int getBodyZaVitaza() {
        return bodyZaVitaza;
    }

    public void setBodyZaVitaza(int bodyZaVitaza) {
        this.bodyZaVitaza = bodyZaVitaza;
    }

    public int getBodyZaGolyDomaci() {
        return bodyZaGolyDomaci;
    }

    public void setBodyZaGolyDomaci(int bodyZaGolyDomaci) {
        this.bodyZaGolyDomaci = bodyZaGolyDomaci;
    }

    public int getBodyZaGolyHostia() {
        return bodyZaGolyHostia;
    }

    public void setBodyZaGolyHostia(int bodyZaGolyHostia) {
        this.bodyZaGolyHostia = bodyZaGolyHostia;
    }

    public int getBody() {
        return getBodyZaVitaza() + getBodyZaGolyDomaci() + getBodyZaGolyHostia();
    }

    public MuzstvoDto getVitaz() {
        if (getGolyDomaci().equals(getGolyHostia())) {
            return new MuzstvoDto();
        }

        return getGolyDomaci() > getGolyHostia()
                ? getZapas().getDomaci()
                : getZapas().getHostia();
    }

    public String toText() {
        return "TipNaZapasDto{" +
                "datum=" + datum +
                ", login=" + (pouzivatel == null ? "null" : pouzivatel.getLogin()) +
                ", zapas=" + (zapas == null ? "null" : zapas.toText()) +
                ", goly={" + golyDomaci +":" + golyHostia + "}" +
                "}";
    }

    @Override
    public String toString() {
        return "TipNaZapasDto{" +
                "datum=" + datum +
                ", pouzivatel=" + (pouzivatel == null ? "null" : pouzivatel.getLogin()) +
                ", zapas=" + (zapas == null ? "null" : zapas.toText()) +
                ", goly={" + golyDomaci +":" + golyHostia + "}" +
                ", nahodnyTip=" + nahodnyTip +
                ", body={" + bodyZaVitaza + "+" + bodyZaGolyDomaci + "+" + bodyZaGolyHostia + "}" +
                "}";
    }
}
