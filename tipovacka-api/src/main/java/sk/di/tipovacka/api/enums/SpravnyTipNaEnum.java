package sk.di.tipovacka.api.enums;

/**
 * Enum na typov na co sa spravne tipovalo.
 *
 * User: filusik
 * Date: 19.6.2013
 */
public enum SpravnyTipNaEnum {

    NIC,        // nic nebolo spravne tipnute
    GOL,        // spravny tip iba na jeden z poctu strelenych golov
    VITAZ,      // spravny tip iba na vitaza
    VITAZGOL,   // spravny tip na vitaza a na jeden z poctu strelenych golov
    VITAZGOLGOL // spravny tip na vysledok, vitaz aj pocty strelenych golov

}
