package sk.di.tipovacka.api.comparator;

import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.OrderDirectionEnum;

import java.util.Comparator;

/**
 * Zoradi zapasy podla datumu.
 * <p/>
 * User: Dano
 * Date: 2.6.2013
 */
public class ZapasDatumComparator implements Comparator<ZapasDto> {

    private OrderDirectionEnum direction;

    public ZapasDatumComparator() {
        this.direction = OrderDirectionEnum.ASC;
    }

    public ZapasDatumComparator(OrderDirectionEnum direction) {
        this.direction = direction;
    }

    @Override
    public int compare(ZapasDto z1, ZapasDto z2) {
        return z1.getDatum().compareTo(z2.getDatum())
                * (OrderDirectionEnum.ASC.equals(direction) ? 1 : -1);
    }
}
