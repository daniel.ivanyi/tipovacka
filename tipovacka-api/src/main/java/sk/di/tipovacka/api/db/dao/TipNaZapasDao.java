package sk.di.tipovacka.api.db.dao;

import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;

import java.util.List;

/**
 * Dao pre pracu s databazovym objektom tip na zapas
 *
 * User: Dano
 * Date: 10.5.2013
 */
public interface TipNaZapasDao extends EntityDao<TipNaZapas> {

    /**
     * Dohlada tipu na zapas podla zadanych kriterii
     *
     * @param kriteria vyhladavacie kriteria
     * @return zoznam tipov
     */
    List<TipNaZapas> findTipyNaZapas(TipNaZapasKriteria kriteria);

    /**
     * Vrati vsetky tipy na zapas.
     *
     * @param zapasId identifikator zapasu
     * @return zoznam tipov
     */
    List<TipNaZapas> getTipyNaZapas(Long zapasId);

    /**
     * Vrati distinct zoznam tiperov na zapas daneho rocnika.
     *
     * @param rocnikId identifikator rocnika
     * @return zoznam
     */
    List<Pouzivatel> getTipujucichNaZapas(Long rocnikId);

    /**
     * Vrati ID najlepsieho tipujuceho vitaza zapasu.
     *
     * @param rocnikId identifikator rocnika
     * @return najlepsi tipujuci
     */
    List<String> findNajlepsiTipujuciNaVitazaZapasu(Long rocnikId);

    /**
     * Vrati najlepsie tipujuceho konecne skore zapasu.
     *
     * @param rocnikId identifikator rocnika
     * @return najlepsi tipujuci
     */
    List<String> findNajlepsieTipujuciSkoreZapasu(Long rocnikId);

    /**
     * Vrati najlepsie tipujuceho pomocou nahodnych tipov.
     *
     * @param rocnikId identifikator rocnika
     * @return najlepsi tipujuci
     */
    List<String> findNajlepsieNahodneTipujuci(Long rocnikId);
}
