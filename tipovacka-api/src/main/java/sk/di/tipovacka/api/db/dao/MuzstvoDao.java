package sk.di.tipovacka.api.db.dao;

import sk.di.tipovacka.api.db.Muzstvo;

/**
 * Dao pre pracu s databazovym objektom pre muzstvo
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public interface MuzstvoDao extends EntityDao<Muzstvo> {

    /**
     * Vrati muzstvo podla zadaneho kodu.
     *
     * @param kod kod muzstva
     * @return najdene muzstvo
     */
    Muzstvo getMuzstvoPodlaKodu(String kod);

    /**
     * Vrati muzstvo podla zadaneho nazvu.
     *
     * @param nazov nazov muzstva
     * @return najdene muzstvo
     */
    Muzstvo getMuzstvoPodlaNazvu(String nazov);
}
