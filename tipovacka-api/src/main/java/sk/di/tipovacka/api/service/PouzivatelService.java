package sk.di.tipovacka.api.service;

import org.springframework.security.core.userdetails.UserDetailsService;
import sk.di.tipovacka.api.exception.EntityUniqueExeption;
import sk.di.tipovacka.api.dto.PouzivatelDto;

import javax.persistence.EntityNotFoundException;
import java.util.List;

/**
 * Service pre pracu s pouzivatelom
 * <p/>
 * User: Dano
 * Date: 9.5.2013
 */
public interface PouzivatelService extends UserDetailsService {

    /**
     * Vytvori noveho pouzivatela.
     *
     * @param autor      autor zmeny
     * @param pouzivatel pridavany pouzivatel
     * @return id pridaneho pouzivatela
     */
    Long addPouzivatel(String autor, PouzivatelDto pouzivatel) throws EntityUniqueExeption;

    /**
     * Zedituje existujuceho pouzivatela.
     *
     * @param autor      autor zmeny
     * @param pouzivatel existujuci pouzivatel so zmenenymi hodnotami
     * @throws EntityNotFoundException ak nebol podla identifikatora pouzivatela dohladany ziaden pouzivatel
     */
    void editPouzivatel(String autor, PouzivatelDto pouzivatel) throws EntityNotFoundException;

    /**
     * Nastavi pre zadaneho pouzivatela priznak, ze uz nie je aktivny.
     *
     * @param autor        autor zmeny
     * @param pouzivatelId identifikator mazaneho pouzivatela
     * @throws EntityNotFoundException ked nebol podla zadaneho id najdeny ziaden pouzivatel
     */
    void removePouzivatel(String autor, Long pouzivatelId) throws EntityNotFoundException;

    /**
     * Ziska pouzivatela podla jeho identifikatora.
     *
     * @param pouzivatelId identifikator pouzivatela
     * @return najdeny pouzivatel, alebo null
     */
    PouzivatelDto getPouzivatel(Long pouzivatelId);

    /**
     * Ziska pouzivatela podla loginu.
     *
     * @param login prihlasovaci login pouzivatela
     * @return najdeny pouzivatel, alebo null
     */
    PouzivatelDto getPouzivatelPodlaLoginu(String login);

    /**
     * Vrati zoznam pouzivatelov.
     *
     * @return zoznam
     */
    List<PouzivatelDto> getPouzivatelia();

    /**
     * Zmena prihlasovacieho hesla.
     *
     * @param pouzivatelId identifikator pouzivatela
     * @param stareHeslo   stare heslo
     * @param noveHeslo    nove heslo
     * @return priznak, ci sa podarilo zmenit heslo
     */
    boolean zmenaHesla(Long pouzivatelId, String stareHeslo, String noveHeslo);

    /**
     * Ked pouzivatel zabudne heslo, po zadani mail mu bude vygenerovane nove a poslane na zadany mail.
     *
     * @param mail mail pouzivatela
     */
    boolean zabudnuteHeslo(String mail);

    /**
     * Odosle tipy pouzivatela na jeho mail
     *
     * @param pouzivatelId identifikator pouzivatela
     * @return priznak, ci sa podarilo odoslat mail
     */
    boolean odoslaniePouzivatelovychTipov(Long pouzivatelId);
}
