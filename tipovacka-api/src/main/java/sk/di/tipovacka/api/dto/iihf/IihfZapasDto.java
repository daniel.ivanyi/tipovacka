package sk.di.tipovacka.api.dto.iihf;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Zapas z IIHF Rest sluzby.
 *
 * Created by divanyi on 13. 4. 2018.
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class IihfZapasDto {

    private Long dateTime;
    private String gamePhase;
    private String group;
    private  String homeTeam;
    private String guestTeam;
    private  String scoreByPeriod;
    private  int progressPerc;
    private  String progressCode;
    private  int homeTeamScore;
    private  int guestTeamScore;
    private String highlightsURL;
    private  String highlightsVimeoURL;

    public Long getDateTime() {
        return dateTime;
    }

    public void setDateTime(Long dateTime) {
        this.dateTime = dateTime;
    }

    public String getGamePhase() {
        return gamePhase;
    }

    public void setGamePhase(String gamePhase) {
        this.gamePhase = gamePhase;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getGuestTeam() {
        return guestTeam;
    }

    public void setGuestTeam(String guestTeam) {
        this.guestTeam = guestTeam;
    }

    public String getScoreByPeriod() {
        return scoreByPeriod;
    }

    public void setScoreByPeriod(String scoreByPeriod) {
        this.scoreByPeriod = scoreByPeriod;
    }

    public int getProgressPerc() {
        return progressPerc;
    }

    public void setProgressPerc(int progressPerc) {
        this.progressPerc = progressPerc;
    }

    public String getProgressCode() {
        return progressCode;
    }

    public void setProgressCode(String progressCode) {
        this.progressCode = progressCode;
    }

    public int getHomeTeamScore() {
        return homeTeamScore;
    }

    public void setHomeTeamScore(int homeTeamScore) {
        this.homeTeamScore = homeTeamScore;
    }

    public int getGuestTeamScore() {
        return guestTeamScore;
    }

    public void setGuestTeamScore(int guestTeamScore) {
        this.guestTeamScore = guestTeamScore;
    }

    public String getHighlightsURL() {
        return highlightsURL;
    }

    public void setHighlightsURL(String highlightsURL) {
        this.highlightsURL = highlightsURL;
    }

    public String getHighlightsVimeoURL() {
        return highlightsVimeoURL;
    }

    public void setHighlightsVimeoURL(String highlightsVimeoURL) {
        this.highlightsVimeoURL = highlightsVimeoURL;
    }

    @Override
    public String toString() {
        return "IihfZapasDto{" +
                "dateTime=" + dateTime +
                ", gamePhase='" + gamePhase + '\'' +
                ", group='" + group + '\'' +
                ", homeTeam='" + homeTeam + '\'' +
                ", guestTeam='" + guestTeam + '\'' +
                ", scoreByPeriod='" + scoreByPeriod + '\'' +
                ", progressPerc=" + progressPerc +
                ", progressCode='" + progressCode + '\'' +
                ", homeTeamScore=" + homeTeamScore +
                ", guestTeamScore=" + guestTeamScore +
                ", highlightsURL='" + highlightsURL + '\'' +
                ", highlightsVimeoURL='" + highlightsVimeoURL + '\'' +
                '}';
    }
}
