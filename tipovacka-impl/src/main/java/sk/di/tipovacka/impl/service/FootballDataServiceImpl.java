package sk.di.tipovacka.impl.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.db.Muzstvo;
import sk.di.tipovacka.api.db.dao.MuzstvoDao;
import sk.di.tipovacka.api.dto.MuzstvoDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.dto.footballdata.FootballDataMuzstvoDto;
import sk.di.tipovacka.api.dto.footballdata.FootballDataProgramDto;
import sk.di.tipovacka.api.dto.footballdata.FootballDataUcastniciDto;
import sk.di.tipovacka.api.dto.footballdata.FootballDataZapasDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.enums.KoniecZapasuTypEnum;
import sk.di.tipovacka.api.enums.ZapasTypEnum;
import sk.di.tipovacka.impl.util.ParseUtils;

import javax.annotation.PostConstruct;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class FootballDataServiceImpl {

    private final Logger logger = Logger.getLogger(getClass());

    private MuzstvoDao muzstvoDao;

    private final ObjectMapper mapper = new ObjectMapper();
    private MuzstvoDto tbd;

    @PostConstruct
    public void seTbdMuzstvo() {
        tbd = MuzstvoDto.create(muzstvoDao.getMuzstvoPodlaKodu("TBD"));
    }

    // https://sport.aktuality.sk/vysledky/futbal/medzistatny/majstrovstva-sveta/program/
    public List<ZapasDto> getZapasReport(String statistika, boolean init) {
        logger.info("getZapasReport(statistika=" + statistika + ", init=" + init + ")");
        Assert.notNull(statistika, "url statistikz musi byt zadana");

        // nacitame muzstva
        Map<String, String> muzstva;
        try {
            muzstva = getUcastnikov(statistika);
        } catch (Exception e) {
            throw new IllegalStateException("nepodarilo sa nacitat muzstva rocnika[url=" + statistika + "]", e);
        }

        // nacitame zapasy
        try {
            return getZapasy(statistika, muzstva);
        } catch (Exception e) {
            throw new IllegalStateException("nepodarilo sa nacitat program rocnika[url=" + statistika + "]", e);
        }
    }

    /**
     * Nacita zapasy rocnika.
     *
     * @param statistika root url
     * @param muzstva    mapa hrajucich muzstiev
     * @return zoznam zapasov
     */
    private List<ZapasDto> getZapasy(String statistika, Map<String, String> muzstva) throws IOException {
        if (StringUtils.isEmpty(statistika)) {
            throw new IllegalArgumentException("url statistiky musi byt zadana");
        }

        String response = getResponse(statistika + "/fixtures");

        FootballDataProgramDto program = mapper.readValue(response, FootballDataProgramDto.class);
        if (program == null || program.getFixtures() == null || program.getFixtures().isEmpty()) {
            throw new IllegalStateException("ziaden program nebol nacitany");
        }

        List<ZapasDto> zapasy = new ArrayList<>();
        for (FootballDataZapasDto z : program.getFixtures()) {
            ZapasDto dto = new ZapasDto();
            dto.setDatum(ParseUtils.parseDatumFromZonedDateTime(z.getDate()));
            dto.setTypZapasu(ParseUtils.parseTypZapasu(dto.getDatum()));
            dto.setKoniecZapasuTyp(parseTypKoncaZapasu(z.getStatus()));
            MuzstvoDto domaci = parseMuzstvo(z.getHomeTeamName(), muzstva);
            dto.setDomaciId(domaci.getId());
            dto.setDomaci(domaci);
            MuzstvoDto hostia = parseMuzstvo(z.getAwayTeamName(), muzstva);
            dto.setHostiaId(hostia.getId());
            dto.setHostia(hostia);
            if (z.getResult() != null) {
                dto.setGolyDomaci(z.getResult().getGoalsHomeTeam());
                dto.setGolyHostia(z.getResult().getGoalsAwayTeam());
            }

            zapasy.add(dto);
        }
        logger.trace("zapasy=" + zapasy);

        return zapasy;
    }

    /**
     * Vyparsuje typ konca zapasu podla stavu zapasu.
     *
     * @param status aktualny stav zapasu
     * @return typ konca zapasu
     */
    private KoniecZapasuTypEnum parseTypKoncaZapasu(String status) {
        if (StringUtils.isEmpty(status)) {
            throw new IllegalArgumentException("stav zapasu musi byt zadany");
        }

        if ("FINISHED".equals(status)) {
            return KoniecZapasuTypEnum.RIADNY_CAS;
        } else {
            return KoniecZapasuTypEnum.NEZACAL;
        }
    }

    /**
     * Nacitanie muzstiev rocnika.
     *
     * @param statistika root url REST sluzby
     * @return ucastnici vo forme mapy, kde klucik je nazov a hodnota je kod
     * @throws IOException ked sa neda nacitat service
     */
    private Map<String, String> getUcastnikov(String statistika) throws IOException {
        if (StringUtils.isEmpty(statistika)) {
            throw new IllegalArgumentException("url statistiky musi byt zadana");
        }

        String response = getResponse(statistika + "/teams");
        FootballDataUcastniciDto ucastnici = mapper.readValue(response, FootballDataUcastniciDto.class);

        if (ucastnici == null || ucastnici.getTeams() == null || ucastnici.getTeams().isEmpty()) {
            throw new IllegalStateException("ziadne muzstva neboli nacitane");
        }
        logger.info("nacitanych[" + ucastnici.getTeams().size() + "] muzstiev");
        logger.trace("muzstva=" + ucastnici);

        Map<String, String> map = new HashMap<>();
        for (FootballDataMuzstvoDto m : ucastnici.getTeams()) {
            map.put(m.getName(), m.getCode());
        }

        return map;
    }

    /**
     * Vrati muzstvo podla nazvu.
     *
     * @param nazov   nazov muzstva
     * @param muzstva dostupne muztva
     * @return najdene muzstvo
     */
    private MuzstvoDto parseMuzstvo(String nazov, Map<String, String> muzstva) {
        if (StringUtils.isEmpty(nazov)) {
            return tbd;
        }
        // ked sa jedna iba o navestie buduceho zapasu (osemfinal, stvrtfinale, semi)
        if (nazov.matches("\\d[ABCDEFGH]|W\\d{2}|L\\d{2}")) {
            return tbd;
        }

        // vyhladame podla nazvu muzstva kod muzstva a samotne muzstvo
        String kod = muzstva.get(nazov);
        MuzstvoDto muzstvo = MuzstvoDto.create(muzstvoDao.getMuzstvoPodlaKodu(kod));
        if (muzstvo == null) {
            logger.warn("najdene nezname muzstvo[" + nazov + "/" + kod + "]");
            Muzstvo m = new Muzstvo();
            m.setKod(kod);
            m.setNazov(nazov);
            m.setStatus(EntityStatusEnum.AKTIVNY);
            m.setId(muzstvoDao.save(m));
            logger.info("nezname muzstvo[" + nazov + "/" + kod + "] bolo pridane");

            return MuzstvoDto.create(m);
        }

        return muzstvo;
    }

    /**
     * Zavolanie REST sluzby pre ziskanie udajov.
     *
     * @param path pozadovana urlka
     * @return odpoved ako string
     * @throws IOException ak sa nepodari
     */
    private String getResponse(String path) throws IOException {
        logger.info("getResponse(path=" + path + ")");
        if (StringUtils.isEmpty(path)) {
            throw new IllegalArgumentException("urlka sluzby musi byt zadana");
        }

        URL url = new URL(path);
        HttpURLConnection con = (HttpURLConnection) url.openConnection();
        con.setRequestMethod("GET");
        con.setRequestProperty("X-Auth-Token", "54c37b4baf344b8fa55b418fcf659dc3");

        int status = con.getResponseCode();
        if (200 != status) {
            logger.warn("volanie sluzby skoncilo NOT_OK[" + status + "]");

            return null;
        }

        BufferedReader in = new BufferedReader(new InputStreamReader(con.getInputStream()));
        String inputLine;
        StringBuilder response = new StringBuilder();
        while ((inputLine = in.readLine()) != null) {
            response.append(inputLine);
        }
        logger.trace("response=" + response.toString());
        in.close();

        con.disconnect();

        return response.toString();
    }

    public void setMuzstvoDao(MuzstvoDao muzstvoDao) {
        this.muzstvoDao = muzstvoDao;
    }
}
