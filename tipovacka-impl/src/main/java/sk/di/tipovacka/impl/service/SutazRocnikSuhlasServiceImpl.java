package sk.di.tipovacka.impl.service;

import org.apache.log4j.Logger;
import org.apache.poi.openxml4j.util.ZipInputStreamZipEntrySource;
import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.SutazRocnikSuhlas;
import sk.di.tipovacka.api.db.dao.PouzivatelDao;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.dao.SutazRocnikSuhlasDao;
import sk.di.tipovacka.api.dto.RocnikSutazeSuhlasDto;
import sk.di.tipovacka.api.service.SutazRocnikSuhlasService;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementacia servicu.
 * <p>
 * User: Dano
 * Date: 16.2.2014
 */
public class SutazRocnikSuhlasServiceImpl implements SutazRocnikSuhlasService {

    private final Logger logger = Logger.getLogger(SutazRocnikSuhlasServiceImpl.class);

    private SutazRocnikSuhlasDao sutazRocnikSuhlasDao;
    private SutazRocnikDao sutazRocnikDao;
    private PouzivatelDao pouzivatelDao;

    @Override
    public Long addSuhlas(String autor, Long pouzivatelId, Long rocnikId) {
        logger.info("addSuhlas(autor=" + autor
                + ", pouzivatelId=" + pouzivatelId
                + ", rocnikId=" + rocnikId
                + ")");

        if (pouzivatelId == null) {
            logger.error("pouzivatelId nemoze byt null");
            throw new IllegalArgumentException("pouzivatelId nemoze byt null");
        }
        if (rocnikId == null) {
            logger.error("rocnikId nemoze byt null");
            throw new IllegalArgumentException("rocnikId nemoze byt null");
        }

        Pouzivatel pouzivatel = pouzivatelDao.get(pouzivatelId);
        if (pouzivatel == null) {
            logger.error("pouzivatel id[" + pouzivatelId + "] nebol najdeny");
            throw new IllegalArgumentException("pouzivatel id[" + pouzivatelId + "] nebol najdeny");
        }
        SutazRocnik rocnik = sutazRocnikDao.get(rocnikId);
        if (rocnik == null) {
            logger.error("rocnik id[" + rocnikId + "] nebol najdeny");
            throw new IllegalArgumentException("rocnik id[" + rocnikId + "] nebol najdeny");
        }

        SutazRocnikSuhlas suhlas = new SutazRocnikSuhlas();
        suhlas.setDatum(new Date());
        suhlas.setSutazRocnik(rocnik);
        suhlas.setSutazRocnikId(rocnikId);
        suhlas.setPouzivatel(pouzivatel);
        suhlas.setPouzivatelId(pouzivatelId);
        suhlas.setSuhlas(true);
        Long suhlasId = sutazRocnikSuhlasDao.save(suhlas);
        logger.info("suhlas[pouzivatelId=" + pouzivatelId + ", rocnikId=" + rocnikId + "] bol zaznamenany.");

        return suhlasId;
    }

    @Override
    public void addZaplatene(String autor, Long suhlasId) {
        logger.info("addZaplatene(autor=" + autor
                + ", suhlasId=" + suhlasId
                + ")");

        SutazRocnikSuhlas suhlas = sutazRocnikSuhlasDao.get(suhlasId);
        suhlas.setZaplatene(new Timestamp(System.currentTimeMillis()));
        sutazRocnikSuhlasDao.saveOrUpdate(suhlas);

        logger.info("priznak zaplatenia[id=" + suhlasId + "] bol zaznamenany");
    }

    @Override
    public void removeZaplatene(String autor, Long suhlasId) {
        logger.info("removeZaplatene(autor=" + autor
                + ", suhlasId=" + suhlasId
                + ")");

        SutazRocnikSuhlas suhlas = sutazRocnikSuhlasDao.get(suhlasId);
        suhlas.setZaplatene(null);
        sutazRocnikSuhlasDao.saveOrUpdate(suhlas);

        logger.info("priznak zaplatenia[id=" + suhlasId + "] bol zruseny");
    }

    @Override
    public List<RocnikSutazeSuhlasDto> findAllCoNezaplatili(Long rocnikId) {
        logger.info("findAllCoNezaplatili(rocnikId=" + rocnikId + ")");

        List<RocnikSutazeSuhlasDto> nesplnili = new ArrayList<>();
        for (SutazRocnikSuhlas s : sutazRocnikSuhlasDao.findAllByRocnik(rocnikId)) {
            if (s.getZaplatene() == null) {
                nesplnili.add(RocnikSutazeSuhlasDto.create(s));
            }
        }

        return nesplnili;
    }

    @Override
    public List<String> findAllLoginCoNezaplatili(Long rocnikId) {
        logger.info("findAllCoNezaplatili(rocnikId=" + rocnikId + ")");

        List<String> nesplnili = new ArrayList<>();
        for (RocnikSutazeSuhlasDto s : findAllCoNezaplatili(rocnikId)) {
            nesplnili.add(s.getPouzivatel().getLogin());
        }

        return nesplnili;
    }

    @Override
    public List<RocnikSutazeSuhlasDto> findAllByRocnik(Long rocnikId) {
        logger.info("findAllByRocnik(rocnikId=" + rocnikId + ")");

        List<RocnikSutazeSuhlasDto> suhlasy = new ArrayList<>();
        for (SutazRocnikSuhlas s : sutazRocnikSuhlasDao.findAllByRocnik(rocnikId)) {
            suhlasy.add(RocnikSutazeSuhlasDto.create(s));
        }

        return suhlasy;
    }

    @Override
    public boolean isSplnenePodmienky(String login, Long rocnikId) {
        return !findAllLoginCoNezaplatili(rocnikId).contains(login);
    }


    public void setSutazRocnikSuhlasDao(SutazRocnikSuhlasDao sutazRocnikSuhlasDao) {
        this.sutazRocnikSuhlasDao = sutazRocnikSuhlasDao;
    }

    public void setSutazRocnikDao(SutazRocnikDao sutazRocnikDao) {
        this.sutazRocnikDao = sutazRocnikDao;
    }

    public void setPouzivatelDao(PouzivatelDao pouzivatelDao) {
        this.pouzivatelDao = pouzivatelDao;
    }
}
