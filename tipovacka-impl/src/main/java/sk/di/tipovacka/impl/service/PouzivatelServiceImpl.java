package sk.di.tipovacka.impl.service;

import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.commons.lang.RandomStringUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.dao.PouzivatelDao;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.dao.SutazRocnikSuhlasDao;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.exception.EntityUniqueExeption;
import sk.di.tipovacka.api.service.MailService;
import sk.di.tipovacka.api.service.PouzivatelService;

import javax.persistence.EntityNotFoundException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Implementacia servisu pre pracu s pouzivatelom
 * <p/>
 * User: Dano
 * Date: 9.5.2013
 */
public class PouzivatelServiceImpl implements PouzivatelService {

    private final Logger logger = Logger.getLogger(getClass());

    // vyskladanie zakladneho textu do mailu
    @Value("${mail.pouzivatel.pridany.text}")
    private String mailTextPouzivatelPridany;
    @Value("${mail.pouzivatel.registrovany.text}")
    private String mailTextPouzivatelRegistrovany;
    @Value("${url.app.home}")
    private String urlAppHome;
    @Value("${url.konto.aktivacia}")
    private String urlKontoAktivacia;

    private PouzivatelDao pouzivatelDao;
    private PasswordEncoder passwordEncoder;
    private TipNaZapasDao tipNaZapasDao;
    private SutazRocnikDao sutazRocnikDao;
    private SutazRocnikSuhlasDao sutazRocnikSuhlasDao;

    private MailService mailService;

    private SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    @Override
    public Long addPouzivatel(String autor, PouzivatelDto pouzivatel) {
        logger.info("addPouzivatel(autor=" + autor
                + ", pouzivatel=...)");
        Assert.notNull(pouzivatel, "pouzivatel nemoze byt null");
        Assert.isTrue(StringUtils.isNotEmpty(pouzivatel.getLogin()),
                "login nemoze byt null");
        Assert.isTrue(StringUtils.isNotEmpty(pouzivatel.getMail()),
                "mail nemoze byt null");
        Assert.isTrue(StringUtils.isNotEmpty(pouzivatel.getMeno()),
                "meno nemoze byt null");
        Assert.isTrue(StringUtils.isNotEmpty(pouzivatel.getPriezvisko()),
                "priezvisko nemoze byt null");
        Assert.isTrue(pouzivatel.getRoly() != null && !pouzivatel.getRoly().isEmpty(),
                "rola nemoze byt null");

        // overit na jednoznacnost podla loginu a mailu
        Pouzivatel pLogin = pouzivatelDao.getPouzivatelPodlaLoginu(pouzivatel.getLogin());
        if (pLogin != null) {
            logger.warn("login[" + pouzivatel.getLogin() + "] je uz pouzity");
            throw new EntityUniqueExeption("login[" + pouzivatel.getLogin() + "] je uz pouzity");
        }
        Pouzivatel pMail = pouzivatelDao.getPouzivatelPodlaMailu(pouzivatel.getMail());
        if (pMail != null) {
            logger.warn("mail[" + pouzivatel.getMail() + "] je uz pouzity");
            throw new EntityUniqueExeption("mail[" + pouzivatel.getMail() + "] je uz pouzity");
        }

        // text do mailu
        String mailText;

        // ak heslo nebolo zadane, vygenerujeme
        String heslo = pouzivatel.getHeslo();
        if (StringUtils.isEmpty(heslo)) {
            heslo = RandomStringUtils.randomAlphanumeric(8);
            pouzivatel.setHeslo(heslo);
            // zmenime aj hlasku mailu
            mailText = mailTextPouzivatelPridany;
        } else {
            mailText = mailTextPouzivatelRegistrovany;
        }
        // heslo zahashujeme
        pouzivatel.setHeslo(passwordEncoder.encodePassword(pouzivatel.getHeslo(), pouzivatel.getLogin()));

        // presypeme pouzivatela
        Pouzivatel p = new Pouzivatel();
        p.setLogin(pouzivatel.getLogin());
        p.setHeslo(pouzivatel.getHeslo());
        p.setMail(pouzivatel.getMail());
        p.setMeno(pouzivatel.getMeno());
        p.setPriezvisko(pouzivatel.getPriezvisko());
        StringBuilder sb = new StringBuilder();
        for (GrantedAuthority rola : pouzivatel.getRoly()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(rola.getAuthority());
        }
        p.setRoly(sb.toString());
        p.setStatus(EntityStatusEnum.NEAKTIVNY);

        // ulozime
        Long pouzivatelId = pouzivatelDao.save(p);
        logger.info("pouzivatel[pouzivatelId=" + pouzivatelId + "] bol zalozeny");

        // aktivacny link
        String ativacnyLink = String.format(urlKontoAktivacia, new String[] {
                String.valueOf(pouzivatelId)
        });

        // a posleme mail pouzivatelovi, ze bol zadany
        String text = String.format(mailText, new String[]{
                urlAppHome,
                pouzivatel.getLogin(),
                heslo,
                ativacnyLink});
        mailService.sendMail(p.getMail(), "zalozeny pouzivatel", text, null, null, null);

        return pouzivatelId;
    }

    @Override
    public void editPouzivatel(String autor, PouzivatelDto pouzivatel) throws EntityNotFoundException {
        Assert.notNull(pouzivatel, "pouzivatel nemoze byt null");
        Assert.isTrue(pouzivatel.getId() != null, "pouzivatelId nemoze byt null");

        logger.info("editPouzivatel(autor=" + autor
                + ", pouzivatel=" + pouzivatel + ")");

        // overit na jednoznacnost podla loginu a mailu
        Pouzivatel pLogin = pouzivatelDao.getPouzivatelPodlaLoginu(pouzivatel.getLogin());
        if (pLogin != null && !pouzivatel.getId().equals(pLogin.getId())) {
            logger.warn("login[" + pouzivatel.getLogin() + "] je uz pouzity");
            return;
        }
        Pouzivatel pMail = pouzivatelDao.getPouzivatelPodlaMailu(pouzivatel.getMail());
        if (pMail != null && !pouzivatel.getId().equals(pLogin.getId())) {
            logger.warn("mail[" + pouzivatel.getMail() + "] je uz pouzity");
            return;
        }

        // najskor dohladame ci existuje pouzivatel s danym id
        Pouzivatel p = pouzivatelDao.get(pouzivatel.getId());
        if (p == null) {
            logger.warn("pouzivatel[id=" + pouzivatel.getId() + "] nebol najdeny");
            throw new EntityNotFoundException("pouzivatel[id=" + pouzivatel.getId() + "] nebol najdeny");
        }
        p.setMeno(pouzivatel.getMeno());
        p.setPriezvisko(pouzivatel.getPriezvisko());
        p.setMail(pouzivatel.getMail());
        // roly
        StringBuilder sb = new StringBuilder();
        for (GrantedAuthority rola : pouzivatel.getRoly()) {
            if (sb.length() > 0) {
                sb.append(" ");
            }
            sb.append(rola.getAuthority());
        }
        p.setRoly(sb.toString());
        // status
        p.setStatus(pouzivatel.getStatus());

        // ak bol pouzivatel najdeny, ulozime zmenene hodnoty
        pouzivatelDao.saveOrUpdate(p);
        logger.info("pouzivatel[id=" + pouzivatel.getId() + "] bol zmeneny");
    }

    @Override
    public void removePouzivatel(String autor, Long pouzivatelId) throws EntityNotFoundException {
        Assert.isTrue(pouzivatelId != null, "pouzivatelId nemoze byt null");

        logger.info("removePouzivatel(autor=" + autor
                + ", pouzivatelId=" + pouzivatelId + ")");

        // najskor dohladame ci existuje pouzivatel s danym id
        Pouzivatel pouzivatel = pouzivatelDao.get(pouzivatelId);
        if (pouzivatel == null) {
            logger.warn("pouzivatel[id=" + pouzivatelId + "] nebol najdeny");
            throw new EntityNotFoundException("pouzivatel[id=" + pouzivatelId + "] nebol najdeny");
        }

        // ak bol pouzivatel najdeny, zmenime mu stav na neaktivny a ulozime zmeny
        pouzivatel.setStatus(EntityStatusEnum.NEAKTIVNY);
        pouzivatelDao.saveOrUpdate(pouzivatel);
        logger.info("pouzivatel bol deaktivovany");
    }

    @Override
    public PouzivatelDto getPouzivatelPodlaLoginu(String login) {
        logger.info("getPouzivatelPodlaLoginu(login=" + login + ")");

        Pouzivatel pouzivatel = null;
        if (StringUtils.isNotEmpty(login)) {
            try {
                pouzivatel = pouzivatelDao.getPouzivatelPodlaLoginu(login);
            } catch (EntityUniqueExeption e) {
                logger.error("login[" + login + "] nie je unikatny", e);
            }
        }

        return PouzivatelDto.create(pouzivatel);
    }

    @Override
    public List<PouzivatelDto> getPouzivatelia() {
        logger.info("getPouzivatelia()");
        List<PouzivatelDto> pouzivatelia = new ArrayList<>();
        for (Pouzivatel p : pouzivatelDao.findAll()) {
            pouzivatelia.add(PouzivatelDto.create(p));
        }

        return pouzivatelia;
    }

    @Override
    public PouzivatelDto getPouzivatel(Long pouzivatelId) {
        Assert.isTrue(pouzivatelId != null, "pouzivatelId nemoze byt null");

        logger.info("getPouzivatel(pouzivatelId=" + pouzivatelId + ")");

        return PouzivatelDto.create(pouzivatelDao.get(pouzivatelId));
    }

    @Override
    public boolean zmenaHesla(Long pouzivatelId, String stareHeslo, String noveHeslo) {
        logger.info("zmenaHesla(pouzivatelId=" + pouzivatelId
                + ", stareHeslo=..."
                + ", noveHeslo=..."
                + ")");
        if (pouzivatelId == null) {
            logger.error("pouzivatelId nemoze byt null");
            throw  new IllegalArgumentException("pouzivatelId nemoze byt null");
        }
        if (StringUtils.isEmpty(stareHeslo) || StringUtils.isEmpty(noveHeslo)) {
            logger.error("stare a nove heslo musia byt zadane");
            throw  new IllegalArgumentException("stare a nove heslo musia byt zadane");
        }

        // najdeme si pouzivatela
        Pouzivatel pouzivatel = pouzivatelDao.get(pouzivatelId);
        if (pouzivatel != null) {
            if (passwordEncoder
                    .encodePassword(stareHeslo, pouzivatel.getLogin())
                    .equals(pouzivatel.getHeslo())) {
                pouzivatel.setHeslo(passwordEncoder.encodePassword(noveHeslo, pouzivatel.getLogin()));
                pouzivatelDao.saveOrUpdate(pouzivatel);
                logger.info("heslo bolo uspesne zmenene");

                return true;
            } else {
                logger.warn("nespravne zadane stare heslo");
            }
        } else {
            logger.warn("nebol najdeny ziaden pouzivatel[pouzivatelId=" + pouzivatelId + "]");
        }

        return false;
    }

    @Override
    public boolean zabudnuteHeslo(String mail) {
        logger.info("zabudnuteHeslo(mail=" + mail + ")");

        if (StringUtils.isEmpty(mail)) {
            logger.error("mail musi byt zadany");
            throw new IllegalArgumentException("mail musi byt zadany");
        }

        // ziskame pouzivatela
        Pouzivatel pouzivatel = pouzivatelDao.getPouzivatelPodlaMailu(mail);
        if (pouzivatel != null) {
            // vygenerujeme nahodne heslo
            String heslo = RandomStringUtils.randomAlphanumeric(8);
            // ulozime v databaze
            pouzivatel.setHeslo(passwordEncoder.encodePassword(heslo, pouzivatel.getLogin()));
            pouzivatelDao.saveOrUpdate(pouzivatel);
            // vyskladame text pre mail
            String text = String.format(mailTextPouzivatelPridany, new String[]{urlAppHome, pouzivatel.getLogin(), heslo});
            // a posleme
            mailService.sendMail(pouzivatel.getMail(), "nove prihlasovacie udaje", text, null, null, null);

            return true;
        } else {
            logger.warn("pouzivatel[mail=" + mail + "] nebol najdeny");
            throw new EntityNotFoundException("pouzivatel[mail=" + mail + "] nebol najdeny");
        }
    }

    @Override
    public boolean odoslaniePouzivatelovychTipov(Long pouzivatelId) {
        logger.info("odoslaniePouzivatelovychTipov(pouzivatelId=" + pouzivatelId + ")");

        if (pouzivatelId == null) {
            logger.error("pouzivatelId nemoze byt null");
            throw  new IllegalArgumentException("pouzivatelId nemoze byt null");
        }

        // najdeme si pouzivatela
        Pouzivatel pouzivatel = pouzivatelDao.get(pouzivatelId);
        if (pouzivatel != null) {
            boolean odoslane = false;
            for (SutazRocnik rocnik : sutazRocnikDao.getAktualnyRocnikSutaze()) {
                TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
                kriteria.getPouzivatelIds().add(pouzivatelId);
                kriteria.setSutazRocnikId(rocnik.getId());
                List<TipNaZapas> tipy = tipNaZapasDao.findTipyNaZapas(kriteria);
                // zosortujeme DESC najdene typy podla datumu hrania zapasu
                Collections.sort(tipy, new Comparator<TipNaZapas>() {
                    @Override
                    public int compare(TipNaZapas o1, TipNaZapas o2) {
                        // ked nepoznam tip, alebo zapas, nedokazem sortovat
                        if (o1 == null || o2 == null) {
                            return 0;
                        }
                        if (o1.getZapas() == null || o2.getZapas() == null) {
                            return 0;
                        }

                        return o1.getZapas().getDatum().compareTo(o2.getZapas().getDatum()) * -1;
                    }
                });

                HashMap<String, Object> parameterMap = new HashMap<>();
                parameterMap.put("login", pouzivatel.getLogin());
                parameterMap.put("sport", rocnik.getSutaz().getSportTyp().name().toLowerCase());
                parameterMap.put("nazovSutaze", rocnik.getSutaz().getNazov());
                parameterMap.put("rocnikSutaze", String.valueOf(rocnik.getRok()));
                parameterMap.put("datumGenerovania", sdf.format(new Date()));

                JasperDesign design;
                JasperReport report;
                JasperPrint jasperPrint;

                try {
                    design = JRXmlLoader.load(this.getClass().getClassLoader().getResourceAsStream("pdf/mojeTipy.jrxml"));
                    report = JasperCompileManager.compileReport(design);
                    jasperPrint = JasperFillManager.fillReport(report,
                            parameterMap,
                            new JRBeanCollectionDataSource(tipy));
                    byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);

                    try {
                        mailService.sendMail(pouzivatel.getMail(), "Moje tipy", "", pdf, "mojeTipy.pdf", "application/pdf");
                        odoslane = true;
                    } catch (Exception e) {
                        logger.error("nepodarilo sa odoslat mail[adresa=" + pouzivatel.getMail() + ", predmet=Moje tipy]", e);
                    }
                } catch (JRException e) {
                    logger.error("nepodarilo sa vyrenderovat mojeTipy.pdf", e);
                }
            }

            return odoslane;
        } else {
            logger.warn("nebol najdeny ziaden pouzivatel[pouzivatelId=" + pouzivatelId + "]");

            return false;
        }
    }

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        logger.info("loadUserByUsername(login=" + login + ")");

        Pouzivatel pouzivatel = pouzivatelDao.getPouzivatelPodlaLoginu(login);
        if (pouzivatel == null) {
            logger.warn("Pouzivatel[login=" + login + "] nebol najdeny.");
            throw new UsernameNotFoundException("Pouzivatel[login=" + login + "] nebol najdeny.");
        } else if (EntityStatusEnum.NEAKTIVNY.equals(pouzivatel.getStatus())) {
            logger.warn("Pouzivatel[login=" + login + "] je neaktivny.");
            throw new IllegalArgumentException("Pouzivatel[login=" + login + "] je neaktivny.");
        }

        PouzivatelDto p = PouzivatelDto.create(pouzivatel);
        // dohladame suhlas pouzivatela pre aktualne rocniky, na ktory sa da tipovat
        List<SutazRocnik> aktualneRocnikySutaze = sutazRocnikDao.getAktualnyRocnikSutaze();
        for (SutazRocnik rocnik : aktualneRocnikySutaze) {
            if (sutazRocnikSuhlasDao.isPouzivatelSuhlas(p.getId(), rocnik.getId())) {
                p.getAktivneSutazeId().add(rocnik.getId());
                // ak je iba jeden aktivny rocnik, nasetujeme aj vybrany rocnik
                if (aktualneRocnikySutaze.size() == 1) {
                    p.setVybranyRocnik(RocnikSutazeDto.create(rocnik));
                }
            }
        }

        return p;
    }

    public void setPouzivatelDao(PouzivatelDao pouzivatelDao) {
        this.pouzivatelDao = pouzivatelDao;
    }

    public void setPasswordEncoder(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    public void setTipNaZapasDao(TipNaZapasDao tipNaZapasDao) {
        this.tipNaZapasDao = tipNaZapasDao;
    }

    public void setSutazRocnikDao(SutazRocnikDao sutazRocnikDao) {
        this.sutazRocnikDao = sutazRocnikDao;
    }

    public void setSutazRocnikSuhlasDao(SutazRocnikSuhlasDao sutazRocnikSuhlasDao) {
        this.sutazRocnikSuhlasDao = sutazRocnikSuhlasDao;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
}
