package sk.di.tipovacka.impl.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.db.Muzstvo;
import sk.di.tipovacka.api.db.dao.MuzstvoDao;
import sk.di.tipovacka.api.dto.MuzstvoDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.dto.worldcupsfgio.WorldCupMuzstvoDto;
import sk.di.tipovacka.api.dto.worldcupsfgio.WorldCupZapasDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.enums.KoniecZapasuTypEnum;
import sk.di.tipovacka.impl.util.ParseUtils;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class WorldCupSfgIoServiceImpl {

    private final Logger logger = Logger.getLogger(getClass());

    private MuzstvoDao muzstvoDao;

    private final ObjectMapper mapper = new ObjectMapper();
    private MuzstvoDto tbd;

    @PostConstruct
    public void seTbdMuzstvo() {
        tbd = MuzstvoDto.create(muzstvoDao.getMuzstvoPodlaKodu("TBD"));
    }

    public List<ZapasDto> getZapasReport(String statistika, boolean init) {
        logger.info("getZapasReport(statistika=" + statistika + ", init=" + init + ")");
        Assert.notNull(statistika, "url statistikz musi byt zadana");

        List<ZapasDto> zapasy = new ArrayList<>();
        try {
            List<WorldCupZapasDto> program = mapper.readValue(new URL(statistika), new TypeReference<List<WorldCupZapasDto>>() {
            });
            if (program != null && !program.isEmpty()) {
                for (WorldCupZapasDto z : program) {
                    ZapasDto dto = new ZapasDto();
                    dto.setDatum(ParseUtils.parseDatumFromZonedDateTime(z.getDatetime()));
                    dto.setTypZapasu(ParseUtils.parseTypZapasu(dto.getDatum()));
                    dto.setKoniecZapasuTyp(parseTypKoncaZapasu(z.getStatus()));
                    MuzstvoDto domaci = parseMuzstvo(z.getHomeTeam());
                    dto.setDomaciId(domaci.getId());
                    dto.setDomaci(domaci);
                    MuzstvoDto hostia = parseMuzstvo(z.getAwayTeam());
                    dto.setHostiaId(hostia.getId());
                    dto.setHostia(hostia);
                    dto.setGolyDomaci(z.getHomeTeam().getGoals());
                    dto.setGolyHostia(z.getAwayTeam().getGoals());
                    // ak sa kopali penalty, tak musime podla rozstrelu pridat vysledny gol
                    if (isPenaltovyRozstrel(z.getHomeTeam(), z.getAwayTeam())) {
                        if (z.getHomeTeam().getPenalties() > z.getAwayTeam().getPenalties()) {
                            dto.setGolyDomaci(dto.getGolyDomaci() + 1);
                        } else {
                            dto.setGolyHostia(dto.getGolyHostia() + 1);
                        }
                    }

                    zapasy.add(dto);
                }
                logger.trace("zapasy=" + zapasy);
            }
        } catch (IOException e) {
            logger.error("nepodarilo sa naciatat udaje[url=" + statistika + "]", e);
        }

        return zapasy;
    }

    /**
     * Vyparsuje typ konca zapasu podla stavu zapasu.
     *
     * @param status aktualny stav zapasu
     * @return typ konca zapasu
     */
    private KoniecZapasuTypEnum parseTypKoncaZapasu(String status) {
        if (StringUtils.isEmpty(status)) {
            throw new IllegalArgumentException("stav zapasu musi byt zadany");
        }

        if ("completed".equalsIgnoreCase(status)) {
            return KoniecZapasuTypEnum.RIADNY_CAS;
        } else if ("in progress".equalsIgnoreCase(status)) {
            return KoniecZapasuTypEnum.HRA_SA;
        } else {
            return KoniecZapasuTypEnum.NEZACAL;
        }
    }

    /**
     * Vrati muzstvo z DB podla zadaneho muzstva v programe.
     *
     * @param m pre z programu
     * @return najdene muzstvo
     */
    private MuzstvoDto parseMuzstvo(WorldCupMuzstvoDto m) {
        if (m == null || StringUtils.isEmpty(m.getCode())) {
            return tbd;
        }

        // vyhladame podla nazvu muzstva kod muzstva a samotne muzstvo
        String kod = m.getCode();
        MuzstvoDto muzstvo = MuzstvoDto.create(muzstvoDao.getMuzstvoPodlaKodu(kod));
        if (muzstvo == null) {
            String nazov = m.getCountry();
            logger.warn("muzstvo[" + nazov + "/" + kod + "] nie je v db, pridame");
            Muzstvo team = new Muzstvo();
            team.setKod(kod);
            team.setNazov(nazov);
            team.setStatus(EntityStatusEnum.AKTIVNY);
            team.setId(muzstvoDao.save(team));
            logger.info("muzstvo[" + nazov + "/" + kod + "] bolo pridane do db");

            return MuzstvoDto.create(team);
        }

        return muzstvo;
    }

    /**
     * Priznak, ze zapas skoncil penaltovym rozstrelom.
     *
     * @param domaci domaci
     * @param hostia hostia
     * @return priznak
     */
    private boolean isPenaltovyRozstrel(WorldCupMuzstvoDto domaci, WorldCupMuzstvoDto hostia) {
        return domaci.getPenalties() != null
                && hostia.getPenalties() != null
                && (domaci.getPenalties() + hostia.getPenalties() > 0);
    }

    public void setMuzstvoDao(MuzstvoDao muzstvoDao) {
        this.muzstvoDao = muzstvoDao;
    }
}
