package sk.di.tipovacka.impl.service;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import sk.di.tipovacka.api.db.Muzstvo;
import sk.di.tipovacka.api.db.dao.MuzstvoDao;
import sk.di.tipovacka.api.dto.iihf.IihfTurnajDto;
import sk.di.tipovacka.api.dto.iihf.IihfZapasDto;
import sk.di.tipovacka.api.dto.MuzstvoDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.KoniecZapasuTypEnum;
import sk.di.tipovacka.api.enums.ZapasTypEnum;

import javax.annotation.Resource;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Iihf Rest service klient.
 * <p>
 * Created by divanyi on 13. 4. 2018.
 */
public class IihfServiceImpl {

    private final Logger logger = Logger.getLogger(getClass());
    private final ObjectMapper mapper = new ObjectMapper();

    @Resource
    private MuzstvoDao muzstvoDao;

    /**
     * Dohlada cez iihf rest service informacie k pozadovanemu turnaju.
     *
     * @param url urlka, z ktorej sa ma citat
     * @return zoznam zapasov
     */
    public List<ZapasDto> getZapasyTurnaja(String url) {
        logger.info("getZapasyTurnaja(url=" + url + ")");

        List<ZapasDto> zapasy = new ArrayList<>();
        try {
            String turnajListUrl = url + "&requestName=tournamentList";
            // nacitame zoznam turnajov pre danu sezonu, linku
            logger.debug("nacitanie zoznamu turnajov podla url[" + turnajListUrl + "]");
            List<IihfTurnajDto> turnaje = mapper.readValue(new URL(turnajListUrl), new TypeReference<List<IihfTurnajDto>>() {
            });
            if (turnaje == null || turnaje.isEmpty()) {
                logger.info("ziaden zoznam turnajov pre url[" + turnajListUrl + "]");

                return Collections.emptyList();
            }
            logger.debug("bolo nacitanych " + turnaje.size() + " turnajov");

            for (IihfTurnajDto t : turnaje) {
                // v najdenych turnajov berieme len svetovu muzsku diviziu
                if ("WM Top Div".equalsIgnoreCase(t.getLongName())) {
                    // a dohladame si zapasy daneho turnaja
                    String urlTurnaj = String.format("%s&requestName=games&tournamentId=%d",
                            url, t.getTournamentID());
                    logger.debug("nacitanie zoznamu zapasov podla url[" + urlTurnaj + "]");
                    List<IihfZapasDto> iihfZapasy = mapper.readValue(new URL(urlTurnaj), new TypeReference<List<IihfZapasDto>>() {
                    });
                    for (IihfZapasDto z : iihfZapasy) {
                        zapasy.add(convertIhfZapas(z));
                    }
                    logger.debug("bolo nacitanych " + zapasy.size() + " zapasov");
                }
            }
        } catch (IOException e) {
            logger.error("nepodarilo sa dohladat zoznam zapasov pre url[" + url + "]", e);
        }

        return zapasy;
    }

    /**
     * Prekonvertuje iihf zapas na zapas dto, ktory potrebujeme.
     *
     * @param iihf iihf zapas z rest sluzby
     * @return zapas dto
     */
    private ZapasDto convertIhfZapas(IihfZapasDto iihf) {
        ZapasDto zapas = new ZapasDto();
        // koniec zapasu
        zapas.setKoniecZapasuTyp(getKoniecZapasuTypEnum(iihf.getScoreByPeriod(), iihf.getProgressPerc()));
        // tretiny
        zapas.setPriebeh(iihf.getScoreByPeriod());
        // typ zapasu
        zapas.setTypZapasu(ZapasTypEnum.valueOf(iihf.getGamePhase()));
        // skupina
        zapas.setSkupina(iihf.getGroup());
        // datum hrania
        zapas.setDatum(new Timestamp(iihf.getDateTime() * 1000));
        // domaci
        MuzstvoDto domaci = MuzstvoDto.create(muzstvoDao.getMuzstvoPodlaKodu(iihf.getHomeTeam()));
        // ak podla daneho kodu neexistuje v db muzstvo, pridame
        if (domaci == null) {
            logger.warn("nezname muzstvo domacich[" + iihf.getHomeTeam() + "], pridame do db");
            Muzstvo d = new Muzstvo();
            d.setKod(iihf.getHomeTeam());
            d.setId(muzstvoDao.save(d));
            domaci = MuzstvoDto.create(d);
        }
        zapas.setDomaci(domaci);
        zapas.setDomaciId(domaci.getId());
        // hostia
        MuzstvoDto hostia = MuzstvoDto.create(muzstvoDao.getMuzstvoPodlaKodu(iihf.getGuestTeam()));
        // ak podla daneho kodu neexistuje v db muzstvo, pridame
        if (hostia == null) {
            logger.warn("nezname muzstvo hosti[" + iihf.getGuestTeam() + "], pridame do db");
            Muzstvo h = new Muzstvo();
            h.setKod(iihf.getGuestTeam());
            h.setId(muzstvoDao.save(h));
            hostia = MuzstvoDto.create(h);
        }
        zapas.setHostia(hostia);
        zapas.setHostiaId(hostia.getId());
        // goly domaci
        zapas.setGolyDomaci(iihf.getHomeTeamScore());
        // goly hostia
        zapas.setGolyHostia(iihf.getGuestTeamScore());
        // highlights
        zapas.setHighlightsUrl(StringUtils.isNotEmpty(iihf.getHighlightsURL())
                ? iihf.getHighlightsURL()
                : iihf.getHighlightsVimeoURL());

        return zapas;
    }

    /**
     * Podla skore vyhodnoti stav konca zapasu.
     *
     * @param skore   aktualne skore zapasu
     * @param priebeh percento z odohrateho zapasu
     * @return typ konca
     */
    private KoniecZapasuTypEnum getKoniecZapasuTypEnum(String skore, Integer priebeh) {
        if (StringUtils.isEmpty(skore)) {
            return null;
        }
        if (priebeh == 0) {
            return KoniecZapasuTypEnum.NEZACAL;
        } else if (priebeh < 100) {
            return KoniecZapasuTypEnum.HRA_SA;
        } else {
            String[] tretiny = skore.split("\\|");
            String nehraloSa = "-:-";
            if (!nehraloSa.equals(tretiny[4])) {
                return KoniecZapasuTypEnum.ROZSTREL;
            } else if (!nehraloSa.equals(tretiny[3])) {
                return KoniecZapasuTypEnum.PREDLZENIE;
            } else {
                return KoniecZapasuTypEnum.RIADNY_CAS;
            }
        }
    }
}
