package sk.di.tipovacka.impl.job;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.service.MailService;
import sk.di.tipovacka.api.service.TipNaZapasService;
import sk.di.tipovacka.impl.util.ParseUtils;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Job pre kontrolu mailovej schranky aplikacie a spracovanie neprecitanych mailov, v ktorych
 * moze byt nadefinovany tip na zapas.
 * User: Dano
 * Date: 4.3.2014
 */
public class SpracujNeprecitaneTipyNaZapasJob extends QuartzJobBean implements Serializable {
    private static final long serialVersionUID = -6356418446436226515L;

    private final Logger logger = Logger.getLogger(getClass());
    private final SimpleDateFormat formatDatumu = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private MailService mailService;
    private TipNaZapasService tipNaZapasService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        jobRun();
    }

    public synchronized void jobRun() {
        logger.debug("jobRun() started ...");

        try {
            List<TipNaZapasDto> tipy = mailService.getNeprecitaneTipyNaZapas();
            if (!tipy.isEmpty()) {
                logger.info("pocet neprecitanych tipov na zapas=" + tipy.size());
                // zoznam pouzivatelov, ktorym sme spracovavali tipy
                Set<PouzivatelDto> pouzivatelia = new HashSet<>();
                // mapa spracovanych tipov
                Map<String, List<TipNaZapasDto>> spracovaneTipy = new HashMap<>();
                // mapa nespracovanych tipov
                Map<String, List<TipNaZapasDto>> nespracovaneTipy = new HashMap<>();
                for (TipNaZapasDto tip : tipy) {
                    String login = tip.getPouzivatel().getLogin();
                    pouzivatelia.add(tip.getPouzivatel());
                    try {
                        if (tip.getId() != null) {
                            tipNaZapasService.editTipNaZapas("mail", tip, true);
                        } else {
                            tipNaZapasService.addTipNaZapas("mail", tip, true);
                        }
                        // po spracovani tipu si ho pridame do mapy podla loginu
                        if (spracovaneTipy.get(login) == null) {
                            spracovaneTipy.put(login, new ArrayList<TipNaZapasDto>());
                        }
                        spracovaneTipy.get(login).add(tip);
                    } catch (Exception e) {
                        logger.error("nepodarilo sa ulozit tip[" + tip + "]", e);
                        // ak sa nepodarilo ulozit tip, zadelime ho medzi nespracovane
                        if (nespracovaneTipy.get(login) == null) {
                            nespracovaneTipy.put(login, new ArrayList<TipNaZapasDto>());
                        }
                        nespracovaneTipy.get(login).add(tip);
                    }
                }

                // po spracovani odosleme info pouzivatelom
                if (!pouzivatelia.isEmpty()) {
                    for (PouzivatelDto pouzivatel : pouzivatelia) {
                        String login = pouzivatel.getLogin();
                        // vyskladame text spravy
                        StringBuilder text = new StringBuilder();
                        // spracovane tipy
                        if (spracovaneTipy.get(login) != null) {
                            for (TipNaZapasDto tip : spracovaneTipy.get(login)) {
                                text.append(getTipText(tip))
                                        .append(" ... uspesne ulozeny<br>");
                            }
                        }
                        // nespracovane tipy
                        if (nespracovaneTipy.get(login) != null) {
                            for (TipNaZapasDto tip : nespracovaneTipy.get(login)) {
                                text.append(getTipText(tip))
                                        .append(" ... nepodarilo sa ulozit<br>");
                            }
                        }
                        // a odosleme mail
                        mailService.sendMail(pouzivatel.getMail(),
                                "info o spracovani tipov na zapas",
                                text.toString(),
                                null,
                                null,
                                null);
                    }
                }
            }
        } catch (Exception e) {
            logger.error("nepodarilo sa nacitat maily,", e);
        }
        logger.debug("jobRun() ... ended");
    }

    /**
     * Naformatuje datum.
     *
     * @param datum datum
     * @return text
     */
    private synchronized String getNaformatovanyDatum(Date datum) {
        if (datum == null) {
            return "";
        }

        try {
            return formatDatumu.format(datum);
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Vyskladat text zo zadaneho tipu.
     *
     * @param tip zadany tip
     * @return text
     */
    private String getTipText(TipNaZapasDto tip) {
        return String.format(ParseUtils.getTipCezMailTemplate(),
                getNaformatovanyDatum(tip.getZapas().getDatum()),
                tip.getZapas().getDomaci().getKod(),
                tip.getGolyDomaci(),
                tip.getGolyHostia(),
                tip.getZapas().getHostia().getKod());
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }

    public void setTipNaZapasService(TipNaZapasService tipNaZapasService) {
        this.tipNaZapasService = tipNaZapasService;
    }
}
