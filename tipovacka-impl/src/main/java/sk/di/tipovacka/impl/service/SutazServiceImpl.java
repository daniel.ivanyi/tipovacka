package sk.di.tipovacka.impl.service;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.db.dao.SutazDao;
import sk.di.tipovacka.api.db.Sutaz;
import sk.di.tipovacka.api.dto.SutazDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.service.SutazService;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementacia service.
 *
 * User: Dano
 * Date: 10.5.2013
 */
public class SutazServiceImpl implements SutazService {

    private final Logger logger = Logger.getLogger(getClass());

    private SutazDao sutazDao;

    @Override
    public Long addSutaz(String autor, SutazDto sutaz) {
        logger.info("addSutaz(autor=" + autor + ", sutaz=" + sutaz + ")");
        Assert.notNull(sutaz, "sutaz nemoze byt null");

        // overime, ci zadany nazov je jedinecny
        if (!sutazDao.getSutazPodlaNazvu(sutaz.getNazov(), sutaz.getSportTyp(), true).isEmpty()) {
            logger.error("existuje sutaz s rovnakym nazovom");
            throw new IllegalArgumentException("existuje sutaz s rovnakym nazovom");
        }

        Sutaz s = new Sutaz();
        s.setNazov(sutaz.getNazov());
        s.setSportTyp(sutaz.getSportTyp());
        s.setSutazTyp(sutaz.getSutazTyp());
        Long sutazId = sutazDao.save(s);

        logger.info("sutaz[id=" + sutazId+ "] bola zalozena");

        return sutazId;
    }

    @Override
    public void editSutaz(String autor, SutazDto sutaz) {
        logger.info("editSutaz(autor=" + autor + ", sutaz=" + sutaz + ")");
        Assert.notNull(sutaz, "sutaz nemoze byt null");
        Assert.notNull(sutaz.getId(), "sutazId nemoze byt null");

        // overime, ci zadany nazov je jedinecny
        List<Sutaz> sutazPodlaNazvu = sutazDao.getSutazPodlaNazvu(sutaz.getNazov(), sutaz.getSportTyp(), true);
        if (!sutazPodlaNazvu.isEmpty()
                && !sutazPodlaNazvu.get(0).getId().equals(sutaz.getId())) {
            logger.error("existuje sutaz s rovnakym nazovom");
            throw new IllegalArgumentException("existuje sutaz s rovnakym nazovom");
        }

        Sutaz s = sutazDao.get(sutaz.getId());
        if (s == null) {
            logger.error("sutaz[id=" + sutaz.getId() + "] nebola najdena");
            throw new IllegalArgumentException("sutaz[id=" + sutaz.getId() + "] nebola najdena");
        }
        // typ sportu sa nemeni
        s.setNazov(sutaz.getNazov());
        s.setStatus(sutaz.getStatus());
        s.setSportTyp(sutaz.getSportTyp());
        s.setSutazTyp(sutaz.getSutazTyp());

        sutazDao.saveOrUpdate(s);

        logger.info("sutaz[id=" + sutaz.getId() + "] bola zmenena");
    }

    @Override
    public void removeSutaz(String autor, Long sutazId) {
        logger.info("removeSutaz(autor=" + autor + ", sutazId=" + sutazId + ")");
        Assert.notNull(sutazId, "sutazId nemoze byt null");

        // overime, ci podla zadaneho identifikatora najdeme sutaz
        Sutaz s = sutazDao.get(sutazId);
        if (s == null) {
            logger.error("sutaz[id=" + sutazId + "] nebola najdena");
            throw new IllegalArgumentException("sutaz[id=" + sutazId + "] nebola najdena");
        }

        // nastavime stav sutaze
        s.setStatus(EntityStatusEnum.NEAKTIVNY);
        sutazDao.saveOrUpdate(s);

        logger.info("sutaz[id=" + sutazId + "] bola deaktivovana");
    }

    @Override
    public SutazDto getSutaz(Long sutazId) {
        logger.info("getSutaz(sutazId=" + sutazId + ")");
        Assert.notNull(sutazId, "sutazId nemoze byt null");

        return SutazDto.create(sutazDao.get(sutazId));
    }

    @Override
    public List<SutazDto> getSutaze() {
        List<SutazDto> sutaze = new ArrayList<>();

        for (Sutaz s : sutazDao.findAll()) {
            sutaze.add(SutazDto.create(s));
        }

        return sutaze;
    }

    public void setSutazDao(SutazDao sutazDao) {
        this.sutazDao = sutazDao;
    }
}
