package sk.tempest.sfz.support.dao;

import org.hibernate.cfg.ImprovedNamingStrategy;

/**
 * Hibernate NamingStrategy pre konvencie projektu
 */
public class NamingStrategy extends ImprovedNamingStrategy {

    private static final long serialVersionUID = 1L;

    @Override
    public String classToTableName(String className) {
        return "t_" + super.classToTableName(className);
    }

    @Override
    public String collectionTableName(String ownerEntity, String ownerEntityTable, String associatedEntity,
            String associatedEntityTable, String propertyName) {
        String collectionTableName = super.collectionTableName(ownerEntity,
                ownerEntityTable, associatedEntity, associatedEntityTable,
                propertyName);
        if (!collectionTableName.startsWith("t_")) {
            return "t_" + collectionTableName;
        } else {
            return collectionTableName;
        }
    }

    @Override
    public String foreignKeyColumnName(String propertyName,
            String propertyEntityName, String propertyTableName,
            String referencedColumnName) {
        return referencedColumnName;
    }
}
