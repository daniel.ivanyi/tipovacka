package sk.di.tipovacka.impl.dao;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import sk.di.tipovacka.api.db.SutazRocnikSuhlas;
import sk.di.tipovacka.api.db.dao.SutazRocnikSuhlasDao;

import java.util.List;

/**
 * Implementacia dao.
 *
 * User: Dano
 * Date: 16.2.2014
 */
public class SutazRocnikSuhlasDaoImpl extends EntityDaoImpl<SutazRocnikSuhlas> implements SutazRocnikSuhlasDao {

    private final Logger logger = Logger.getLogger(SutazRocnikSuhlasDaoImpl.class);

    @Override
    @SuppressWarnings("unchecked")
    public boolean isPouzivatelSuhlas(Long pouzivatelId, Long rocnikId) {
        if (pouzivatelId == null) {
            logger.error("pouzivatelId nesmie byt prazdne");
            throw new IllegalArgumentException("pouzivatelId nesmie byt prazdne");
        }
        if (rocnikId == null) {
            logger.error("rocnikId nesmie byt prazdne");
            throw new IllegalArgumentException("rocnikId nesmie byt prazdne");
        }

        DetachedCriteria criteria = DetachedCriteria.forClass(SutazRocnikSuhlas.class)
                .add(Restrictions.eq("sutazRocnikId", rocnikId))
                .add(Restrictions.eq("pouzivatelId", pouzivatelId));

        List<SutazRocnikSuhlas> vysledok = getHibernateTemplate().findByCriteria(criteria);
        if (vysledok == null || vysledok.isEmpty()) {
            return false;
        } else if (vysledok.size() == 1) {
            return vysledok.get(0).isSuhlas();
        } else {
            logger.error("pre pouzivatela[id=" + pouzivatelId
                    + "] rocnik[" + rocnikId + "] bolo najdenych viacej ako jeden zaznam");
            return false;
        }
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SutazRocnikSuhlas> findAllByRocnik(Long rocnikId) {
        if (rocnikId == null) {
            logger.error("rocnikId nesmie byt prazdne");
            throw new IllegalArgumentException("rocnikId nesmie byt prazdne");
        }
        DetachedCriteria criteria = DetachedCriteria.forClass(SutazRocnikSuhlas.class)
                .add(Restrictions.eq("sutazRocnikId", rocnikId));

        return getHibernateTemplate().findByCriteria(criteria);
    }
}
