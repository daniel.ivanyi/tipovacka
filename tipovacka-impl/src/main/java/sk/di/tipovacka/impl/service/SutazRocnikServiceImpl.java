package sk.di.tipovacka.impl.service;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.comparator.RocnikSutazeComparator;
import sk.di.tipovacka.api.db.Muzstvo;
import sk.di.tipovacka.api.db.Sutaz;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.TipNaVitaza;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.db.dao.MuzstvoDao;
import sk.di.tipovacka.api.db.dao.SutazDao;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.dao.TipNaVitazaDao;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;
import sk.di.tipovacka.api.db.dao.ZapasDao;
import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.dto.RocnikSutazeHlavickaDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.enums.ZapasTypEnum;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.ZapasService;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Implementacia service.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public class SutazRocnikServiceImpl implements SutazRocnikService {

    private final Logger logger = Logger.getLogger(getClass());

    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");

    private SutazDao sutazDao;
    private SutazRocnikDao sutazRocnikDao;
    private ZapasDao zapasDao;
    private TipNaZapasDao tipNaZapasDao;
    private TipNaVitazaDao tipNaVitazaDao;
    private MuzstvoDao muzstvoDao;

    private ZapasService zapasService;

    @Value("${tip.zapas.body.goly}")
    private Integer bodyTipZapasGoly;
    @Value("${tip.zapas.body.vitaz}")
    private Integer bodyTipZapasVitaz;
    @Value("${tip.rocnik.body.vitaz}")
    private Integer bodyTipRocnikVitaz;

    @Override
    public Long addSutazRocnik(String autor, RocnikSutazeDto sutazRocnik) {
        logger.info("addSutazRocnik(autor=" + autor + ", sutazRocnik=...)");
        Assert.notNull(sutazRocnik, "sutazRocnik nemoze byt null");
        Assert.notNull(sutazRocnik.getSutaz(), "sutaz nemoze byt null");
        Assert.notNull(sutazRocnik.getSutaz().getId(), "sutaz.id nemoze byt null");

        // overime, ci zadana sutaz existuje
        Sutaz sutaz = sutazDao.get(sutazRocnik.getSutaz().getId());
        if (sutaz == null) {
            logger.error("sutaz[id=" + sutazRocnik.getSutaz().getId() + "] nebola najdena");
            throw new IllegalArgumentException("sutaz[id=" + sutazRocnik.getSutaz().getId() + "] nebola najdena");
        } else if (EntityStatusEnum.NEAKTIVNY.equals(sutaz.getStatus())) {
            logger.error("sutaz[id=" + sutazRocnik.getSutaz().getId() + "] nie je aktivna");
            throw new IllegalArgumentException("sutaz[id=" + sutazRocnik.getSutaz().getId() + "] nie je aktivna");
        }
        // overime, ci pre zadanu sutaz, uz neexistuje zadany rocnik
        SutazRocnik sr = sutazRocnikDao
                .getRocnikSutazePodlaSutazeRoka(sutazRocnik.getSutaz().getId(), sutazRocnik.getRok());
        if (sr != null) {
            logger.error("pre zadanu sutaz, existuje zadany rocnik");
            throw new IllegalArgumentException("pre zadanu sutaz, existuje zadany rocnik");
        }

        SutazRocnik rocnik = new SutazRocnik();
        rocnik.setSutazId(sutazRocnik.getSutaz().getId());
        rocnik.setSutaz(sutaz);
        rocnik.setRok(sutazRocnik.getRok());
        rocnik.setStatistikaUrl(sutazRocnik.getStatistikaUrl());
        rocnik.setAktualny(sutazRocnik.isAktualny());

        Long sutazRocnikId = sutazRocnikDao.save(rocnik);

        logger.info("rocnik sutaze[id=" + sutazRocnikId + "] bola pridana");

        return sutazRocnikId;
    }

    @Override
    public void editSutazRocnik(String autor, RocnikSutazeDto sutazRocnik) {
        logger.info("editSutazRocnik(autor=" + autor + ", sutazRocnik=...)");
        Assert.notNull(sutazRocnik, "sutazRocnik nemoze byt null");
        Assert.notNull(sutazRocnik.getId(), "sutazRocnik.id nemoze byt null");

        SutazRocnik sr = sutazRocnikDao.get(sutazRocnik.getId());
        if (sr == null) {
            logger.error("rocnik sutaze[id=" + sutazRocnik.getId() + "] nebol najdeny");
            throw new IllegalArgumentException("rocnik sutaze[id=" + sutazRocnik.getId() + "] nebol najdeny");
        }

        sr.setSutazId(sutazRocnik.getSutazId());
        sr.setRok(sutazRocnik.getRok());
        sr.setStatistikaUrl(sutazRocnik.getStatistikaUrl());
        sr.setStatus(sutazRocnik.getStatus());
        sr.setAktualizacia(sutazRocnik.getAktualizacia());
        sr.setAktualny(sutazRocnik.isAktualny());
        // ak menime vitaza
        if (sutazRocnik.getVitaz() != null) {
            if (sr.getVitaz() != null && !sr.getVitaz().getKodMuzstva().equals(sutazRocnik.getVitaz().getKodMuzstva())) {
                setVitazRocnika(autor, sutazRocnik.getId(), sutazRocnik.getVitaz().getId());
            }
        }

        sutazRocnikDao.saveOrUpdate(sr);

        logger.info("sutazRocnik[id=" + sutazRocnik.getId() + "] bol zmeneny");
    }

    @Override
    public RocnikSutazeDto getRocnikSutaze(Long sutazRocnikId) {
        logger.info("getRocnikSutaze(sutazRocnikId=" + sutazRocnikId + ")");
        Assert.notNull(sutazRocnikId, "sutazRocnikId nemoze byt null");

        return RocnikSutazeDto.create(sutazRocnikDao.get(sutazRocnikId));
    }

    @Override
    public List<RocnikSutazeDto> getAktualnyRocnikSutaze() {
        logger.trace("getAktualnyRocnikSutaze()");

        List<RocnikSutazeDto> rocniky = new ArrayList<>();
        for (SutazRocnik rocnik : sutazRocnikDao.getAktualnyRocnikSutaze()) {
            rocniky.add(RocnikSutazeDto.create(rocnik));
        }

        return rocniky;
    }

    @Override
    public List<RocnikSutazeDto> getRocnikySutaze() {
        logger.info("getRocnikySutaze()");

        List<RocnikSutazeDto> rocniky = new ArrayList<>();
        for (SutazRocnik rocnik : sutazRocnikDao.findAll()) {
            rocniky.add(RocnikSutazeDto.create(rocnik));
        }

        Collections.sort(rocniky, new RocnikSutazeComparator());

        return rocniky;
    }

    @Override
    public void removeRocnik(String autor, Long sutazRocnikId) {
        logger.info("removeRocnik(autor=" + autor + ", sutazRocnikId=" + sutazRocnikId + ")");
        Assert.notNull(sutazRocnikId, "sutazRocnikId nemoze byt null");

        // overime, ci rocnik sutaze existuje
        SutazRocnik sr = sutazRocnikDao.get(sutazRocnikId);
        if (sr == null) {
            logger.error("rocnik sutaze[id=" + sutazRocnikId + "] nebol najdeny");
            throw new IllegalArgumentException("rocnik sutaze[id=" + sutazRocnikId + "] nebol najdeny");
        }

        // nastavime neaktivnost
        sr.setStatus(EntityStatusEnum.NEAKTIVNY);
        sutazRocnikDao.saveOrUpdate(sr);

        logger.info("rocnikSutaze[id=" + sutazRocnikId + "] bol deaktivovany");
    }

    @Override
    public void inicializujRocnikSutaze(String autor, Long sutazRocnikId) throws IOException {
        logger.info("inicializujRocnikSutaze(autor=" + autor + ", sutazRocnikId=" + sutazRocnikId + ")");
        Assert.notNull(sutazRocnikId, "sutazRocnikId nemoze byt null");

        SutazRocnik rocnik = sutazRocnikDao.get(sutazRocnikId);
        if (rocnik == null) {
            logger.error("rocnik[id=" + sutazRocnikId + "] nebol najdene");
            throw new IllegalArgumentException("rocnik[id=" + sutazRocnikId + "] nebol najdene");
        }

        logger.info("statistika=" + rocnik.getStatistikaUrl());

        Set<Long> ids = new HashSet<>();
        List<ZapasDto> zapasReport = zapasService.getZapasReport(rocnik.getStatistikaUrl(),
                rocnik.getSutaz().getSportTyp(),
                rocnik.getSutaz().getSutazTyp(),
                true);

        if (!zapasReport.isEmpty()) {
            logger.info("nacitane zapasy[" + zapasReport.size() + "]");

            // ak bol dany rocnik v test mode, vymazeme vsetky doterajsie zapasy a tipy
            sutazRocnikDao.clearTipy(sutazRocnikId);
            logger.info("rocnik[id=" + sutazRocnikId + "] bol vycisteny");

            try {
                for (ZapasDto zapas : zapasReport) {
                    Zapas z = new Zapas();
                    z.setSutazRocnikId(sutazRocnikId);
                    z.setSutazRocnik(rocnik);
                    z.setSkupina(zapas.getSkupina());
                    z.setDatum(zapas.getDatum());
                    z.setDomaciId(zapas.getDomaciId());
                    if (z.getDomaciId() != null) {
                        z.setDomaci(muzstvoDao.get(z.getDomaciId()));
                    }
                    z.setHostiaId(zapas.getHostiaId());
                    if (z.getHostiaId() != null) {
                        z.setHostia(muzstvoDao.get(z.getHostiaId()));
                    }
                    z.setGolyDomaci(zapas.getGolyDomaci());
                    z.setGolyHostia(zapas.getGolyHostia());
                    z.setPriebeh(zapas.getPriebeh());
                    z.setKoniecZapasuTyp(zapas.getKoniecZapasuTyp());
                    z.setTypZapasu(zapas.getTypZapasu());

                    ids.add(zapasDao.save(z));
                }
            } catch (Exception e) {
                logger.error("nepodarilo sa inicializovat rocnik[id=" + sutazRocnikId + "]", e);
                throw new IllegalArgumentException(e);
            }
        } else {
            logger.warn("neboli nacitane ziadne zapasy");
        }
        logger.info("do rocnika sutaze[id=" + sutazRocnikId + "] bolo pridanych " + ids.size() + " zapasov.");

        // este zmenim aj rocnik sutaze
        rocnik.setInit(true);
        rocnik.setAktualizacia(new Date());
        sutazRocnikDao.saveOrUpdate(rocnik);
        logger.info("rocnik sutaze[id=" + sutazRocnikId + ", init=" + rocnik.isInit() + "] bol inicializovany");
    }

    @Override
    public void updatePriebehRocnikaSutaze(String autor, Long sutazRocnikId) throws IOException {
        logger.info("updatePriebehRocnikaSutaze(autor=" + autor + ", sutazRocnikId=" + sutazRocnikId + ")");

        if (sutazRocnikId == null) {
            logger.error("sutazRocnikId nemoze byt null");
            throw new IllegalArgumentException("sutazRocnikId nemoze byt null");
        }

        RocnikSutazeDto rocnik = getRocnikSutaze(sutazRocnikId);
        if (rocnik == null) {
            logger.error("rocnik[id=" + sutazRocnikId + "] nebol najdene");
            throw new IllegalArgumentException("rocnik[id=" + sutazRocnikId + "] nebol najdene");
        }

        // zapasy dnes
        ZapasKriteria kriteria = new ZapasKriteria();
        kriteria.setSutazRocnikId(sutazRocnikId);
        List<ZapasDto> zapasy = zapasService.findZapasy(kriteria);

        List<ZapasDto> zapasyReport = null;
        try {
            zapasyReport = zapasService.getZapasReport(rocnik.getStatistikaUrl(), rocnik.getSutaz().getSportTyp(), rocnik.getSutaz().getSutazTyp(), false);
        } catch (Exception e) {
            logger.error("nepodarilo sa nacitat statistiku", e);
        }

        // zistime ci dany rocnik nie je v test mode,
        // ak ano, posunieme datumy podla zaciatku testovania
        if (rocnik.isTest()) {
            logger.info("rocnik[id=" + rocnik.getId() + ", rok=" + rocnik.getRok() + "] je v test mode");
            zapasService.rollDatumZapasov(zapasyReport, rocnik.getZaciatokRocnikaTest());
        }

        // prehodime do map
        Map<String, ZapasDto> zapasyMapa = getMapaZapasov(zapasy);
        Map<String, ZapasDto> zapasyReportMapa = getMapaZapasov(zapasyReport);
        SutazRocnik sutazRocnik = sutazRocnikDao.get(sutazRocnikId);
        // a doplnime vysledky
        for (String key : zapasyReportMapa.keySet()) {
            ZapasDto zapasReport = zapasyReportMapa.get(key);
            // identifikator zapasu, s ktorym pracujeme
            Long zapasId;
            // overime, ci dany zapas je uz ulozeny
            if (zapasyMapa.get(key) == null) {
                logger.debug("zapas[" + key + "] nebol najdeny v DB");
                // overime ci pre dany termin existuje zapas s nenasetovanymi supermi
                Zapas zapasBezSuperov = findZapasBezSuperov(zapasReport.getDatum());
                if (zapasBezSuperov == null) {
                    logger.debug("pre termin[" + sdf.format(zapasReport.getDatum()) + "] nie je zadany ani zapas bez superov, pridame novy zapas");
                    Zapas z = new Zapas();
                    z.setSutazRocnikId(sutazRocnikId);
                    z.setSutazRocnik(sutazRocnik);
                    z.setSkupina(zapasReport.getSkupina());
                    z.setDatum(zapasReport.getDatum());
                    z.setDomaciId(zapasReport.getDomaciId());
                    if (z.getDomaciId() != null) {
                        z.setDomaci(muzstvoDao.get(z.getDomaciId()));
                    }
                    z.setHostiaId(zapasReport.getHostiaId());
                    if (z.getHostiaId() != null) {
                        z.setHostia(muzstvoDao.get(z.getHostiaId()));
                    }
                    z.setGolyDomaci(zapasReport.getGolyDomaci());
                    z.setGolyHostia(zapasReport.getGolyHostia());
                    z.setPriebeh(zapasReport.getPriebeh());
                    z.setKoniecZapasuTyp(zapasReport.getKoniecZapasuTyp());
                    z.setTypZapasu(zapasReport.getTypZapasu());
                    zapasId = zapasDao.save(z);
                    logger.info("zapas[" + zapasId + "] bol ulozeny");
                } else {
                    logger.debug("pre termin[" + sdf.format(zapasReport.getDatum()) + "] je zadany zapas bez superov, doplnime muzstva");
                    zapasBezSuperov.setDomaciId(zapasReport.getDomaciId());
                    if (zapasBezSuperov.getDomaciId() != null) {
                        zapasBezSuperov.setDomaci(muzstvoDao.get(zapasBezSuperov.getDomaciId()));
                    }
                    zapasBezSuperov.setHostiaId(zapasReport.getHostiaId());
                    if (zapasBezSuperov.getHostiaId() != null) {
                        zapasBezSuperov.setHostia(muzstvoDao.get(zapasBezSuperov.getHostiaId()));
                    }
                    zapasId = zapasBezSuperov.getId();
                    zapasDao.saveOrUpdate(zapasBezSuperov);
                    logger.info("zapas[" + zapasId + "] bol zmeneny");
                }
            } else {
                zapasId = zapasyMapa.get(key).getId();
            }
            logger.debug("pracujem so zapasom[key=" + key
                    + ", id=" + zapasId
                    + ", koniec[typ=" + zapasReport.getKoniecZapasuTyp()
                    + ", priznak=" + zapasReport.getKoniecZapasuTyp().isKoniecZapasu()
                    + "]]");
            // ak zapas skoncil, updatneme
            if (zapasReport.getKoniecZapasuTyp().isKoniecZapasu()) {
                Zapas zapas = zapasDao.get(zapasId);
                zapas.setGolyDomaci(zapasReport.getGolyDomaci());
                zapas.setGolyHostia(zapasReport.getGolyHostia());
                zapas.setPriebeh(zapasReport.getPriebeh());
                zapas.setKoniecZapasuTyp(zapasReport.getKoniecZapasuTyp());
                zapas.setHighlightsUrl(zapasReport.getHighlightsUrl());

                zapasDao.saveOrUpdate(zapas);
                logger.info("zapas[" + zapas + "] bol zmeneny");

                // a spracujeme tipy na zapas, ked bol zapas odohraty
                if (zapas.getKoniecZapasuTyp().isKoniecZapasu()) {
                    List<TipNaZapas> tipy = tipNaZapasDao.getTipyNaZapas(zapas.getId());
                    logger.debug("zapas[" + key + "] skoncil spracujeme tipy[" + tipy.size() + "] na zapas");
                    for (TipNaZapas t : tipy) {
                        // nasetujeme priznaky, ci boli spravne tipnute goly
                        t.setBodyZaGolyDomaci(t.getGolyDomaci() == zapas.getGolyDomaci()
                                ? bodyTipZapasGoly : 0);
                        t.setBodyZaGolyHostia(t.getGolyHostia() == zapas.getGolyHostia()
                                ? bodyTipZapasGoly : 0);
                        // ak je remiza, nasetujeme podla tipnutia rovnosti golov
                        if (zapasReport.isRemiza()) {
                            t.setBodyZaVitaza(t.getGolyDomaci() == t.getGolyHostia()
                                    ? bodyTipZapasVitaz : 0);
                            // inak podla vitaza nasetujeme body za vitazstvo
                        } else {
                            t.setBodyZaVitaza(zapasReport.getVitaz().getKodMuzstva().equals(t.getVitaz().getKodMuzstva())
                                    ? bodyTipZapasVitaz : 0);
                        }

                        tipNaZapasDao.saveOrUpdate(t);
                        logger.info("tip[" + t + "] bol zmeneny");
                    }

                    // ak je spracovavany posledny zapas (hra o zlato), spracujeme aj tipy na vitaza
                    if (ZapasTypEnum.GMG.equals(zapas.getTypZapasu())) {
                        if (rocnik.getPoslednyZapas().getVitaz() != null) {
                            logger.info("rocnik sutaze ma vitaza[" + rocnik.getPoslednyZapas().getVitaz() + "]");
                            rocnik.setVitaz(rocnik.getPoslednyZapas().getVitaz());
                            rocnik.setVitazId(rocnik.getPoslednyZapas().getVitaz().getId());
                            setVitazRocnika(autor, sutazRocnikId, rocnik.getPoslednyZapas().getVitaz().getId());
                        }
                    }
                }
            }
        }

//        // este overime, ci mame nejake zapasy, ktore nemaju nasetovanych superov
//        List<Zapas> zapasyBezSuperov = zapasDao.getZapasyBezSuperov(sutazRocnikId);
//        if (!zapasyBezSuperov.isEmpty()) {
//            // ak mame zapasy bez superov, prehodime si nacitanu statistiku do mapy podla poradia
//            Map<String, ZapasDto> mapaZapasovPodlaDatumu = getMapaZapasovPodlaDatumu(zapasyReport);
//            for (Zapas z : zapasyBezSuperov) {
//                // podla datumu zapasu bez superov si dohladame id zapasu, ktory je stiahnuty z netu a
//                // aktualizujeme nim zapas bez superov
//                String idZapasu = null;
//                for (String key : mapaZapasovPodlaDatumu.keySet()) {
//                    if (key.contains(sdf.format(z.getDatum()))) {
//                        idZapasu = key;
//                        break;
//                    }
//                }
//                // ak sme nasli zapas podla casu, spracujeme
//                if (idZapasu != null) {
//                    // vytiahneme zapas podla poradia zo statistiky
//                    ZapasDto zap = mapaZapasovPodlaDatumu.get(idZapasu);
//                    // a ak ma nasetovanych superov doplnime a ulozime
//                    if (zap != null
//                            && !"TBD".equals(zap.getDomaci().getKod())
//                            && !"TBD".equals(zap.getHostia().getKod())) {
//                        z.setDomaciId(zap.getDomaciId());
//                        z.setDomaci(muzstvoDao.get(zap.getDomaciId()));
//                        z.setHostiaId(zap.getHostiaId());
//                        z.setHostia(muzstvoDao.get(zap.getHostiaId()));
//
//                        zapasDao.saveOrUpdate(z);
//                        logger.info("zapasu[id=" + z.getId() + ", datum=" + sdf.format(z.getDatum()) + "] " +
//                                "boli doplneny supery[domaci=" + z.getDomaci().getKod() + ", hostia=" + z.getHostia().getKod() + "]");
//                    }
//
//                    // po spracovanim vyhodime z mapy
//                    mapaZapasovPodlaDatumu.remove(idZapasu);
//                }
//            }
//        }

        // zaznamename, ze sme volali update priebehu rocnika
        Date aktualizacia = new Date();
        rocnik.setAktualizacia(aktualizacia);
        logger.info("priebeh rocnika[" + sutazRocnikId + "] bol aktualizovany[" + aktualizacia + "]");
        editSutazRocnik(autor, rocnik);
    }

    @Override
    public void setVitazRocnika(String autor, Long rocnikId, Long vitazId) {
        logger.info("setVitazRocnika(autor=" + autor
                + ", rocnikId=" + rocnikId
                + ", vitazId=" + vitazId
                + ")");
        if (StringUtils.isEmpty(autor)) {
            logger.error("autor zmeny nemoze byt prazdny");
            throw new IllegalArgumentException("autor zmeny nemoze byt prazdny");
        }
        if (rocnikId == null) {
            logger.error("identifikator rocnika nemoze byt prazdny");
            throw new IllegalArgumentException("identifikator rocnika nemoze byt prazdny");
        }
        if (vitazId == null) {
            logger.error("identifikator muzstva nemoze byt prazdny");
            throw new IllegalArgumentException("identifikator muzstva nemoze byt prazdny");
        }

        SutazRocnik rocnik = sutazRocnikDao.get(rocnikId);
        if (rocnik == null) {
            logger.error("rocnik[id=" + rocnikId + "] nebol najdeny");
            throw new IllegalArgumentException("rocnik[id=" + rocnikId + "] nebol najdeny");
        }

        Muzstvo vitaz = muzstvoDao.get(vitazId);
        if (vitaz == null) {
            logger.error("muzstvo[id=" + vitazId + "] nebolo najdene");
            throw new IllegalArgumentException("muzstvo[id=" + vitazId + "] nebolo najdene");
        }

        // nastavit vitaza mozeme len ked, je rocnik ukonceny
        RocnikSutazeDto rocnikSutaze = RocnikSutazeDto.create(rocnik);
        if (rocnikSutaze.isKoniecRocnika()) {
            // prebehneme vsetky tipy na vitaza a nasetujeme body
            List<TipNaVitaza> tipyVitaz = tipNaVitazaDao.getTipyNaVitaza(rocnik.getId());
            for (TipNaVitaza t : tipyVitaz) {
                t.setBody(t.getMuzstvo().getKodMuzstva().equals(vitaz.getKodMuzstva())
                        ? (rocnik.getZapasy().size() < bodyTipRocnikVitaz
                        ? rocnik.getZapasy().size()
                        : bodyTipRocnikVitaz)
                        : 0);
                // ak ziskal tiper ZERO, pokracujeme s dalsim tiperom
                if (t.getBody() == 0) {
                    continue;
                }
                // upravime vysledny pocet bodov podla toho, kedy bol zadany tip na vitaza
                if (t.getDatum().after(rocnikSutaze.getPrvyZapas().getDatum())) {
                    // ak bol v zakladnej casti, odratame polovicu
                    int body = t.getBody() / 2;
                    // ak bol tip zadany po odohrati zakladnej casti, odratame dalsiu polovicu
                    Date koniecZakladnejCasti = rocnikSutaze.getKoniecCasti(ZapasTypEnum.PRE);
                    if (koniecZakladnejCasti == null) {
                        koniecZakladnejCasti = rocnikSutaze.getKoniecCasti(ZapasTypEnum.PR);
                    }
                    if (t.getDatum().after(koniecZakladnejCasti)) {
                        body = body / 2;
                        // ak bol tip zadany vo stvrtfinale, odratame dalsiu polovicu
                        if (t.getDatum().after(rocnikSutaze.getKoniecCasti(ZapasTypEnum.QF))) {
                            body = body / 2;
                            // ak bol zadany po skonceni semifinale, 1 bod
                            if (t.getDatum().after((rocnikSutaze.getKoniecCasti(ZapasTypEnum.SF)))) {
                                body = 1;
                            }
                        }
                    }
                    t.setBody(body);
                }

                tipNaVitazaDao.saveOrUpdate(t);
            }
            logger.info("rocnik[" + rocnikId + "] tipy na vitaza boli spracovane");

            // zmenime a ulozime rocnik
            rocnik.setVitaz(vitaz);
            rocnik.setVitazId(vitazId);
            sutazRocnikDao.saveOrUpdate(rocnik);
            logger.info("rocnik[" + rocnikId + "] vitaz rocnika bol nasetovany");

        } else {
            logger.warn("rocnik[" + rocnikId + "] este neskoncil");
            throw new IllegalArgumentException("rocnik[" + rocnikId + "] este neskoncil");
        }
    }

    @Override
    public List<RocnikSutazeHlavickaDto> getUkonceneRocniky() {
        logger.info("getUkonceneRocniky()");

        List<RocnikSutazeHlavickaDto> rocniky = new ArrayList<>();
        for (SutazRocnik s : sutazRocnikDao.getUkonceneRocniky()) {
            rocniky.add(RocnikSutazeHlavickaDto.create(s));
        }

        return rocniky;
    }

    /**
     * Vrati zapas bez super pre dany termin.
     *
     * @param datum
     * @return
     */
    private Zapas findZapasBezSuperov(Date datum) {
        ZapasKriteria zk = new ZapasKriteria();
        zk.setDatumOdPresne(datum);
        zk.setDatumDoPresne(datum);
        List<Zapas> bezSuperov = zapasDao.findByKriteria(zk);
        for (Zapas z : bezSuperov) {
            // kod nenadefinovaneho muzstva moze nadobudat hodnoty TBD, W\d{2}, L\d{2}
            String pattern = "TBD|W\\d{2}|L\\d{2}";
            if (z.getDomaci().getKod().matches(pattern)
                    || z.getHostia().getKod().matches(pattern)) {
                return z;
            }
        }

        return null;
    }

    /**
     * Vrati mapu zapasov, kde klucik je jednoznacny identifikator.
     *
     * @param zapasy zoznam zapasov
     * @return mapa zapasov
     */
    private Map<String, ZapasDto> getMapaZapasov(List<ZapasDto> zapasy) {
        Map<String, ZapasDto> mapa = new HashMap<>();

        if (zapasy != null) {
            for (ZapasDto z : zapasy) {
                // nasetujem iba tie zapasy, kde su jasni superi
                if (z.getDomaci() != null && z.getHostia() != null) {
                    mapa.put(getZapasyMapaKey(z), z);
                }
            }
        }

        return mapa;
    }

    /**
     * Vrati mapu zapasov podla datumu.
     *
     * @param zapasy zoznam zapasov
     * @return mapa zapasov
     */
    private Map<String, ZapasDto> getMapaZapasovPodlaDatumu(List<ZapasDto> zapasy) {
        Map<String, ZapasDto> mapa = new HashMap<>();
        if (!zapasy.isEmpty()) {
            for (ZapasDto z : zapasy) {
                mapa.put(getZapasyMapaKey(z), z);
            }
        }

        return mapa;
    }

    /**
     * Vrati jednoznacny identifikator zapasu.
     *
     * @param zapas zapas
     * @return identifikator
     */
    private String getZapasyMapaKey(ZapasDto zapas) {
        if (zapas == null) {
            return null;
        }

        return sdf.format(zapas.getDatum())
                + "_" + (zapas.getDomaci() == null ? "null" : zapas.getDomaci().getKodMuzstva())
                + "_" + (zapas.getHostia() == null ? "null" : zapas.getHostia().getKodMuzstva());
    }

    public void setSutazRocnikDao(SutazRocnikDao sutazRocnikDao) {
        this.sutazRocnikDao = sutazRocnikDao;
    }

    public void setSutazDao(SutazDao sutazDao) {
        this.sutazDao = sutazDao;
    }

    public void setZapasDao(ZapasDao zapasDao) {
        this.zapasDao = zapasDao;
    }

    public void setTipNaZapasDao(TipNaZapasDao tipNaZapasDao) {
        this.tipNaZapasDao = tipNaZapasDao;
    }

    public void setTipNaVitazaDao(TipNaVitazaDao tipNaVitazaDao) {
        this.tipNaVitazaDao = tipNaVitazaDao;
    }

    public void setMuzstvoDao(MuzstvoDao muzstvoDao) {
        this.muzstvoDao = muzstvoDao;
    }

    public void setZapasService(ZapasService zapasService) {
        this.zapasService = zapasService;
    }
}
