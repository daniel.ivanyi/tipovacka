package sk.di.tipovacka.impl.util;

import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.text.StrBuilder;
import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import sk.di.tipovacka.api.enums.ZapasTypEnum;

import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;

/**
 * Utilitka pre rozne parsovacky
 * <p/>
 * User: Dano
 * Date: 12.5.2013
 */
public class ParseUtils {

    private static final Logger logger = Logger.getLogger(ParseUtils.class);

    private static List<String> isoKodKrajiny = new ArrayList<>();

    public static Date parseDateFromDateTextGmt(String datumText) {
        Date datum = null;
        // upravime dvojnasobne medzery
        datumText = datumText.replaceAll("  ", " ").trim();
        // zistime casovu zonu z datumu
        String casovaZona = StringUtils.substringAfterLast(datumText, " ");
        // odstrihneme casovu zonu z datumu
        datumText = StringUtils.remove(datumText, casovaZona).trim();

        // podporovane formaty datumu
        String[] datePattens = new String[]{
                "dd.MM.yyyy HH:mm",
                "dd.MM.yyyy E HH:mm",
                "dd MMM yyyy, E HH:mm",
                "dd.MMM.yyyy E HH:mm"
        };

        // prebehneme vsetky datumove formaty, ak vyparsujeme datum, koniec
        for (String datePattern : datePattens) {
            // spracuvavame datum s us textaciou
            SimpleDateFormat sdf = new SimpleDateFormat(datePattern, Locale.US);
            // a nastavime spravnu casovu zonu
            sdf.setTimeZone(TimeZone.getTimeZone(casovaZona));
            try {
                datum = sdf.parse(datumText);
                break;
            } catch (ParseException e) {
                // logger.error("nemozem parsovat datum[" + datumText + "] podla patternu[" + datePattern + "]", e);
            }
        }

        if (datum == null) {
            logger.warn("parsovany datum[" + datumText + "] nesplna ziaden predpisany format datumu[" + Arrays.toString(datePattens) + "]");
        }

        return datum;
    }

    /**
     * Vrati zadany text bez interpunkcie
     *
     * @param text zadany text
     * @return  osetreny text
     */
    public static String getNoInterpunktString(String text) {
        if (StringUtils.isEmpty(text)) {
            return "";
        }

        String interpunkcia    = "àáâãäåāăąæççćĉċčďđēĕėęěèéêëìíîïðñòóôõöøþĝğġģĥħĩīĭįıĳĵķĸĺļľŀłńņňŉŋōŏőœŕŗřśŝşšţťŧũūŭůűųùúûüŵýÿŷźżž";
        String nonInterpunkcia = "aaaaaaaaaaccccccddeeeeeeeeeiiiinnoooooopgggghhiiiiiijkklllllnnnnnoooorrrsssstttuuuuuuuuuuwyyyzzz";

        // upravymi zadany text
        text = text.replace("\\'", "")
                .replace("-", "")
                .replace(".", "")
                .replace(",", "")
                .replace(" ", "_")
                .toLowerCase();
        // vysledny string
        StrBuilder vysledok = new StrBuilder(text);
        // rozbijeme zadany string na znaky a overime, ci je medzi interpunkciou
        for (int i = 0; i < text.length(); i++) {
            int interpunkcnyZnak = interpunkcia.indexOf(text.charAt(i));
            if (interpunkcnyZnak != -1) {
                vysledok.setCharAt(i, nonInterpunkcia.charAt(interpunkcnyZnak));
            }
        }

        return vysledok.toString();
    }

    /**
     * Sablona pre vyskladanie tipu do mailu.
     *
     * @return sablona
     */
    public static String getTipCezMailTemplate() {
        // tip [12.02.2014] FC Barcelona (1 - 2) Real Madrid
        return "tip [%s] %s (%d - %d) %s";
    }

    /**
     * Vrati regex pattern pre tip zadanu s kodom muzstva,
     * napr. tip [01.07.2016 21:00] WAL (2 - 1) BEL
     *
     * @return pattern
     */
    public static String getTipCezMailKodMuzstvaPatter() {
        return ".*tip \\[(\\d{2}.\\d{2}.\\d{4} \\d{2}:\\d{2})\\] ([A-Z]{3}) \\((\\d) - (\\d)\\) ([A-Z]{3}).*";
    }

    /**
     * Vrati regex pattern pre tip zadanu s kodom muzstva,
     * napr. tip [01.07.2016 21:00] WAL (2 - 1) BEL
     *
     * @return pattern
     */
    public static String getTipCezMailnazovMuzstvaPatter() {
        return ".*tip \\[(\\d{2}.\\d{2}.\\d{4} \\d{2}:\\d{2})\\] (\\w) \\((\\d) - (\\d)\\) (\\w).*";
    }

    /**
     * Vyparsuje typ zapasu podla datumu kedy sa zapas hra.
     *
     * @param date datum zapasu
     * @return typ zapasu
     */
    public static ZapasTypEnum parseTypZapasu(Date date) {
        if (date == null) {
            throw new IllegalArgumentException("datum zapasu musi byt zadany");
        }

        ZapasTypEnum zapasTyp = ZapasTypEnum.PR;
        Calendar osemfinale = Calendar.getInstance();
        osemfinale.set(2018, Calendar.JUNE, 29);
        if (date.after(new Timestamp(osemfinale.getTimeInMillis()))) {
            zapasTyp = ZapasTypEnum.EF;

            Calendar stvrtfinale = Calendar.getInstance();
            stvrtfinale.set(2018, Calendar.JULY, 4);
            if (date.after(new Timestamp(stvrtfinale.getTimeInMillis()))) {
                zapasTyp = ZapasTypEnum.QF;

                Calendar semifinale = Calendar.getInstance();
                semifinale.set(2018, Calendar.JULY, 8);
                if (date.after(new Timestamp(semifinale.getTimeInMillis()))) {
                    zapasTyp = ZapasTypEnum.SF;

                    Calendar bronz = Calendar.getInstance();
                    bronz.set(2018, Calendar.JULY, 12);
                    if (date.after(new Timestamp(bronz.getTimeInMillis()))) {
                        zapasTyp = ZapasTypEnum.BMG;

                        Calendar zlato = Calendar.getInstance();
                        zlato.set(2018, Calendar.JULY, 14, 23, 59, 59);
                        if (date.after(new Timestamp(zlato.getTimeInMillis()))) {
                            zapasTyp = ZapasTypEnum.GMG;
                        }
                    }
                }
            }
        }

        return zapasTyp;
    }

    /**
     * Vrati datum zapasu ako java.util.Date.
     *
     * @param datum datum hrania zapasu
     * @return datum zapasu
     */
    public static Date parseDatumFromZonedDateTime(String datum) {
        if (StringUtils.isEmpty(datum)) {
            throw new IllegalArgumentException("datum zapasu musi byt zadany");
        }
        ZonedDateTime origin = ZonedDateTime.parse(datum);
        ZoneId bratislava = ZoneId.of("Europe/Bratislava");
        ZonedDateTime bratislavskyDateTime = origin.withZoneSameInstant(bratislava);

        String datePattern = "dd.MM.yyyy HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(datePattern);
        try {
            return DateUtils.parseDate(bratislavskyDateTime.format(formatter), new String[]{datePattern});
        } catch (ParseException e) {
            return null;
        }
    }
}
