package sk.di.tipovacka.impl.service;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.comparator.ZapasDatumComparator;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.dao.ZapasDao;
import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.KoniecZapasuTypEnum;
import sk.di.tipovacka.api.enums.SportTypEnum;
import sk.di.tipovacka.api.enums.SutazTypEnum;
import sk.di.tipovacka.api.service.ZapasService;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Implementacia service.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public class ZapasServiceImpl implements ZapasService {

    private final Logger logger = Logger.getLogger(getClass());

    private SutazRocnikDao sutazRocnikDao;
    private ZapasDao zapasDao;

    private IihfServiceImpl iihfService;
//    private FootballDataServiceImpl footballDataService;
    private WorldCupSfgIoServiceImpl worldCupSfgIoService;

    @Override
    public Long addZapas(String autor, ZapasDto zapas) {
        logger.info("addZapas(autor=" + autor + ", zapas=" + zapas + ")");
        Assert.notNull(zapas, "zapas nemoze byt null");

        Zapas z = new Zapas();
        z.setSutazRocnikId(zapas.getRocnikSutazeId());
        z.setDatum(zapas.getDatum());
        z.setDomaciId(zapas.getDomaciId());
        z.setHostiaId(zapas.getHostiaId());
        z.setGolyDomaci(zapas.getGolyDomaci());
        z.setGolyHostia(zapas.getGolyHostia());
        z.setPriebeh(zapas.getPriebeh());
        z.setKoniecZapasuTyp(zapas.getKoniecZapasuTyp());
        z.setTypZapasu(zapas.getTypZapasu());

        Long zapasId = zapasDao.save(z);
        logger.info("zapas[zapasId=" + zapasId + "] bol zalozeny");

        return zapasId;
    }

    @Override
    public void editZapas(String autor, ZapasDto zapas) throws EntityNotFoundException {
        logger.info("editZapas(autor=" + autor + ", zapas=" + zapas + ")");
        Assert.notNull(zapas, "zapas nemoze byt null");
        Assert.isTrue(zapas.getId() != null, "zapasId nemoze byt null");

        Zapas z = zapasDao.get(zapas.getId());
        if (z == null) {
            logger.error("zapas[id=" + zapas.getId() + "] nebol najdeny");
            throw new EntityNotFoundException("zapas[id=" + zapas.getId() + "] nebol najdeny");
        }

        z.setPriebeh(zapas.getPriebeh());
        z.setGolyDomaci(zapas.getGolyDomaci());
        z.setGolyHostia(zapas.getGolyHostia());
        z.setKoniecZapasuTyp(zapas.getKoniecZapasuTyp());
        z.setStatus(zapas.getStatus());
        z.setHighlightsUrl(zapas.getHighlightsUrl());

        zapasDao.saveOrUpdate(z);
        logger.info("zapas[id=" + zapas.getId() + "] bol zmeneny");
    }

    @Override
    public ZapasDto getZapas(Long zapasId) {
        logger.info("getZapas(zapasId=" + zapasId + ")");
        Assert.notNull(zapasId, "zapasId nemoze byt null");

        return ZapasDto.create(zapasDao.get(zapasId));
    }

    @Override
    public List<ZapasDto> getZapasyPodlaRocnikaSutaze(Long rocnikSutazeId) {
        logger.info("getZapasyPodlaRocnikaSutaze(rocnikSutazeId=" + rocnikSutazeId + ")");
        // vyskladame vyhladavacie kriteria
        ZapasKriteria kriteria = new ZapasKriteria();
        kriteria.setSutazRocnikId(rocnikSutazeId);
        // preklopime do portaloveho zoznamu
        List<ZapasDto> zapasy = new ArrayList<>();
        for (Zapas z : zapasDao.findByKriteria(kriteria)) {
            zapasy.add(ZapasDto.create(z));
        }

        return zapasy;
    }

    @Override
    public List<ZapasDto> findZapasy(ZapasKriteria kriteria) {
        logger.info("findZapasy(kriteria=" + kriteria + ")");
        Assert.notNull(kriteria, "vyhladavacie kriteria nemozu byt null");

        List<ZapasDto> zapasy = new ArrayList<>();
        for (Zapas z : zapasDao.findByKriteria(kriteria)) {
            zapasy.add(ZapasDto.create(z));
        }

        return zapasy;
    }

    @Override
    public List<ZapasDto> getZapasyBezSuperov(Long sutazRocnikId) {
        logger.info("getZapasyBezSuperov(Long sutazRocnikId)");

        if (sutazRocnikId == null) {
            logger.error("sutazRocnikId nemoze byt null");
            throw new IllegalArgumentException("sutazRocnikId nemoze byt null");
        }

        List<ZapasDto> zapasy = new ArrayList<>();
        for (Zapas z : zapasDao.getZapasyBezSuperov(sutazRocnikId)) {
            zapasy.add(ZapasDto.create(z));
        }

        return zapasy;

    }

    @Override
    public List<ZapasDto> getZapasReport(String url, SportTypEnum sportTyp, SutazTypEnum sutazTyp, boolean init) {
        logger.info("getZapasReport(url=" + url + ", init=" + init + ")");
        Assert.notNull(url, "url nemoze byt null");

        String nepodporovane = "nepodporovany sport[" + sportTyp + "] a jeho sutaz[" + sutazTyp + "]";
        switch (sportTyp) {
            case HOKEJ:
                if (SutazTypEnum.MAJSTROVSTVA_SVETA.equals(sutazTyp)) {
                    return iihfService.getZapasyTurnaja(url);
                }
                throw new UnsupportedOperationException(nepodporovane);
            case FUTBAL:
                if (SutazTypEnum.MAJSTROVSTVA_SVETA.equals(sutazTyp)
                        || SutazTypEnum.MAJSTROVSTVA_EUROPY.equals(sutazTyp)) {
                    return worldCupSfgIoService.getZapasReport(url, false);
                }
                throw new UnsupportedOperationException(nepodporovane);
            default:
                throw new UnsupportedOperationException(nepodporovane);
        }
    }

    @Override
    public void initZapasyNaTest(Long rocnikSutazeId, Date zaciatok) {
        logger.info("initZapasyNaTest(rocnikSutazeId=" + rocnikSutazeId + ", zaciatok=" + zaciatok + ")");

        if (rocnikSutazeId == null) {
            logger.error("rocnikSutazeId nemoze byt null");
            throw new IllegalArgumentException("rocnikSutazeId nemoze byt null");
        }
        if (zaciatok == null) {
            logger.error("zaciatok nemoze byt null");
            throw new IllegalArgumentException("zaciatok nemoze byt null");
        }

        Zapas prvyZapas = zapasDao.getPvyZapasRocnikaSutaze(rocnikSutazeId);
        if (prvyZapas == null) {
            logger.error("zapasy neboli inicializovane");
            throw new IllegalArgumentException("zapasy neboli inicializovane");
        }

        // zadefinujeme, ze rocnik sutaze je v test mode
        SutazRocnik rocnik = sutazRocnikDao.get(rocnikSutazeId);
        rocnik.setInit(false);
        rocnik.setTest(true);
        rocnik.setZaciatokRocnikaTest(zaciatok);
        sutazRocnikDao.saveOrUpdate(rocnik);
        logger.info("rocnik[id=" + rocnikSutazeId + "] bol nastaveny do test modu");

        // vymazeme tipy pre dany rocnik
        sutazRocnikDao.clearTipy(rocnikSutazeId);
        logger.info("rocnik[id=" + rocnikSutazeId + "] vsetky tipy boli vymazane");

        // zistime o kolko musime posunut vsetky zapasy
        Date zaciatokPrvyZapas = prvyZapas.getDatum();
        logger.debug("prvy zapas[datum=" + zaciatokPrvyZapas + "]");
        int pocetDni = Days.daysBetween(new DateTime(zaciatokPrvyZapas), new DateTime(zaciatok)).getDays();
        logger.info("posun[pocetDni=" + pocetDni + "]");

        // zoberieme vsetky zapasy pre dany rocnik
        for (Zapas z : rocnik.getZapasy()) {
            // posunieme o dany usek
            z.setDatum(DateUtils.addDays(z.getDatum(), pocetDni));
            // vymazeme udaje
            z.setGolyDomaci(null);
            z.setGolyHostia(null);
            z.setPriebeh(null);
            z.setKoniecZapasuTyp(KoniecZapasuTypEnum.NEZACAL);

            // a upraveny zapas ulozime
            zapasDao.saveOrUpdate(z);
        }
    }

    @Override
    public List<ZapasDto> rollDatumZapasov(List<ZapasDto> zapasy, Date zaciatok) {
        logger.info("rollDatumZapasov(zapasy=..., zaciatok=" + zaciatok + ")");

        if (zapasy != null && !zapasy.isEmpty()) {
            // zoradime zapasy od prveho po posledny
            Collections.sort(zapasy, new ZapasDatumComparator());
            ZapasDto prvyZapas = zapasy.get(0);

            // zistime o kolko dni musime posunut datum zapasov
            Date zaciatokPrvyZapas = prvyZapas.getDatum();
            logger.debug("prvy zapas[datum=" + zaciatokPrvyZapas + "]");
            int pocetDni = Days.daysBetween(new DateTime(zaciatokPrvyZapas), new DateTime(zaciatok)).getDays();
            logger.info("posun[pocetDni=" + pocetDni + "]");

            for (ZapasDto z : zapasy) {
                z.setDatum(DateUtils.addDays(z.getDatum(), pocetDni));
            }
        }

        return zapasy;
    }

    /**
     * Vrati textovu prezentaciu udajov v bunke,
     * ked je viacej prvkov podla tagu,
     * vo vysledku ich oddelime |.
     *
     * @param element element, z ktoreho vyberame udaje
     * @param tag     tag, kde su uzavrete udaje
     * @return textova prezentacia hodnoty bunky
     */
    private String getBunkaAkoText(Element element, String tag) {
        StringBuilder sb = new StringBuilder();
        Elements elements = element.getElementsByTag(tag);
        if (elements != null && !elements.isEmpty()) {
            for (Element e : elements) {
                if (sb.length() != 0) {
                    sb.append("|");
                }
                String text = e.html()
                        // vyhodime tvrde medzery
                        .replaceAll("&nbsp;", " ")
                        // vyhodime breakline
                        .replaceAll("<br />", " ")
                        // vyhodime znak noveho riadku
                        .replaceAll("\\n", "")
                        // vyhodime znak zalomenia riadku
                        .replaceAll("\\r", "");
                // a pridame do vysledku
                sb.append(text.trim());
            }
        }

        return sb.toString();
    }

    public void setZapasDao(ZapasDao zapasDao) {
        this.zapasDao = zapasDao;
    }

    public void setSutazRocnikDao(SutazRocnikDao sutazRocnikDao) {
        this.sutazRocnikDao = sutazRocnikDao;
    }

    public void setIihfService(IihfServiceImpl iihfService) {
        this.iihfService = iihfService;
    }

//    public void setFootballDataService(FootballDataServiceImpl footballDataService) {
//        this.footballDataService = footballDataService;
//    }

    public void setWorldCupSfgIoService(WorldCupSfgIoServiceImpl worldCupSfgIoService) {
        this.worldCupSfgIoService = worldCupSfgIoService;
    }
}
