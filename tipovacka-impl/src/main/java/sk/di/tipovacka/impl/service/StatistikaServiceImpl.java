package sk.di.tipovacka.impl.service;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import sk.di.tipovacka.api.comparator.PouzivatelovTipStatistikaComparator;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.*;
import sk.di.tipovacka.api.enums.SpravnyTipNaEnum;
import sk.di.tipovacka.api.service.StatistikaService;
import sk.di.tipovacka.api.service.SutazRocnikSuhlasService;
import sk.di.tipovacka.api.service.TipNaVitazaService;
import sk.di.tipovacka.api.service.TipNaZapasService;

import java.util.*;

/**
 * Implementacia interfacu.
 *
 * User: Dano
 * Date: 13.6.2013
 */
public class StatistikaServiceImpl implements StatistikaService {

    private final Logger logger = Logger.getLogger(getClass());

    private SutazRocnikDao sutazRocnikDao;

    private TipNaZapasService tipNaZapasService;
    private TipNaVitazaService tipNaVitazaService;
    private SutazRocnikSuhlasService rocnikSuhlasService;

    @Override
    public List<PouzivatelovTipStatistikaDto> getStatistikaTiperov(Long rocnikId) {
        logger.info("getStatistikaTiperov(rocnikId=" + rocnikId + ")");

        if (rocnikId == null) {
            logger.error("rocnikId nemoze byt null");
            throw new IllegalArgumentException("rocnikId nemoze byt null");
        }

        // natiahneme si zadany rocnik
        RocnikSutazeDto rocnik = RocnikSutazeDto.create(sutazRocnikDao.get(rocnikId));
        if (rocnik == null) {
            logger.error("rocnik[id=" + rocnikId + "] nebol najdeny");
            throw new IllegalArgumentException("rocnik[id=" + rocnikId + "] nebol najdeny");
        }

        // suhlasy pre dany rocnik
        List<RocnikSutazeSuhlasDto> suhlasy = rocnikSuhlasService.findAllByRocnik(rocnikId);

        // vyskladame kriteria zapasov, ktore skoncili
        TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
        kriteria.setSutazRocnikId(rocnikId);

        // klucik mapy je login pouzivatela
        Map<String, PouzivatelovTipStatistikaDto> mapa = new HashMap<>();
        List<TipNaZapasDto> tipy = tipNaZapasService.findTipyNaZapas(kriteria);
        // ak mame odohrate zapasy, dohladame tipy
        if (!tipy.isEmpty()) {
            for (TipNaZapasDto tip : tipy) {
                String login = tip.getPouzivatel().getLogin();
                // inicializacia klucika mapy
                if (mapa.get(login) == null) {
                    mapa.put(login, new PouzivatelovTipStatistikaDto());
                    mapa.get(login).setPouzivatel(tip.getPouzivatel());
                }

                // ak pouzivatel nema splnene podmienky, tak pre neho nezistujeme statistiku
                if (!isPodmienkySplnene(login, suhlasy)) {
                    continue;
                }

                // statistika daneho pouzivatela
                PouzivatelovTipStatistikaDto statistika = mapa.get(login);
                statistika.setPocetZadanychTipov(statistika.getPocetZadanychTipov() + 1);

                // spracujeme tip iba vtedy, ked zapas bol odohraty
                if (tip.getZapas().getKoniecZapasuTyp().isKoniecZapasu()) {
                    statistika.setBody(statistika.getBody()
                            + tip.getBodyZaGolyDomaci()
                            + tip.getBodyZaGolyHostia()
                            + tip.getBodyZaVitaza());
                    // co bolo spravne typnute
                    StringBuilder spravnyTip = new StringBuilder();

                    // tip na vitaza
                    if (tip.getBodyZaVitaza() != 0) {
                        statistika.addPocetTipovNaVitazaZapasu();
                        spravnyTip.append("VITAZ");
                    }
                    // tip na goly domacich
                    if (tip.getBodyZaGolyDomaci() != 0) {
                        statistika.addPocetTipovNaGolDomaci();
                        spravnyTip.append("GOL");
                    }
                    // tip na goly hostia
                    if (tip.getBodyZaGolyHostia() != 0) {
                        statistika.addPocetTipovNaGolHostia();
                        spravnyTip.append("GOL");
                    }

                    // podla toho, co bolo spravne tipnute naplnime mapu
                    if (spravnyTip.length() == 0) {
                        int nic = statistika.getMapaSpravnychTipov().get(SpravnyTipNaEnum.NIC) + 1;
                        statistika.getMapaSpravnychTipov().put(SpravnyTipNaEnum.NIC, nic);
                    } else {
                        SpravnyTipNaEnum tipEnum = SpravnyTipNaEnum.valueOf(spravnyTip.toString());
                        int pocet = statistika.getMapaSpravnychTipov().get(tipEnum) + 1;
                        statistika.getMapaSpravnychTipov().put(tipEnum, pocet);
                    }
                }
            }
        }

        // vysledny zoznam statistiky
        List<PouzivatelovTipStatistikaDto> zoznam = new ArrayList<>();
        if (!mapa.isEmpty()) {
            // spracujeme este tipy na vitaza
            List<TipNaVitazaDto> tipyVitaz = tipNaVitazaService.getTipyNaVitaza(rocnikId);
            for (TipNaVitazaDto t : tipyVitaz) {
                String login = t.getPouzivatel().getLogin();
                if (mapa.get(login) != null) {

                    // ak pouzivatel nema splnene podmienky, tak pre neho nezistujeme statistiku
                    if (!isPodmienkySplnene(login, suhlasy)) {
                        continue;
                    }

                    PouzivatelovTipStatistikaDto statistika = mapa.get(login);
                    statistika.setTipnutyVitaz(t.getBody() != 0);
                    statistika.setBody(statistika.getBody() + t.getBody());
                }
            }
        } else {
            // ked nemame ziadne tipy pre statistiku, pokusime sa aspon inicializovat,
            // kolko tiper tipovalo a su potencialne subjekty pre statistiku
            List<PouzivatelDto> tiperi = tipNaZapasService.getTipujucichNaZapas(rocnikId);
            for (PouzivatelDto p : tiperi) {
                PouzivatelovTipStatistikaDto stat = new PouzivatelovTipStatistikaDto();
                stat.setPouzivatel(p);
                mapa.put(p.getLogin(), stat);
            }
        }

        // presypeme mapu do zoznamu
        for (String login : mapa.keySet()) {
            zoznam.add(mapa.get(login));
        }

        // zosortujeme vysledny zoznam
        Collections.sort(zoznam, new PouzivatelovTipStatistikaComparator());

        // a mame hotovo
        return zoznam;
    }

    /**
     * Vrati priznak, ci dany pouzivatel splnil podmienky ucasti.
     *
     * @param login   login overovaneho pouzivatela
     * @param suhlasy zoznam suhlasov
     * @return
     */
    private boolean isPodmienkySplnene(String login, List<RocnikSutazeSuhlasDto> suhlasy) {
        if (suhlasy == null) {
            return false;
        }
        if (StringUtils.isEmpty(login)) {
            return false;
        }

        for (RocnikSutazeSuhlasDto suhlas : suhlasy) {
            if (login.equals(suhlas.getPouzivatel().getLogin())) {
                return suhlas.isZaplatene();
            }
        }

        return false;
    }

    public void setSutazRocnikDao(SutazRocnikDao sutazRocnikDao) {
        this.sutazRocnikDao = sutazRocnikDao;
    }

    public void setTipNaZapasService(TipNaZapasService tipNaZapasService) {
        this.tipNaZapasService = tipNaZapasService;
    }

    public void setTipNaVitazaService(TipNaVitazaService tipNaVitazaService) {
        this.tipNaVitazaService = tipNaVitazaService;
    }

    public void setRocnikSuhlasService(SutazRocnikSuhlasService rocnikSuhlasService) {
        this.rocnikSuhlasService = rocnikSuhlasService;
    }
}
