package sk.di.tipovacka.impl.service;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.comparator.MuzstvoNazovComparator;
import sk.di.tipovacka.api.db.Muzstvo;
import sk.di.tipovacka.api.db.dao.MuzstvoDao;
import sk.di.tipovacka.api.dto.MuzstvoDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.service.MuzstvoService;
import sk.di.tipovacka.impl.util.ParseUtils;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implementacia service.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public class MuzstvoServiceImpl implements MuzstvoService {

    private final Logger logger = Logger.getLogger(getClass());

    private MuzstvoDao muzstvoDao;

    private IihfServiceImpl  iihfService;

    @Override
    public Long addMuzstvo(String autor, MuzstvoDto muzstvo) {
        logger.info("addMuzstvo(autor=" + autor + ", muzstvo=" + muzstvo + ")");
        Assert.notNull(muzstvo, "muzstvo nemoze byt null");

        if (muzstvoDao.getMuzstvoPodlaKodu(muzstvo.getKod()) != null) {
            logger.error("muzstvo s kodom[" + muzstvo.getKod() + "] uz existuje");
            throw new IllegalArgumentException("muzstvo s kodom[" + muzstvo.getKod() + "] uz existuje");
        }

        Muzstvo m = new Muzstvo();
        m.setKod(muzstvo.getKod());
        m.setNazov(muzstvo.getNazov());

        Long muzstvoId = muzstvoDao.save(m);
        logger.info("muzstvo[id=" + muzstvoId + "] bolo zalozene");

        return muzstvoId;
    }

    @Override
    public void editMuzstvo(String autor, MuzstvoDto muzstvo) {
        logger.info("editMuzstvo(autor=" + autor + ", muzstvo=" + muzstvo + ")");
        Assert.notNull(muzstvo, "muzstvo nemoze byt null");
        Assert.notNull(muzstvo.getId(), "muzstvo.id nemoze byt null");
        // overime, ci editovane muzstvo uz existuje
        Muzstvo m = muzstvoDao.get(muzstvo.getId());
        if (m == null) {
            logger.error("muzstvo[id=" + muzstvo.getId() + "] nebolo najdene");
            throw new IllegalArgumentException("muzstvo[id=" + muzstvo.getId() + "] nebolo najdene");
        }
        // overime, ci nezadavame existujuci kod muzstva
        Muzstvo mPodlaKodu = muzstvoDao.getMuzstvoPodlaKodu(muzstvo.getKod());
        if (!mPodlaKodu.getId().equals(muzstvo.getId())) {
            logger.error("muzstvo s kodom[" + muzstvo.getKod() + "] uz existuje");
            throw new IllegalArgumentException("muzstvo s kodom[" + muzstvo.getKod() + "] uz existuje");
        }
        // nasetujeme udaje
        m.setKod(muzstvo.getKod());
        m.setNazov(muzstvo.getNazov());
        m.setStatus(muzstvo.getStatus());

        muzstvoDao.saveOrUpdate(m);
        logger.info("muzstvo[id=" + muzstvo.getId() + "] bolo zmenene");
    }

    @Override
    public MuzstvoDto getMuzstvo(Long muzstvoId) {
        logger.info("getMuzstvo(muzstvoId=" + muzstvoId + ")");
        Assert.notNull(muzstvoId, "muzstvoId nemoze byt null");

        return MuzstvoDto.create(muzstvoDao.get(muzstvoId));
    }

    @Override
    public List<MuzstvoDto> getMuzstva() {
        logger.info("getMuzstva()");

        List<MuzstvoDto> muzstva = new ArrayList<>();
        for (Muzstvo m : muzstvoDao.findAll()) {
            muzstva.add(MuzstvoDto.create(m));
        }
        // zosortujeme podla kodu
        Collections.sort(muzstva, new MuzstvoNazovComparator());

        return muzstva;
    }

    @Override
    public MuzstvoDto getMuzstvoPodlaKodu(String kod) {
        logger.info("getMuzstvoPodlaKodu(kod=" + kod + ")");
        Assert.notNull(kod, "kod muzstva nemoze byt null");

        return MuzstvoDto.create(muzstvoDao.getMuzstvoPodlaKodu(kod));
    }

    @Override
    public void removeMuzstvo(String autor, Long muzstvoId) {
        logger.info("removeMuzstvo(autor=" + autor + ", muzstvoId=" + muzstvoId + ")");
        if (muzstvoId == null) {
            logger.error("mizstvoId nemoze byt null");
            throw new IllegalArgumentException("mizstvoId nemoze byt null");
        }

        Muzstvo muzstvo = muzstvoDao.get(muzstvoId);
        if (muzstvo == null) {
            logger.error("muzstvo[id=" + muzstvoId + "] nebolo najdene");
            throw new IllegalArgumentException("muzstvo[id=" + muzstvoId + "] nebolo najdene");
        }

        muzstvo.setStatus(EntityStatusEnum.NEAKTIVNY);
        muzstvoDao.saveOrUpdate(muzstvo);

        logger.info("muzstvo[id=" + muzstvoId + "] bolo zmenene");
    }

    /**
     * Ulozi logo daneho muzstva
     *
     * @param logoUrl url, kde najdeme logo daneho muzstva
     * @param muzstvo nazov kod muzstva
     * @return priznak, ci sa podarilo ulozit logo
     */
    private boolean saveLogo(String logoUrl, String muzstvo) {

        try {
            URL url = new URL(logoUrl);
            InputStream in = new BufferedInputStream(url.openStream());
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            byte[] buf = new byte[1024];
            int n;
            while (-1 != (n = in.read(buf))) {
                out.write(buf, 0, n);
            }
            out.close();
            in.close();
            byte[] response = out.toByteArray();

            String fileName = muzstvo + ".png";
            try (FileOutputStream fos = new FileOutputStream("c:\\vlajky\\" + fileName)) {
                fos.write(response);
            } catch (Exception e) {
                logger.error("nepodarilo sa ulozit logo", e);
            }

            return true;
        } catch (Exception e) {
            logger.error("nepodarilo sa ulozit logo[" + logoUrl + "]", e);
            return false;
        }
    }

    public void setMuzstvoDao(MuzstvoDao muzstvoDao) {
        this.muzstvoDao = muzstvoDao;
    }

    public void setIihfService(IihfServiceImpl iihfService) {
        this.iihfService = iihfService;
    }
}
