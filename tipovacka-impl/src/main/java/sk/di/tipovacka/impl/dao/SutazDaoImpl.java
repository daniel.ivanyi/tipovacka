package sk.di.tipovacka.impl.dao;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import sk.di.tipovacka.api.db.dao.SutazDao;
import sk.di.tipovacka.api.db.Sutaz;
import sk.di.tipovacka.api.enums.SportTypEnum;

import java.util.List;

/**
 * Implementacia dao.
 *
 * User: Dano
 * Date: 10.5.2013
 */
public class SutazDaoImpl extends EntityDaoImpl<Sutaz> implements SutazDao {

    private final Logger logger = Logger.getLogger(getClass());

    @Override
    @SuppressWarnings("unchecked")
    public List<Sutaz> getSutazPodlaNazvu(String nazovSutaze, SportTypEnum typ, boolean strict) {
        logger.info("getSutazPodlaNazvu(nazovSutaze=" + nazovSutaze + ", strict=" + strict + ")");

        DetachedCriteria criteria = DetachedCriteria.forClass(Sutaz.class)
                .add(Restrictions.eq("sportTyp", typ));
        // podla strictnosti dohladame
        if (strict) {
            criteria.add(Restrictions.eq("nazov", nazovSutaze));
        } else {
            criteria.add(Restrictions.like("nazov", nazovSutaze));
        }

        return getHibernateTemplate().findByCriteria(criteria);
    }
}
