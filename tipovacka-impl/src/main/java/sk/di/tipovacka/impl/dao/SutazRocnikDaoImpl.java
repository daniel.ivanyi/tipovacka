package sk.di.tipovacka.impl.dao;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.SutazRocnik;

import java.util.List;

/**
 * Implementacia dao.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public class SutazRocnikDaoImpl extends EntityDaoImpl<SutazRocnik> implements SutazRocnikDao {

    private final Logger logger = Logger.getLogger(getClass());

    @Override
    @SuppressWarnings("unchecked")
    public String getRocnikSutazeStatistikaUrl(Long sutazRocnikId) {
        Assert.notNull(sutazRocnikId, "sutazRocnikId nemoze byt null");
        // vyskladame si dopyt na to, aby sme vratili iba urlku,
        // kde sa na webe nachadza statistika k danemu rocniku sutaze
        DetachedCriteria criteria = DetachedCriteria.forClass(SutazRocnik.class)
                .add(Restrictions.eq("id", sutazRocnikId))
                .setProjection(Projections.property("statistikaUrl"));
        // overime si, ci sme nieco nasli a ak ano vratime hodnotu inak null
        List<String> result = getHibernateTemplate().findByCriteria(criteria);
        if (!result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public SutazRocnik getRocnikSutazePodlaSutazeRoka(Long sutazId, int rok) {
        Assert.notNull(sutazId, "sutazId nemoze byt null");
        Assert.notNull(rok != 0, "rok nemoze byt ZERO");

        // vyskladame kriteria
        DetachedCriteria criteria = DetachedCriteria.forClass(SutazRocnik.class)
                .add(Restrictions.eq("sutazId", sutazId))
                .add(Restrictions.eq("rok", rok));

        // overime si, ci sme nieco nasli a ak ano vratime hodnotu inak null
        List<SutazRocnik> result = getHibernateTemplate().findByCriteria(criteria);
        if (!result.isEmpty()) {
            return result.get(0);
        }

        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SutazRocnik> getAktualnyRocnikSutaze() {
        DetachedCriteria criteria = DetachedCriteria.forClass(SutazRocnik.class)
                .add(Restrictions.eq("aktualny", true));

        // overime si, ci sme nieco nasli a ak ano vratime hodnotu inak null
        List<SutazRocnik> result = getHibernateTemplate().findByCriteria(criteria);
        if (!result.isEmpty()) {
            return result;
        }

        return null;
    }

    @Override
    public void clearTipy(Long sutazRocnikId) {
        logger.info("clearTipy(sutazRocnikId=" + sutazRocnikId + ")");
        int pocetVymazanych = 0;

        // odstranime tipy na vitaza rocnika sutaze
        String deleteTipyVitazRocnika = "delete from TipNaVitaza where sutazRocnik.id= :sutazRocnikId";
        pocetVymazanych = getSession()
                .createQuery(deleteTipyVitazRocnika)
                .setLong("sutazRocnikId", sutazRocnikId)
                .executeUpdate();
        logger.info("pocet vymazanych tipov na vitaza rocnika sutaze[id=" + sutazRocnikId + "] = " + pocetVymazanych);

        // odstranenie tipov na zapas
        String deleteTipyNaZapas =
                "delete from TipNaZapas as tipNaZapas"
                        + " where tipNaZapas.zapas.id in " +
                        " (select z.id from Zapas z where z.sutazRocnik.id = :sutazRocnikId)";
        pocetVymazanych = getSession()
                .createQuery(deleteTipyNaZapas)
                .setLong("sutazRocnikId", sutazRocnikId)
                .executeUpdate();
        logger.info("pocet vymazanych tipov na zapas rocnika sutaze[id=" + sutazRocnikId + "] = " + pocetVymazanych);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<SutazRocnik> getUkonceneRocniky() {
        logger.info("getUkonceneRocniky()");
        DetachedCriteria criteria = DetachedCriteria.forClass(SutazRocnik.class)
                .add(Restrictions.isNotNull("vitazId"));

        return getHibernateTemplate().findByCriteria(criteria);
    }
}
