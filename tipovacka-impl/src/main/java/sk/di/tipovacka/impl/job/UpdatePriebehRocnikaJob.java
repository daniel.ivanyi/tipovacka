package sk.di.tipovacka.impl.job;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.scheduling.quartz.QuartzJobBean;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.service.SutazRocnikService;

import java.io.IOException;
import java.io.Serializable;

/**
 * Job pre updatenutie priebehu rocnika sutaze.
 * <p/>
 * User: Dano
 * Date: 3.6.2013
 */
public class UpdatePriebehRocnikaJob extends QuartzJobBean implements Serializable {
    private static final long serialVersionUID = 6167084791707795788L;

    private final Logger logger = Logger.getLogger(getClass());

    private SutazRocnikService sutazRocnikService;

    public UpdatePriebehRocnikaJob() {
    }

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        jobRun();
    }

    public synchronized void jobRun() {
        logger.debug("jobRun() started ...");

        for (RocnikSutazeDto rocnik : sutazRocnikService.getAktualnyRocnikSutaze()) {
            try {
                sutazRocnikService.updatePriebehRocnikaSutaze("job", rocnik.getId());
            } catch (IOException e) {
                logger.error("nepodarilo sa updatenut priebeh rocnika[id=" + rocnik.getId() + "]", e);
            }
        }
        logger.debug("jobRun() ... ended");
    }

    public void setSutazRocnikService(SutazRocnikService sutazRocnikService) {
        this.sutazRocnikService = sutazRocnikService;
    }
}
