package sk.di.tipovacka.impl.service;

import org.apache.commons.lang.StringUtils;
import org.apache.log4j.Logger;
import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.db.dao.PouzivatelDao;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException;
import sk.di.tipovacka.api.service.MailService;
import sk.di.tipovacka.impl.util.ParseUtils;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import javax.mail.search.FlagTerm;
import javax.mail.util.ByteArrayDataSource;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Implementacia mailovych servisnych metod.
 *
 * Naposledy (03/05/2019) ked sa nedali zo servera odosielat emaily, pomohlo
 * https://support.google.com/mail/answer/78754 a konretne
 * - ak tipy uvedené vyššie nepomohli, prejdite na stránku https://www.google.com/accounts/DisplayUnlockCaptcha
 *   a postupujte podľa krokov, ktoré sú na nej uvedené.
 *
 * User: Dano
 * Date: 27.2.2014
 */
public class MailServiceImpl implements MailService {

    private final Logger logger = Logger.getLogger(getClass());
    private final SimpleDateFormat dateFormatZapasu = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private PouzivatelDao pouzivatelDao;
    private SutazRocnikDao sutazRocnikDao;
    private TipNaZapasDao tipNaZapasDao;

    // pattern pre parsovanie tipu poslaneho cez mail
    private final Pattern patternTipCezMailKodMuzstva = Pattern.compile(ParseUtils.getTipCezMailKodMuzstvaPatter());

    @Override
    public void sendMail(String mail, String predmet, String text, byte[] priloha, String prilohaFileName, String prilohaMimeType) {

        logger.info("sendMail(mail=" + mail
                + ", predmed=" + predmet
                + ", text=...)");

        try {
            // prihlasenie do gmailu
            Session session = getMailSession(getSmtpProps());

            // vytvorime spravu
            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress("tipovacka.app@gmail.com", "Tipovacka"));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(mail));
            message.setSubject(predmet);
            message.setSentDate(new Date());

            // pridame text
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setContent(text, "text/html; charset=utf-8");

            // vytvorime multipart
            Multipart multipart = new MimeMultipart();
            multipart.addBodyPart(messageBodyPart);

            // ak mame prilohu, pripneme
            if (priloha != null && priloha.length > 0) {
                logger.debug("priloha je nadefinovana, prilozime");
                MimeBodyPart attachPart = new MimeBodyPart();
                DataSource dataSource = new ByteArrayDataSource(priloha, prilohaMimeType);
                attachPart.setDataHandler(new DataHandler(dataSource));
                attachPart.setFileName(prilohaFileName);
                multipart.addBodyPart(attachPart);
            }

            // nastavime obsah
            message.setContent(multipart);

            // a posielame
            Transport.send(message);

            logger.info("mail odoslany na mailovu adresu[" + mail + "]");

        } catch (Exception e) {
            logger.error("nepodarilo sa poslat mail na mailovu adresu[" + mail + "]", e);
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public List<TipNaZapasDto> getNeprecitaneTipyNaZapas() throws Exception {
        logger.info("getNeprecitaneTipyNaZapas()");

        List<TipNaZapasDto> tipy = new ArrayList<>();

        Session session = getMailSession(getImapProps());

        try {
            Store store = session.getStore("imaps");
            store.connect();
            Folder inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_WRITE);
            Message messages[] = inbox.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false));
            // nacitame si zapasy aktualnych rocnikov, pre ktory sa tipuju zapasy
            for (SutazRocnik rocnik : sutazRocnikDao.getAktualnyRocnikSutaze()) {
                List<Zapas> zapasy = new ArrayList<>(rocnik.getZapasy());
                // zosortujeme si zapasy, aby isli od najonvsieho po najstarsie
                Collections.sort(zapasy, new Comparator<Zapas>() {

                    @Override
                    public int compare(Zapas o1, Zapas o2) {
                        return -1 * o1.getDatum().compareTo(o2.getDatum());
                    }
                });
                for (Message message : messages) {
                    Address[] from = message.getFrom();
                    // spracuvavame maily s predmetom, ktory obsahuje slovo tipovacka
                    String predmet = StringUtils.isEmpty(message.getSubject())
                            ? ""
                            : message.getSubject().toLowerCase();
                    if (predmet.contains("tipovacka")) {
                        // podla mailu dohladame pouzivatela
                        String odosielatel = ((InternetAddress) from[0]).getAddress();
                        Pouzivatel pouzivatel = pouzivatelDao.getPouzivatelPodlaMailu(odosielatel);
                        if (pouzivatel != null) {
                            // nacitame obsah mailu
                            String text = getText(message);
                            if (StringUtils.isNotEmpty(text)) {
                                Scanner scanner = new Scanner(text);
                                if (!scanner.hasNextLine()) {
                                    logger.warn("mail[odosielatel=" + odosielatel + "] neobsahuje ziadne riadky na spracovanie");
                                }
                                // prebehneme riadky
                                while (scanner.hasNextLine()) {
                                    String line = scanner.nextLine();
                                    // rozparsujeme si prijate riadky, riadok musi obsahovat tip podla predpisu
                                    Matcher matcher = patternTipCezMailKodMuzstva.matcher(line);
                                    if (matcher.find()) {
                                        logger.info("najdeny riadok s tipom[odosielatel=" + odosielatel + ", line=" + line + "]");
                                        TipNaZapasDto tip = getTipNaZapas(message.getSentDate(), line, pouzivatel, zapasy);
                                        if (tip != null) {
                                            tipy.add(tip);
                                        } else {
                                            logger.warn("nepodarilo sa vyparsovat tip[odosielatel=" + odosielatel + ", line=" + line + "]");
                                        }
                                    }
                                }
                                scanner.close();
                            } else {
                                logger.warn("mail neobsahuje ziaden text");
                            }
                            logger.debug("spracovany mail[adresa=" + odosielatel + "]");
                        } else {
                            logger.warn("pouzivatel[mail=" + odosielatel + "] nebol najdeny, preto jeho tipy nebudu spracovane");
                        }
                    }
                }
            }
            // zavrieme za sebou dvere ;)
            inbox.close(true);
            store.close();
        } catch (Exception e) {
            throw new Exception("nepodarilo sa nacitat maily,", e);
        }

        return tipy;
    }

    /**
     * Nacitanie neprecitanych sprav.
     *
     * @return spravy
     */
    public List<Message> getNeprecitaneSpravy() {
        logger.info("getNeprecitaneSpravy()");

        Session session = getMailSession(getImapProps());

        Store store = null;
        Folder inbox = null;
        try {
            store = session.getStore("imaps");
            store.connect();
            inbox = store.getFolder("INBOX");
            inbox.open(Folder.READ_WRITE);
            return Arrays.asList(inbox.search(new FlagTerm(new Flags(Flags.Flag.SEEN), false)));
        } catch (Exception e) {
            logger.error("nepodarilo sa nacitat spravy", e);

            return Collections.emptyList();
        } finally {
            // zavrieme za sebou dvere ;)
            if (inbox != null) {
                try {
                    inbox.close(true);
                } catch (MessagingException e) {
                    /* nemame ako zareagovat */
                }
            }
            if (store != null) {
                try {
                    store.close();
                } catch (MessagingException e) {
                    /* nemame ako zareagovat */
                }
            }
        }
    }

    /**
     * Return the primary text content of the message.
     */
    private String getText(Part p) throws MessagingException, IOException {
        if (p.isMimeType("text/*")) {
            return (String) p.getContent();
        }

        if (p.isMimeType("multipart/alternative")) {
            // prefer html text over plain text
            Multipart mp = (Multipart) p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                Part bp = mp.getBodyPart(i);
                if (bp.isMimeType("text/plain")) {
                    return getText(bp);
                } else {
                    return getText(bp);
                }
            }
        } else if (p.isMimeType("multipart/*")) {
            Multipart mp = (Multipart) p.getContent();
            for (int i = 0; i < mp.getCount(); i++) {
                String s = getText(mp.getBodyPart(i));
                if (s != null)
                    return s;
            }
        }

        return null;
    }

    /**
     * Zo zadaneho riadku tipu v maily vysklada tip na nadefinovany zapas
     * predpis: tip dd.MM.yyyy HH:mm domaci_kod hostia_kod tip_goly_domaci tip_goly_hostia
     * priklad: tip [09.05.2014 15:45] CZE (2 - 4) CAN
     *
     * @param datumOdoslania datum odoslania mailu
     * @param riadok         riadok zadaneho tipu v maily
     * @param pouzivatel     pouzivatel, pre ktoreho vyskladavame tip
     * @param zapasy         zapasy aktualneho rocnika, pre ktory sa tipuje
     */
    private TipNaZapasDto getTipNaZapas(Date datumOdoslania,
                                        String riadok,
                                        Pouzivatel pouzivatel,
                                        List<Zapas> zapasy) throws IllegalArgumentException, CasNaTipovanieVyprsalException {
        logger.info("getTipNaZapas(datumOdoslania=" + datumOdoslania
                + ", riadok=" + riadok
                + ", pouzivatel=" + pouzivatel.getLogin()
                + ", zapasy.size=" + (zapasy == null ? 0 : zapasy.size())
                + ")");

        if (datumOdoslania == null) {
            logger.error("datum odoslania mailu nemoze byt null");
            return null;
        }
        if (zapasy == null || zapasy.isEmpty()) {
            logger.error("zapasy aktualneho rocnika nie su nadefinovane, nedaju sa zadavat tipy na zapas");
            return null;
        }

        // rozparsujeme si prijaty tip, predpis = tip [%s] %s (%d - %d) %s
        Matcher matcher = patternTipCezMailKodMuzstva.matcher(riadok);
        // riadok tipu musi obsahovat presny format
        if (matcher.find()) {
            String datum = matcher.group(1);
            String domaci = matcher.group(2);
            String domaciGoly = matcher.group(3);
            String hostiaGoly = matcher.group(4);
            String hostia = matcher.group(5);

            Date datumZapasu;
            Integer golyDomaci;
            Integer golyHostia;
            // datum zapasu
            try {
                datumZapasu = getDatumZapasuFromTip(datum);
                if (datumZapasu == null) {
                    logger.warn("datum hrania zapasu[" + riadok + "] nie je nastaveny");
                    return null;
                }
            } catch (ParseException e) {
                logger.error("nepodarilo sa rozparsovat datum[" + datum + "]", e);
                return null;
            }
            // datum odoslania mailu musi byt pred zaciatkom zapasu
            if (datumOdoslania.after(datumZapasu)) {
                logger.warn("datum odoslania[" + datumOdoslania + "] je po zaciatku zapasu[datum=" + datumZapasu + "]");
                return null;
            }
            // tipnute skore
            try {
                golyDomaci = Integer.parseInt(domaciGoly);
            } catch (NumberFormatException e) {
                logger.error("nepodarilo sa rozparsovat goly domacich[" + domaciGoly + "]", e);
                return null;
            }
            try {
                golyHostia = Integer.parseInt(hostiaGoly);
            } catch (NumberFormatException e) {
                logger.error("nepodarilo sa rozparsovat goly hosti[" + hostiaGoly + "]", e);
                return null;
            }
            Zapas zapas = null;
            // dohladame zapas podla datumu zapasu a superov
            for (Zapas z : zapasy) {
                if (z.getDomaci() != null && z.getHostia() != null) {
                    if (z.getDomaci().getKod().equalsIgnoreCase(domaci)
                            && z.getHostia().getKod().equalsIgnoreCase(hostia)
                            && getFormatDatumZapasuFromTip(z.getDatum()).equals(datum)) {
                        zapas = z;
                        break;
                    }
                }
            }
            // pokusime sa naparovat poslany tip na realny zapas
            if (zapas != null) {
                TipNaZapas tip = new TipNaZapas();
                // dohladame ci, dany pouzivatel tipoval na danny zapas
                TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
                kriteria.getPouzivatelIds().add(pouzivatel.getId());
                kriteria.getZapasIds().add(zapas.getId());
                List<TipNaZapas> tipy = tipNaZapasDao.findTipyNaZapas(kriteria);
                if (!tipy.isEmpty()) {
                    tip = tipy.get(0);
                }
                tip.setDatum(new Date());
                tip.setPouzivatel(pouzivatel);
                tip.setPouzivatelId(pouzivatel.getId());
                tip.setZapas(zapas);
                tip.setZapasId(zapas.getId());
                tip.setGolyDomaci(golyDomaci);
                tip.setGolyHostia(golyHostia);

                return TipNaZapasDto.create(tip);
            } else {
                logger.warn("nepodarilo sa najst zapas[" + datum + ", " + domaci + ", " + hostia + "]");
            }
        } else {
            logger.warn("nespravny format tipu[vzor[" + ParseUtils.getTipCezMailTemplate() + "], riadok[" + riadok + "]]");
        }

        return null;
    }

    /**
     * Vyparsuje datum z textoveho zadania datumu hrania zapasu.
     *
     * @param datumText datum, kedy sa ma hrat zapas
     * @return java.util.Date
     * @throws ParseException ak sa nepodari vyparsovat Date
     */
    private synchronized Date getDatumZapasuFromTip(String datumText) throws ParseException {
        if (StringUtils.isEmpty(datumText)) {
            return null;
        }

        return dateFormatZapasu.parse(datumText);
    }

    /**
     * Naformatuje datum hrania zapasu na String.
     *
     * @param datum datum hrania zapasu
     * @return naformatovany datum
     */
    private synchronized String getFormatDatumZapasuFromTip(Date datum) {
        return dateFormatZapasu.format(datum);
    }

    /**
     * Vrati mail session pre pracu s gmailovym kontom.
     *
     * @param props aktualne properties pre prihlasenie
     * @return session
     */
    private Session getMailSession(Properties props) {
        return Session.getInstance(props,
                new javax.mail.Authenticator() {
                    @Override
                    protected PasswordAuthentication getPasswordAuthentication() {
                        return new PasswordAuthentication("tipovacka.app", "Dobr7De+nTipova4ka");
                    }
                });
    }

    /**
     * Properties pre pracu so smtp.
     *
     * @return properties
     */
    private Properties getSmtpProps() {
        Properties props = new Properties();
        props.put("mail.smtp.auth", "true");
        props.put("mail.smtp.starttls.enable", "true");
        props.put("mail.smtp.host", "smtp.gmail.com");
        props.put("mail.smtp.port", 587);
        props.put("mail.smtp.debug", "true");

        return props;
    }

    /**
     * Properties pre pracu s imap.
     *
     * @return properties
     */
    private Properties getImapProps() {
        Properties props = new Properties();
        props.put("mail.host", "imap.gmail.com");
        props.put("mail.port", 995);
        props.put("mail.transport.protocol", "imap");

        return props;
    }

    public void setPouzivatelDao(PouzivatelDao pouzivatelDao) {
        this.pouzivatelDao = pouzivatelDao;
    }

    public void setSutazRocnikDao(SutazRocnikDao sutazRocnikDao) {
        this.sutazRocnikDao = sutazRocnikDao;
    }

    public void setTipNaZapasDao(TipNaZapasDao tipNaZapasDao) {
        this.tipNaZapasDao = tipNaZapasDao;
    }
}