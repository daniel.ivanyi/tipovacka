package sk.di.tipovacka.impl.dao;

import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import sk.di.tipovacka.api.db.dao.TipNaVitazaDao;
import sk.di.tipovacka.api.db.TipNaVitaza;

import java.util.List;

/**
 * Implementacia dao.
 *
 * User: Dano
 * Date: 16.5.2013
 */
public class TipNaVitazaDaoImpl extends EntityDaoImpl<TipNaVitaza> implements TipNaVitazaDao {

    @Override
    @SuppressWarnings("unchecked")
    public List<TipNaVitaza> getTipyNaVitaza(Long sutazRocnikId) {

        DetachedCriteria criteria = DetachedCriteria.forClass(TipNaVitaza.class)
                .add(Restrictions.eq("sutazRocnikId", sutazRocnikId));

        return getHibernateTemplate().findByCriteria(criteria);
    }

    @Override
    @SuppressWarnings("unchecked")
    public TipNaVitaza getTipyNaVitazaRocnikaPodlaLogin(Long pouzivatelId, Long rocnikId) {

        DetachedCriteria criteria = DetachedCriteria.forClass(TipNaVitaza.class)
                .add(Restrictions.eq("pouzivatelId", pouzivatelId))
                .add(Restrictions.eq("sutazRocnikId", rocnikId));

        List<TipNaVitaza> result = getHibernateTemplate().findByCriteria(criteria);

        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<TipNaVitaza> getTipyNaVitazaPodlaLogin(Long pouzivatelId) {

        DetachedCriteria criteria = DetachedCriteria.forClass(TipNaVitaza.class)
                .add(Restrictions.eq("pouzivatelId", pouzivatelId));

        return getHibernateTemplate().findByCriteria(criteria);
    }
}
