package sk.di.tipovacka.impl.util;

import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import sk.di.tipovacka.api.dto.MuzstvoDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.service.MuzstvoService;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * Updater pre muzstvo v sutazi
 *
 * User: IVAN1497
 * Date: 31.10.2013
 */
public class MuzstvoUpdater {

    private final static Logger logger = Logger.getLogger(MuzstvoUpdater.class);

    public static void main(String[] args) {
        ApplicationContext applicationContext = new ClassPathXmlApplicationContext("/applicationContext.xml");

        MuzstvoService muzstvoService = (MuzstvoService) applicationContext.getBean("muzstvoService");

        MuzstvoUpdater updater = new MuzstvoUpdater();

        Set<String> kody = updater.getKodyMuzstiev(muzstvoService.getMuzstva());
        Map<String, String> kodyKrajin = updater.getKodyKrajin("kodKrajiny.txt");
        List<MuzstvoDto> chybajuceMuzstva = new ArrayList<>();

        for (String kod : kodyKrajin.keySet()) {
            if (!kody.contains(kod)) {
                MuzstvoDto m = new MuzstvoDto();
                m.setKod(kod);
                m.setNazov(kodyKrajin.get(kod));
                m.setStatus(EntityStatusEnum.AKTIVNY);

                chybajuceMuzstva.add(m);
            }
        }
        // nasypeme do db
        for (MuzstvoDto m : chybajuceMuzstva) {
            muzstvoService.addMuzstvo("admin", m);
            System.out.println("muzstvo[" + m.getNazov() + ", " + m.getKod() + "] bolo pridane");
        }
    }

    /**
     * Natiahne kody a nazov krajin zo zadaneho subora
     *
     * @return
     */
    private Map<String, String> getKodyKrajin(String fileName) {
        Map<String, String> mapa = new HashMap<>();

        BufferedReader br = new BufferedReader(new InputStreamReader(ParseUtils
                .class
                .getClassLoader()
                .getResourceAsStream(fileName)));

        String line;
        try {
            while ((line = br.readLine()) != null) {
                String kod = line.substring(0, 3);
                String nazov = line.substring(4);
                mapa.put(kod, nazov);
            }
        } catch (IOException e) {
            logger.error("nepodarilo sa nacitat subor[" + fileName + "] s kodmi krajin, ", e);
        } finally {
            try {
                br.close();
            } catch (IOException ignored) { }
        }

        return mapa;
    }

    /**
     * Vrati kody zo zoznamu muzstiev.
     *
     * @param muzstva
     * @return
     */
    private Set<String> getKodyMuzstiev(List<MuzstvoDto> muzstva) {
        Set<String> kody = new HashSet<>();
        for (MuzstvoDto m : muzstva) {
            kody.add(m.getKod());
        }

        return kody;
    }
}
