package sk.di.tipovacka.impl.service;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.db.dao.PouzivatelDao;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;
import sk.di.tipovacka.api.db.dao.ZapasDao;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException;
import sk.di.tipovacka.api.service.TipNaZapasService;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implementacia service.
 *
 * User: Dano
 * Date: 10.5.2013
 */
public class TipNaZapasServiceImpl implements TipNaZapasService {

    private final Logger logger = Logger.getLogger(getClass());

    private ZapasDao zapasDao;
    private TipNaZapasDao tipNaZapasDao;
    private PouzivatelDao pouzivatelDao;

    @Override
    public Long addTipNaZapas(String autor, TipNaZapasDto tipNaZapas) throws CasNaTipovanieVyprsalException {
        return addTipNaZapas(autor, tipNaZapas, false);
    }

    @Override
    public synchronized Long addTipNaZapas(String autor,
                                           TipNaZapasDto tipNaZapas,
                                           boolean admin) throws CasNaTipovanieVyprsalException, EntityNotFoundException {
        logger.info("addTipNaZapas(autor=" + autor
                + ", tipNaZapas=" + (tipNaZapas == null ? "null" : tipNaZapas.toText())
                + ", admin=" + admin + ")");
        if(tipNaZapas == null) {
            logger.error("tipNaZapas nemoze byt null");
            throw new IllegalArgumentException("tipNaZapas nemoze byt null");
        }
        if (tipNaZapas.getZapas() == null) {
            logger.error("zapas nemoze byt null");
            throw new IllegalArgumentException("zapas nemoze byt null");
        }

        // overime, ci zadavame tip na realny zapas
        Zapas zapas = zapasDao.get(tipNaZapas.getZapas().getId());
        if (zapas == null) {
            logger.error("zapas id[" + tipNaZapas.getZapas().getId() + "] nebol najdeny");
            throw new IllegalArgumentException("zapas id[" + tipNaZapas.getZapas().getId() + "] nebol najdeny");
        }
        // overime, ci pouzivatel pridal tip na zapas
        for (TipNaZapas tip : tipNaZapasDao.getTipyNaZapas(zapas.getId())) {
            if (tip.getPouzivatel().getId().equals(tipNaZapas.getPouzivatel().getId())) {
                logger.warn("pouzivatel[login=" + tipNaZapas.getPouzivatel().getLogin() + "] uz ma natipovany zapas[id=" + tipNaZapas.getZapas().getId() + "]");
                throw new IllegalArgumentException("pouzivatel[login=" + tipNaZapas.getPouzivatel().getLogin() + "] uz ma natipovany zapas[id=" + tipNaZapas.getZapas().getId() + "]");
            }
        }

        // ak tip nepridava admin, overime, ci sa nepokusame zadat tip po zaciatku zapasu
        if (!admin) {
            if (zapas.getDatum().before(new Date())) {
                logger.warn("cas pre tipovanie zapasu[" + zapas.getDomaci().getKod() + ":" + zapas.getHostia().getKod() + "] vyprsal[" + zapas.getDatum() + "]");
                throw new CasNaTipovanieVyprsalException("cas pre tipovanie zapasu[" + zapas.getDomaci().getKod() + ":" + zapas.getHostia().getKod() + "] vyprsal[" + zapas.getDatum() + "]");
            }
        }

        TipNaZapas t = new TipNaZapas();
        t.setDatum(new Date());
        t.setPouzivatelId(tipNaZapas.getPouzivatel().getId());
        t.setPouzivatel(pouzivatelDao.get(t.getPouzivatelId()));
        t.setZapasId(tipNaZapas.getZapas().getId());
        t.setZapas(zapasDao.get(t.getZapasId()));
        t.setGolyDomaci(tipNaZapas.getGolyDomaci());
        t.setGolyHostia(tipNaZapas.getGolyHostia());
        t.setNahodnyTip(tipNaZapas.isNahodnyTip());

        // ulozime
        Long tipNaZapasId = tipNaZapasDao.save(t);
        logger.info("tipNaZapas[" + t + "] bol zadany");

        return tipNaZapasId;
    }

    @Override
    public void editTipNaZapas(String autor, TipNaZapasDto tipNaZapas) throws CasNaTipovanieVyprsalException, EntityNotFoundException {
        editTipNaZapas(autor, tipNaZapas, false);
    }

    @Override
    public void editTipNaZapas(String autor, TipNaZapasDto tipNaZapas, boolean admin) throws CasNaTipovanieVyprsalException, EntityNotFoundException {
        logger.info("editTipNaZapas(autor=" + autor
                + ", tipNaZapas=" + (tipNaZapas == null ? "null" : tipNaZapas.toText())
                + ", admin=" + admin + ")");
        if(tipNaZapas == null) {
            logger.error("tipNaZapas nemoze byt null");
            throw new IllegalArgumentException("tipNaZapas nemoze byt null");
        }
        if (tipNaZapas.getZapas() == null) {
            logger.error("zapas nemoze byt null");
            throw new IllegalArgumentException("zapas nemoze byt null");
        }

        // overime, ci editujeme tip na realny zapas
        Zapas zapas = zapasDao.get(tipNaZapas.getZapas().getId());
        if (zapas == null) {
            logger.error("zapas id[" + tipNaZapas.getZapas().getId() + "] nebol najdeny");
            throw new IllegalArgumentException("zapas id[" + tipNaZapas.getZapas().getId() + "] nebol najdeny");
        }

        // ak zmenu nevyvola admin, overime, ci sa nepokusa editovat uz hrany zapas
        if (!admin) {
            // overime, ci sa nepokusame editovat tip po zaciatku zapasu
            if (zapas.getDatum().before(new Date())) {
                logger.warn("cas pre tipovanie zapasu[" + zapas.getDomaci().getKod() + ":" + zapas.getHostia().getKod() + "] vyprsal[" + zapas.getDatum() + "]");
                throw new CasNaTipovanieVyprsalException("cas pre tipovanie zapasu[" + zapas.getDomaci().getKod() + ":" + zapas.getHostia().getKod() + "] vyprsal[" + zapas.getDatum() + "]");
            }
        }

        // overime, ci editujeme realny tip
        TipNaZapas t = tipNaZapasDao.get(tipNaZapas.getId());
        if (t == null) {
            logger.warn("tip[id=" + tipNaZapas.getId() + "] nebol najdeny");
            throw new EntityNotFoundException("tip[id=" + tipNaZapas.getId() + "] nebol najdeny");
        }

        t.setDatum(new Date());
        t.setPouzivatelId(tipNaZapas.getPouzivatel().getId());
        t.setPouzivatel(pouzivatelDao.get(t.getPouzivatelId()));
        t.setZapasId(tipNaZapas.getZapas().getId());
        t.setZapas(zapasDao.get(t.getZapasId()));
        t.setGolyDomaci(tipNaZapas.getGolyDomaci());
        t.setGolyHostia(tipNaZapas.getGolyHostia());
        t.setNahodnyTip(tipNaZapas.isNahodnyTip());
        t.setBodyZaVitaza(tipNaZapas.getBodyZaVitaza());
        t.setBodyZaGolyDomaci(tipNaZapas.getBodyZaGolyDomaci());
        t.setBodyZaGolyHostia(tipNaZapas.getBodyZaGolyHostia());

        tipNaZapasDao.saveOrUpdate(t);
        logger.info("tipNaZapas[" + t + "] bol zmeneny");
    }

    @Override
    public TipNaZapasDto getTipNaZapas(Long tipNaZapasId) {
        logger.info("getTipNaZapas(tipNaZapasId=" + tipNaZapasId + ")");
        Assert.notNull(tipNaZapasId, "tipNaZapasId nemoze byt null");

        return TipNaZapasDto.create(tipNaZapasDao.get(tipNaZapasId));
    }

    @Override
    public List<TipNaZapasDto> getTipyNaZapasPodlaZapasId(List<Long> zapasIds) {
        logger.info("getTipyNaZapasPodlaZapasId(zapasIds=" + zapasIds + ")");

        // vyskladame si, ze chceme dohladavat tipy pre konkretne zapasy
        TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
        kriteria.getZapasIds().addAll(zapasIds);

        List<TipNaZapasDto> tipy = new ArrayList<>();
        for (TipNaZapas t : tipNaZapasDao.findTipyNaZapas(kriteria)) {
            tipy.add(TipNaZapasDto.create(t));
        }

        return tipy;
    }

    @Override
    public List<TipNaZapasDto> getTipyNaZapas() {
        logger.info("getTipyNaZapas()");

        List<TipNaZapasDto> tipy = new ArrayList<>();
        for (TipNaZapas t : tipNaZapasDao.findAll()) {
            tipy.add(TipNaZapasDto.create(t));
        }

        return tipy;
    }

    @Override
    public List<TipNaZapasDto> findTipyNaZapas(TipNaZapasKriteria kriteria) {
        logger.info("findTipyNaZapas(kriteria=" + kriteria + ")");
        Assert.notNull(kriteria, "kriteria nemozu byt null");

        List<TipNaZapasDto> tipy = new ArrayList<>();
        for (TipNaZapas t : tipNaZapasDao.findTipyNaZapas(kriteria)) {
            tipy.add(TipNaZapasDto.create(t));
        }

        return tipy;
    }

    @Override
    public List<PouzivatelDto> getTipujucichNaZapas(Long rocnikId) {
        logger.info("getTipujucichNaZapas(rocnikId=" + rocnikId + ")");
        List<PouzivatelDto> tiperi = new ArrayList<>();
        for (Pouzivatel p : tipNaZapasDao.getTipujucichNaZapas(rocnikId)) {
            tiperi.add(PouzivatelDto.create(p));
        }
        return tiperi;
    }

    @Override
    public List<String> getNajlepsieTipujuciVitazaZapasu(Long rocnikId) {
        logger.info("getNajlepsieTipujuciVitazaZapasu(rocnikId=" + rocnikId+ ")");

        return tipNaZapasDao.findNajlepsiTipujuciNaVitazaZapasu(rocnikId);
    }

    @Override
    public List<String> getNajlepsieTipujuciSkoreZapasu(Long rocnikId) {
        logger.info("getNajlepsieTipujuciSkoreZapasu(rocnikId=" + rocnikId+ ")");

        return tipNaZapasDao.findNajlepsieTipujuciSkoreZapasu(rocnikId);
    }

    @Override
    public List<String> getNajlepsieNahodneTipujuci(Long rocnikId) {
        logger.info("getNajlepsieNahodneTipujuci(rocnikId=" + rocnikId+ ")");

        return tipNaZapasDao.findNajlepsieNahodneTipujuci(rocnikId);
    }

    public void setTipNaZapasDao(TipNaZapasDao tipNaZapasDao) {
        this.tipNaZapasDao = tipNaZapasDao;
    }

    public void setZapasDao(ZapasDao zapasDao) {
        this.zapasDao = zapasDao;
    }

    public void setPouzivatelDao(PouzivatelDao pouzivatelDao) {
        this.pouzivatelDao = pouzivatelDao;
    }
}
