package sk.di.tipovacka.impl.dao;

import org.apache.log4j.Logger;
import org.hibernate.criterion.CriteriaSpecification;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Projections;
import org.hibernate.criterion.Restrictions;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;

import javax.annotation.Resource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Implementacia dao.
 * <p>
 * User: Dano
 * Date: 10.5.2013
 */
public class TipNaZapasDaoImpl extends EntityDaoImpl<TipNaZapas> implements TipNaZapasDao {

    private final Logger logger = Logger.getLogger(getClass());

    @Resource
    private JdbcTemplate jdbcTemplate;

    @Override
    @SuppressWarnings("unchecked")
    public List<TipNaZapas> findTipyNaZapas(TipNaZapasKriteria kriteria) {
        DetachedCriteria criteria = DetachedCriteria.forClass(TipNaZapas.class);

        if (!kriteria.getZapasIds().isEmpty()) {
            criteria.add(Restrictions.in("zapasId", kriteria.getZapasIds()));
        }

        if (!kriteria.getPouzivatelIds().isEmpty()) {
            criteria.add(Restrictions.in("pouzivatelId", kriteria.getPouzivatelIds()));
        }

        if (kriteria.getSutazRocnikId() != null) {
            criteria.createAlias("zapas", "zapas", CriteriaSpecification.INNER_JOIN)
                    .add(Restrictions.eq("zapas.sutazRocnik.id", kriteria.getSutazRocnikId()));
        }

        return getHibernateTemplate().findByCriteria(criteria);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<TipNaZapas> getTipyNaZapas(Long zapasId) {
        DetachedCriteria criteria = DetachedCriteria.forClass(TipNaZapas.class)
                .add(Restrictions.eq("zapas.id", zapasId));

        return getHibernateTemplate().findByCriteria(criteria);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Pouzivatel> getTipujucichNaZapas(Long rocnikId) {

        DetachedCriteria criteria = DetachedCriteria.forClass(TipNaZapas.class)
                .createAlias("zapas", "zapas", CriteriaSpecification.INNER_JOIN)
                .add(Restrictions.eq("zapas.sutazRocnik.id", rocnikId))
                .setProjection(Projections.distinct(Projections.property("pouzivatel")));

        return getHibernateTemplate().findByCriteria(criteria);
    }

    @Override
    public List<String> findNajlepsiTipujuciNaVitazaZapasu(Long rocnikId) {
        String sqlMax = String.format(
                "select max(body) as max\n" +
                        "  from (\n" +
                        "    select sum(C_BODY_ZA_VITAZA) as body\n" +
                        "    from T_TIP_NA_ZAPAS as tz\n" +
                        "      join T_ZAPAS as za on za.C_ZAPAS_ID = tz.C_ZAPAS_ID\n" +
                        "      join T_SUTAZ_ROCNIK_SUHLAS as srs on srs.C_POUZIVATEL_ID = tz.C_POUZIVATEL_ID and srs.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "    where za.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "    and srs.C_ZAPLATENE is not null\n" +
                        "    group by tz.C_POUZIVATEL_ID\n" +
                        "  )", rocnikId, rocnikId);
        final Long max = jdbcTemplate.queryForLong(sqlMax);

        String sql = String.format(
                "select p.C_LOGIN, sum(C_BODY_ZA_VITAZA) as body\n" +
                        "from T_TIP_NA_ZAPAS as tz\n" +
                        "  join T_ZAPAS as za on za.C_ZAPAS_ID = tz.C_ZAPAS_ID\n" +
                        "  join T_POUZIVATEL as p on tz.C_POUZIVATEL_ID = p.C_POUZIVATEL_ID\n" +
                        "  join T_SUTAZ_ROCNIK_SUHLAS as srs on srs.C_POUZIVATEL_ID = tz.C_POUZIVATEL_ID and srs.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "where za.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "and srs.C_ZAPLATENE is not null\n" +
                        "group by tz.C_POUZIVATEL_ID", rocnikId, rocnikId);

        return parseNajlepsichTiperov(sql, max);
    }

    @Override
    public List<String> findNajlepsieTipujuciSkoreZapasu(Long rocnikId) {
        String sqlMax = String.format(
                "select max(body)\n" +
                        "from (\n" +
                        "  select (sum(C_BODY_ZA_GOLY_DOMACI) + sum(C_BODY_ZA_GOLY_HOSTIA)) as body\n" +
                        "  from T_TIP_NA_ZAPAS as tz\n" +
                        "    join T_ZAPAS as za on za.C_ZAPAS_ID = tz.C_ZAPAS_ID\n" +
                        "    join T_SUTAZ_ROCNIK_SUHLAS as srs on srs.C_POUZIVATEL_ID = tz.C_POUZIVATEL_ID and srs.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "  where za.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "  and srs.C_ZAPLATENE is not null\n" +
                        "  group by tz.C_POUZIVATEL_ID\n" +
                        ")", rocnikId, rocnikId);
        final Long max = jdbcTemplate.queryForLong(sqlMax);

        String sql = String.format(
                "select p.C_LOGIN, (sum(C_BODY_ZA_GOLY_DOMACI) + sum(C_BODY_ZA_GOLY_HOSTIA)) as body\n" +
                        "from T_TIP_NA_ZAPAS as tz\n" +
                        "  join T_ZAPAS as za on za.C_ZAPAS_ID = tz.C_ZAPAS_ID\n" +
                        "  join T_POUZIVATEL as p on tz.C_POUZIVATEL_ID = p.C_POUZIVATEL_ID\n"+
                        "  join T_SUTAZ_ROCNIK_SUHLAS as srs on srs.C_POUZIVATEL_ID = tz.C_POUZIVATEL_ID and srs.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "where za.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "and srs.C_ZAPLATENE is not null\n" +
                        "group by tz.C_POUZIVATEL_ID", rocnikId, rocnikId);

        return parseNajlepsichTiperov(sql, max);
    }

    @Override
    public List<String> findNajlepsieNahodneTipujuci(Long rocnikId) {
        String sqlMax = String.format(
                "select max(body)\n" +
                        "from (\n" +
                        "  select (sum(C_BODY_ZA_VITAZA) + sum(C_BODY_ZA_GOLY_DOMACI) + sum(C_BODY_ZA_GOLY_HOSTIA)) as body\n" +
                        "  from T_TIP_NA_ZAPAS as tz\n" +
                        "    join T_ZAPAS as za on za.C_ZAPAS_ID = tz.C_ZAPAS_ID\n" +
                        "    join T_SUTAZ_ROCNIK_SUHLAS as srs on srs.C_POUZIVATEL_ID = tz.C_POUZIVATEL_ID and srs.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "  where za.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "  and srs.C_ZAPLATENE is not null\n" +
                        "  and tz.C_NAHODNY_TIP = true\n" +
                        "  group by tz.C_POUZIVATEL_ID\n" +
                        ")", rocnikId, rocnikId);
        final Long max = jdbcTemplate.queryForLong(sqlMax);

        String sql = String.format(
                "select p.C_LOGIN, (sum(C_BODY_ZA_VITAZA) + sum(C_BODY_ZA_GOLY_DOMACI) + sum(C_BODY_ZA_GOLY_HOSTIA)) as body\n" +
                        "from T_TIP_NA_ZAPAS as tz\n" +
                        "  join T_ZAPAS as za on za.C_ZAPAS_ID = tz.C_ZAPAS_ID\n" +
                        "  join T_POUZIVATEL as p on tz.C_POUZIVATEL_ID = p.C_POUZIVATEL_ID\n"+
                        "  join T_SUTAZ_ROCNIK_SUHLAS as srs on srs.C_POUZIVATEL_ID = tz.C_POUZIVATEL_ID and srs.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "where za.C_SUTAZ_ROCNIK_ID = %d\n" +
                        "and tz.C_NAHODNY_TIP = true\n" +
                        "and srs.C_ZAPLATENE is not null\n" +
                        "group by tz.C_POUZIVATEL_ID", rocnikId, rocnikId);

        return parseNajlepsichTiperov(sql, max);
    }


    /**
     * Vyhlada najlesich tipujucich na vitaza zapasu, alebo na skore.
     *
     * @param sql aktualne sql
     * @param max maximalna hodnota pre dany select
     * @return zoznam pouzivatelov
     */
    private List<String> parseNajlepsichTiperov(String sql, final Long max) {
        final List<String> tiperi = new ArrayList<>();
        jdbcTemplate.query(sql, new RowMapper<Long>() {
            @Override
            public Long mapRow(ResultSet resultSet, int i) throws SQLException {
                Long body = resultSet.getLong("body");
                if (body.equals(max)) {
                    tiperi.add(resultSet.getString("C_LOGIN"));
                }

                return null;
            }
        });

        return tiperi;
    }
}
