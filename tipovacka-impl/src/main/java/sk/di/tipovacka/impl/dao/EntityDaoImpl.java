package sk.di.tipovacka.impl.dao;

import java.util.Map;

import org.hibernate.Criteria;
import org.hibernate.FetchMode;
import org.hibernate.HibernateException;
import org.hibernate.LockMode;
import org.hibernate.Session;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import org.hibernate.transform.DistinctRootEntityResultTransformer;
import org.springframework.orm.hibernate3.HibernateCallback;
import org.springframework.orm.hibernate3.support.HibernateDaoSupport;
import sk.di.tipovacka.api.db.Entity;
import sk.di.tipovacka.api.db.dao.EntityDao;
import sk.di.tipovacka.api.db.kriteria.VyhladavacieKriteria;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

/**
 * Hibernate Dao implementacia pre abstraktnu entitu
 * <p/>
 * User: Dano
 * Date: 9.5.2013
 */
public class EntityDaoImpl<T extends Entity> extends HibernateDaoSupport implements EntityDao<T> {

    private Class<T> persistentClass;

    @Override
    @SuppressWarnings("unchecked")
    protected void initDao() throws Exception {
        super.initDao();
        if (persistentClass == null) {
            // ak nie je nastavena perzistentna trieda, tak sa ju zistime z
            // aktualneho typu aktualnej triedy (toto funguje iba pre podtriedy
            // EntityDaoImpl, ktore zadefinuju konkretny typ pre parameter T)
            Type[] typeArguments = ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments();
            if (typeArguments.length > 0) {
                persistentClass = (Class<T>) typeArguments[0];
            } else {
                throw new IllegalStateException(
                        "persistentClass parameter has to be set!");
            }
        }
    }

    @SuppressWarnings("unchecked")
    @Override
    public T get(long id) {
        return (T) getHibernateTemplate().get(persistentClass, id);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T get(long id, Map<String, FetchMode> associationFetchModeMap) {

        //ak nemame ziadne asociacie, tak volame klasicky get
        if (associationFetchModeMap == null || associationFetchModeMap.isEmpty()) {
            return get(id);
        }

        //criteria
        DetachedCriteria criteria = DetachedCriteria.forClass(persistentClass);
        criteria.add(Restrictions.eq("id", id));

        //nasetujeme si asociacie ktore fetchnut 
        for (String a : associationFetchModeMap.keySet()) {
            FetchMode fm = associationFetchModeMap.get(a);
            criteria.setFetchMode(a, fm);
            criteria.setResultTransformer(DistinctRootEntityResultTransformer.INSTANCE);
        }

        //vyhladame si
        List<T> list = getHibernateTemplate().findByCriteria(criteria);

        if (list != null && list.size() > 1) {
            throw new IllegalStateException("found more than one object for id='" + id + "!");
        }

        return list != null && !list.isEmpty() ? list.get(0) : null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<T> get(List<Long> ids) {
        if (ids.isEmpty()) {
            return Collections.EMPTY_LIST;
        }
        DetachedCriteria criteria = DetachedCriteria.forClass(persistentClass).add(Restrictions.in("id", ids));
        List list = getHibernateTemplate().findByCriteria(criteria);
        return list;
    }

    @Override
    @SuppressWarnings("unchecked")
    public T load(long id) {
        return (T) getHibernateTemplate().load(persistentClass, id);
    }

    @Override
    public void lock(T entity) {
        getHibernateTemplate().lock(entity, LockMode.UPGRADE);
    }

    @Override
    @SuppressWarnings("unchecked")
    public T merge(T entity) {
        return (T) getHibernateTemplate().merge(entity);
    }

    @Override
    public Long save(T entity) {
        return (Long) getHibernateTemplate().save(entity);
    }

    @Override
    public void saveOrUpdate(T entity) {
        getHibernateTemplate().saveOrUpdate(entity);
    }

    @Override
    public void delete(T entity) {
        getHibernateTemplate().delete(entity);
        getHibernateTemplate().flush();
        getHibernateTemplate().evict(entity);

    }

    @Override
    public void delete(long id) {
        Entity e = load(id);
        getHibernateTemplate().delete(e);
        getHibernateTemplate().flush();
        getHibernateTemplate().evict(e);
    }

    @SuppressWarnings("unchecked")
    protected List findByCriteria(final DetachedCriteria detachedCriteria,
                                  final boolean cacheable) {
        return getHibernateTemplate().executeFind(new HibernateCallback() {

            @Override
            public Object doInHibernate(Session session)
                    throws HibernateException, SQLException {
                Criteria criteria = detachedCriteria.getExecutableCriteria(
                        session).setCacheable(cacheable);
                Object result = criteria.list();
                return result;
            }
        });
    }

    @Override
    public void flush() {
        getHibernateTemplate().flush();
    }

    public List<T> findAll() {
        DetachedCriteria criteria = DetachedCriteria.forClass(persistentClass);
        List list = getHibernateTemplate().findByCriteria(criteria);

        return list;
    }

    public void setPersistentClass(Class<T> persistentClass) {
        this.persistentClass = persistentClass;
    }

    /**
     * Prida defaultne vyhladavacie kriteria do detached criterii
     *
     * @param criteria             detached criteria
     * @param vyhladavacieKriteria vyhladvacie kriteria
     */
    protected void addDefaultVyhladavacieKriteria(DetachedCriteria criteria, VyhladavacieKriteria vyhladavacieKriteria) {
        // defaultne kriteria
        if (vyhladavacieKriteria.getSutazRocnikId() != null && !vyhladavacieKriteria.getSutazRocnikId().equals(0L)) {
            criteria = criteria.add(Restrictions.eq("sutazRocnikId", vyhladavacieKriteria.getSutazRocnikId()));
        }
        if (vyhladavacieKriteria.getDatumOd() != null) {
            criteria = criteria.add(Restrictions.ge("datum", vyhladavacieKriteria.getDatumOd()));
        }
        if (vyhladavacieKriteria.getDatumDo() != null) {
            criteria = criteria.add(Restrictions.le("datum", vyhladavacieKriteria.getDatumDo()));
        }

        // sortovanie
        switch (vyhladavacieKriteria.getOrderDirection()) {
            case DESC:
                criteria.addOrder(Order.desc(vyhladavacieKriteria.getOrderBy()));
                break;
            case ASC:
                criteria.addOrder(Order.asc(vyhladavacieKriteria.getOrderBy()));
                break;
        }
    }
}
