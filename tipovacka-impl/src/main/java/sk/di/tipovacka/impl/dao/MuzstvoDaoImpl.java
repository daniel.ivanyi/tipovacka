package sk.di.tipovacka.impl.dao;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import sk.di.tipovacka.api.db.dao.MuzstvoDao;
import sk.di.tipovacka.api.db.Muzstvo;
import sk.di.tipovacka.api.dto.MuzstvoDto;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Implementacia dao
 *
 * User: Dano
 * Date: 10.5.2013
 */
public class MuzstvoDaoImpl extends EntityDaoImpl<Muzstvo> implements MuzstvoDao {

    private final Logger logger = Logger.getLogger(getClass());

    @Override
    @SuppressWarnings("unchecked")
    public Muzstvo getMuzstvoPodlaKodu(String kod) {
//        logger.debug("getMuzstvoPodlaKodu(kod=" + kod + ")");

        DetachedCriteria criteria = DetachedCriteria.forClass(Muzstvo.class)
                .add(Restrictions.eq("kod", kod));

        List<Muzstvo> result = getHibernateTemplate().findByCriteria(criteria);

        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Muzstvo getMuzstvoPodlaNazvu(String nazov) {
        if ("SLOVENSKO".equals(nazov)) {
            nazov = "Slovensko";
        }
//        logger.debug("getMuzstvoPodlaNazvu(nazov=" + nazov + ")");

        DetachedCriteria criteria = DetachedCriteria.forClass(Muzstvo.class)
                .add(Restrictions.eq("nazov", nazov));

        List<Muzstvo> result = getHibernateTemplate().findByCriteria(criteria);

        return result.isEmpty() ? null : result.get(0);
    }
}
