package sk.di.tipovacka.impl.job;

import org.apache.log4j.Logger;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.quartz.QuartzJobBean;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.service.MailService;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.TipNaZapasService;
import sk.di.tipovacka.api.service.ZapasService;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Trieda zabezpecujuca kontrolu, ci pouzivatel natipoval vsetky zapasy, ktore sa hraju v pracovny den a cas.
 *
 * User: Dano
 * Date: 4.3.2014
 */
public class KontrolaNezadanychTipovJob extends QuartzJobBean implements Serializable {
    private static final long serialVersionUID = -1921856904316611180L;

    private final Logger logger = Logger.getLogger(getClass());

    private final SimpleDateFormat formatDatumu = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    private final List<String> statneSviatky =
            Arrays.asList("1.1","6.1","1.5","8.5","5.7","29.8","1.9","15.9","1.11","17.11","24.12","25.12","26.12");

    @Value("${url.app.home}")
    private String urlAppHome;
    @Value("${mail.zapasy.nenatipovane}")
    private String mailNenatipovaneZapasyText;

    private SutazRocnikService sutazRocnikService;
    private ZapasService zapasService;
    private TipNaZapasService tipNaZapasService;
    private MailService mailService;

    @Override
    protected void executeInternal(JobExecutionContext jobExecutionContext) throws JobExecutionException {
        jobRun();
    }

    public synchronized void jobRun() {
        logger.debug("jobRun() started ...");
        // vyhladame zapasy od 12.00 do 12.00 prveho nasledujuceho pracovneho dna
        Calendar datumOd = Calendar.getInstance();
        datumOd.set(Calendar.HOUR_OF_DAY, 12);
        datumOd.set(Calendar.MINUTE, 0);
        datumOd.set(Calendar.SECOND, 0);
        datumOd.set(Calendar.MILLISECOND, 0);

        Calendar datumDo = getDalsiPracovnyDen(Calendar.getInstance());
        datumDo.set(Calendar.HOUR_OF_DAY, 12);
        datumDo.set(Calendar.MINUTE, 0);
        datumDo.set(Calendar.SECOND, 0);
        datumDo.set(Calendar.MILLISECOND, 0);

        // vyskladame kriteria
        ZapasKriteria kriteria = new ZapasKriteria();
        kriteria.setDatumOdPresne(datumOd.getTime());
        kriteria.setDatumDoPresne(datumDo.getTime());
        // prebehneme zapasy a vyberieme zapasy, na ktore malo byt natipovane
        List<Long> zapasIds = new ArrayList<>();
        List<ZapasDto> zapasy = zapasService.findZapasy(kriteria);
        Map<Long, ZapasDto> mapaZapasov = new HashMap<>();
        for (ZapasDto zapas : zapasy) {
            // zaujimaju nas zapasy, ktore maju nadefinovanych superov
            if (zapas.getDomaci() != null && zapas.getHostia() != null) {
                // aby sme nenotifikovali aj po zacati zapasu, tak do zoznamu dame len tie zapasy, ktore este nezacali
                if (zapas.getDatum().before(new Date())) {
                    zapasIds.add(zapas.getId());
                    mapaZapasov.put(zapas.getId(), zapas);
                }
            }
        }
        if (mapaZapasov.isEmpty()) {
            logger.info("netreba mat natipovane na ziaden zapas");
            logger.debug("jobRun() ... ended");

            return;
        } else {
            logger.info("treba mat natipovane na " + mapaZapasov.size() + " zapasov " + zapasIds);
        }

        // vyberieme tipy a nasypeme ich do mapy, kde klucik je pouzivatel
        // a hodnota je zoznam ID zapasov, ktore natipoval
        TipNaZapasKriteria tipyKriteria = new TipNaZapasKriteria();
        tipyKriteria.setZapasIds(zapasIds);
        Map<Long, List<Long>> mapaTipov = new HashMap<>();
        for (TipNaZapasDto tip : tipNaZapasService.findTipyNaZapas(tipyKriteria)) {
            if (mapaTipov.get(tip.getPouzivatel().getId()) == null) {
                mapaTipov.put(tip.getPouzivatel().getId(), new ArrayList<Long>());
            }
            mapaTipov.get(tip.getPouzivatel().getId()).add(tip.getZapas().getId());
        }

        for (RocnikSutazeDto rocnik : sutazRocnikService.getAktualnyRocnikSutaze()) {
            // vysledna mapa, kde klucik je pouzivatel a hodnota je zoznam zapasov,
            // na ktore dany pouzivatel nenatipoval
            Map<PouzivatelDto, List<ZapasDto>> nenatipovaneZapasy = new HashMap<>();
            // vyberieme si pouzivatelov, ktori sa prihlasili do rocnika sutaze a mali by mat natipovane
            for (PouzivatelDto p : tipNaZapasService.getTipujucichNaZapas(rocnik.getId())) {
                // prebehneme mapu tipov na zapas podla pouzivatela
                // a nasetujeme pouzivatelov nenatipovane zapasy
                if (mapaTipov.get(p.getId()) != null) {
                    // prebehneme zapasy
                    for (Long zapasId : mapaZapasov.keySet()) {
                        // overime, ci pouzivatel tipoval na dany zapas
                        if (!mapaTipov.get(p.getId()).contains(zapasId)) {
                            if (nenatipovaneZapasy.get(p) == null) {
                                nenatipovaneZapasy.put(p, new ArrayList<ZapasDto>());
                            }
                            nenatipovaneZapasy.get(p).add(mapaZapasov.get(zapasId));
                        }
                    }
                } else {
                    List<ZapasDto> zz = new ArrayList<>();
                    zz.addAll(mapaZapasov.values());
                    nenatipovaneZapasy.put(p, zz);
                }
            }

            // ak mame nenatipovane zapasy posleme notifikacny mail pouzivatelom
            if (!nenatipovaneZapasy.isEmpty()) {
                for (PouzivatelDto p : nenatipovaneZapasy.keySet()) {
                    logger.info("pouzivatel[login=" + p.getLogin() + "] ma " + nenatipovaneZapasy.get(p).size() + " nenatipovane zapasy");
                    // vyskladame text
                    String text = String.format(mailNenatipovaneZapasyText + "\r\n",
                            p.getLogin(),
                            urlAppHome);
                    StringBuilder sb = new StringBuilder();
                    sb.append(text);
                    for (ZapasDto z : nenatipovaneZapasy.get(p)) {
                        sb.append(String.format("tip [%s] %s (? - ?) %s<br>\r\n",
                                getNaformatovanyDatum(z.getDatum()),
                                z.getDomaci().getKodMuzstva(),
                                z.getHostia().getKodMuzstva()));
                    }
                    sb.append("\r\n<br>Prajeme veľa správnych tipov.");

                    // a odosleme mail
                    mailService.sendMail(p.getMail(),
                            "tipovacka - nenatipovane zapasy",
                            sb.toString(),
                            null,
                            null,
                            null);
                    logger.info("mail[adresa=" + p.getMail()
                            + ", predmet=tipovacka - nenatipovane zapasy"
                            + ", text=" + sb.toString() + "]");
                }
            }
        }

        logger.debug("jobRun() ... ended");
    }

    /**
     * Vrati dalsi pracujuci den od zadaneho datumu.
     *
     * @param datum datum, od ktoreho hladame dalsi nasledujuci pracovny den
     * @return      dalsi pracovny den
     */
    private Calendar getDalsiPracovnyDen(Calendar datum) {
        for ( ;; ) {
            // pridame den
            datum.add(Calendar.DAY_OF_MONTH, 1);
            if (datum.get(Calendar.DAY_OF_WEEK) != Calendar.SATURDAY
                && datum.get(Calendar.DAY_OF_WEEK) != Calendar.SUNDAY
                && !isStatnySviatok(datum)) {
                return datum;
            }
        }
    }

    /**
     * Overi, ci dany datum spadol do statnych sviatkov.
     *
     * @param datum datum
     * @return priznak, ci je dany datum statny sviatok
     */
    private boolean isStatnySviatok(Calendar datum) {
        return isVelkyPiatok(datum)
                || isVelkyPondelok(datum)
                || statneSviatky.contains(datum.get(Calendar.DAY_OF_MONTH) + "." + (datum.get(Calendar.MONTH) + 1));
    }

    /**
     * Overenie, ci zadany datum nie je velky piatok
     *
     * @param c kontrolovany datum
     * @return priznak vysledku
     */
    private boolean isVelkyPiatok(Calendar c) {
        // zaujima nas iba piatok
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.FRIDAY) {
            // pridame 3 dni a checkneme na velky pondelok
            Calendar velkyPiatok = (Calendar) c.clone();
            velkyPiatok.add(Calendar.DAY_OF_MONTH, 3);

            return isVelkyPondelok(velkyPiatok);
        }

        return false;
    }

    /**
     * Overenie, ci zadany datum nie je velky pondelok.
     *
     * @param c kontrolovany datum
     * @return priznak vysledku
     */
    private boolean isVelkyPondelok(Calendar c) {
        if (c.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY) {
            // pre istotu vyresetujeme casovu zlozku
            c.set(Calendar.HOUR_OF_DAY, 0);
            c.set(Calendar.MINUTE, 0);
            c.set(Calendar.SECOND, 0);
            c.set(Calendar.MILLISECOND, 0);

            return c.equals(getVelkonocnyPondelok(c.get(Calendar.YEAR)));
        }

        return false;
    }

    /**
     * Vrati datum kedy bude Velka noc v aktualnom roku.
     * podla http://astroportal.sk/kalendar/velka_noc.html
     *
     * @param rok aktualny rok
     * @return datum Velkej noci
     */
    private Calendar getVelkonocnyPondelok(int rok) {

        int c = rok / 100;
        int n = rok - 19 * (rok / 19);
        int k = ((c - 17) / 25);
        int i1 = c - (c / 4) - ((c - k) / 3) + 19 * n + 15;
        int i2 = i1 - 30 * (i1 / 30);
        int i3 = i2 - (i2 / 28) * (1 - (i2 / 28) * (29 / (i2 + 1)) * ((21 - n) / 11));
        int j1 = rok + (rok / 4) + i3 + 2 - c + (c / 4);
        int j2 = j1 - 7 * (j1 / 7);
        int l = i3 - j2;

        int mesiac = 3 + ((l + 40) / 44);
        int den = l + 28 - 31 * (mesiac / 4);
        Calendar velkonocnyPondelok = Calendar.getInstance();
        // vyresetujeme casovu zlozku, den zacina prvou milisecundou
        velkonocnyPondelok.set(rok, mesiac - 1, den + 1, 0, 0, 0);
        velkonocnyPondelok.set(Calendar.MILLISECOND, 0);

        return velkonocnyPondelok;
    }

    /**
     * Naformatuje datum.
     *
     * @param datum datum
     * @return text
     */
    private synchronized String getNaformatovanyDatum(Date datum) {
        if (datum == null) {
            return "";
        }

        try {
            return formatDatumu.format(datum);
        } catch (Exception e) {
            return "";
        }
    }

    public void setZapasService(ZapasService zapasService) {
        this.zapasService = zapasService;
    }

    public void setTipNaZapasService(TipNaZapasService tipNaZapasService) {
        this.tipNaZapasService = tipNaZapasService;
    }

    public void setSutazRocnikService(SutazRocnikService sutazRocnikService) {
        this.sutazRocnikService = sutazRocnikService;
    }

    public void setMailService(MailService mailService) {
        this.mailService = mailService;
    }
}
