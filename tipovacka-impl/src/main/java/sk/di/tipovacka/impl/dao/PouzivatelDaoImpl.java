package sk.di.tipovacka.impl.dao;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Restrictions;
import sk.di.tipovacka.api.exception.EntityUniqueExeption;
import sk.di.tipovacka.api.db.dao.PouzivatelDao;
import sk.di.tipovacka.api.db.Pouzivatel;

import java.util.ArrayList;
import java.util.List;

/**
 * Implemetacia pouzivatela dao.
 *
 * User: Dano
 * Date: 9.5.2013
 */
public class PouzivatelDaoImpl extends EntityDaoImpl<Pouzivatel> implements PouzivatelDao {

    private final Logger logger = Logger.getLogger(getClass());

    @Override
    @SuppressWarnings("unchecked")
    public Pouzivatel getPouzivatelPodlaLoginu(String login) {
        logger.debug("getPouzivatelPodlaLoginu(login=" + login + ")");

        // vyskladame vyhladavacie kriteria
        DetachedCriteria criteria = DetachedCriteria.forClass(Pouzivatel.class)
                .add(Restrictions.eq("login", login));
        // dohladame
        List<Pouzivatel> pouzivatels = getHibernateTemplate().findByCriteria(criteria);
        // overime jedinecnost
        if (pouzivatels.size() > 1) {
            logger.warn("pre hladany login[" + login + "] existuje viac ako jeden pouzivatel");
            throw new EntityUniqueExeption("pre hladany login[" + login + "] existuje viac ako jeden pouzivatel");
        }

        // a vratime najdenca
        return pouzivatels.isEmpty() ? null : pouzivatels.get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public Pouzivatel getPouzivatelPodlaMailu(String mail) throws EntityUniqueExeption {
        logger.debug("getPouzivatelPodlaMailu(mail=" + mail + ")");

        // dohladame
        List<Pouzivatel> pouzivatels = new ArrayList<>();
        for (Pouzivatel p : findAll()) {
            if (p.getMail().equalsIgnoreCase(mail)) {
                pouzivatels.add(p);
            }
        }
        // overime jedinecnost
        if (pouzivatels.size() > 1) {
            logger.warn("pre hladany mail[" + mail + "] existuje viac ako jeden pouzivatel");
            throw new EntityUniqueExeption("pre hladany mail[" + mail + "] existuje viac ako jeden pouzivatel");
        }

        // a vratime najdenca
        return pouzivatels.isEmpty() ? null : pouzivatels.get(0);
    }
}
