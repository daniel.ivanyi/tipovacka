package sk.di.tipovacka.impl.service;

import org.apache.log4j.Logger;
import org.springframework.util.Assert;
import sk.di.tipovacka.api.db.dao.*;
import sk.di.tipovacka.api.db.TipNaVitaza;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.dto.TipNaVitazaDto;
import sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException;
import sk.di.tipovacka.api.service.TipNaVitazaService;

import javax.persistence.EntityNotFoundException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Implemtacia service pre pracu s tipom na vitaza.
 * <p/>
 * User: Dano
 * Date: 16.5.2013
 */
public class TipNaVitazaServiceImpl implements TipNaVitazaService {

    private final Logger logger = Logger.getLogger(getClass());

    private TipNaVitazaDao tipNaVitazaDao;
    private ZapasDao zapasDao;
    private SutazRocnikDao sutazRocnikDao;
    private MuzstvoDao muzstvoDao;
    private PouzivatelDao pouzivatelDao;

    @Override
    public Long addTipNaVitaza(String autor, TipNaVitazaDto tipNaVitaza) throws CasNaTipovanieVyprsalException {
        logger.info("addTipNaVitaza(autor=" + autor + ", tipNaVitaza=" + tipNaVitaza + ")");

        Assert.notNull(tipNaVitaza.getPouzivatel(), "pouzivatel nemoze byt null");
        Assert.notNull(tipNaVitaza, "tipNaVitaza nemoze byt null");
        Assert.notNull(tipNaVitaza.getRocnikSutaze(), "tipNaVitaza.sutazRocnika nemoze byt null");
        Assert.notNull(tipNaVitaza.getMuzstvo(), "tipNaVitaza.muzstvo nemoze byt null");

        // overime, ci sa nezacal prvy zapas rocnika sutaze
//        Zapas prvyZapas = zapasDao.getPvyZapasRocnikaSutaze(tipNaVitaza.getRocnikSutaze().getId());
//        if (prvyZapas.getDatum().before(new Date())) {
//            logger.warn("Cas pre tipovanie vitaza vyprsal");
//            throw new CasNaTipovanieVyprsalException("Cas pre tipovanie vitaza vyprsal");
//        }
        TipNaVitaza tip = new TipNaVitaza();
        try {
            tip.setDatum(new Date());
            tip.setPouzivatelId(tipNaVitaza.getPouzivatel().getId());
            tip.setPouzivatel(pouzivatelDao.get(tip.getPouzivatelId()));
            tip.setSutazRocnikId(tipNaVitaza.getRocnikSutaze().getId());
            tip.setSutazRocnik(sutazRocnikDao.get(tip.getSutazRocnikId()));
            tip.setMuzstvoId(tipNaVitaza.getMuzstvo().getId());
            tip.setMuzstvo(muzstvoDao.get(tip.getMuzstvoId()));
            tip.setNahodnyTip(tipNaVitaza.isNahodnyTip());

            Long tipNaVitazaId = tipNaVitazaDao.save(tip);
            logger.info("tipNaVitaza[" + tip + "] bol zadany");

            return tipNaVitazaId;
        } catch (Exception e) {
            logger.error("nepodarilo sa pridat tip[" + tip + "]", e);
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public void editTipNaVitaza(String autor, TipNaVitazaDto tipNaVitaza) throws CasNaTipovanieVyprsalException, EntityNotFoundException {
        logger.info("editTipNaVitaza(autor=" + autor + ", tipNaVitaza=" + tipNaVitaza + ")");

        Assert.notNull(tipNaVitaza.getPouzivatel(), "pouzivatel nemoze byt null");
        Assert.notNull(tipNaVitaza, "tipNaVitaza nemoze byt null");
        Assert.notNull(tipNaVitaza.getRocnikSutaze(), "tipNaVitaza.sutazRocnika nemoze byt null");
        Assert.notNull(tipNaVitaza.getMuzstvo(), "tipNaVitaza.muzstvo nemoze byt null");

        // overime, ci sa nezacal prvy zapas rocnika sutaze
//        Zapas prvyZapas = zapasDao.getPvyZapasRocnikaSutaze(tipNaVitaza.getRocnikSutaze().getId());
//        if (prvyZapas.getDatum().before(new Date())) {
//            logger.warn("Cas pre tipovanie vitaza vyprsal");
//            throw new CasNaTipovanieVyprsalException("Cas pre tipovanie vitaza vyprsal");
//        }

        // overime ci editujeme existujuci tip
        TipNaVitaza tip = tipNaVitazaDao.get(tipNaVitaza.getId());
        if (tip == null) {
            logger.error("tipNaVitaza[id=" + tipNaVitaza.getId() + "] nebol najdeny");
            throw new EntityNotFoundException("tipNaVitaza[id=" + tipNaVitaza.getId() + "] nebol najdeny");
        }

        try {
            tip.setDatum(new Date());
            tip.setPouzivatelId(tipNaVitaza.getPouzivatel().getId());
            tip.setPouzivatel(pouzivatelDao.get(tip.getPouzivatelId()));
            tip.setSutazRocnikId(tipNaVitaza.getRocnikSutaze().getId());
            tip.setSutazRocnik(sutazRocnikDao.get(tip.getSutazRocnikId()));
            tip.setMuzstvoId(tipNaVitaza.getMuzstvo().getId());
            tip.setMuzstvo(muzstvoDao.get(tip.getMuzstvoId()));
            tip.setNahodnyTip(tipNaVitaza.isNahodnyTip());

            tipNaVitazaDao.saveOrUpdate(tip);
            logger.info("tipNaVitaza[id=" + tipNaVitaza + "] bol zmeneny");
        } catch (Exception e) {
            logger.error("nepodarilo sa zmenit tip[" + tip + "]", e);
            throw new IllegalArgumentException(e);
        }
    }

    @Override
    public TipNaVitazaDto getTipNaVitaza(Long tipNaVitazaId) {
        logger.info("getTipNaVitaza(tipNaVitazaId=" + tipNaVitazaId + ")");
        Assert.notNull(tipNaVitazaId, "tipNaVitazaId nemoze byt null");

        return TipNaVitazaDto.create(tipNaVitazaDao.get(tipNaVitazaId));
    }

    @Override
    public TipNaVitazaDto getPouzivatelovTipNaVitaza(Long pouzivatelId, Long rocnikId) {
        logger.info("getPouzivatelovTipNaVitaza(pouzivatelId=" + pouzivatelId + ", rocnikId=" + rocnikId + ")");
        Assert.notNull(pouzivatelId, "pouzivatelId nemoze byt prazdny");
        Assert.notNull(rocnikId, "rocnikId nemoze byt null");

        return TipNaVitazaDto.create(tipNaVitazaDao
                .getTipyNaVitazaRocnikaPodlaLogin(pouzivatelId, rocnikId));
    }

    @Override
    public List<TipNaVitazaDto> getTipyNaVitaza(Long sutazRocnikaId) {
        logger.info("getTipyNaVitaza(sutazRocnikaId=" + sutazRocnikaId + ")");
        Assert.notNull(sutazRocnikaId, "sutazRocnikaId nemoze byt null");

        List<TipNaVitazaDto> tipy = new ArrayList<>();
        for (TipNaVitaza t : tipNaVitazaDao.getTipyNaVitaza(sutazRocnikaId)) {
            tipy.add(TipNaVitazaDto.create(t));
        }

        return tipy;
    }

    @Override
    public List<TipNaVitazaDto> getPouzivateloveTipyNaVitaza(Long pouzivatelId) {
        logger.info("getPouzivateloveTipyNaVitaza(pouzivatelId=" + pouzivatelId + ")");
        Assert.notNull(pouzivatelId, "pouzivatelId nemoze byt prazdny");

        List<TipNaVitazaDto> tipy = new ArrayList<>();
        for (TipNaVitaza t : tipNaVitazaDao.getTipyNaVitazaPodlaLogin(pouzivatelId)) {
            tipy.add(TipNaVitazaDto.create(t));
        }

        return tipy;
    }

    public void setTipNaVitazaDao(TipNaVitazaDao tipNaVitazaDao) {
        this.tipNaVitazaDao = tipNaVitazaDao;
    }

    public void setZapasDao(ZapasDao zapasDao) {
        this.zapasDao = zapasDao;
    }

    public void setSutazRocnikDao(SutazRocnikDao sutazRocnikDao) {
        this.sutazRocnikDao = sutazRocnikDao;
    }

    public void setMuzstvoDao(MuzstvoDao muzstvoDao) {
        this.muzstvoDao = muzstvoDao;
    }

    public void setPouzivatelDao(PouzivatelDao pouzivatelDao) {
        this.pouzivatelDao = pouzivatelDao;
    }
}
