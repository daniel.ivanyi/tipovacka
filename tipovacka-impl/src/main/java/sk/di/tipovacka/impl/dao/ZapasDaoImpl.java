package sk.di.tipovacka.impl.dao;

import org.apache.log4j.Logger;
import org.hibernate.criterion.DetachedCriteria;
import org.hibernate.criterion.Order;
import org.hibernate.criterion.Restrictions;
import sk.di.tipovacka.api.db.dao.ZapasDao;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;

import java.util.ArrayList;
import java.util.List;

/**
 * Implementacia dao.
 * <p/>
 * User: Dano
 * Date: 10.5.2013
 */
public class ZapasDaoImpl extends EntityDaoImpl<Zapas> implements ZapasDao {

    private final Logger logger = Logger.getLogger(getClass());

    @Override
    @SuppressWarnings("unchecked")
    public List<Zapas> findByKriteria(ZapasKriteria kriteria) {

        DetachedCriteria criteria = DetachedCriteria.forClass(Zapas.class);

        // ak mame zadane muzstva, pre ktore sa maju dohladat zapasy
        if (!kriteria.getMuzstvoIds().isEmpty()) {
            criteria = criteria.add(Restrictions.or(
                    Restrictions.in("domaciId", kriteria.getMuzstvoIds()),
                    Restrictions.in("hostiaId", kriteria.getMuzstvoIds())));
        }
        // defaultne kriteria
        addDefaultVyhladavacieKriteria(criteria, kriteria);

        List<Zapas> result = getHibernateTemplate().findByCriteria(criteria);
        logger.debug("podla vyhladavacich kriterii[" + kriteria + "] bolo najdenych " + result.size() + " zapasov");

        return result;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Zapas getPvyZapasRocnikaSutaze(Long sutazRocnikId) {

        DetachedCriteria criteria = DetachedCriteria.forClass(Zapas.class)
                .add(Restrictions.eq("sutazRocnikId", sutazRocnikId))
                .addOrder(Order.asc("datum"));

        List<Zapas> result = getHibernateTemplate().findByCriteria(criteria);

        return result.isEmpty() ? null : result.get(0);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Zapas> getZapasyBezSuperov(Long sutazRocnikId) {
        DetachedCriteria criteria = DetachedCriteria.forClass(Zapas.class)
                .add(Restrictions.eq("sutazRocnikId", sutazRocnikId));

        List<Zapas> zapasy = new ArrayList<>();
        for (Zapas z : (List<Zapas>)getHibernateTemplate().findByCriteria(criteria)) {
            if ("TBD".equals(z.getDomaci().getKodMuzstva())
                    && "TBD".equals(z.getHostia().getKodMuzstva())) {
                zapasy.add(z);
            }
        }

        return zapasy;
    }
}
