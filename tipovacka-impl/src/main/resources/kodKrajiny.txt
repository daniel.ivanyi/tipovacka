AFG Afganistan
ALB Albánsko
ALG Alžírsko
ASA Americká Samoa
AND Andorra
ANG Angola
ATG Antigua a Barbuda
AZE Azerbajdžan
ARG Argentína
AUS Austrália
AUT Rakúsko
BAH Bahamy
BHR Bahrajn
BAN Bangladéš
ARM Arménsko
BRB Barbados
BEL Belgicko
BER Bermudy
BHU Bhután
BOL Bolívia
BIH Bosna a Hercegovina
BOT Botswana
BRA Brazília
BLZ Belize
SOL Šalamúnove ostrovy
VGB Britské Panenské ostrovy
BRU Brunej
BUL Bulharsko
BDI Burundi
BLR Bielorusko
CAM Kambodža
CMR Kamerun
CAN Kanada
CPV Kapverdy
CAY Kajmanie ostrovy
CTA Stredoafrická republika
CHA Čad
CHI Čile
CHN Čína
TPE Taiwan
COL Kolumbia
COM Komory
CGO Kongo
COD Kongo
COK Cookove ostrovy
CRC Kostarika
CRO Chorvátsko
CUB Kuba
CYP Cyprus
CZE Česko
CZE Česká republika
BEN Benin
DEN Dánsko
DMA Dominika
DOM Dominikánska republika
ECU Ekvádor
SLV Salvádor
EQG Rovníková Guinea
ETH Etiópia
ERI Eritrea
EST Estónsko
FRO Faerské ostrovy
FIJ Fidži
FIN Fínsko
FRA Francúzsko
TAH Francúzska Polynézia
DJI Džibutsko
GAB Gabon
GEO Gruzínsko
GAM Gambia
PLE Palestína
GER Nemecko
GHA Ghana
GRE Grécko
GRN Grenada
GUA Guatemala
GUM Guam
GUY Guyana
HAI Haiti
HON Honduras
HKG Hongkong
HUN Maďarsko
ISL Island
IND India
IDN Indonézia
IRN Irán
IRQ Irak
IRL Írsko
ISR Izrael
ITA Taliansko
CIV Pobrežie Slonoviny
JAM Jamajka
JPN Japonsko
KAZ Kazachstan
JOR Jordánsko
KEN Keňa
PRK Kórejská ľudovodemokratická republika
KOR Kórejská republika
KUW Kuvajt
KGZ Kirgizsko
LAO Laos
LIB Libanon
LES Lesotho
LAT Lotyšsko
LBR Libéria
LBY Líbya
LIE Lichtenštajnsko
LTU Litva
LUX Luxembursko
MAC Macao
MAD Madagaskar
MWI Malawi
MAS Malajzia
MDV Maldivy
MLI Mali
MLT Malta
MTN Mauritánia
MRI Maurícius
MEX Mexiko
MDA Moldavsko
MNE Čierna Hora
MSR Montserrat
MAR Maroko
MOZ Mozambik
OMA Omán
NAM Namíbia
NED Holandsko
ARU Aruba
NCL Nová Kaledónia
VAN Vanuatu
NZL Nový Zéland
NCA Nikaragua
NIG Niger
NGA Nigéria
NOR Nórsko
PAK Pakistan
PAN Panama
PNG Papua-Nová Guinea
PAR Paraguaj
PER Peru
PHI Filipíny
POL Poľsko
POR Portugalsko
GNB Guinea-Bissau
TLS Východný Timor
PUR Portoriko
QAT Katar
ROU Rumunsko
RUS Rusko
RWA Rwanda
SKN Svätý Krištof a Nevis
AIA Anguilla
VIN Svätý Vincent a Grenadíny
SMR San Maríno
SMR San Marino
STP Svätý Tomáš a Princov ostrov
KSA Saudská Arábia
SEN Senegal
SRB Srbsko
SEY Seychely
SLE Sierra Leone
SIN Singapur
SVK Slovensko
VIE Vietnam
SLO Slovinsko
SVN Slovinsko
SOM Somálsko
RSA Juhoafrická republika
ZIM Zimbabwe
ESP Španielsko
SDN Sudán
SUR Surinam
SWZ Svazijsko
SWE Švédsko
SUI Švajčiarsko
SYR Sýria
TJK Tadžikistan
THA Thajsko
TOG Togo
TGA Tonga
TRI Trinidad a Tobago
UAE Spojené arabské emiráty
TUN Tunisko
TUR Turecko
TKM Turkménsko
TCA Turks a Caicos
UGA Uganda
UKR Ukrajina
MKD Macedónsko
EGY Egypt
ENG Anglicko
TAN Tanzánia
USA USA
VIR Americké panenské ostrovy
BFA Burkina Faso
URU Uruguaj
UZB Uzbekistan
VEN Venezuela
SAM Samoa
YEM Jemen
ZAM Zambia
WAL Wales
NIR Severné Írsko
SCO Škótsko