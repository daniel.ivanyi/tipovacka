package sk.di.tipovacka.test.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import sk.di.tipovacka.impl.service.MailServiceImpl;

@Configuration
@PropertySource("classpath:configuration-test.properties")
public class TestConfig {

    @Bean
    public MailServiceImpl mailService() {
        MailServiceImpl mailService = new MailServiceImpl();
        mailService.setPouzivatelDao(null);
        mailService.setSutazRocnikDao(null);
        mailService.setTipNaZapasDao(null);

        return mailService;
    }

    /**
     * Property placeholder configurer needed to process @Value annotations
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertyConfigurer() {
        return new PropertySourcesPlaceholderConfigurer();
    }
}
