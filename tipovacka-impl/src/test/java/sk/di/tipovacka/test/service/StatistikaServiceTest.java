package sk.di.tipovacka.test.service;

import junit.framework.Assert;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.dto.MuzstvoStatistikaDto;
import sk.di.tipovacka.api.dto.PouzivatelovTipStatistikaDto;
import sk.di.tipovacka.api.service.StatistikaService;

import javax.annotation.Resource;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

/**
 * Testovacka pre statistiku
 *
 * User: filusik
 * Date: 14.6.2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class StatistikaServiceTest {

    @Resource
    private StatistikaService statistikaService;

    @Test
    public void getStatistikaTiperovTest() {
        Long rocnikId = 13L;

        List<PouzivatelovTipStatistikaDto> stats = statistikaService.getStatistikaTiperov(rocnikId);

        for (PouzivatelovTipStatistikaDto stat : stats) {
            System.out.println("statistika[" + stat.getPouzivatel().getLogin()
                    + ", " + stat.getPocetTipov()
                    + ", " + stat.getPocetZadanychTipov()
                    + ", " + stat.getHodnota()
                    + ", " + stat.getBody()
                    + ", " + stat.getPocetTipovNic()
                    + ", " + stat.getPocetTipovGoly()
                    + ", " + stat.getPocetTipovVitaz()
                    + ", " + stat.getPocetTipovVitazGol()
                    + ", " + stat.getPocetTipovVysledok()
                    + "]");
        }
    }

    @Test
    public void test() {
        List<String> logins = Arrays.asList("admin",
                "dano",
                "rich1289",
                "Total winner",
                "IGOR",
                "RomanM",
                "Googo",
                "pefo",
                "roman.bokor",
                "iszabo",
                "candrej29",
                "pmajek");

        System.out.println("logins=" + logins);
        Collections.sort(logins, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                Collator collator = Collator.getInstance(new Locale("sk", "SK"));
                collator.setStrength(Collator.PRIMARY);

                return o1.toLowerCase().compareTo(o2.toLowerCase());
            }
        });
        System.out.println("sorted=" + logins);
    }
}
