package sk.di.tipovacka.test.pdf;

import junit.framework.Assert;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import sk.di.tipovacka.api.db.Muzstvo;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.Zapas;
import sk.di.tipovacka.api.db.dao.PouzivatelDao;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.enums.SportTypEnum;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

/**
 * Testovacka pre generovanie pdfka.
 *
 * User: Dano
 * Date: 11.2.2014
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
public class PdfTest {

    @Autowired
    private TipNaZapasDao tipNaZapasDao;

    @Autowired
    private PouzivatelDao pouzivatelDao;

    @Test
    public void generujMojeTipyTest() {
        System.out.println("generujMojeTipyTest()");

        Collection<TipNaZapas> tipy = new ArrayList<>();
        TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
        kriteria.getPouzivatelIds().add(107L);
        kriteria.setSutazRocnikId(13L);

        Muzstvo domaci = new Muzstvo();
        domaci.setKod("USA");
        domaci.setNazov("Spojene staty americke");
        Muzstvo hostia = new Muzstvo();
        hostia.setKod("GER");
        hostia.setNazov("Nemecka republika");
        Zapas zapas = new Zapas();
        zapas.setDatum(new Date());
        zapas.setDomaci(domaci);
        zapas.setHostia(hostia);
        zapas.setGolyDomaci(10);
        zapas.setGolyHostia(20);
        TipNaZapas tip = new TipNaZapas();
        tip.setZapas(zapas);
        tip.setGolyDomaci(10);
        tip.setGolyHostia(10);
        tip.setBodyZaGolyDomaci(1);
        tip.setBodyZaGolyHostia(2);
        tip.setBodyZaVitaza(7);

        tipy.add(tip);

        Zapas zapas2 = new Zapas();
        zapas2.setDatum(new Date());
        zapas2.setDomaci(domaci);
        zapas2.setHostia(hostia);

        TipNaZapas tip2 = new TipNaZapas();
        tip2.setZapas(zapas2);
        tip2.setGolyDomaci(10);
        tip2.setGolyHostia(10);
        tip2.setBodyZaGolyDomaci(1);
        tip2.setBodyZaGolyHostia(2);
        tip2.setBodyZaVitaza(7);

        tipy.add(tip2);

        generate(tipy);
    }

    @Test
    public void testMojeTipy() {
        TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
        kriteria.getPouzivatelIds().add(pouzivatelDao.getPouzivatelPodlaLoginu("dano").getId());
        List<TipNaZapas> tipy = tipNaZapasDao.findTipyNaZapas(kriteria);

        generate(tipy);
    }

    /**
     * Samotne generovanie reportu tipov.
     *
     * @param tipy tipy
     */
    private void generate(Collection<TipNaZapas> tipy) {
        JasperDesign design;
        JasperReport report;
        JasperPrint jasperPrint;

        try {

            SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

            HashMap<String, Object> parameterMap = new HashMap<>();
            parameterMap.put("login", "Junit.Los.Manos");
            parameterMap.put("sport", SportTypEnum.HOKEJ.name().toLowerCase());
            parameterMap.put("nazovSutaze", "Olympijske hry");
            parameterMap.put("rocnikSutaze", "2014");
            parameterMap.put("datumGenerovania", sdf.format(new Date()));;

            design = JRXmlLoader.load(this.getClass().getClassLoader().getResourceAsStream("pdf/mojeTipy.jrxml"));
            report = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(report,
                    parameterMap,
                    new JRBeanCollectionDataSource(tipy));
            JasperExportManager.exportReportToPdfFile(jasperPrint, "mojeTipy.pdf");
        } catch (JRException e) {
            e.printStackTrace();
            Assert.fail("padlo do chyby");
        }
    }
}
