package sk.di.tipovacka.test.service;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import sk.di.tipovacka.api.service.MailService;
import sk.di.tipovacka.impl.service.MailServiceImpl;
import sk.di.tipovacka.test.config.TestConfig;

import javax.annotation.Resource;
import javax.mail.Message;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {TestConfig.class})
public class MailServiceBaseTest {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Resource
    private MailServiceImpl mailService;

    @Test
    public void mozemPoslaMail() {
        logger.info("mozemPoslaMail()");

        try {
            mailService.sendMail("divanyi@pss.sk", "JUnitTest", "just testing[" + new Date() + "]", null, null, null);
        } catch (Exception e) {
            logger.error("padlo do chyby", e);
            Assert.fail("padlo do chyby");
        }
    }

    @Test
    public void mozemNacitatSpravy() {
        logger.info("mozemNacitatSpravy()");

        List<Message> spravy = mailService.getNeprecitaneSpravy();
        Assert.assertTrue("ziadne spravy nebolo najdene",
                !spravy.isEmpty());
    }
}
