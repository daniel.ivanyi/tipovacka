package sk.di.tipovacka.test.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.impl.service.IihfServiceImpl;

import javax.annotation.Resource;
import java.util.List;

/**
 * Testovacka pre pracu s iihf rest servicom.
 *
 * Created by divanyi on 14. 4. 2018.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class IihfServiceTest {

    @Resource
    private IihfServiceImpl iihfService;

    @Test
    public void mozemNacitatZapasyTurnajaPodlaUrl() {
        System.out.println("mozemNacitatZapasyTurnajaPodlaUrl()");

        String url = "http://iihf.egluservices.com/tournaments1617/query.php?lastUpdate=0&os=ios";
        List<ZapasDto> zapasy = iihfService.getZapasyTurnaja(url);

        Assert.assertTrue("ziadne zapasy pre turnaj",
                !zapasy.isEmpty());
        System.out.println("zapasy:" + zapasy);
    }
}
