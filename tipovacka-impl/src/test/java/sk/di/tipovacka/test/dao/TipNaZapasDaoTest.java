package sk.di.tipovacka.test.dao;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@TransactionConfiguration
@Transactional
public class TipNaZapasDaoTest {

    @Resource
    private TipNaZapasDao tipNaZapasDao;

    @Test
    public void mozemNajstNajlepsieTipujucehoNaVitaza() {
        Long rocnikId = 21L;
        List<String> tiperi = tipNaZapasDao.findNajlepsiTipujuciNaVitazaZapasu(rocnikId);
        Assert.assertNotNull("nikto nenajdeny", tiperi);
        System.out.println("najlepsi tipujuci na vitaza zapasu[" + tiperi + "]");
    }

    @Test
    public void mozemNajstNajlepsieTipujucehoSkoreZapasu() {
        Long rocnikId = 21L;
        List<String> tiperi = tipNaZapasDao.findNajlepsieTipujuciSkoreZapasu(rocnikId);
        Assert.assertNotNull("nikto nenajdeny", tiperi);
        System.out.println("najlepsi tipujuci na skore zapasu[" + tiperi + "]");
    }
}
