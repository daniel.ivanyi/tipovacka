package sk.di.tipovacka.test.job;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.impl.job.KontrolaNezadanychTipovJob;

import javax.annotation.Resource;

/**
 * Testovacka pre kontrolu nezadanych tipov na zapas.
 *
 * User: Dano
 * Date: 4.3.2014
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class KontrolaNezadanychTipovJobTest {

    @Resource
    private KontrolaNezadanychTipovJob kontrolaNezadanychTipovJob;

    @Test
    public void kontrolaNezadanychTipovTest() {
        System.out.println("kontrolaNezadanychTipovTest()");
        kontrolaNezadanychTipovJob.jobRun();
    }
}
