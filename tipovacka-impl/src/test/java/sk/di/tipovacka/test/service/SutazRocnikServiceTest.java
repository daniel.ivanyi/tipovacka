package sk.di.tipovacka.test.service;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.comparator.TipNaZapasDatumComparator;
import sk.di.tipovacka.api.db.Sutaz;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.dao.SutazDao;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.dto.RocnikSutazeHlavickaDto;
import sk.di.tipovacka.api.dto.SutazDto;
import sk.di.tipovacka.api.dto.TipNaVitazaDto;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.SportTypEnum;
import sk.di.tipovacka.api.enums.ZapasTypEnum;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.TipNaVitazaService;
import sk.di.tipovacka.api.service.TipNaZapasService;
import sk.di.tipovacka.api.service.ZapasService;

import javax.annotation.Resource;
import java.io.IOException;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Testovacka pre pracu s rocnikom sutaze
 * <p/>
 * User: Dano
 * Date: 24.5.2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class SutazRocnikServiceTest {

    private final String autor = "junit";

    @Resource
    private SutazRocnikService sutazRocnikService;
    @Resource
    private ZapasService zapasService;
    @Resource
    private SutazDao sutazDao;
    @Resource
    private SutazRocnikDao sutazRocnikDao;
    @Resource
    private TipNaZapasService tipNaZapasService;
    @Resource
    private TipNaVitazaService tipNaVitazaService;

    @Test
    public void addSutazRocnikTest() {
        System.out.println("addSutazRocnikTest()");

        Sutaz sutaz = new Sutaz();
        sutaz.setNazov("Test sutaz");
        sutaz.setSportTyp(SportTypEnum.HOKEJ);

        Long sutazId = sutazDao.save(sutaz);
        Assert.assertNotNull("sutazId sa rovna null", sutazId);

        RocnikSutazeDto sutazRocnik = new RocnikSutazeDto();
        sutazRocnik.setSutaz(SutazDto.create(sutaz));
        sutazRocnik.setSutazId(sutaz.getId());
        sutazRocnik.setRok(Calendar.getInstance().get(Calendar.YEAR));
        sutazRocnik.setStatistikaUrl("http://stats.iihf.com/Hydra/352/index.html");

        Long sutazRocnikaId = sutazRocnikService.addSutazRocnik(autor, sutazRocnik);
        Assert.assertTrue("sutazRocnikaId rovna sa null",
                sutazRocnikaId != null);

        SutazRocnik sr = sutazRocnikDao.get(sutazRocnikaId);
        Assert.assertNotNull("sutaz je null", sr.getSutaz());
        Assert.assertNotNull("sutaz.id je null", sr.getSutaz().getId());

        System.out.println("sutazRocnika[" + sutazRocnik + "]");
    }

    @Test
    public void getRocnikSutazeTest() {
        Long id = 1L;
        RocnikSutazeDto rocnik = sutazRocnikService.getRocnikSutaze(id);
        Assert.assertNotNull("nebol najdeny ziaden rocnik[id=" + id + "]",
                rocnik);
        System.out.println("rocnik=" + rocnik);
    }

    @Test
    public void getZaciatokCastiSutazeTest() {
        Long id = 14L;
        RocnikSutazeDto rocnik = sutazRocnikService.getRocnikSutaze(id);
        Assert.assertNotNull("nebol najdeny ziaden rocnik[id=" + id + "]",
                rocnik);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2014);
        calendar.set(Calendar.MONTH, Calendar.MAY);
        calendar.set(Calendar.DAY_OF_MONTH, 9);
        calendar.set(Calendar.HOUR, 15);
        calendar.set(Calendar.MINUTE, 45);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        Date zaciatok = rocnik.getZaciatokCasti(ZapasTypEnum.PRE);
        Assert.assertTrue("zaciatok zakadnej casti[" + zaciatok + "] je nespravny[" + calendar.getTime() + "]",
                calendar.getTime().equals(zaciatok));
    }

    @Test
    public void getKoniecCastiSutazeTest() {
        Long id = 19L;
        RocnikSutazeDto rocnik = sutazRocnikService.getRocnikSutaze(id);
        Assert.assertNotNull("nebol najdeny ziaden rocnik[id=" + id + "]",
                rocnik);
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, 2014);
        calendar.set(Calendar.MONTH, Calendar.MAY);
        calendar.set(Calendar.DAY_OF_MONTH, 9);
        calendar.set(Calendar.HOUR, 15);
        calendar.set(Calendar.MINUTE, 45);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
        for (ZapasTypEnum typ : ZapasTypEnum.values()) {
            System.out.println(String.format("faza[%s], zaciatok: %s, koniec:%s",
                    typ,
                    rocnik.getZaciatokCasti(typ),
                    rocnik.getKoniecCasti(typ)));
        }
    }

    @Test
    public void inicializujRocnikSutazePodlaIihfTest() {
        Long rocnikId = 1L;

        try {
            sutazRocnikService.inicializujRocnikSutaze(autor, rocnikId);
        } catch (IOException e) {
            Assert.fail(e.getLocalizedMessage());
        }

        RocnikSutazeDto rocnik = sutazRocnikService.getRocnikSutaze(rocnikId);
        Assert.assertTrue("rocnik[id=" + rocnikId + "] nema ziadne zapasy",
                !rocnik.getZapasy().isEmpty());
    }

    @Test
    public void updatePriebehRocnikaSutazeTest() {
        System.out.println("updatePriebehRocnikaSutazeTest");
        Long rocnikId = 16L;

        try {
            sutazRocnikService.updatePriebehRocnikaSutaze("jUnit", rocnikId);
        } catch (IOException e) {
            Assert.fail(e.getLocalizedMessage());
        }

        TipNaZapasKriteria tipKriteria = new TipNaZapasKriteria();
        tipKriteria.setSutazRocnikId(rocnikId);
        List<TipNaZapasDto> tipy = tipNaZapasService.findTipyNaZapas(tipKriteria);
        Collections.sort(tipy, new TipNaZapasDatumComparator());

        for (TipNaZapasDto t : tipy) {
            ZapasDto z = t.getZapas();
            if ((t.getPouzivatel().getLogin().contains("candrej29")
                || t.getPouzivatel().getLogin().contains("pmajek"))
                    && t.getGolyDomaci().equals(t.getGolyHostia())) {
                System.out.println("typ[" + t.getId()
                        + " " + z.getDatum()
                        + " " + t.getPouzivatel().getLogin()
                        + ", " +  (z.getDomaci() == null ? "NUL" : z.getDomaci().getKod())
                        + " " + (z.getHostia() == null ? "NULL" : z.getHostia().getKod())
                        + " "
                        + (z.getGolyDomaci() == null ? " " : z.getGolyDomaci())
                        + ":"
                        + (z.getGolyHostia() == null ? " " : z.getGolyHostia())
                        + " " + t.getGolyDomaci() +  ":" + t.getGolyHostia()
                        + " " + t.getBody()
                        + "]");
            }

//            System.out.println("typ[" + t.getId()
//                    + " " + z.getDatum()
//                    + " " + t.getPouzivatel().getLogin()
//                    + ", " + (z.getDomaci() == null ? "NUL" : z.getDomaci().getKod())
//                    + " " + (z.getHostia() == null ? "NULL" : z.getHostia().getKod())
//                    + " "
//                    + (z.getGolyDomaci() == null ? " " : z.getGolyDomaci())
//                    + ":"
//                    + (z.getGolyHostia() == null ? " " : z.getGolyHostia())
//                    + " " + t.getGolyDomaci() + ":" + t.getGolyHostia()
//                    + " " + t.getBody()
//                    + "]");
        }
    }

    @Test
    public void getUkonceneRocnikyTest() {
        System.out.println("getUkonceneRocnikyTest");

        List<RocnikSutazeHlavickaDto> ukonceneRocniky = sutazRocnikService.getUkonceneRocniky();

        Assert.assertTrue("ziadne ukoncene rocniky neboli najdene",
                !ukonceneRocniky.isEmpty());

        for (RocnikSutazeHlavickaDto r : ukonceneRocniky) {
            System.out.println("rocnik[nazov=" + r.getSutaz().getNazov() + " - " + r.getRok()
                    + "]");
        }
    }

    @Test
    public void setVitazRocnikaTest() {
        System.out.printf("setVitazRocnikaTest()");
        sutazRocnikService.setVitazRocnika("JUnit", 14L, 11L);

        int pocetBodov = 1;
        TipNaVitazaDto tip = tipNaVitazaService.getTipNaVitaza(29L);
        Assert.assertTrue("nespravny pocet bodov[" + tip.getBody() + "/" + pocetBodov + "]",
                tip.getBody() == pocetBodov);
    }
}
