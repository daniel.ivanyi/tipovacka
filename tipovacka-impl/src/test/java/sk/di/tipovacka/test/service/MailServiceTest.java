package sk.di.tipovacka.test.service;

import junit.framework.Assert;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.service.MailService;
import sk.di.tipovacka.test.config.TestConfig;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Popis triedy
 * <p/>
 * User: Dano
 * Date: 28.2.2014
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration
@Transactional
public class MailServiceTest {

    @Resource
    private MailService mailService;

    @Test
    public void getNeprecitaneTipyNaZapasTest() {

        List<TipNaZapasDto> tipy = null;

        try {
            tipy = mailService.getNeprecitaneTipyNaZapas();
        } catch (Exception e) {
            e.printStackTrace(System.err);
            Assert.fail("padlo do chyby");
        }

        Assert.assertTrue("ziadne neprecitane tipy na zapas",
                !tipy.isEmpty());

        for (TipNaZapasDto tip : tipy) {
            System.out.println("tip[" + ToStringBuilder.reflectionToString(tip, ToStringStyle.SHORT_PREFIX_STYLE) + "]");
        }
    }

    @Test
    public void parseTipTest() {

        List<String> text = Arrays.asList(
                " ads dsa sda sad tip [02.07.2016 21:00] GER (0 - 1) ITAdsadsa dsad sd",
                " sdsad sadtip [01.07.2016 21:00] WAL (2 - 1) BEL dsd sad sadsa",
                " dsad sad sadtip [03.07.2016 21:00] FRA (2 - 0) ISL sa dsa dsa dsa d");

        Pattern pattern = Pattern.compile(".*tip \\[(\\d{2}.\\d{2}.\\d{4} \\d{2}:\\d{2})\\] ([A-Z]{3}) \\((\\d) - (\\d)\\) ([A-Z]{3}).*");
        for (String t : text) {
            Matcher matcher = pattern.matcher(t);
            if (matcher.find()) {
                String datum = matcher.group(1);
                String domaci = matcher.group(2);
                String domaciGol = matcher.group(3);
                String hostiaGol = matcher.group(4);
                String hostia = matcher.group(5);

                System.out.println(String.format("hlada sa zapas %s : %s, ktory sa hra %s. Pouzivatel tipuje, ze zapas skonci %s : %s.",
                        domaci,
                        hostia,
                        datum,
                        domaciGol,
                        hostiaGol));
            }
        }

    }
}
