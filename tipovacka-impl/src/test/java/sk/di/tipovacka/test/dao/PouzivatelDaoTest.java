package sk.di.tipovacka.test.dao;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.db.Pouzivatel;
import sk.di.tipovacka.api.db.dao.PouzivatelDao;

import javax.annotation.Resource;

/**
 * Created by IVAN1497 on 23.5.2014.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class PouzivatelDaoTest {

    @Resource
    public PouzivatelDao pouzivatelDao;

    @Test
    public void getPouzivatelPodlaMailuTest() {
        System.out.println("getPouzivatelPodlaMailuTest()");

        String mail = "RBokor@pss.sk";
        Pouzivatel p = pouzivatelDao.getPouzivatelPodlaMailu(mail);
        Assert.assertTrue("pouzivatel[mail=" + mail + "] bol najdeny",
                p != null);
        System.out.println("pouzivatel=" + p);
    }
}
