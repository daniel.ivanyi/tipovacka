package sk.di.tipovacka.test.service;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.impl.service.WorldCupSfgIoServiceImpl;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration
@Transactional
public class WorldCupSfgIoServiceTest {

    @Resource
    private WorldCupSfgIoServiceImpl service;

    @Test
    public void mozemProgramMajstrovstiev() {
        String url = "http://worldcup.sfg.io/matches?by_date=ASC";
        List<ZapasDto> zapasy = service.getZapasReport(url, false);

        Assert.assertTrue("ziadne zapasy pre turnaj",
                !zapasy.isEmpty());
        for (ZapasDto z : zapasy) {
            System.out.println(z);
        }
    }
}
