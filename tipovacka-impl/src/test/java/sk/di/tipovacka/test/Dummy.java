package sk.di.tipovacka.test;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.commons.lang.time.DateUtils;
import org.junit.Assert;
import org.junit.Test;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.net.URL;
import java.sql.Timestamp;
import java.text.ParseException;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class Dummy {

    @Test
    public void mozemKonvertovatNaBratislavskyTimestamp() {
        System.out.println("mozemKonvertovatNaBratislavskyTimestamp()");

        String text = "2018-06-14T15:00:00Z";
        System.out.println("ts=" + Timestamp.valueOf(text));

        ZonedDateTime origin = ZonedDateTime.parse(text);
        ZoneId bratislava = ZoneId.of("Europe/Bratislava");
        ZonedDateTime bratislavskyDateTime = origin.withZoneSameInstant(bratislava);

        String datePattern = "dd.MM.yyyy HH:mm";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(datePattern);
        try {
            Date date = DateUtils.parseDate(bratislavskyDateTime.format(formatter), new String[]{datePattern});

            System.out.println("nas = " + date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getDataFromIihfRestService() {
        long start = System.currentTimeMillis();

        Calendar now = Calendar.getInstance();
        int rok = now.get(Calendar.YEAR) - 2000;
        String sezona = String.format("%d%d", rok - 1, rok);
        String iihfSezonaUrl = String.format("http://iihf.egluservices.com/tournaments%s/query.php?lastUpdate=0&os=ios",
                sezona);

        String url = String.format("%s&requestName=tournamentList",
                iihfSezonaUrl);

        System.out.println("url=" + url);
        ObjectMapper mapper = new ObjectMapper();
        try {
            List<Tournament> tournaments = mapper.readValue(new URL(url), mapper.getTypeFactory().constructCollectionType(List.class, Tournament.class));
            Assert.assertTrue("ziaden turnaj",
                    !tournaments.isEmpty());
            System.out.println("list turnajov = " + tournaments.size());
            for (Tournament t : tournaments) {
                if ("WM Top Div".equalsIgnoreCase(t.getLongName())) {
                    System.out.println(t);
                    String urlTurnaj = String.format("%s&requestName=games&tournamentId=%d",
                            iihfSezonaUrl, t.getTournamentID());
                    System.out.println("urlTurnaj=" + urlTurnaj);
                    List<Game> games = mapper.readValue(new URL(urlTurnaj), new TypeReference<List<Game>>(){});
                    Assert.assertTrue("ziadne zapasy v turnaji",
                            !games.isEmpty());
                    for (Game g : games) {
                        System.out.println(g);
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace(System.err);
            Assert.fail();
        }
        System.out.println("trvanie=" + (System.currentTimeMillis() - start));
    }

    @Test
    public void getDataFromUrlTest() {
        System.out.println("getDataFromUrlTest()");

        String url = "http://stats.iihf.com/hydra/2018-wm/widget_en_2018_wm_tournament.js";//"http://stats.iihf.com/hydra/2018-Olympic/widget_men_2018_olympic_tournament.js";
        Sutaz sutaz = null;

        try {
            String data = IOUtils.toString(new URL(url));
            // zaujimaju nas len udaje o zapasoch
            data = StringUtils.substringBetween(data.trim(), "games: [", "]")
                    .trim()
                    // upravim ukoncovanie objektov
                    .replaceAll(",}", "}");
            // posledny znak je , taku odstranime
            data = data.substring(0, data.length() - 1);
            // upravime na json format
            String json = String.format("{\"games\":[%s]}", data);
            // a prehodime do java objectu
            ObjectMapper mapper = new ObjectMapper();
            sutaz = mapper.readValue(json, Sutaz.class);
        } catch (Exception e) {
            e.printStackTrace(System.err);
        }

        Assert.assertTrue("sutaz nebola nacitana",
                sutaz != null && !sutaz.getHry().isEmpty());

        for (Hra h : sutaz.getHry()) {
            System.out.println(String.format("hra[%s, %s %s %s]",
                    DateFormatUtils.format(h.getDatumCas().getTime(), "dd.MM.yyyy HH:mm"),
                    h.getDomaci(),
                    h.getVysledok() == null ? "nehralo sa" : h.getVysledok(),
                    h.getHostia()));
        }
    }

    @Test
    public void readValueTest() {
        System.out.println("readValueTest()");

        String json = "{\"games\":[{\"n\":\"1\",\"d\":\"2018-02-14\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"B\",\"e\":\"7\",\"h\":\"SVK\",\"g\":\"OAR\",\"r\":\"3-2\",\"s\":\"?\"},{\"n\":\"2\",\"d\":\"2018-02-14\",\"t\":\"21:10 GMT+9\",\"v\":\"2\",\"p\":\"PRE\",\"group\":\"B\",\"e\":\"7\",\"h\":\"USA\",\"g\":\"SLO\",\"r\":\"2-3\",\"s\":\"OT\"},{\"n\":\"3\",\"d\":\"2018-02-15\",\"t\":\"12:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"C\",\"e\":\"7\",\"h\":\"FIN\",\"g\":\"GER\",\"r\":\"5-2\",\"s\":\"?\"},{\"n\":\"4\",\"d\":\"2018-02-15\",\"t\":\"16:40 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"C\",\"e\":\"7\",\"h\":\"NOR\",\"g\":\"SWE\",\"r\":\"0-4\",\"s\":\"?\"},{\"n\":\"5\",\"d\":\"2018-02-15\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"A\",\"e\":\"7\",\"h\":\"CZE\",\"g\":\"KOR\",\"r\":\"2-1\",\"s\":\"?\"},{\"n\":\"6\",\"d\":\"2018-02-15\",\"t\":\"21:10 GMT+9\",\"v\":\"2\",\"p\":\"PRE\",\"group\":\"A\",\"e\":\"7\",\"h\":\"SUI\",\"g\":\"CAN\",\"r\":\"1-5\",\"s\":\"?\"},{\"n\":\"7\",\"d\":\"2018-02-16\",\"t\":\"12:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"B\",\"e\":\"7\",\"h\":\"USA\",\"g\":\"SVK\",\"r\":\"2-1\",\"s\":\"?\"},{\"n\":\"8\",\"d\":\"2018-02-16\",\"t\":\"16:40 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"B\",\"e\":\"7\",\"h\":\"OAR\",\"g\":\"SLO\",\"r\":\"8-2\",\"s\":\"?\"},{\"n\":\"9\",\"d\":\"2018-02-16\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"C\",\"e\":\"7\",\"h\":\"FIN\",\"g\":\"NOR\",\"r\":\"5-1\",\"s\":\"?\"},{\"n\":\"10\",\"d\":\"2018-02-16\",\"t\":\"21:10 GMT+9\",\"v\":\"2\",\"p\":\"PRE\",\"group\":\"C\",\"e\":\"7\",\"h\":\"SWE\",\"g\":\"GER\",\"r\":\"1-0\",\"s\":\"?\"},{\"n\":\"11\",\"d\":\"2018-02-17\",\"t\":\"12:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"A\",\"e\":\"7\",\"h\":\"CAN\",\"g\":\"CZE\",\"r\":\"2-3\",\"s\":\"PSS\"},{\"n\":\"12\",\"d\":\"2018-02-17\",\"t\":\"16:40 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"A\",\"e\":\"7\",\"h\":\"KOR\",\"g\":\"SUI\",\"r\":\"0-8\",\"s\":\"?\"},{\"n\":\"13\",\"d\":\"2018-02-17\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"B\",\"e\":\"7\",\"h\":\"OAR\",\"g\":\"USA\",\"r\":\"4-0\",\"s\":\"?\"},{\"n\":\"14\",\"d\":\"2018-02-17\",\"t\":\"21:10 GMT+9\",\"v\":\"2\",\"p\":\"PRE\",\"group\":\"B\",\"e\":\"7\",\"h\":\"SLO\",\"g\":\"SVK\",\"r\":\"3-2\",\"s\":\"PSS\"},{\"n\":\"15\",\"d\":\"2018-02-18\",\"t\":\"12:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"C\",\"e\":\"7\",\"h\":\"GER\",\"g\":\"NOR\",\"r\":\"2-1\",\"s\":\"PSS\"},{\"n\":\"16\",\"d\":\"2018-02-18\",\"t\":\"16:40 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"A\",\"e\":\"7\",\"h\":\"CZE\",\"g\":\"SUI\",\"r\":\"4-1\",\"s\":\"?\"},{\"n\":\"17\",\"d\":\"2018-02-18\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"PRE\",\"group\":\"A\",\"e\":\"7\",\"h\":\"CAN\",\"g\":\"KOR\",\"r\":\"4-0\",\"s\":\"?\"},{\"n\":\"18\",\"d\":\"2018-02-18\",\"t\":\"21:10 GMT+9\",\"v\":\"2\",\"p\":\"PRE\",\"group\":\"C\",\"e\":\"7\",\"h\":\"SWE\",\"g\":\"FIN\",\"r\":\"3-1\",\"s\":\"?\"},{\"n\":\"19\",\"d\":\"2018-02-20\",\"t\":\"12:10 GMT+9\",\"v\":\"1\",\"p\":\"QP\",\"e\":\"7\",\"h\":\"USA\",\"g\":\"SVK\",\"r\":\"5-1\",\"s\":\"?\"},{\"n\":\"20\",\"d\":\"2018-02-20\",\"t\":\"16:40 GMT+9\",\"v\":\"1\",\"p\":\"QP\",\"e\":\"7\",\"h\":\"SLO\",\"g\":\"NOR\",\"r\":\"1-2\",\"s\":\"OT\"},{\"n\":\"21\",\"d\":\"2018-02-20\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"QP\",\"e\":\"7\",\"h\":\"FIN\",\"g\":\"KOR\",\"r\":\"5-2\",\"s\":\"?\"},{\"n\":\"22\",\"d\":\"2018-02-20\",\"t\":\"21:10 GMT+9\",\"v\":\"2\",\"p\":\"QP\",\"e\":\"7\",\"h\":\"SUI\",\"g\":\"GER\",\"r\":\"1-2\",\"s\":\"OT\"},{\"n\":\"23\",\"d\":\"2018-02-21\",\"t\":\"12:10 GMT+9\",\"v\":\"1\",\"p\":\"QF\",\"e\":\"7\",\"h\":\"CZE\",\"g\":\"USA\",\"r\":\"3-2\",\"s\":\"PSS\"},{\"n\":\"24\",\"d\":\"2018-02-21\",\"t\":\"16:40 GMT+9\",\"v\":\"1\",\"p\":\"QF\",\"e\":\"7\",\"h\":\"OAR\",\"g\":\"NOR\",\"r\":\"6-1\",\"s\":\"?\"},{\"n\":\"25\",\"d\":\"2018-02-21\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"QF\",\"e\":\"7\",\"h\":\"CAN\",\"g\":\"FIN\",\"r\":\"1-0\",\"s\":\"?\"},{\"n\":\"26\",\"d\":\"2018-02-21\",\"t\":\"21:10 GMT+9\",\"v\":\"2\",\"p\":\"QF\",\"e\":\"7\",\"h\":\"SWE\",\"g\":\"GER\",\"r\":\"3-4\",\"s\":\"OT\"},{\"n\":\"27\",\"d\":\"2018-02-23\",\"t\":\"16:40 GMT+9\",\"v\":\"1\",\"p\":\"SF\",\"e\":\"7\",\"h\":\"CZE\",\"g\":\"OAR\",\"r\":\"0-3\",\"s\":\"?\"},{\"n\":\"28\",\"d\":\"2018-02-23\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"SF\",\"e\":\"7\",\"h\":\"CAN\",\"g\":\"GER\",\"r\":\"3-4\",\"s\":\"?\"},{\"n\":\"29\",\"d\":\"2018-02-24\",\"t\":\"21:10 GMT+9\",\"v\":\"1\",\"p\":\"BMG\",\"e\":\"7\",\"h\":\"CZE\",\"g\":\"CAN\",\"r\":\"4-6\",\"s\":\"?\"},{\"n\":\"30\",\"d\":\"2018-02-25\",\"t\":\"13:10 GMT+9\",\"v\":\"1\",\"p\":\"GMG\",\"e\":\"7\",\"h\":\"OAR\",\"g\":\"GER\",\"r\":\"4-3\",\"s\":\"OT\"}]}";
        ObjectMapper mapper = new ObjectMapper();
        try {
            Sutaz sutaz = mapper.readValue(json, Sutaz.class);
            System.out.println("sutaz=" + sutaz);
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    @Test
    public void writeValueTest() {
        System.out.println("writeValueTest()");

        Sutaz sutaz = new Sutaz();
        sutaz.setHry(new ArrayList<Hra>());

        Hra hra = new Hra();
        hra.setId("1");
        hra.setDatum("2018-02-27");
        hra.setCas("21:10 GMT+9");
        hra.setStadion("1");
        hra.setFaza("PRE");
        hra.setSkupina("B");
        hra.setE("7");
        hra.setDomaci("SVK");
        hra.setHostia("OAR");
        hra.setVysledok("3-2");
        hra.setPredlzenie("?");
        sutaz.getHry().add(hra);
        sutaz.getHry().add(hra);
        sutaz.getHry().add(hra);

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        ObjectMapper mapper = new ObjectMapper();
        try {
            mapper.writeValue(out, sutaz);
            System.out.println("json=" + out.toString());
        } catch (IOException e) {
            e.printStackTrace(System.err);
        }
    }

    @Test
    public void parseDateTest() throws ParseException {
        System.out.println("parseDateTest()");

        Date datumOri = DateUtils.parseDate("15.02.2018 06:10", new String[]{"dd.MM.yyyy HH:mm"});
        System.out.println("datumOri=" + datumOri);

        Hra hra = new Hra();
        hra.setDatum("2018-02-14");
        hra.setCas("21:10 GMT+9");

        Date datum = hra.getDatumCas();
        Assert.assertTrue("datumy sa nerovnaju[ori=" + datumOri + ", parse=" + datum + "]",
                datumOri.equals(datum));
    }
}

class Tournament {
    int tournamentID;
    String name;
    String longName;
    String shortName;
    String displayName;
    int category;
    String categoryName;
    String startDate;
    String endDate;
    String logo;
    String tournamentDescription;
    int type;
    String gamePhases;
    String venues;
    int icerink;
    int versionRequired;
    int featured;
    int featuredPosition;
    String bannerUrl;
    String graphicThemePrefix;
    String logoUrl;
    String featuredImageUrl;
    int sort;

    public int getTournamentID() {
        return tournamentID;
    }

    public void setTournamentID(int tournamentID) {
        this.tournamentID = tournamentID;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLongName() {
        return longName;
    }

    public void setLongName(String longName) {
        this.longName = longName;
    }

    public String getShortName() {
        return shortName;
    }

    public void setShortName(String shortName) {
        this.shortName = shortName;
    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public int getCategory() {
        return category;
    }

    public void setCategory(int category) {
        this.category = category;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getEndDate() {
        return endDate;
    }

    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    public String getLogo() {
        return logo;
    }

    public void setLogo(String logo) {
        this.logo = logo;
    }

    public String getTournamentDescription() {
        return tournamentDescription;
    }

    public void setTournamentDescription(String tournamentDescription) {
        this.tournamentDescription = tournamentDescription;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public String getGamePhases() {
        return gamePhases;
    }

    public void setGamePhases(String gamePhases) {
        this.gamePhases = gamePhases;
    }

    public String getVenues() {
        return venues;
    }

    public void setVenues(String venues) {
        this.venues = venues;
    }

    public int getIcerink() {
        return icerink;
    }

    public void setIcerink(int icerink) {
        this.icerink = icerink;
    }

    public int getVersionRequired() {
        return versionRequired;
    }

    public void setVersionRequired(int versionRequired) {
        this.versionRequired = versionRequired;
    }

    public int getFeatured() {
        return featured;
    }

    public void setFeatured(int featured) {
        this.featured = featured;
    }

    public int getFeaturedPosition() {
        return featuredPosition;
    }

    public void setFeaturedPosition(int featuredPosition) {
        this.featuredPosition = featuredPosition;
    }

    public String getBannerUrl() {
        return bannerUrl;
    }

    public void setBannerUrl(String bannerUrl) {
        this.bannerUrl = bannerUrl;
    }

    public String getGraphicThemePrefix() {
        return graphicThemePrefix;
    }

    public void setGraphicThemePrefix(String graphicThemePrefix) {
        this.graphicThemePrefix = graphicThemePrefix;
    }

    public String getLogoUrl() {
        return logoUrl;
    }

    public void setLogoUrl(String logoUrl) {
        this.logoUrl = logoUrl;
    }

    public String getFeaturedImageUrl() {
        return featuredImageUrl;
    }

    public void setFeaturedImageUrl(String featuredImageUrl) {
        this.featuredImageUrl = featuredImageUrl;
    }

    public int getSort() {
        return sort;
    }

    public void setSort(int sort) {
        this.sort = sort;
    }

    @Override
    public String toString() {
        return "Tournament{" +
                "tournamentID=" + tournamentID +
                ", name='" + name + '\'' +
                ", longName='" + longName + '\'' +
                ", shortName='" + shortName + '\'' +
                ", displayName='" + displayName + '\'' +
                ", category=" + category +
                ", categoryName='" + categoryName + '\'' +
                ", startDate='" + startDate + '\'' +
                ", endDate='" + endDate + '\'' +
                ", logo='" + logo + '\'' +
                ", tournamentDescription='" + tournamentDescription + '\'' +
                ", type=" + type +
                ", gamePhases='" + gamePhases + '\'' +
                ", venues='" + venues + '\'' +
                ", icerink=" + icerink +
                ", versionRequired=" + versionRequired +
                ", featured=" + featured +
                ", featuredPosition=" + featuredPosition +
                ", bannerUrl='" + bannerUrl + '\'' +
                ", graphicThemePrefix='" + graphicThemePrefix + '\'' +
                ", logoUrl='" + logoUrl + '\'' +
                ", featuredImageUrl='" + featuredImageUrl + '\'' +
                ", sort=" + sort +
                '}';
    }
}

@JsonIgnoreProperties(ignoreUnknown = true)
class Game {
    Long dateTime;
    String gamePhase;
    String group;
    String homeTeam;
    String guestTeam;
    String scoreByPeriod;
    int progressPerc;
    String progressCode;
    int homeTeamScore;
    int guestTeamScore;
    String highlightsVimeoURL;

    public Long getDateTime() {
        return dateTime;
    }

    public void setDateTime(Long dateTime) {
        this.dateTime = dateTime;
    }

    public String getGamePhase() {
        return gamePhase;
    }

    public void setGamePhase(String gamePhase) {
        this.gamePhase = gamePhase;
    }

    public String getGroup() {
        return group;
    }

    public void setGroup(String group) {
        this.group = group;
    }

    public String getHomeTeam() {
        return homeTeam;
    }

    public void setHomeTeam(String homeTeam) {
        this.homeTeam = homeTeam;
    }

    public String getGuestTeam() {
        return guestTeam;
    }

    public void setGuestTeam(String guestTeam) {
        this.guestTeam = guestTeam;
    }

    public String getScoreByPeriod() {
        return scoreByPeriod;
    }

    public void setScoreByPeriod(String scoreByPeriod) {
        this.scoreByPeriod = scoreByPeriod;
    }

    public int getProgressPerc() {
        return progressPerc;
    }

    public void setProgressPerc(int progressPerc) {
        this.progressPerc = progressPerc;
    }

    public String getProgressCode() {
        return progressCode;
    }

    public void setProgressCode(String progressCode) {
        this.progressCode = progressCode;
    }

    public int getHomeTeamScore() {
        return homeTeamScore;
    }

    public void setHomeTeamScore(int homeTeamScore) {
        this.homeTeamScore = homeTeamScore;
    }

    public int getGuestTeamScore() {
        return guestTeamScore;
    }

    public void setGuestTeamScore(int guestTeamScore) {
        this.guestTeamScore = guestTeamScore;
    }

    public String getHighlightsVimeoURL() {
        return highlightsVimeoURL;
    }

    public void setHighlightsVimeoURL(String highlightsVimeoURL) {
        this.highlightsVimeoURL = highlightsVimeoURL;
    }

    @Override
    public String toString() {
        return "Game{" +
                "dateTime=" + dateTime +
                ", timestamp='" + new Timestamp(dateTime*1000) + '\'' +
                ", gamePhase='" + gamePhase + '\'' +
                ", group='" + group + '\'' +
                ", homeTeam='" + homeTeam + '\'' +
                ", guestTeam='" + guestTeam + '\'' +
                ", scoreByPeriod='" + scoreByPeriod + '\'' +
                ", progressPerc=" + progressPerc +
                ", progressCode='" + progressCode + '\'' +
                ", homeTeamScore=" + homeTeamScore +
                ", guestTeamScore=" + guestTeamScore +
                ", highlightsVimeoURL='" + highlightsVimeoURL + '\'' +
                '}';
    }
}

class Sutaz {
    @JsonProperty("games")
    private List<Hra> hry;

    public List<Hra> getHry() {
        return hry;
    }

    public void setHry(List<Hra> hry) {
        this.hry = hry;
    }

    @Override
    public String toString() {
        return "Sutaz{" +
                "hry=" + hry +
                '}';
    }
}

class Hra {
    @JsonProperty("n")
    private String id;
    @JsonProperty("d")
    private String datum;
    @JsonProperty("t")
    private String cas;
    @JsonProperty("v")
    private String stadion;
    @JsonProperty("p")
    private String faza;
    @JsonProperty("group")
    private String skupina;
    private String e;
    @JsonProperty("h")
    private String domaci;
    @JsonProperty("g")
    private String hostia;
    @JsonProperty("r")
    private String vysledok;
    @JsonProperty("s")
    private String predlzenie;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDatum() {
        return datum;
    }

    public void setDatum(String datum) {
        this.datum = datum;
    }

    public String getCas() {
        return cas;
    }

    public void setCas(String cas) {
        this.cas = cas;
    }

    /**
     * Spojenie datumu a casu.
     *
     * @return datum s casom
     */
    public Date getDatumCas() {
        if (StringUtils.isNotEmpty(datum) && StringUtils.isNotEmpty(cas)) {
            try {
                String datumCas = String.format("%s %s", datum, cas);
                datumCas = StringUtils.substringBefore(datumCas, " GMT");
                String zone = StringUtils.substringAfter(cas, "GMT");
                Date date = DateUtils.parseDate(datumCas, new String[]{"yyyy-MM-dd HH:mm"});
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(date);
                calendar.add(Calendar.HOUR_OF_DAY, Integer.parseInt(zone));

                return calendar.getTime();
            } catch (ParseException e) {
                e.printStackTrace(System.err);

                return null;
            }
        }

        return null;
    }

    public String getStadion() {
        return stadion;
    }

    public void setStadion(String stadion) {
        this.stadion = stadion;
    }

    public String getFaza() {
        return faza;
    }

    public void setFaza(String faza) {
        this.faza = faza;
    }

    public String getSkupina() {
        return skupina;
    }

    public void setSkupina(String skupina) {
        this.skupina = skupina;
    }

    public String getE() {
        return e;
    }

    public void setE(String e) {
        this.e = e;
    }

    public String getDomaci() {
        return domaci;
    }

    public void setDomaci(String domaci) {
        this.domaci = domaci;
    }

    public String getHostia() {
        return hostia;
    }

    public void setHostia(String hostia) {
        this.hostia = hostia;
    }

    public String getVysledok() {
        return vysledok;
    }

    public void setVysledok(String vysledok) {
        this.vysledok = vysledok;
    }

    public String getPredlzenie() {
        return predlzenie;
    }

    public void setPredlzenie(String predlzenie) {
        this.predlzenie = predlzenie;
    }

    @Override
    public String toString() {
        return "Hra{" +
                "id='" + id + '\'' +
                ", datum=" + datum +
                ", cas='" + cas + '\'' +
                ", stadion='" + stadion + '\'' +
                ", faza='" + faza + '\'' +
                ", skupina='" + skupina + '\'' +
                ", e='" + e + '\'' +
                ", domaci='" + domaci + '\'' +
                ", hostia='" + hostia + '\'' +
                ", vysledok='" + vysledok + '\'' +
                ", predlzenie='" + predlzenie + '\'' +
                '}';
    }
}