package sk.di.tipovacka.test.service;

import junit.framework.Assert;
import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.comparator.TipNaZapasDatumComparator;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.TipNaZapasService;
import sk.di.tipovacka.test.dao.SutazRocnikDaoTest;

import javax.annotation.Resource;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * Testovacka pre tip na zapas
 *
 * User: filusik
 * Date: 20.6.2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class TipNaZapasServiceTest {

    @Resource
    private TipNaZapasService tipNaZapasService;

    @Resource
    private SutazRocnikService sutazRocnikService;

    @Test
    public void getTipujucichNaZapasTest() {
        System.out.println("getTipujucichNaZapasTest()");
        Long rocnikId = 4L;
        List<PouzivatelDto> tiperi = tipNaZapasService.getTipujucichNaZapas(rocnikId);

        Assert.assertTrue("pre rocnik[id=" + rocnikId + "] neboli najdeny ziadni tipujuci",
                !tiperi.isEmpty());
        System.out.println("tipujuci[rocnikId=" + rocnikId + ", pocet=" + tiperi.size() + "]");
    }

    @Test
    public void findTipyNaZapasTest() {
        System.out.println("findTipyNaZapasTest");

        Long pouzivatelId = 107L;
        RocnikSutazeDto rocnik = sutazRocnikService.getRocnikSutaze(16L);
        TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
        kriteria.getPouzivatelIds().add(pouzivatelId);
        kriteria.setSutazRocnikId(rocnik.getId());
        List<TipNaZapasDto> tipy = tipNaZapasService.findTipyNaZapas(kriteria);
        Assert.assertTrue("ziadne typy pre aktualny rocnik[id=" + rocnik.getId() + "] neboli dohladane",
                !tipy.isEmpty());
        Collections.sort(tipy, new TipNaZapasDatumComparator());
        for (TipNaZapasDto tip : tipy) {
            System.out.println("tip[" + tip.getZapas().getDatum()
                    + ", " + tip.getZapas().getDomaci().getKod()
                    + ", " + tip.getZapas().getHostia().getKod()
                    + ", " + tip.getZapas().getGolyDomaci()
                    + ", " + tip.getZapas().getGolyHostia()
                    + ", " + tip.getGolyDomaci()
                    + ", " + tip.getGolyHostia()
                    + "]");
        }
    }
}
