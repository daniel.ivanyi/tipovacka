package sk.di.tipovacka.test.dao;

import junit.framework.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.TipNaVitaza;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.dao.TipNaVitazaDao;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;

import javax.annotation.Resource;
import java.util.List;

/**
 * Testovacka pre rocnik sutaze dao.
 *
 * User: Dano
 * Date: 9.7.2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class SutazRocnikDaoTest {

    @Resource
    private SutazRocnikDao sutazRocnikDao;

    @Resource
    private TipNaVitazaDao tipNaVitazaDao;

    @Resource
    private TipNaZapasDao tipNaZapasDao;

    @Test
    public void getAktualnyRocnikTest() {
        System.out.println("getAktualnyRocnikTest");

        List<SutazRocnik> rocniky = sutazRocnikDao.getAktualnyRocnikSutaze();
        Assert.assertTrue("ziaden aktualny rocnik nebol najdeny",
                rocniky.isEmpty());
        for (SutazRocnik rocnik : rocniky) {
            Assert.assertTrue("najdeny rocnik nie je aktualny",
                    rocnik.isAktualny());
            System.out.println("aktualnyRocnik[id=" + rocnik.getId() + "]");
        }
    }

    @Test
    public void clearTest() {
        Long rocnikId = 4L;
        try {
            sutazRocnikDao.clearTipy(rocnikId);
        } catch (Exception e) {
            e.printStackTrace();
            Assert.fail("padlo do chyby," + e);
        }

        // overenie, ci zostali nejake zapasy
        SutazRocnik rocnik = sutazRocnikDao.get(rocnikId);
        Assert.assertTrue("boli najdene zapasy pre rocnik",
                rocnik.getZapasy().isEmpty());

        // overenie ci zostali nejake tipy na vitaza
        List<TipNaVitaza> tipyVitaz = tipNaVitazaDao.getTipyNaVitaza(rocnikId);
        Assert.assertTrue("boli najdene tipy na vitaza pre rocnik",
                tipyVitaz.isEmpty());

        // overenie ci zostali nejake tipy na zapasy
        TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
        kriteria.setSutazRocnikId(rocnikId);
        List<TipNaZapas> tipyZapas = tipNaZapasDao.findTipyNaZapas(kriteria);
        Assert.assertTrue("boli najdene tipy na zapas rocnika",
                tipyZapas.isEmpty());
    }
}
