package sk.di.tipovacka.test.service;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.apache.commons.lang.builder.ToStringStyle;
import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.SportTypEnum;
import sk.di.tipovacka.api.enums.SutazTypEnum;
import sk.di.tipovacka.api.service.MuzstvoService;
import sk.di.tipovacka.api.service.ZapasService;
import sk.di.tipovacka.impl.service.FootballDataServiceImpl;

import javax.annotation.Resource;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * User: Dano
 * Date: 12.5.2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class ZapasServiceTest {

    private final Logger logger = Logger.getLogger(getClass());

    private String autor = "junit";

    @Resource
    ZapasService service;

    @Resource
    MuzstvoService muzstvoService;

    @Resource
    private FootballDataServiceImpl footballDataService;

    @Test
    public void addZapasTest() {
        System.out.println("addZapasTest()");

        ZapasDto zapas = new ZapasDto();
        Long zapasId = service.addZapas(autor, zapas);
        Assert.assertTrue("zapas nebol pridany", zapasId != null);
    }

    @Test
    public void getZapasReportIihfTest() {
        System.out.println("getZapasReportIihfTest()");
        String iihf2012 = "http://stats.iihf.com/Hydra/272/index.html";
        String iihf2013 = "http://stats.iihf.com/Hydra/352/index.html";
        InputStream is = null;
        try {
            is = new URL(iihf2013).openStream();
        } catch (IOException e) {
            Assert.fail(e.getLocalizedMessage());
        }

        List<ZapasDto> report = service.getZapasReport(iihf2012, SportTypEnum.HOKEJ, SutazTypEnum.MAJSTROVSTVA_SVETA, false);
        Assert.assertNotNull("report je null", report);
        Assert.assertTrue("report je prazdny", !report.isEmpty());

        for (ZapasDto z : report) {
            System.out.println("zapas="
                    + ToStringBuilder.reflectionToString(z, ToStringStyle.SHORT_PREFIX_STYLE));
        }
    }

    @Test
    public void isUrlOkTest() {
        System.out.println("isUrlOkTest()");

        List<String> urls = Arrays.asList(
//                "http://sport.aktuality.sk/vysledky/futbal/kvalifikacia-ms-2014/program/",
                "http://sport.aktuality.sk/ms-vo-futbale-2014-brazilia/program"
        );

        for (String u : urls) {

            System.out.println("spracovavana url=" + u);

            try {
                URL url = new URL(u);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.addRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64; rv:30.0) Gecko/20100101 Firefox/30.0");
                connection.setRequestMethod("GET");
                connection.connect();

                int code = connection.getResponseCode();
                Assert.assertTrue("HTTP status kode[" + code + "] nie je ok",
                        200 == code);
                InputStream is = connection.getInputStream();

//                // stranka do textu
//                Reader r = new InputStreamReader(is, "UTF-8");
//                StringBuilder buf = new StringBuilder();
//                while (true) {
//                    int ch = r.read();
//                    if (ch < 0)
//                        break;
//                    buf.append((char) ch);
//                }
//                String str = buf.toString();
//                System.out.println("page=" + str);

                List<ZapasDto> report = service.getZapasReport("", SportTypEnum.FUTBAL, SutazTypEnum.MAJSTROVSTVA_SVETA, false);
                Assert.assertNotNull("report je null", report);
                Assert.assertTrue("report je prazdny", !report.isEmpty());

                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                for (ZapasDto zapas : report) {
                    logger.debug("zapas["
                            + "datum=" + sdf.format(zapas.getDatum())
                            + ", domaci=" + (zapas.getDomaci() == null ? "" : zapas.getDomaci().getKod())
                            + ", hostia=" + (zapas.getHostia() == null ? "" : zapas.getHostia().getKod())
                            + ", skore=" + zapas.getSkore()
                            + ", priebeh=" + zapas.getPriebeh()
                            + ", koniec=" + zapas.getKoniecZapasuTyp()
                            + ", typ=" + zapas.getTypZapasu()
                            + ", skupina=" + zapas.getSkupina()
                            + "]");
                }
            } catch (MalformedURLException e) {
                e.printStackTrace(System.err);
                Assert.fail("MalformedURLException");
            } catch (ProtocolException e) {
                e.printStackTrace(System.err);
                Assert.fail("ProtocolException");
            } catch (IOException e) {
                e.printStackTrace(System.err);
                Assert.fail("IOException");
            } catch (Exception e) {
                e.printStackTrace(System.err);
                Assert.fail("Exception");
            }
        }
    }

    @Test
    public void getZapasReportAktualityTest() {
        System.out.println("getZapasReportAktualityTest()");

        List<String> urls = Arrays.asList(
//                "http://sport.aktuality.sk/vysledky/futbal/kvalifikacia-ms-2014/program/",
                "http://sport.aktuality.sk/ms-vo-futbale-2014-brazilia/program"
        );

        for (String url : urls) {
            try {
                System.out.println("spracovavana url=" + url);
                InputStream is = new URL(url).openStream();//getClass().getClassLoader().getResourceAsStream(url);

                List<ZapasDto> report = service.getZapasReport("", SportTypEnum.FUTBAL, SutazTypEnum.MAJSTROVSTVA_SVETA, false);
                Assert.assertNotNull("report je null", report);
                Assert.assertTrue("report je prazdny", !report.isEmpty());

                SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");
                for (ZapasDto zapas : report) {
                    Assert.assertNotNull("datum zapasu je null", zapas.getDatum());
                    Assert.assertNotNull("domaci su null", zapas.getDomaci());
                    Assert.assertNotNull("hostia su null", zapas.getHostia());
                    Assert.assertNotNull("typ zapasu je null", zapas.getTypZapasu());

                    logger.debug("zapas["
                            + "datum=" + sdf.format(zapas.getDatum())
                            + ", domaci=" + zapas.getDomaci().getKod()
                            + ", hostia=" + zapas.getHostia().getKod()
                            + ", skore=" + zapas.getSkore()
                            + ", priebeh=" + zapas.getPriebeh()
                            + ", koniec=" + zapas.getKoniecZapasuTyp()
                            + ", typ=" + zapas.getTypZapasu()
                            + ", skupina=" + zapas.getSkupina()
                            + "]");
                }
            } catch (Exception e) {
                e.printStackTrace();
                Assert.fail();
            }
        }
    }

    @Test
    public void mozemParsovatSportkyZoznam() throws IOException {
        System.out.println("mozemParsovatSportkyZoznam");

        // https://api.football-data.org/
        // http://api.football-data.org/v1/competitions/467
        // http://api.football-data.org/v1/competitions/467/fixtures

        String statistika = "http://api.football-data.org/v1/competitions/467";
        List<ZapasDto> zapasy = footballDataService.getZapasReport(statistika, false);
        for (ZapasDto z : zapasy) {
            System.out.println("zapas=" + z);
        }
    }

    /**
     * Vrati mapu zapasov, kde klucik je jednoznacny identifikator.
     *
     * @param zapasy zoznam zapasov
     * @return mapa zapasov
     */
    private Map<String, ZapasDto> getZapasyMapa(List<ZapasDto> zapasy) {
        Map<String, ZapasDto> mapa = new HashMap<>();

        if (zapasy != null) {
            for (ZapasDto z : zapasy) {
                // nasetujem iba tie zapasy, kde su jasni superi
                if (z.getDomaci() != null && z.getHostia() != null) {
                    mapa.put(getZapasyMapaKey(z), z);
                }
            }
        }

        return mapa;

    }

    /**
     * Vrati jednoznacny identifikator zapasu.
     *
     * @param zapas zapas
     * @return identifikator
     */
    private String getZapasyMapaKey(ZapasDto zapas) {
        if (zapas == null) {
            return null;
        }

        SimpleDateFormat sdf = new SimpleDateFormat("yyyyMMddHHmm");

        return sdf.format(zapas.getDatum())
                + "_" + zapas.getDomaci().getKod()
                + "_" + zapas.getHostia().getKod();
    }
}
