package sk.di.tipovacka.test.service;

import junit.framework.Assert;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.design.JasperDesign;
import net.sf.jasperreports.engine.xml.JRXmlLoader;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.encoding.PasswordEncoder;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.db.SutazRocnik;
import sk.di.tipovacka.api.db.TipNaZapas;
import sk.di.tipovacka.api.db.dao.SutazRocnikDao;
import sk.di.tipovacka.api.db.dao.TipNaZapasDao;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.enums.RolaEnum;
import sk.di.tipovacka.api.service.PouzivatelService;

import javax.annotation.Resource;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Test pre servis na pracu s pouzivatelom
 *
 * User: Dano
 * Date: 9.5.2013
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration(defaultRollback = true)
@Transactional
public class PouzivatelServiceTest {

    @Resource
    private SutazRocnikDao sutazRocnikDao;

    @Resource
    private TipNaZapasDao tipNaZapasDao;

    @Resource
    private PouzivatelService pouzivatelService;

    @Resource
    private PasswordEncoder passwordEncoder;

    @Value("${mail.pouzivatel.pridany.text}")
    private String text;
    @Value("${url.app.home}")
    private String urlAppHome;

    @Test
    public void getShaHesloTest() {
        String login = "admin";
        String heslo = "kl[4ikodtipova4ka";
        String hesloSha = passwordEncoder.encodePassword(heslo, login);
        System.out.println("login=" + login + ", heslo=" + heslo + ", hesloSha=" + hesloSha);
    }

    @Test
    public void addPouzivatelTest() {
        System.out.println("addPouzivatelTest()");
        PouzivatelDto pouzivatel = new PouzivatelDto();
        pouzivatel.setLogin("junit");
        pouzivatel.setHeslo("Junit123");
        pouzivatel.setMeno("Junit");
        pouzivatel.setPriezvisko("Testic");
        pouzivatel.setMail("tipovacka.app@gmail.com");
        pouzivatel.getRoly().add(RolaEnum.ADMIN);

        Long pouzivatelId = pouzivatelService.addPouzivatel("junit", pouzivatel);

        Assert.assertTrue("pouzivatelId je null", pouzivatelId != null);
    }

    @Test
    public void test() {
        String mailText = String.format(text,
                new String[] {
                        urlAppHome,
                        "login",
                        "heslo"});
        System.out.println("text=" + mailText);
    }

    @Test
    public void getPouzivatelPodlaLoginuTest() {
        System.out.println("getPouzivatelPodlaLoginuTest");
        String login = "dano";
        PouzivatelDto pouzivatel = pouzivatelService.getPouzivatelPodlaLoginu(login);
        System.out.println("pouzivatel=" + pouzivatel);

        Assert.assertTrue("pouzivatel[login=" + login + "] nebol najdeny",
                pouzivatel != null);
        Assert.assertTrue("pouzivatel[login=" + pouzivatel.getLogin() + "] nema spravny login=" + login,
                login.equals(pouzivatel.getLogin()));
    }

    @Test
    public void odoslaniePouzivatelovychTipovTest() {
        System.out.println("odoslaniePouzivatelovychTipovTest");

        Long pouzivatelId = 107L;
        try {
            pouzivatelService.odoslaniePouzivatelovychTipov(pouzivatelId);
        } catch (Exception e) {
            e.printStackTrace(System.out);
            Assert.fail("padlo do chyby");
        }
    }

    @Test
    public void tipyPouzivatelaTest() {

        Long pouzivatelId = 107L;
        Long rocnikId = 16L;

        SutazRocnik rocnik = sutazRocnikDao.get(rocnikId);
        TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
        kriteria.getPouzivatelIds().add(pouzivatelId);
        kriteria.setSutazRocnikId(rocnik.getId());
        List<TipNaZapas> tipy = tipNaZapasDao.findTipyNaZapas(kriteria);
        Collections.sort(tipy, new Comparator<TipNaZapas>() {
            @Override
            public int compare(TipNaZapas o1, TipNaZapas o2) {
                // ked nepoznam tip, alebo zapas, nedokazem sortovat
                if (o1 == null || o2 == null) {
                    return 0;
                }
                if (o1.getZapas() == null || o2.getZapas() == null) {
                    return 0;
                }

                return o1.getZapas().getDatum().compareTo(o2.getZapas().getDatum()) * -1;
            }
        });
        HashMap<String, Object> parameterMap = new HashMap<>();
        parameterMap.put("login", "JUnit");
        parameterMap.put("sport", rocnik.getSutaz().getSportTyp().name().toLowerCase());
        parameterMap.put("nazovSutaze", rocnik.getSutaz().getNazov());
        parameterMap.put("rocnikSutaze", String.valueOf(rocnik.getRok()));
        parameterMap.put("datumGenerovania", new SimpleDateFormat("dd.MM.yyyy HH:mm").format(new Date()));

        JasperDesign design;
        JasperReport report;
        JasperPrint jasperPrint;

        try {
            design = JRXmlLoader.load(this.getClass().getClassLoader().getResourceAsStream("pdf/mojeTipy.jrxml"));
            report = JasperCompileManager.compileReport(design);
            jasperPrint = JasperFillManager.fillReport(report,
                    parameterMap,
                    new JRBeanCollectionDataSource(tipy));
            byte[] pdf = JasperExportManager.exportReportToPdf(jasperPrint);
            IOUtils.write(pdf, new FileOutputStream("mojeTipy.pdf"));
        } catch (Exception e) {
            e.printStackTrace(System.err);
            Assert.fail("padlo do chyby");
        }
    }
}
