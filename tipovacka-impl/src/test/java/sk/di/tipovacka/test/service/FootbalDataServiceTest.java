package sk.di.tipovacka.test.service;


import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.transaction.TransactionConfiguration;
import org.springframework.transaction.annotation.Transactional;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.impl.service.FootballDataServiceImpl;

import javax.annotation.Resource;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:applicationContext-test.xml"})
@TransactionConfiguration
@Transactional
public class FootbalDataServiceTest {

    @Resource
    private FootballDataServiceImpl service;

    @Test
    public void mozemNacitatZapasy() {
        String url = "http://api.football-data.org/v1/competitions/467";
        List<ZapasDto> zapasy = service.getZapasReport(url, false);

        Assert.assertTrue("ziadne zapasy pre turnaj",
                !zapasy.isEmpty());
        System.out.println("zapasy:" + zapasy);
    }
}
