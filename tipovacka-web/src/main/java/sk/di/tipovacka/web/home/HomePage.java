package sk.di.tipovacka.web.home;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.Page;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.ComponentTag;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Button;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.joda.time.DateMidnight;
import sk.di.tipovacka.api.comparator.MuzstvoNazovComparator;
import sk.di.tipovacka.api.comparator.TipNaZapasDatumComparator;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;
import sk.di.tipovacka.api.dto.*;
import sk.di.tipovacka.api.enums.OrderDirectionEnum;
import sk.di.tipovacka.api.enums.SportTypEnum;
import sk.di.tipovacka.api.exception.CasNaTipovanieVyprsalException;
import sk.di.tipovacka.api.service.*;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.commons.panel.NadpisPanel;
import sk.di.tipovacka.web.commons.panel.vlajka.klub.VlajkaKlubPanel;
import sk.di.tipovacka.web.commons.panel.vlajka.stat.VlajkaStatPanel;
import sk.di.tipovacka.web.home.podmienky.PodmienkyPage;
import sk.di.tipovacka.web.home.vyber.VyberPage;
import sk.di.tipovacka.web.menu.MenuItemEnum;
import sk.di.tipovacka.web.utils.ParserUtils;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

public class HomePage extends Template {
    private static final long serialVersionUID = 1L;

    private final SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    private final SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    private final SimpleDateFormat fullFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm:ss");

    private final Random random = new Random();
    // dostupne moznosti na tipovanie golov
    private static final List<Integer> goly =
            Arrays.asList(0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10);

    @SpringBean
    private SutazRocnikService sutazRocnikService;
    @SpringBean
    private SutazRocnikSuhlasService sutazRocnikSuhlasService;
    @SpringBean
    private TipNaVitazaService tipNaVitazaService;
    @SpringBean
    private MuzstvoService muzstvoService;
    @SpringBean
    private ZapasService zapasService;
    @SpringBean
    private TipNaZapasService tipNaZapasService;
    @SpringBean
    private StatistikaService statistikaService;

    private TipNaVitazaDto tipNaVitaza;

    // mapa tipov na zapas, kde klucik je den, kedy sa zapasy hraju
    // a hodnota je zoznam tipo na zapas pre dany den
    private Map<Date, List<TipNaZapasDto>> tipNaZapasPodlaDna = new HashMap<>();

    // datum, za ktory su zobrazene zapasy a tipy
    private Date vybranyDatum;

    // zoznam tipov pre dany datum
    private List<TipNaZapasDto> tipy = new ArrayList<>();

    // container tipy na zapas
    private WebMarkupContainer tipNaZapasDiv = new WebMarkupContainer("tipNaZapasDiv");
    // model, ktory si drzi tipy na zapas
    final CompoundPropertyModel<List<TipNaZapasDto>> tipNaZapasModel = new CompoundPropertyModel<List<TipNaZapasDto>>(tipy);
    // link dopredu
    private AjaxLink<String> linkDopredu;
    // link dozadu
    private AjaxLink<String> linkDozadu;
    // label aktualny den
    private Label vybranyDenLabel;
    private Label vybranyDenTyzdniLabel;
    // button na spracovanie tipov na zapas
    private Button spracujButton = new Button("spracovatTipyNaZapasButton");
    private Link nahodneTipyDenButton;

    public HomePage(final PageParameters parameters) {
        super(parameters);

        // ak pouzivatel nema vybranu ziadnu aktivnu sutaz, presmerujeme ho na stranku vyberu sutazi
        if (vybranyRocnik == null) {
            throw new RestartResponseException(VyberPage.class, parameters.add("forward", "home"));
        } else {
            // ak je vybrany rocnik, overime, ci prihlaseny pouzivatel ma pristup na dany rocnik
            if (!pouzivatel.isSuhlas(vybranyRocnik.getId())) {
                throw new RestartResponseException(PodmienkyPage.class);
            }
        }

        // nadpis - panel
        add(new NadpisPanel("nadpisPanel", getString("nadpis.home"), vybranyRocnik));

        // priznak, ci pouzivatel ma splnen podmienky ucasti
        boolean isSplnenePodmienky = sutazRocnikSuhlasService.isSplnenePodmienky(pouzivatel.getLogin(), vybranyRocnik.getId());
        // info o tom, ze pouzivatel si nesplnil podmienky ucasti
        WebMarkupContainer nesplnilPodmienkyPanel = new WebMarkupContainer("nesplnilPodmienkyPanel");
        nesplnilPodmienkyPanel.setVisible(!isSplnenePodmienky);
        add(nesplnilPodmienkyPanel);

        // TIP NA VITAZA
        // dotiahneme si pre aktualne prihlaseneho pouzivatela tip na vitaza pre vybrany rocnik
        tipNaVitaza = tipNaVitazaService.getPouzivatelovTipNaVitaza(pouzivatel.getId(), vybranyRocnik.getId());
        if (tipNaVitaza == null) {
            tipNaVitaza = new TipNaVitazaDto();
            tipNaVitaza.setRocnikSutaze(vybranyRocnik);
            tipNaVitaza.setPouzivatel(pouzivatel);
        }

        // priznak, ci sa moze este tipovat
        boolean isMozemTipovatVitaza = !vybranyRocnik.isKoniecRocnika();

        // tip na vitaza - model
        final CompoundPropertyModel<TipNaVitazaDto> model = new CompoundPropertyModel<TipNaVitazaDto>(tipNaVitaza);

        // tip na vitaza - form, zobrazuje sa, ked sa da tipovat
        final Form<TipNaVitazaDto> tipForm = new Form<TipNaVitazaDto>("tipNaVitazaForm", model) {
            private static final long serialVersionUID = 6626706221510861922L;

            @Override
            protected void onSubmit() {

                parameters.set("datum", dateFormat.format(vybranyDatum));
                HomePage page;
                try {
                    if (tipNaVitaza.getId() == null) {
                        tipNaVitazaService.addTipNaVitaza(pouzivatel.getLogin(), tipNaVitaza);
                    } else {
                        // overime, ci sa tip na vitaza realne zmenil
                        TipNaVitazaDto staryTip = tipNaVitazaService.getTipNaVitaza(tipNaVitaza.getId());
                        if (!staryTip.getMuzstvo().getKodMuzstva().equals(tipNaVitaza.getMuzstvo().getKodMuzstva())) {
                            tipNaVitaza.setNahodnyTip(false);
                        }
                        tipNaVitazaService.editTipNaVitaza(pouzivatel.getLogin(), tipNaVitaza);
                    }
                    page = new HomePage(parameters);
                    page.info(getString("tip.vitaz.info.spracovany"));
                } catch (CasNaTipovanieVyprsalException e) {
                    page = new HomePage(parameters);
                    page.error(getString("tip.vitaz.error.casVyprsal"));
                } catch (Exception e) {
                    page = new HomePage(parameters);
                    page.error(getString("tip.vitaz.error.spracovat"));
                }
                setResponsePage(page);
            }
        };
        add(tipForm);

        // zaciatok a koniec rocnika
        final Date zaciatokRocnika = vybranyRocnik.getZaciatokRocnika();
        final Date koniecRocnika = vybranyRocnik.getKoniecRocnika();

        // status tipu - contaner
        WebMarkupContainer statusIcon = new WebMarkupContainer("statusIcon");
        String statusIconTipNaVitaza = "fa fa-fw fa-question";
        String titleTipNaVitaza = "Ešte netipované";
        if (tipNaVitaza.getId() != null) {
            statusIconTipNaVitaza = tipNaVitaza.isNahodnyTip() ? "fa fa-fw fa-thumbs-up" : "fa fa-fw fa-pencil";
            titleTipNaVitaza = tipNaVitaza.isNahodnyTip()
                    ? "Náhodny tip, posledná zmena " + getDatum(tipNaVitaza.getDatum())
                    : "Tip zadaný použivateľom, posledna zmena " + getDatum(tipNaVitaza.getDatum());
        }
        statusIcon.add(new AttributeAppender("class", isMozemTipovatVitaza ? statusIconTipNaVitaza : "fa fa-fw fa-lock"));
        statusIcon.add(new AttributeAppender("title", titleTipNaVitaza));
        tipForm.add(statusIcon);

        // vlajka - container
        final WebMarkupContainer tipNaVitazaVlajkaDiv = new WebMarkupContainer("tipNaVitazaVlajkaDiv");
        tipNaVitazaVlajkaDiv.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        tipForm.add(tipNaVitazaVlajkaDiv);

        // vlajka - icona
        tipNaVitazaVlajkaDiv.add(new VlajkaStatPanel("tipNaVitazaVlajka", tipNaVitaza.getMuzstvo(), false));

        // vyberieme z muzstiev iba tie, ktore realne sutazia v rocniku
        final List<MuzstvoDto> muzstvaRocnika = vybranyRocnik.getMuzstva();
        // vyhodime placeholdre
        List<MuzstvoDto> toRemove = new ArrayList<>();
        for (MuzstvoDto m : muzstvaRocnika) {
            if ("L61,L62,W61,W62,TBD".contains(m.getKod())) {
                toRemove.add(m);
            }
        }
        muzstvaRocnika.removeAll(toRemove);
        muzstvaRocnika.sort(new MuzstvoNazovComparator());

        // muzstvo - combo
        final DropDownChoice<MuzstvoDto> tipNaVitazaCombo = new DropDownChoice<MuzstvoDto>("tipNaVitaza",
                new PropertyModel<MuzstvoDto>(model, "muzstvo"),
                muzstvaRocnika,
                new ChoiceRenderer<MuzstvoDto>() {
                    private static final long serialVersionUID = -7923078574478258414L;

                    @Override
                    public Object getDisplayValue(MuzstvoDto object) {
                        if (object == null) {
                            return "";
                        }

                        return object.getNazov() + " (" + (object.getKodMuzstva()) + ")";
                    }
                }) {

            private static final long serialVersionUID = -270498390881455231L;

            @Override
            protected boolean isSelected(MuzstvoDto object, int index, String selected) {
                try {
                    return model.getObject().getMuzstvo().getKodMuzstva().equals(object.getKodMuzstva());
                } catch (Exception e) {
                    return super.isSelected(object, index, selected);
                }
            }

            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
        };
        tipNaVitazaCombo.setNullValid(true)
                .setRequired(true)
                .setEnabled(isMozemTipovatVitaza);
        // ak nemozem tipovat na vitaza sutaze oznamime to pouzivatelovi
        if (!isMozemTipovatVitaza) {
            tipNaVitazaCombo.add(new AttributeAppender("title", getString("tip.vitaz.rocnik.koniec")));
        }
        tipNaVitazaCombo.add(new AjaxFormComponentUpdatingBehavior("onchange") {
            private static final long serialVersionUID = -8492554328174229262L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                tipNaVitaza.setMuzstvo(tipNaVitazaCombo.getModelObject());
                target.add(tipNaVitazaVlajkaDiv);
            }
        });
        tipForm.add(tipNaVitazaCombo);

        // body za tip - label
        tipForm.add(new Label("body", Model.of(tipNaVitaza.getBody())));

        // spracovat - tlacitko
        tipForm.add(new Button("spracovatTipNaVitazaButton").setVisible(isMozemTipovatVitaza));
        // nahodny tip na vitaza - tlacitko
        Link nahodneTipyNaVitazaButton = new Link("nahodneTipyNaVitazaButton") {

            private static final long serialVersionUID = -415960083378466175L;

            @Override
            public void onClick() {
                parameters.set("datum", dateFormat.format(vybranyDatum));
                HomePage page;
                // nahodne vyberieme jedno z dostupnych muzstiev
                tipNaVitaza.setMuzstvo(muzstvaRocnika.get(random.nextInt(muzstvaRocnika.size())));
                tipNaVitaza.setNahodnyTip(true);
                try {
                    if (tipNaVitaza.getId() == null) {
                        tipNaVitazaService.addTipNaVitaza(pouzivatel.getLogin(), tipNaVitaza);
                    } else {
                        tipNaVitazaService.editTipNaVitaza(pouzivatel.getLogin(), tipNaVitaza);
                    }
                    page = new HomePage(parameters);
                    page.info(getString("tip.vitaz.nahodny.info.spracovany"));
                } catch (Exception e) {
                    page = new HomePage(parameters);
                    page.error(getString("tip.vitaz.nahodny.error.spracovat"));
                }
                setResponsePage(page);
            }
        };
        nahodneTipyNaVitazaButton.setVisible(isMozemTipovatVitaza);
        tipForm.add(nahodneTipyNaVitazaButton);

        // TIP NA ZAPAS
        // nasetujeme tipy na vsetky zapasy
        setTipNaZapasPodlaDna(0);

        // a vyberieme aktualny, alebo prvy najblizsi den, kedy sa hraju zapasy
        vybranyDatum = getNajblizsiDenHraniaZapasov();

        String vybranyDatumParam = parameters.get("datum").toString();
        if (StringUtils.isNotEmpty(vybranyDatumParam)) {
            try {
                vybranyDatum = ParserUtils.parseDate(vybranyDatumParam);
            } catch (IllegalArgumentException e) {
                System.err.println("datum[" + vybranyDatumParam + "] ma neplatny format datumu");
            }
        }

        // tip na zapas - container
        tipNaZapasDiv.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        add(tipNaZapasDiv);

        // tip na den - model
        tipy = tipNaZapasPodlaDna.get(vybranyDatum);
        tipNaZapasModel.setObject(tipy);

        // tip na den - formular
        Form<List<TipNaZapasDto>> tipNaDenForm = new Form<List<TipNaZapasDto>>("tipNaZapasForm", tipNaZapasModel) {
            private static final long serialVersionUID = 4635370977141581051L;

            @Override
            protected void onSubmit() {
                List<String> nespracovaneZapasy = new ArrayList<>();
                for (TipNaZapasDto tip : tipy) {
                    // ak na dany zapas nemozem tipovat, pokracujem dalej
                    if (!tip.getZapas().isMozemTipovatZapas()) {
                        continue;
                    }
                    // ak nie su vyplnene tipovane goly na zapas, pokracujem dalej
                    if (tip.getGolyDomaci() == null || tip.getGolyHostia() == null) {
                        continue;
                    }
                    // spracujeme nadefinovane tipy
                    try {
                        if (tip.getId() == null) {
                            tipNaZapasService.addTipNaZapas(pouzivatel.getLogin(), tip);
                        } else {
                            // overime, ci sa tip realne zmenil
                            TipNaZapasDto staryTip = tipNaZapasService.getTipNaZapas(tip.getId());
                            if (!staryTip.getGolyDomaci().equals(tip.getGolyDomaci())
                                    || !staryTip.getGolyHostia().equals(tip.getGolyHostia())) {
                                tip.setNahodnyTip(false);
                            }
                            tipNaZapasService.editTipNaZapas(pouzivatel.getLogin(), tip);
                        }
                    } catch (Exception e) {
                        String zapasText =
                                tip.getZapas().getDomaci().getKodMuzstva()
                                        + " : "
                                        + tip.getZapas().getHostia().getKodMuzstva();
                        System.out.println("tip na zapas[" + zapasText + "] sa nepodarilo spracovat, " + e);
                        nespracovaneZapasy.add(zapasText);
                    }
                }

                // ak mame nejake nespracovane tipy error
                if (nespracovaneZapasy.isEmpty()) {
                    info(getString("tip.zapas.info.spracovanie"));
                } else {
                    error(new StringResourceModel("tip.zapas.error.spracovanie",
                            this,
                            null,
                            new Object[]{nespracovaneZapasy}).getString());
                }

                parameters.set("datum", dateFormat.format(vybranyDatum));
                setResponsePage(new HomePage(parameters));
            }
        };
        tipNaZapasDiv.add(tipNaDenForm);

        // den dozadu - link
        linkDozadu = new AjaxLink<String>("denDozaduLink") {
            private static final long serialVersionUID = -7611181764014200646L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                // zmenime aktualne vybrany datum
                Date predchadazajuciDen = getPredchadzajuciDen();
                vybranyDatum = predchadazajuciDen;
                vybranyDenLabel.setDefaultModel(Model.of(dateFormat.format(vybranyDatum)));
                vybranyDenTyzdniLabel.setDefaultModel(Model.of(getDenTyzdni(vybranyDatum)));
                // refresneme zoznam tipov
                tipNaZapasModel.setObject(tipNaZapasPodlaDna.get(predchadazajuciDen));
                tipy = tipNaZapasModel.getObject();
                // nastavime viditelnost liniek
                this.setVisible(isPoZaciatku(vybranyDatum, zaciatokRocnika));
                linkDopredu.setVisible(isPredKoncom(vybranyDatum, koniecRocnika));
                spracujButton.setVisible(isMozemSpracovatTipy(tipNaZapasModel.getObject()));
                nahodneTipyDenButton.setVisible(isMozemSpracovatTipy(tipNaZapasModel.getObject()));
                // a refresneme container
                target.add(tipNaZapasDiv);
            }
        };
        linkDozadu.setVisible(vybranyDatum != null && isPoZaciatku(vybranyDatum, zaciatokRocnika));
        tipNaDenForm.add(linkDozadu);

        // den dopredu - link
        linkDopredu = new AjaxLink<String>("denDopreduLink") {
            private static final long serialVersionUID = 8435380106423234111L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                Date nasledujuciDen = getNasledujuciDen();
                // zmenime aktualne vybrany datum
                vybranyDatum = nasledujuciDen;
                vybranyDenLabel.setDefaultModel(Model.of(dateFormat.format(vybranyDatum)));
                vybranyDenTyzdniLabel.setDefaultModel(Model.of(getDenTyzdni(vybranyDatum)));
                // refresneme zoznam tipov
                tipNaZapasModel.setObject(tipNaZapasPodlaDna.get(nasledujuciDen));
                tipy = tipNaZapasModel.getObject();
                // nastavime viditelnost liniek
                this.setVisible(isPredKoncom(vybranyDatum, koniecRocnika));
                linkDozadu.setVisible(isPoZaciatku(vybranyDatum, zaciatokRocnika));
                spracujButton.setVisible(isMozemSpracovatTipy(tipNaZapasModel.getObject()));
                nahodneTipyDenButton.setVisible(isMozemSpracovatTipy(tipNaZapasModel.getObject()));
                // a refresneme container
                target.add(tipNaZapasDiv);
            }
        };
        linkDopredu.setVisible(vybranyDatum != null && isPredKoncom(vybranyDatum, koniecRocnika));
        tipNaDenForm.add(linkDopredu);

        // vybrany den - label
        vybranyDenLabel = new Label("den", vybranyDatum == null ? "" : dateFormat.format(vybranyDatum));
        tipNaDenForm.add(vybranyDenLabel);

        // vybrany den v tyzdni - label
        vybranyDenTyzdniLabel = new Label("denTyzdni", vybranyDatum == null ? "" : getDenTyzdni(vybranyDatum));
        tipNaDenForm.add(vybranyDenTyzdniLabel);

        // tipy na zapas - riadok
        tipNaDenForm.add(new ListView<TipNaZapasDto>("tipNaZapasRiadok", tipNaZapasModel) {
            private static final long serialVersionUID = -441073222382791281L;

            @Override
            protected void populateItem(ListItem<TipNaZapasDto> item) {
                // natiahneme si tip pre dany zapas
                final TipNaZapasDto tip = item.getModelObject();
                // zapas
                final ZapasDto zapas = tip.getZapas();
                // ak pouzivatel nesplnil podmienky ucasti, tak moze natipovat, ale po zamknuti zapasu, sa mu zneviditelnia jeho tipy
                if (!zapas.isMozemTipovatZapas() && !isSplnenePodmienky) {
                    tip.setGolyHostia(null);
                    tip.setGolyDomaci(null);
                    tip.setBodyZaGolyHostia(0);
                    tip.setBodyZaGolyDomaci(0);
                    tip.setBodyZaVitaza(0);
                }
                // status tipu - contaner
                WebMarkupContainer statusIcon = new WebMarkupContainer("statusIcon");
                statusIcon.add(new AttributeAppender("class", zapas.getTipNaZapasStavIcon(tip)));
                statusIcon.add(new AttributeAppender("title", (tip.isNahodnyTip() ? "Náhodný tip" : "Tip zadaný používateľom")));
                if (StringUtils.isNotEmpty(zapas.getHighlightsUrl())) {
                    statusIcon.setVisible(false);
                }
                item.add(statusIcon);
                // hightlights
                Link highlight = new Link("highlights") {
                    @Override
                    public void onClick() {

                    }

                    @Override
                    protected void onComponentTag(ComponentTag tag) {
                        super.onComponentTag(tag);
                        tag.getAttributes().put("href", zapas.getHighlightsUrl());
                    }
                };
                if (StringUtils.isEmpty(zapas.getHighlightsUrl())) {
                    highlight.setVisible(false);
                }
                item.add(highlight);
                // datum zapasu - label
                item.add(new Label("datum", timeFormat.format(zapas.getDatum())));
                // domaci nazov - label
                item.add(new Label("domaciNazov", zapas.getDomaci().getNazov()));
                // vlajka domaci - container
                item.add(new VlajkaStatPanel("domaciVlajka", zapas.getDomaci(), false));
                // tip na goly domaci - combo
                item.add(new DropDownChoice<Integer>("golyDomaci",
                        new PropertyModel<Integer>(tip, "golyDomaci"), goly)
                        .setEnabled(zapas.isMozemTipovatZapas()));
                // skore zapasu - label
                item.add(new Label("skore", zapas.getSkore())
                        .add(new AttributeAppender("title", zapas.getPriebeh())));
                // tip na goly hostia - combo
                item.add(new DropDownChoice<Integer>("golyHostia",
                        new PropertyModel<Integer>(tip, "golyHostia"), goly)
                        .setEnabled(zapas.isMozemTipovatZapas()));
                // vlajka hostia - container
                item.add(new VlajkaStatPanel("hostiaVlajka", zapas.getHostia(), false));
                // nazov hostia - label
                item.add(new Label("hostiaNazov", zapas.getHostia().getNazov()));
                // body za tip
                item.add(new Label("body", Model.of(tip.getBody())));
            }
        });

        // nahodny tip na zapasy hracieho dna - tlacitko
        nahodneTipyDenButton = new Link("nahodneTipyNaDenButton") {

            private static final long serialVersionUID = -415960083378466175L;

            @Override
            public void onClick() {
                nahodneTipuj(tipy, false);
                parameters.set("datum", dateFormat.format(vybranyDatum));
                setResponsePage(new HomePage(parameters));
            }
        };

        // spracovat - tlacitko
        spracujButton.setVisible(isMozemSpracovatTipy(tipNaZapasModel.getObject()));
        nahodneTipyDenButton.setVisible(isMozemSpracovatTipy(tipNaZapasModel.getObject()));
        tipNaDenForm.add(spracujButton);
        tipNaDenForm.add(nahodneTipyDenButton);

        // nahodne tipy rocnika - text
        WebMarkupContainer nahodneTipyRocnikaText = new WebMarkupContainer("nahodneTipyRocnikText");
        nahodneTipyRocnikaText.setVisible(vybranyRocnik.isMozemTipovatZapasy());
        add(nahodneTipyRocnikaText);

        // nahodne tipy rocnika - tlacitko
        Link nahodneTipyRocnika = new Link("nahodneTipyRocnikButton") {

            private static final long serialVersionUID = 7905002663852791543L;

            @Override
            public void onClick() {
                // prebehneme si mapu tipov podla dna a vyskladame si z nej zoznam dostupnych tipov
                List<TipNaZapasDto> tipy = new ArrayList<>();
                for (Date key : tipNaZapasPodlaDna.keySet()) {
                    tipy.addAll(tipNaZapasPodlaDna.get(key));
                }
                // nahodne natipujeme na dostupne tipy
                nahodneTipuj(tipy, true);
                parameters.set("datum", dateFormat.format(vybranyDatum));
                setResponsePage(new HomePage(parameters));
            }
        };
        nahodneTipyRocnika.setVisible(vybranyRocnik.isMozemTipovatZapasy());
        add(nahodneTipyRocnika);

        // hlavicka
        add(new Link<String>("aktualizujTabulkuLink") {
            private static final long serialVersionUID = 2766930228509332620L;

            @Override
            public void onClick() {
                Page page;
                try {
                    sutazRocnikService.updatePriebehRocnikaSutaze(pouzivatel.getLogin(), vybranyRocnik.getId());
                    page = new HomePage(parameters);
                    page.info(new StringResourceModel("sutaz.rocnik.info.update",
                            this,
                            null,
                            new Object[]{
                                    vybranyRocnik.getSutaz().getNazov(),
                                    vybranyRocnik.getRok()
                            }).getString());
                } catch (IOException e) {
                    page = new HomePage(new PageParameters());
                    page.error(new StringResourceModel("sutaz.rocnik.error.update",
                            this,
                            null,
                            new Object[]{
                                    vybranyRocnik.getSutaz().getNazov(),
                                    vybranyRocnik.getRok()
                            }).getString());
                }
                setResponsePage(page);
            }
        });
        add(new Label("tipNicHead", "0b"));
        add(new Label("tipGolHead",
                PouzivatelovTipStatistikaDto.BODY_TIP_GOL + "b"));
        add(new Label("tipVitazHead",
                PouzivatelovTipStatistikaDto.BODY_TIP_VITAZ + "b"));
        add(new Label("tipVitazGolHead",
                PouzivatelovTipStatistikaDto.BODY_TIP_GOL
                        + PouzivatelovTipStatistikaDto.BODY_TIP_VITAZ
                        + "b"));
        add(new Label("tipVysledokHead",
                PouzivatelovTipStatistikaDto.BODY_TIP_GOL
                        + PouzivatelovTipStatistikaDto.BODY_TIP_GOL
                        + PouzivatelovTipStatistikaDto.BODY_TIP_VITAZ
                        + "b"));

        // natiahneme si statistiku
        final List<PouzivatelovTipStatistikaDto> zoznam = statistikaService.getStatistikaTiperov(vybranyRocnik.getId());
        final PouzivatelovTipStatistikaDto priebeznyVitaz = getPriebeznyVitaz(zoznam);

        final List<String> najlepsieTipujuciVitaza = tipNaZapasService.getNajlepsieTipujuciVitazaZapasu(vybranyRocnik.getId());
        final List<String> najlepsieTipujuciSkore = tipNaZapasService.getNajlepsieTipujuciSkoreZapasu(vybranyRocnik.getId());
        final List<String> najlepsienahodneTipujuci= tipNaZapasService.getNajlepsieNahodneTipujuci(vybranyRocnik.getId());
        final List<String> nesplniliPodmienky = sutazRocnikSuhlasService.findAllLoginCoNezaplatili(vybranyRocnik.getId());

        add(new ListView<PouzivatelovTipStatistikaDto>("riadokTabulky", zoznam) {
            private static final long serialVersionUID = 6830822446123149315L;

            @Override
            protected void populateItem(ListItem<PouzivatelovTipStatistikaDto> item) {
                String predoslaHodnota = (item.getIndex() == 0
                        ? ""
                        : zoznam.get(item.getIndex() - 1).getHodnota());
                PouzivatelovTipStatistikaDto stat = item.getModelObject();
                item.add(new Label("poradie", predoslaHodnota.equals(stat.getHodnota())
                        ? ""
                        : String.valueOf(item.getIndex() + 1)));
                String login = stat.getPouzivatel().getLogin();
                item.add(new Label("login", login));
                item.add(new WebMarkupContainer("bestNaZapas").setVisible(najlepsieTipujuciVitaza.contains(login)));
                item.add(new WebMarkupContainer("bestNaSkore").setVisible(najlepsieTipujuciSkore.contains(login)));
                item.add(new WebMarkupContainer("bestNahoda").setVisible(najlepsienahodneTipujuci.contains(login)));
                item.add(new WebMarkupContainer("nezaplatil").setVisible(nesplniliPodmienky.contains(login)));
                item.add(new Label("pocetTipov", String.valueOf(stat.getPocetTipov())));
                item.add(new Label("pocetZadanychTipov", String.valueOf(stat.getPocetZadanychTipov())));
                item.add(new Label("body", String.valueOf(stat.getBody())));
                item.add(new Label("rozdiel", "(" + String.valueOf(stat.getBody() - (priebeznyVitaz == null ? 0 : priebeznyVitaz.getBody())) + ")"));
                item.add(new Label("pocetNic", String.valueOf(stat.getPocetTipovNic())));
                item.add(new Label("pocetGoly", String.valueOf(stat.getPocetTipovGoly())));
                item.add(new Label("pocetVitazstvo", String.valueOf(stat.getPocetTipovVitaz())));
                item.add(new Label("pocetVitazstvoGol", String.valueOf(stat.getPocetTipovVitazGol())));
                item.add(new Label("pocetVysledok", String.valueOf(stat.getPocetTipovVysledok())));
            }
        });

        add(new Label("aktualizovane", vybranyRocnik.getAktualizacia() == null
                ? "nikdy"
                : fullFormat.format(vybranyRocnik.getAktualizacia())));
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.HOME;
    }

    /**
     * Nasetuje mapu tipov na zapas daneho rocnika tak, ze klucik bude den, kedy sa hraju zapasy
     * a hodnota je zoznam tipov aktualne prihlaseneho pouzivatela na dany zapas.
     */
    private void setTipNaZapasPodlaDna(int casovyPosun) {
        // vycistime mapu tipov
        tipNaZapasPodlaDna.clear();

        // dohladame zapasy aktualneho rocnika
        ZapasKriteria zapasKriteria = new ZapasKriteria();
        zapasKriteria.setSutazRocnikId(vybranyRocnik.getId());
        zapasKriteria.setOrderBy("datum");
        zapasKriteria.setOrderDirection(OrderDirectionEnum.ASC);
        List<ZapasDto> zapasy = zapasService.findZapasy(zapasKriteria);
        // ak mame casovy posun rozny od nuly aplikujeme
        if (casovyPosun != 0) {
            for (ZapasDto z : zapasy) {
                Calendar calendar = Calendar.getInstance();
                calendar.setTime(z.getDatum());
                calendar.add(Calendar.HOUR_OF_DAY, casovyPosun);
                z.setDatum(calendar.getTime());
            }
        }

        // potriedime zapasy podla dna
        Map<Date, List<ZapasDto>> zapasyPodlaDna = new HashMap<>();
        for (ZapasDto z : zapasy) {
            Date denHraniaZapasu = getDenHraniaZapasu(z);
            if (zapasyPodlaDna.get(denHraniaZapasu) == null) {
                zapasyPodlaDna.put(denHraniaZapasu, new ArrayList<ZapasDto>());
            }
            zapasyPodlaDna.get(denHraniaZapasu).add(z);
        }

        // dohladame tipy aktualneho rocnika
        TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
        kriteria.setSutazRocnikId(vybranyRocnik.getId());
        kriteria.setPouzivatelIds(Arrays.asList(pouzivatel.getId()));
        kriteria.setOrderBy("datum");
        kriteria.setOrderDirection(OrderDirectionEnum.ASC);
        List<TipNaZapasDto> tipy = tipNaZapasService.findTipyNaZapas(kriteria);
        // potriedime tipy podla dna
        Map<Date, List<TipNaZapasDto>> tipyPodlaDna = new HashMap<>();
        for (TipNaZapasDto t : tipy) {
            Date denHraniaZapasu = getDenHraniaZapasu(t.getZapas());
            if (tipyPodlaDna.get(denHraniaZapasu) == null) {
                tipyPodlaDna.put(denHraniaZapasu, new ArrayList<TipNaZapasDto>());
            }
            tipyPodlaDna.get(denHraniaZapasu).add(t);
        }

        // zosortujeme kluciky zapasov na den ASC
        List<Date> datumyZapasov = new ArrayList<>(zapasyPodlaDna.keySet());
        Collections.sort(datumyZapasov);

        // prebehneme zapasy a tipy na nich a nainicializujeme prazdne
        for (Date datum : datumyZapasov) {
            List<TipNaZapasDto> tipyNaZapas = tipyPodlaDna.get(datum);
            List<ZapasDto> zapasyDna = zapasyPodlaDna.get(datum);
            // ak pre dany den zapasov nemame ziaden tip, nainicializujeme si ich vsetky
            if (tipyNaZapas == null) {
                tipNaZapasPodlaDna.put(datum, new ArrayList<TipNaZapasDto>());
                for (ZapasDto z : zapasyDna) {
                    TipNaZapasDto t = new TipNaZapasDto();
                    t.setPouzivatel(pouzivatel);
                    t.setZapas(z);
                    tipNaZapasPodlaDna.get(datum).add(t);
                }
                // ak sa pocet tipov rovna poctu zapasov, znamena to, ze na kazdy zapas je nadefinovany tip
                // a nepotrebujeme doplnat prazdne inicializacie tipov, v opacnom pripade doplnime pre
                // zapas, na ktory pouzivatel netipoval prvotnu inicializaciu
            } else if (tipyNaZapas.size() == zapasyDna.size()) {
                tipNaZapasPodlaDna.put(datum, tipyNaZapas);
            } else {
                tipNaZapasPodlaDna.put(datum, new ArrayList<TipNaZapasDto>());
                // presypeme ich do mapy, kde klucik je zapasId
                Map<Long, TipNaZapasDto> mapa = new HashMap<>();
                for (TipNaZapasDto t : tipyNaZapas) {
                    mapa.put(t.getZapas().getId(), t);
                }
                // pre kazdy zapas overime tip
                for (ZapasDto z : zapasyDna) {
                    // ak je nadefinovany pridame do mapy
                    if (mapa.containsKey(z.getId())) {
                        tipNaZapasPodlaDna.get(datum).add(mapa.get(z.getId()));
                        // inak nainicializujeme
                    } else {
                        TipNaZapasDto t = new TipNaZapasDto();
                        t.setPouzivatel(pouzivatel);
                        t.setZapas(z);
                        tipNaZapasPodlaDna.get(datum).add(t);
                    }
                }
            }
        }
        // na zaver zosortujem tipy na zapasy pre jednotlive dni
        for (Date denHrania : tipNaZapasPodlaDna.keySet()) {
            Collections.sort(tipNaZapasPodlaDna.get(denHrania), new TipNaZapasDatumComparator());
        }
    }

    /**
     * Vrati datum zapasu bez casovej stopy.
     *
     * @param zapas zapas
     * @return datum
     */
    private Date getDenHraniaZapasu(ZapasDto zapas) {
        return new Date(
                new DateMidnight(zapas.getDatum())
                        .toDateTime()
                        .getMillis());
    }

    /**
     * Ak sa hraju zapasy v aktualnom dni, vrati aktualny den, inak najblisi termin, kedy sa hraju zapasy.
     *
     * @return datum
     */
    private Date getNajblizsiDenHraniaZapasov() {
        // nasetujeme dnes bez casovej stopy
        Date dnes = new Date(
                new DateMidnight(new Date())
                        .toDateTime()
                        .getMillis());

        if (tipNaZapasPodlaDna.get(dnes) != null) {
            return dnes;
        } else {
            for (Date datum : getDniKedySaHrajuZapasy()) {
                if (datum.after(dnes)) {
                    return datum;
                }
            }
        }

        // ked neexistuje ziaden nasledujuci den, kedy sa hraju zapasy, vratime posledny
        return getDniKedySaHrajuZapasy().size() == 0
                ? null
                : getDniKedySaHrajuZapasy().get(getDniKedySaHrajuZapasy().size() - 1);
    }

    /**
     * Zoznam datumov, kedy sa hraju zapasy.
     *
     * @return zoznam
     */
    private List<Date> getDniKedySaHrajuZapasy() {
        List<Date> hracieDni = new ArrayList<>(tipNaZapasPodlaDna.keySet());
        Collections.sort(hracieDni, new Comparator<Date>() {
            @Override
            public int compare(Date d1, Date d2) {
                return d1.compareTo(d2);
            }
        });

        return hracieDni;
    }

    /**
     * Najblizsi den, ked sa bude hrat po aktualne vybranom datume.
     *
     * @return datum
     */
    private Date getNasledujuciDen() {
        int count = 0;
        for (Date d : getDniKedySaHrajuZapasy()) {
            count++;
            if (d.equals(vybranyDatum)) {
                // ak spracuvavame poslednu polozku zo zoznamu, tak -1
                if (count == getDniKedySaHrajuZapasy().size()) {
                    count--;
                }
                return getDniKedySaHrajuZapasy().get(count);
            }
        }

        return null;
    }

    /**
     * Najblizsi den, kedy sa hralo pred aktualne vybranom termine.
     *
     * @return datum
     */
    private Date getPredchadzajuciDen() {
        int count = 0;
        for (Date d : getDniKedySaHrajuZapasy()) {
            if (d.equals(vybranyDatum)) {
                count--;
                if (count < 0) {
                    count = 0;
                }
                return getDniKedySaHrajuZapasy().get(count);
            }
            count++;
        }

        return null;
    }

    /**
     * Vrati priznak, ci mozem spracovavat tipy na dany datum
     *
     * @return priznak
     */
    private boolean isMozemSpracovatTipy(List<TipNaZapasDto> tipy) {
        // overime si ci v dany den existuje este zapas, na ktory sa da tipovat
        if (tipy == null) {
            return false;
        }

        boolean isMozneSpracovat = false;
        for (TipNaZapasDto tip : tipy) {
            if (tip.getZapas().isMozemTipovatZapas()) {
                isMozneSpracovat = true;
                break;
            }
        }

        return isMozneSpracovat;
    }

    /**
     * Vrati priznak, ci je zadany datum pred zaciatkom rocnika.
     *
     * @param datum    porovnavany datum
     * @param zaciatok zaciatok rocnika
     * @return priznak
     */
    private boolean isPoZaciatku(Date datum, Date zaciatok) {
        datum = new Date(new DateMidnight(datum).toDateTime().getMillis());
        zaciatok = new Date(new DateMidnight(zaciatok).toDateTime().getMillis());

        return datum.after(zaciatok);
    }

    /**
     * Vrati priznak, ci je porovnavany datum za koncom rocnika.
     *
     * @param datum  porovnavany datum
     * @param koniec koniec rocnika
     * @return priznak
     */
    private boolean isPredKoncom(Date datum, Date koniec) {
        datum = new Date(new DateMidnight(datum).toDateTime().getMillis());
        koniec = new Date(new DateMidnight(koniec).toDateTime().getMillis());

        return datum.before(koniec);
    }

    /**
     * Vrati den v tyzdni podla zadaneho datumu.
     *
     * @param datum datum, z ktoreho sa zistuje den v tyzdni
     * @return den v tyzdni
     */
    private String getDenTyzdni(Date datum) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(datum);

        return Arrays
                .asList("nedeľa",
                        "pondelok",
                        "utorok",
                        "streda",
                        "štvrtok",
                        "piatok",
                        "sobota")
                .get(calendar.get(Calendar.DAY_OF_WEEK) - 1);
    }

    /**
     * Vrati priebezneho vitaza.
     *
     * @param statistika priebezna statistika
     * @return priebezny vitaz
     */
    private PouzivatelovTipStatistikaDto getPriebeznyVitaz(List<PouzivatelovTipStatistikaDto> statistika) {
        int max = 0;
        PouzivatelovTipStatistikaDto vitaz = null;
        for (PouzivatelovTipStatistikaDto riadok : statistika) {
            if (riadok.getBody() > max) {
                max = riadok.getBody();
                vitaz = riadok;
            }
        }

        return vitaz;
    }

    /**
     * Prebehne zadany zoznam tipov na zapas a tym, ktore nie su natipovane
     * vygeneruje nahodny tip a ulozi ho do db.
     *
     * @param tipy zoznam tipov
     */
    private void nahodneTipuj(List<TipNaZapasDto> tipy, boolean isRocnik) {
        List<String> nespracovaneZapasy = new ArrayList<>();
        boolean isHokej = SportTypEnum.HOKEJ.equals(vybranyRocnik.getSutaz().getSportTyp());
        int maxPocetGolov = isHokej ? 5 : 3;

        for (TipNaZapasDto tip : tipy) {
            // ak na dany zapas nemozem tipovat, pokracujem dalej
            if (!tip.getZapas().isMozemTipovatZapas()) {
                continue;
            }

            // nasetujeme priznak, ci mozeme nahodne natipovat
            boolean mozemNatipovat;
            if (isRocnik) {
                mozemNatipovat = tip.getGolyDomaci() == null && tip.getGolyHostia() == null;
            } else {
                mozemNatipovat = true;
            }

            if (mozemNatipovat) {
                // nasetujeme goly domacich
                tip.setGolyDomaci(random.nextInt(maxPocetGolov));
                // nasetujeme goly hosti
                tip.setGolyHostia(random.nextInt(maxPocetGolov));
                // v hokeji nemoze byt remiza
                if (isHokej && tip.getGolyDomaci().equals(tip.getGolyHostia())) {
                    tip.setGolyHostia(tip.getGolyHostia() == 0
                            ? (tip.getGolyHostia() + 1)
                            : (tip.getGolyHostia() - 1));
                }
                tip.setNahodnyTip(true);
            }

            // spracujeme nadefinovane tipy
            try {
                if (tip.getId() == null) {
                    tipNaZapasService.addTipNaZapas(pouzivatel.getLogin(), tip);
                } else {
                    tipNaZapasService.editTipNaZapas(pouzivatel.getLogin(), tip);
                }
            } catch (Exception e) {
                String zapasText =
                        tip.getZapas().getDomaci().getKodMuzstva()
                                + " : "
                                + tip.getZapas().getHostia().getKodMuzstva();
                System.out.println("tip na zapas[" + zapasText + "] sa nepodarilo spracovat, " + e);
                nespracovaneZapasy.add(zapasText);
            }
        }

        // ak mame nejake nespracovane tipy error
        if (nespracovaneZapasy.isEmpty()) {
            info(getString("tip.zapas.nahodne.info.spracovanie"));
        } else {
            error(new StringResourceModel("tip.zapas.nahodne.error.spracovanie",
                    this,
                    null,
                    new Object[]{nespracovaneZapasy}).getString());
        }
    }

    private synchronized String getDatum(Date date) {
        return new SimpleDateFormat("dd.MM.yyyy HH:mm").format(date);
    }
}
