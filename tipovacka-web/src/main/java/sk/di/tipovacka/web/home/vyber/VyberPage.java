package sk.di.tipovacka.web.home.vyber;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.enums.SutazTypEnum;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.commons.panel.NadpisPanel;
import sk.di.tipovacka.web.home.HomePage;
import sk.di.tipovacka.web.menu.MenuItemEnum;
import sk.di.tipovacka.web.tip.prehlad.TipyPrehladPage;

/**
 * Vyber sutazi, ktoru chcem zobrazit.
 *
 * User: Dano
 * Date: 29.9.2014
 */
public class VyberPage extends Template {

    private static final long serialVersionUID = -6479062467625268674L;

    @SpringBean
    private SutazRocnikService sutazRocnikService;

    public VyberPage(final PageParameters parameters) {
        super(parameters);

        // nadpis - panel
        add(new NadpisPanel("nadpisPanel", getString("nadpis.home.vyber"), vybranyRocnik));

        ListView list = new ListView<RocnikSutazeDto>("aktivneSutaze", sutazRocnikService.getAktualnyRocnikSutaze()) {

            private static final long serialVersionUID = -1822118933276418293L;

            @Override
            protected void populateItem(ListItem<RocnikSutazeDto> item) {
                final RocnikSutazeDto rocnik = item.getModelObject();

                // nasetujeme labels
                String sportTyp = rocnik.getSutaz().getSportTyp().name().toLowerCase();
                String sutazTyp = rocnik.getSutaz().getSutazTyp().getSkratka().toLowerCase();
                int rok = rocnik.getRok();
                String rocnikLogo = "/img/logo_" + sportTyp
                        + "_" + sutazTyp
                        + (SutazTypEnum.LIGA_MAJSTROV.equals(rocnik.getSutaz().getSutazTyp()) ? "" : "_" + rok)
                        + ".jpg";
                item.add(new NonCachingImage("rocnikLogo", new ContextRelativeResource(rocnikLogo)));
                item.add(new Label("rocnikNadpis", rocnik.getSutaz().getNazov()));
                item.add(new Label("rocnikRok", String.valueOf(rocnik.getRok())));
                item.add(new Label("typSport", "(" + getString("sport.typ.nadpis." + rocnik.getSutaz().getSportTyp()) + ")" ));
                boolean isPrihlaseny = pouzivatel.isSuhlas(rocnik.getId());
                item.add(new WebMarkupContainer("lock").setVisible(!isPrihlaseny));
                item.add(new WebMarkupContainer("unlock").setVisible(isPrihlaseny));
                item.add(new Link<String>("zobrazitLink") {

                    private static final long serialVersionUID = 2991517869499524081L;

                    @Override
                    public void onClick() {
                        // nasetujeme vybranu sutaz
                        pouzivatel.setVybranyRocnik(rocnik);
                        // na vyber sutaze mozeme prist z roznych stranok, nastavime, kam mame ist, ked si vyberieme
                        Class page = HomePage.class;
                        String forward = parameters.get("forward").toString();
                        if (StringUtils.isNotEmpty(forward) && "tipy".equals(forward)) {
                            page = TipyPrehladPage.class;
                        }
                        setResponsePage(page, parameters);
                    }
                });
            }
        };
        list.setReuseItems(true);
        add(list);
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.HOME;
    }
}
