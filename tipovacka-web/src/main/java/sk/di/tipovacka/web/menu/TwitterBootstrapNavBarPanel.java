package sk.di.tipovacka.web.menu;

import com.google.common.base.Preconditions;
import com.google.common.collect.Iterables;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.BookmarkablePageLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.markup.repeater.RepeatingView;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.web.home.HomePage;
import sk.di.tipovacka.web.home.podmienky.PodmienkyPage;
import sk.di.tipovacka.web.pouzivatel.PosliPouzivateloveTipyPage;
import sk.di.tipovacka.web.pouzivatel.PouzivatelPage;
import sk.di.tipovacka.web.pouzivatel.ZmenaHeslaPage;
import sk.di.tipovacka.web.utils.SecurityUtils;

import java.io.Serializable;
import java.util.Collection;

public class TwitterBootstrapNavBarPanel extends Panel {

    private static final long serialVersionUID = 2598298380790559154L;

    @SpringBean
    private SutazRocnikService sutazRocnikService;

    private TwitterBootstrapNavBarPanel(final Builder builder) {
        super(builder.id);

        BookmarkablePageLink<Void> homePageLink = new BookmarkablePageLink<Void>("homePageLink", builder.homePage);
//        homePageLink.add(new Label("label", builder.applicationName));
//        homePageLink.add(new Label("aktualnyRocnik", builder.aktualnyRocnik));
        add(homePageLink);

        RepeatingView repeatingView = new RepeatingView("menuItems");

        for (MenuItemEnum item : builder.linksMap.keySet()) {
            boolean shouldBeActive = item.equals(builder.activeMenuItem);

            Collection<BookmarkablePageLink<?>> linksInThisMenuItem = builder.linksMap.get(item);

            if (linksInThisMenuItem.size() == 1) {
                BookmarkablePageLink<?> pageLink = Iterables.get(linksInThisMenuItem, 0);

                MenuLinkItem menuLinkItem = new MenuLinkItem(repeatingView.newChildId(),
                        pageLink,
                        shouldBeActive);
                repeatingView.add(menuLinkItem);
            } else {
                MenuDropdownItem menuLinkItem = new MenuDropdownItem(repeatingView.newChildId(),
                        item,
                        linksInThisMenuItem,
                        shouldBeActive);
                repeatingView.add(menuLinkItem);
            }
        }

        add(repeatingView);

        // aktualne prihlaseny pouzivatel
        add(new Label("pouzivatel.menoPriezvisko", SecurityUtils.getCurrentUser().getMenoPriezvisko()));

        // domov - link
        add(new Link<String>("domovLink") {
            private static final long serialVersionUID = 126869351649838457L;

            @Override
            public void onClick() {
                setResponsePage(new HomePage(new PageParameters()));
            }
        });

        // domov - link
        add(new Link<String>("profilLink") {
            private static final long serialVersionUID = -5011297963272358019L;

            @Override
            public void onClick() {
                setResponsePage(new PouzivatelPage(new PageParameters()));
            }
        });

        // zmena hesla - link
        add(new Link<String>("zmenaHeslaLink") {
            private static final long serialVersionUID = 126869351649838457L;

            @Override
            public void onClick() {
                setResponsePage(new ZmenaHeslaPage(new PageParameters()));
            }
        });

        // poslanie pouzivatelovuych tipov - link
        add(new Link<String>("posliMiMojeTipyLink") {
            private static final long serialVersionUID = 126869351649838457L;

            @Override
            public void onClick() {
                setResponsePage(new PosliPouzivateloveTipyPage(new PageParameters()));
            }
        }.setVisible(!SecurityUtils.getCurrentUser().isAdmin()));

        // zoznam aktivnych sutazi
        add(new ListView<RocnikSutazeDto>("aktivneSutaze", sutazRocnikService.getAktualnyRocnikSutaze()) {

            private static final long serialVersionUID = -7262553527537047023L;

            @Override
            protected void populateItem(ListItem<RocnikSutazeDto> item) {
                final RocnikSutazeDto rocnik = item.getModelObject();
                final boolean isPrihlaseny = SecurityUtils
                        .getCurrentUser()
                        .getAktivneSutazeId()
                        .contains(rocnik.getId());
                Link link = new Link<String>("aktivnaSutazLink") {

                    private static final long serialVersionUID = 1307804487573507173L;

                    @Override
                    public void onClick() {
                        SecurityUtils.getCurrentUser().setVybranyRocnik(rocnik);
                        Class clazz = isPrihlaseny ? HomePage.class : PodmienkyPage.class;
                        setResponsePage(clazz, new PageParameters());
                    }
                };
                item.add(link);
                link.add(new WebMarkupContainer("sutazLock").setVisible(!isPrihlaseny));
                link.add(new WebMarkupContainer("sutazUnlock").setVisible(isPrihlaseny));
                link.add(new Label("linktext",
                        rocnik.getSutaz().getSutazTyp().getSkratka()
                                + " " + rocnik.getRok()
                                + " (" + getString("sport.typ.nadpis." + rocnik.getSutaz().getSportTyp()) + ")"));
            }
        });
    }

    public static class Builder implements Serializable {

        private static final long serialVersionUID = 8207766918523156346L;
        private String id;
        private Class<? extends Page> homePage;

        private MenuItemEnum activeMenuItem;

        private Multimap<MenuItemEnum, BookmarkablePageLink<?>> linksMap = LinkedHashMultimap.create();

        public Builder(String id,
                       Class<? extends Page> homePage,
                       MenuItemEnum activeMenuItem) {
            this.id = id;
            this.homePage = homePage;
            this.activeMenuItem = activeMenuItem;
        }

        public Builder withMenuItem(MenuItemEnum menuItem, Class<? extends Page> pageToLink) {
            Preconditions.checkState(!linksMap.containsKey(menuItem), "Builder already contains " + menuItem +
                    ". Please use withMenuItemInDropdown if you need many links in one menu item");
            BookmarkablePageLink<Page> link = new BookmarkablePageLink<Page>("link", pageToLink);
            link.setBody(new Model<String>(menuItem.getLabel()));
            linksMap.put(menuItem, link);
            return this;
        }

        public Builder withMenuItemAsDropdown(final MenuItemEnum menuItem, Class<? extends Page> pageToLink, String label) {
            final BookmarkablePageLink<Page> link = new BookmarkablePageLink<Page>("link", pageToLink);
            link.setBody(new Model<String>(label));
            linksMap.put(menuItem, link);
            return this;
        }

        public TwitterBootstrapNavBarPanel build() {
            return new TwitterBootstrapNavBarPanel(this);
        }
    }
}
