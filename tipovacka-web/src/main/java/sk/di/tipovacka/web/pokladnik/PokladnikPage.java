package sk.di.tipovacka.web.pokladnik;

import org.apache.wicket.RestartResponseException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.RocnikSutazeSuhlasDto;
import sk.di.tipovacka.api.service.MailService;
import sk.di.tipovacka.api.service.SutazRocnikSuhlasService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.home.podmienky.PodmienkyPage;
import sk.di.tipovacka.web.home.vyber.VyberPage;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class PokladnikPage extends Template {

    private WebMarkupContainer ucastniciDiv = new WebMarkupContainer("ucastniciDiv");
    private WebMarkupContainer messageDiv = new WebMarkupContainer("messageDiv");
    private WebMarkupContainer upozornitVsetkychDiv = new WebMarkupContainer("upozornitVsetkychDiv");
    private Label messageLabel = new Label("messageLabel");

    @SpringBean
    private SutazRocnikSuhlasService suhlasService;
    @SpringBean
    private MailService mailService;

    public PokladnikPage(PageParameters parameters) {
        super(parameters);

        // ak pouzivatel nema vybranu ziadnu aktivnu sutaz, presmerujeme ho na stranku vyberu sutazi
        if (vybranyRocnik == null) {
            throw new RestartResponseException(VyberPage.class, parameters.add("forward", "home"));
        }

        final Long rocnikId = vybranyRocnik.getId();
        // ak je vybrany rocnik, overime, ci prihlaseny pouzivatel ma pristup na dany rocnik
        if (!pouzivatel.isSuhlas(rocnikId)) {
            throw new RestartResponseException(PodmienkyPage.class);
        }

        // message container
        messageDiv.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        messageDiv.setVisible(false);
        add(messageDiv);
        // message label
        messageDiv.add(messageLabel);

        // container pre ucastnikov
        ucastniciDiv.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        add(ucastniciDiv);

        final List<RocnikSutazeSuhlasDto> suhlasy = suhlasService.findAllByRocnik(rocnikId);
        suhlasy.sort(Comparator.comparing(o -> o.getPouzivatel().getLogin()));
        final CompoundPropertyModel<List<RocnikSutazeSuhlasDto>> suhlasyModel = new CompoundPropertyModel<>(suhlasy);
        ListView ucastnici = new ListView<RocnikSutazeSuhlasDto>("ucastnici", suhlasyModel) {
            @Override
            protected void populateItem(ListItem<RocnikSutazeSuhlasDto> item) {
                final RocnikSutazeSuhlasDto suhlas = item.getModelObject();

                WebMarkupContainer statusIcon = new WebMarkupContainer("statusIcon");
                statusIcon.add(new AttributeAppender("class", suhlas.isZaplatene() ? "fa fa-smile-o fa-fw" : "fa fa-frown-o fa-fw"));
                statusIcon.add(new AttributeAppender("style", suhlas.isZaplatene() ? "color: green" : "color:red"));
                statusIcon.add(new AttributeAppender("title", suhlas.isZaplatene() ? "Zaplatil" : "Nezaplatil"));
                item.add(statusIcon);

                item.add(new Label("login", suhlas.getPouzivatel().getLogin()));

                AjaxLink splnilLink = new AjaxLink("splnilLink") {
                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        try {
                            suhlasService.addZaplatene(pouzivatel.getLogin(), suhlas.getId());
                        } catch (Exception e) {
                            /**/
                        }
                        suhlasy.clear();
                        suhlasy.addAll(suhlasService.findAllByRocnik(rocnikId));
                        suhlasy.sort(Comparator.comparing(o -> o.getPouzivatel().getLogin()));
                        suhlasyModel.setObject(suhlasy);

                        upozornitVsetkychDiv.setVisible(!getAllCoNezaplatili(vybranyRocnik.getId()).isEmpty());

                        target.add(ucastniciDiv);
                        target.add(upozornitVsetkychDiv);
                    }
                };
                splnilLink.setVisible(!suhlas.isZaplatene());
                item.add(splnilLink);

                AjaxLink nesplnilLink = new AjaxLink("nesplnilLink") {
                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        try {
                            suhlasService.removeZaplatene(pouzivatel.getLogin(), suhlas.getId());
                        } catch (Exception e) {
                            /**/
                        }
                        suhlasy.clear();
                        suhlasy.addAll(suhlasService.findAllByRocnik(rocnikId));
                        suhlasy.sort(Comparator.comparing(o -> o.getPouzivatel().getLogin()));
                        suhlasyModel.setObject(suhlasy);

                        upozornitVsetkychDiv.setVisible(!getAllCoNezaplatili(vybranyRocnik.getId()).isEmpty());

                        target.add(ucastniciDiv);
                        target.add(upozornitVsetkychDiv);
                    }
                };
                nesplnilLink.setVisible(suhlas.isZaplatene());
                item.add(nesplnilLink);
            }
        };
        ucastniciDiv.add(ucastnici);

        // panel pre zobrazenie, ci sa ma odoslat notifikacia tym, ktori nesplnili podmienky ucasti
        upozornitVsetkychDiv
                .setOutputMarkupId(true)
                .setOutputMarkupPlaceholderTag(true)
                .setVisible(!getAllCoNezaplatili(vybranyRocnik.getId()).isEmpty());
        add(upozornitVsetkychDiv);

        final String predmet = getString("mail.upozornenie.predmet");
        final String text = getString("mail.upozornenie.text");
        // poslanie emailu vsetkym, ktory este nezaplatili
        AjaxLink upozorniVsetkychLink = new AjaxLink<String>("upozornitVsetkychLink") {
            @Override
            public void onClick(AjaxRequestTarget target) {
                List<String> neodoslane = new ArrayList<>();
                for (RocnikSutazeSuhlasDto suhlas : suhlasy) {
                    if (!suhlas.isZaplatene()) {
                        try {
                            mailService.sendMail(suhlas.getPouzivatel().getMail(), predmet, text, null, null, null);
                        } catch (Exception e) {
                            neodoslane.add(suhlas.getPouzivatel().getLogin());
                        }
                    }
                }
                messageLabel.setDefaultModel(Model.of(getString(neodoslane.isEmpty()
                        ? "mail.upozornenie.poslane.info"
                        : "mail.upozornenie.poslane.error")));
                messageDiv.add(new AttributeAppender("class", neodoslane.isEmpty()
                        ? "col-xs-12 msg success"
                        : "col-xs-12 msg failure"));
                messageDiv.setVisible(true);
                target.add(messageDiv);
            }
        };
        upozornitVsetkychDiv.add(upozorniVsetkychLink);
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return null;
    }

    /**
     * Vrati zoznam vsetkych pouzivatelov, ktory sa prihlasili do rocnika, ale este nezaplatili.
     *
     * @param rocnikId identifikator rocnika
     * @return zoznam neplaticov
     */
    private List<RocnikSutazeSuhlasDto> getAllCoNezaplatili(Long rocnikId) {
        return suhlasService.findAllCoNezaplatili(rocnikId);
    }
}
