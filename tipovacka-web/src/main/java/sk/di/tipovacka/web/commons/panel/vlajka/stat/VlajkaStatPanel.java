package sk.di.tipovacka.web.commons.panel.vlajka.stat;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import sk.di.tipovacka.api.dto.MuzstvoDto;

/**
 * Panel pre zobrazenie vlajky statu
 */
public class VlajkaStatPanel extends Panel {

    private static final long serialVersionUID = -8177961247483922152L;

    public VlajkaStatPanel(String id, MuzstvoDto muzstvo, boolean isSeda) {
        super(id);

        String vlajka = "";
        String title = "";

        if (muzstvo != null) {
            vlajka = muzstvo.getKod().toLowerCase();
            title = muzstvo.getNazov() + " (" + muzstvo.getKod() + ")";
        }
// http://demosthenes.info/blog/532/convert-images-to-black-and-white-with-css
// http://stackoverflow.com/questions/609273/convert-an-image-to-grayscale-in-html-css
        add(new WebMarkupContainer("vlajka")
                .add(new AttributeAppender("class", (isSeda ? "flagsp_g" : "flagsp")
                        + (StringUtils.isEmpty(vlajka) ? "" : " flagsp_" + vlajka)))
                .add(new AttributeAppender("title", title)));
    }
}
