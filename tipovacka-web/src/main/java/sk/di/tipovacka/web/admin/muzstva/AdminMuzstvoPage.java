package sk.di.tipovacka.web.admin.muzstva;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.MuzstvoDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.service.MuzstvoService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.dialog.DialogAkciaEnum;
import sk.di.tipovacka.web.dialog.potvrdenie.PotvrdenieModalDialog;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.util.List;

/**
 * Administracia muzstva
 *
 * User: Dano
 * Date: 19.5.2013
 */
public class AdminMuzstvoPage extends Template {
    private static final long serialVersionUID = 8667228690575763809L;

    @SpringBean
    private MuzstvoService muzstvoService;

    private MuzstvoDto muzstvo;

    public AdminMuzstvoPage(PageParameters parameters) {
        super(parameters);

        // na zaciatku si nastavime inicializacne udaje
        muzstvo = new MuzstvoDto();

        // v parametroch moze prist muzstvoId
        String muzstvoId = parameters.get("muzstvoId").toString();
        if (StringUtils.isNotEmpty(muzstvoId)) {
            muzstvo = muzstvoService.getMuzstvo(Long.parseLong(muzstvoId));
        }
        
        // div pre formular na managerovanie
        final WebMarkupContainer muzstvoDiv = new WebMarkupContainer("muzstvoDiv");
        muzstvoDiv.setOutputMarkupId(true);
        muzstvoDiv.setOutputMarkupPlaceholderTag(true);
        add(muzstvoDiv);

        // model
        final CompoundPropertyModel<MuzstvoDto> model = new CompoundPropertyModel<MuzstvoDto>(muzstvo);
        // form
        final Form<MuzstvoDto> muzstvoForm = new Form<MuzstvoDto>("muzstvoForm", model) {
            private static final long serialVersionUID = 1596243313647826285L;

            @Override
            protected void onSubmit() {
                super.onSubmit();

                // vysledna stranka
                AdminMuzstvoPage page;
                // spracovanie muzstva
                try {
                    if (muzstvo.getId() == null) {
                        Long muzstvoId = muzstvoService.addMuzstvo(pouzivatel.getLogin(), muzstvo);
                        page = new AdminMuzstvoPage(new PageParameters());
                        page.info(new StringResourceModel("muzstvo.info.pridany",
                                this,
                                null,
                                new Object[]{muzstvo.getNazov(), muzstvo.getKod()}).getString());
                    } else {
                        muzstvoService.editMuzstvo(pouzivatel.getLogin(), muzstvo);
                        page = new AdminMuzstvoPage(new PageParameters());
                        page.info(new StringResourceModel("muzstvo.info.zmeneny",
                                this,
                                null,
                                new Object[]{muzstvo.getNazov(), muzstvo.getKod()}).getString());
                    }
                } catch (Exception e) {
                    page = new AdminMuzstvoPage(new PageParameters());
                    page.error(new StringResourceModel("muzstvo.error.spracovat",
                            this,
                            null,
                            new Object[]{muzstvo.getNazov(), muzstvo.getKod()}).getString());
                }

                setResponsePage(page);
            }
        };
        muzstvoDiv.add(muzstvoForm);

        // nazov - input text
        muzstvoForm.add(new RequiredTextField<String>("nazov"));

        // kod - input text
        muzstvoForm.add(new RequiredTextField<String>("kod"));

        // nadefinovane muzstva - container
        final WebMarkupContainer muzstvaDiv = new WebMarkupContainer("muzstvaDiv");
        muzstvaDiv.setOutputMarkupId(true);
        muzstvaDiv.setOutputMarkupPlaceholderTag(true);
        add(muzstvaDiv);

        // nadefinovane muzstva - model
        List<MuzstvoDto> muzstva = muzstvoService.getMuzstva();
        final CompoundPropertyModel<List<MuzstvoDto>> muzstvaModel =
                new CompoundPropertyModel<List<MuzstvoDto>>(muzstva);

        // potvrdenie vymazania - container
        final WebMarkupContainer potvrdenieModalDialogDiv = new WebMarkupContainer("potvrdenieModalDialogDiv");
        potvrdenieModalDialogDiv.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        add(potvrdenieModalDialogDiv);
        // potvrdenie vymazania - modalne okno
        final PotvrdenieModalDialog potvrdenieModalDialog = new PotvrdenieModalDialog("potvrdenieModalDialog") {
            private static final long serialVersionUID = -1264141580867408924L;

            @Override
            public void vykonaj(AjaxRequestTarget target) {

                MuzstvoDto m = muzstvoService.getMuzstvo(getEntityId());

                AdminMuzstvoPage page;
                try {
                    muzstvoService.removeMuzstvo(pouzivatel.getLogin(), getEntityId());
                    page = new AdminMuzstvoPage(new PageParameters());
                } catch (Exception e) {
                    page = new AdminMuzstvoPage(new PageParameters());
                    page.error(new StringResourceModel("muzstvo.error.odstranit",
                            this,
                            null,
                            new Object[]{
                                    m.getNazov(),
                                    m.getKod()
                            }).getString());
                }
                setResponsePage(page);
            }
        };
        potvrdenieModalDialogDiv.add(potvrdenieModalDialog);

        // nadefinovane muzstva - tabulka
        ListView<MuzstvoDto> muzstvaRiadok = new ListView<MuzstvoDto>("muzstvaRiadok", muzstvaModel) {
            private static final long serialVersionUID = 2416250698773186162L;

            @Override
            protected void populateItem(ListItem<MuzstvoDto> item) {
                final MuzstvoDto m = item.getModelObject();
                item.add(new WebMarkupContainer("iconStatus")
                        .add(new AttributeAppender("class",
                                m.isAktivny()
                                        ? "fa fa-fw fa-check"
                                        : "fa fa-fw fa-trash")));
                item.add(new Label("nazov", m.getNazov()));
                item.add(new Label("kod", m.getKod()));
//                item.add(new VlajkaKodPanel("vlajka", m));
                WebMarkupContainer akcieDiv = new WebMarkupContainer("akcieDiv");
                item.add(akcieDiv.setVisible(m.isAktivny()));

                akcieDiv.add(new Link<String>("zmenitLink") {
                    private static final long serialVersionUID = 5067362756193616674L;

                    @Override
                    public void onClick() {
                        muzstvo = muzstvoService.getMuzstvo(m.getId());
                        setResponsePage(new AdminMuzstvoPage(new PageParameters().add("muzstvoId", m.getId())));
                    }
                });

                akcieDiv.add(new AjaxLink("odstranitLink") {
                    private static final long serialVersionUID = -7271934243532035782L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        // nasetujeme udaje pre potvrdenie
                        potvrdenieModalDialog.setEntityId(m.getId());
                        potvrdenieModalDialog.setAkcia(DialogAkciaEnum.VYMAZANIE);
                        potvrdenieModalDialog.zmenOtazku(new StringResourceModel("muzstvo.vymazanie.potvrdenie",
                                this,
                                null,
                                new Object[]{m.getNazov(), m.getKod()}).getString());
                        target.add(potvrdenieModalDialogDiv);
                        // a zobrazime ho
                        target.appendJavaScript("$('#potvrdenieModal').modal({show:true});");
                    }
                });

                item.add(new AjaxLink<String>("aktivovatLink") {

                    private static final long serialVersionUID = 6819239457324173693L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        try {
                            m.setStatus(EntityStatusEnum.AKTIVNY);
                            muzstvoService.editMuzstvo(pouzivatel.getLogin(), m);
                            setResponsePage(new AdminMuzstvoPage(new PageParameters()));
                        } catch (Exception e) {
                            error(new StringResourceModel("muzstvo.error.aktivovat",
                                    this,
                                    null,
                                    new Object[]{m.getNazov(), m.getKod()}).getString());
                        }
                    }
                }.setVisible(!m.isAktivny()));
            }
        };
        muzstvaDiv.add(muzstvaRiadok);
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ADMIN;
    }
}
