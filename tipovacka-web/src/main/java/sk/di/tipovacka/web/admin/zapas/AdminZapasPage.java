package sk.di.tipovacka.web.admin.zapas;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.comparator.ZapasDatumComparator;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.enums.KoniecZapasuTypEnum;
import sk.di.tipovacka.api.service.MuzstvoService;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.ZapasService;
import sk.di.tipovacka.web.commons.panel.vlajka.VlajkaKodPanel;
import sk.di.tipovacka.web.commons.panel.vlajka.klub.VlajkaKlubPanel;
import sk.di.tipovacka.web.menu.MenuItemEnum;
import sk.di.tipovacka.web.Template;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Administracia zapasu
 *
 * User: Dano
 * Date: 19.5.2013
 */
public class AdminZapasPage extends Template {

    private static final long serialVersionUID = 1894056423809302237L;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    @SpringBean
    private SutazRocnikService rocnikSutazeService;

    @SpringBean
    private MuzstvoService muzstvoService;

    @SpringBean
    private ZapasService zapasService;

    // aktualne vybraty zapas
    private ZapasDto zapas;

    public AdminZapasPage(PageParameters parameters) {
        super(parameters);

        // na zaciatku si nastavime inicializacne udaje
        zapas = new ZapasDto();

        // v parametroch moze prist rocnikSutazeId a zapasId, ak ich mame, nastavime
        final String zapasId = parameters.get("zapasId").toString();
        if (StringUtils.isNotEmpty(zapasId)) {
            zapas = zapasService.getZapas(Long.parseLong(zapasId));
        }
        // nadefinovane zapasy pre zvoleny rocnik sutaze - model
        List<ZapasDto> zapasy = new ArrayList<>();
        final CompoundPropertyModel<List<ZapasDto>> zapasyModel = new CompoundPropertyModel<List<ZapasDto>>(zapasy);

        String rocnikSutazeId = parameters.get("rocnikSutazeId").toString();
        if (StringUtils.isNotEmpty(rocnikSutazeId)) {
            RocnikSutazeDto rocnik = rocnikSutazeService.getRocnikSutaze(Long.parseLong(rocnikSutazeId));
            zapas.setRocnikSutazeId(rocnik.getId());
            zapas.setRocnikSutaze(rocnik);
            zapasyModel.getObject().clear();
            zapasy.clear();
            zapasy.addAll(rocnik.getZapasy());
            Collections.sort(zapasy, new ZapasDatumComparator());
        }
        zapasyModel.getObject().addAll(zapasy);

        // nadefinovane zapasy pre zvoleny rocnik sutaze - container
        final WebMarkupContainer zapasyDiv = new WebMarkupContainer("zapasyDiv");
        zapasyDiv.setOutputMarkupId(true);
        zapasyDiv.setOutputMarkupPlaceholderTag(true);
        add(zapasyDiv);

        // aktualne vybrany zapas - container
        final WebMarkupContainer zapasDiv = new WebMarkupContainer("zapasDiv");
        zapasDiv.setOutputMarkupId(true);
        zapasDiv.setOutputMarkupPlaceholderTag(true);
        add(zapasDiv);

        // aktualne vybrany zapas - model
        final CompoundPropertyModel<ZapasDto> model = new CompoundPropertyModel<ZapasDto>(zapas);

        // aktualne vybrany zapas - form
        final Form<ZapasDto> zapasForm = new Form<ZapasDto>("zapasForm", model) {

            private static final long serialVersionUID = -6943212574272500820L;

            @Override
            protected void onSubmit() {
                super.onSubmit();

                PageParameters params = new PageParameters();
                params.add("rocnikSutazeId", model.getObject().getRocnikSutaze().getId());
                // vysledna stranka
                AdminZapasPage page;
                // spracovanie zapasu
                try {
                    // podla tretin nastavime typ konca zapasu
                    if (StringUtils.isNotEmpty(zapas.getPriebeh())) {
                        StringTokenizer priebehVysledok = new StringTokenizer(zapas.getPriebeh(), ",");
                        switch (priebehVysledok.countTokens()) {
                            case 3:
                                zapas.setKoniecZapasuTyp(KoniecZapasuTypEnum.RIADNY_CAS);
                                break;
                            case 4:
                                zapas.setKoniecZapasuTyp(KoniecZapasuTypEnum.PREDLZENIE);
                                break;
                            case 5:
                                zapas.setKoniecZapasuTyp(KoniecZapasuTypEnum.ROZSTREL);
                                break;
                            default:
                                zapas.setKoniecZapasuTyp(KoniecZapasuTypEnum.HRA_SA);
                                break;
                        }
                    }
                    zapasService.editZapas(pouzivatel.getLogin(), zapas);

                    page = new AdminZapasPage(params);
                    page.info(new StringResourceModel("sutaz.rocnik.zapas.info.zmeneny",
                            this,
                            null,
                            new Object[]{
                                    zapas.getDomaci().getKod(),
                                    zapas.getHostia().getKod()}).getString());
                } catch (Exception e) {
                    page = new AdminZapasPage(params);
                    page.error(new StringResourceModel("sutaz.rocnik.zapas.error.spracovat",
                            this,
                            null,
                            new Object[]{
                                    zapas.getDomaci().getKod(),
                                    zapas.getHostia().getKod()}).getString());
                }

                setResponsePage(page);
            }
        };
        zapasDiv.add(zapasForm);

        // rocnik sutaze - combo
        final DropDownChoice<RocnikSutazeDto> rocnikSutazeCombo = new DropDownChoice<RocnikSutazeDto>("rocnikSutaze",
                new PropertyModel<RocnikSutazeDto>(model, "rocnikSutaze"),
                rocnikSutazeService.getRocnikySutaze(),
                new ChoiceRenderer<RocnikSutazeDto>() {

                    private static final long serialVersionUID = 5611756026710930341L;

                    @Override
                    public Object getDisplayValue(RocnikSutazeDto object) {
                        if (object == null) {
                            return "";
                        }

                        return object.getSutaz().getNazov()
                                + " - " + getString("sport.typ." + object.getSutaz().getSportTyp())
                                + " (ročník " + object.getRok() + ")";
                    }
                }
        ) {

            private static final long serialVersionUID = 5989124459980513721L;

            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected boolean isSelected(RocnikSutazeDto object, int index, String selected) {
                try {
                    return model.getObject().getRocnikSutaze().getId().equals(object.getId());
                } catch (Exception e) {
                    return super.isSelected(object, index, selected);
                }
            }
        };
        rocnikSutazeCombo.setNullValid(false).setRequired(true);
        zapasForm.add(rocnikSutazeCombo);

        // onUpdate - ajax behavior
        rocnikSutazeCombo.add(new AjaxFormComponentUpdatingBehavior("onchange") {

            private static final long serialVersionUID = 5620445949571539011L;

            @Override
            protected boolean getUpdateModel() {
                return true;
            }

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                RocnikSutazeDto vybranyRocnik = rocnikSutazeCombo.getModelObject();
                List<ZapasDto> zapasy = vybranyRocnik.getZapasy();
                Collections.sort(zapasy, new ZapasDatumComparator());
                zapasyModel.setObject(zapasy);
                target.add(zapasyDiv);
            }
        });

        // datum - input date
        zapasForm.add(new DateTextField("datum", "dd.MM.yyyy HH:mm"));

        // goly domaci - input text
        zapasForm.add(new RequiredTextField<String>("golyDomaci"));

        // goly hostia - input text
        zapasForm.add(new RequiredTextField<String>("golyHostia"));

        // priebeh - input text
        zapasForm.add(new RequiredTextField<String>("priebeh"));

        final Long vybranyZapasId = StringUtils.isEmpty(zapasId) ? null : Long.parseLong(zapasId);
        // nadefinovane rocniky - tabulka
        ListView<ZapasDto> rocnikyView = new ListView<ZapasDto>("zapasRiadok", zapasyModel) {

            private static final long serialVersionUID = 7989411748832009126L;

            @Override
            protected void populateItem(ListItem<ZapasDto> item) {
                final ZapasDto z = item.getModelObject();

                // priznak ci sa jedna o klubovu sutaz
                boolean isKlub = z.getRocnikSutaze().getSutaz().isKlub();

                String vybranyZapas = "fa fa-fw fa-check-square-o";
                if (vybranyZapasId != null && vybranyZapasId.equals(z.getId())) {
                    vybranyZapas = "fa fa-fw fa-edit";
                }

                item.add(new WebMarkupContainer("vybrany").add(new AttributeAppender("class", vybranyZapas)));
                item.add(new Label("datum", sdf.format(z.getDatum())));
                item.add(new Label("skupina", z.getSkupina()));
                // domaci
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("domaciVlajka", z.getDomaci(), false));
                } else {
                    item.add(new VlajkaKodPanel("domaciVlajka", z.getDomaci()));
                }
                // skore
                item.add(new Label("skore", z.getSkore()));
                item.add(new Label("priebeh", z.getPriebeh()));
                // hostia
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("hostiaVlajka", z.getHostia(), false));
                } else {
                    item.add(new VlajkaKodPanel("hostiaVlajka", z.getHostia()));
                }
                item.add(new Label("typZapasu", getString("zapas.typ." + z.getTypZapasu())));

                WebMarkupContainer akcieDiv = new WebMarkupContainer("akcieDiv");
                item.add(akcieDiv);

                akcieDiv.add(new Link<String>("zmenitZapasLink") {

                    private static final long serialVersionUID = -1426087643206192764L;

                    @Override
                    public void onClick() {
//                        zapas = zapasService.getZapas(z.getId());
                        setResponsePage(new AdminZapasPage(new PageParameters()
                                .add("zapasId", z.getId())
                                .add("rocnikSutazeId", z.getRocnikSutaze().getId())));
                    }
                });
            }
        };
        zapasyDiv.add(rocnikyView);
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ADMIN;
    }

    public SutazRocnikService getRocnikSutazeService() {
        return rocnikSutazeService;
    }

    public void setRocnikSutazeService(SutazRocnikService rocnikSutazeService) {
        this.rocnikSutazeService = rocnikSutazeService;
    }

    public ZapasDto getZapas() {
        return zapas;
    }

    public void setZapas(ZapasDto zapas) {
        this.zapas = zapas;
    }
}
