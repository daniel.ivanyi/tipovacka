package sk.di.tipovacka.web.dialog.potvrdenie;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import sk.di.tipovacka.web.dialog.DialogAkciaEnum;

/**
 * Panel generujuci potvrdzovacie modalne okno
 *
 * User: Dano
 * Date: 23.5.2013
 */
public abstract class PotvrdenieModalDialog extends Panel {

    private Long entityId;
    // akcia, ktora sa ma vykonat po potvrdeni, defualtne je VYMAZANIE
    private DialogAkciaEnum akcia = DialogAkciaEnum.VYMAZANIE;
    private Label nadpisLabel;
    private Label otazkaLabel;

    private static final long serialVersionUID = -1107215701232555105L;

    public PotvrdenieModalDialog(String id) {
        super(id);

        // nadpis - label
        add(nadpisLabel = new Label("nadpis", "Potvrdenie"));
        // text - otazka
        add(otazkaLabel = new Label("otazka", "Ste si isty?"));
        // tlacitko - ano
        add(new AjaxLink<String>("anoTlacitko") {

            private static final long serialVersionUID = 7633267815240504255L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                vykonaj(target);
            }
        });
    }

    public abstract void vykonaj(AjaxRequestTarget target);

    public DialogAkciaEnum getAkcia() {
        return akcia;
    }

    public void setAkcia(DialogAkciaEnum akcia) {
        this.akcia = akcia;
    }

    public Long getEntityId() {
        return entityId;
    }

    public void setEntityId(Long entityId) {
        this.entityId = entityId;
    }

    public void zmenOtazku(String otazka) {
        otazkaLabel.setDefaultModelObject(otazka);
    }

    public void zmenNadpis(String nadpis) {
        nadpisLabel.setDefaultModelObject(nadpis);
    }
}
