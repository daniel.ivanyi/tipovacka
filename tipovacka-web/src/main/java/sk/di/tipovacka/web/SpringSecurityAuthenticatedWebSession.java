package sk.di.tipovacka.web;

import org.apache.log4j.Logger;
import org.apache.wicket.Session;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.authroles.authorization.strategies.role.Roles;
import org.apache.wicket.injection.Injector;
import org.apache.wicket.request.Request;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.service.PouzivatelService;

import java.util.Date;


/**
 * User: Dano
 * Date: 16.5.2013
 */
public class SpringSecurityAuthenticatedWebSession extends AuthenticatedWebSession {

    private static final long serialVersionUID = -960362991656588115L;

    private static final Logger logger = Logger.getLogger(SpringSecurityAuthenticatedWebSession.class);

    private PouzivatelDto pouzivatel;
    private Date logonDate;

    @SpringBean
    private PouzivatelService pouzivatelService;

    @SpringBean(name = "authenticationManager")
    private AuthenticationManager authenticationManager;

    public PouzivatelDto getPouzivatel() {
        return pouzivatel;
    }

    public String getUsername() {
        return pouzivatel == null ? null : pouzivatel.getLogin();
    }

    public Date getLogonDate() {
        return logonDate;
    }

    public SpringSecurityAuthenticatedWebSession(Request request) {
        super(request);
        injectDependencies();
        ensureDependenciesNotNull();
    }

    public static SpringSecurityAuthenticatedWebSession getSpringWicketWebSession() {
        return (SpringSecurityAuthenticatedWebSession) Session.get();
    }

    private void ensureDependenciesNotNull() {
        if (authenticationManager == null) {
            throw new IllegalStateException("Requires an authentication");
        }
    }

    private void injectDependencies() {
        Injector.get().inject(this);
    }

    @Override
    public boolean authenticate(String username, String password) {
        boolean authenticated = false;
        try {
            Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
            SecurityContextHolder.getContext().setAuthentication(authentication);
            authenticated = authentication.isAuthenticated();

            pouzivatel = (PouzivatelDto) authentication.getPrincipal();
            logonDate = new Date();

            logger.info("Do aplikacie sa prihlasil pouzivatel[login=" + pouzivatel.getLogin() + "]");
        } catch (AuthenticationException ae) {
            logger.warn("nespravne zadane prihlasovacie udaje[login=" + username+ "]");
        }

        return authenticated;
    }

    @Override
    public Roles getRoles() {
        Roles roles = new Roles();
        if (isSignedIn()) {
            for (GrantedAuthority authority : pouzivatel.getAuthorities()) {
                roles.add(authority.getAuthority());
            }
        }
        return roles;
    }
}

