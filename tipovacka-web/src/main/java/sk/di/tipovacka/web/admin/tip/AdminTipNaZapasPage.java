package sk.di.tipovacka.web.admin.tip;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.comparator.ZapasDatumComparator;
import sk.di.tipovacka.api.db.kriteria.TipNaZapasKriteria;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.api.service.PouzivatelService;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.TipNaZapasService;
import sk.di.tipovacka.api.service.ZapasService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.commons.panel.vlajka.VlajkaKodPanel;
import sk.di.tipovacka.web.commons.panel.vlajka.klub.VlajkaKlubPanel;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Popis triedy
 * <p/>
 * User: IVAN1497
 * Date: 14.2.2014
 */
public class AdminTipNaZapasPage extends Template {
    private static final long serialVersionUID = 4023010166311471128L;

    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

    @SpringBean
    private SutazRocnikService sutazRocnikService;

    @SpringBean
    private ZapasService zapasService;

    @SpringBean
    TipNaZapasService tipNaZapasService;

    @SpringBean
    PouzivatelService pouzivatelService;

    // aktualne vybraty tip
    private TipNaZapasDto tipNaZapas;
    private Integer golyDomaci;
    private Integer golyHostia;
    private RocnikSutazeDto rocnikSutaze;
    private PouzivatelDto pouzivatel;

    public AdminTipNaZapasPage(PageParameters parameters) {
        super(parameters);

        tipNaZapas = new TipNaZapasDto();

        final List<ZapasDto> zapasyRocnika = new ArrayList<>();

        // nadefinovane tipy na zapas pre zvoleny rocnik sutaze a pouzivatela - model
        final Map<Long, TipNaZapasDto> mapaTipovNaZapas = new HashMap<>();
//        List<TipNaZapasDto> tipyNaZapas = new ArrayList<>();
//        final CompoundPropertyModel<List<TipNaZapasDto>> tipyNaZapasModel = new CompoundPropertyModel<List<TipNaZapasDto>>(tipyNaZapas);

        // v parametroch moze prist tipNaZapasId, ak ich mame, nastavime
        final String tipNaZapasId = parameters.get("tipNaZapasId").toString();
        if (StringUtils.isNotEmpty(tipNaZapasId)) {
            Long tipId = Long.parseLong(tipNaZapasId);
            // novy tip na zapas
            if (tipId == 0L) {
                ZapasDto zapas = zapasService.getZapas(Long.parseLong(parameters.get("zapasId").toString()));
                tipNaZapas = new TipNaZapasDto();
                tipNaZapas.setPouzivatel(pouzivatel);
                tipNaZapas.setZapas(zapas);
            } else {
                tipNaZapas = tipNaZapasService.getTipNaZapas(tipId);
                golyDomaci = tipNaZapas.getGolyDomaci();
                golyHostia = tipNaZapas.getGolyHostia();
            }
        }

        // ak je vybrany pouzivatel, pre ktoreho pozerame tipy
        String vybranyPouzivatelId = parameters.get("pouzivatelId").toString();
        String vybranyRocnikSutazeId = parameters.get("rocnikId").toString();
        if (StringUtils.isNotEmpty(vybranyPouzivatelId) && StringUtils.isNotEmpty(vybranyRocnikSutazeId)) {
            pouzivatel = pouzivatelService.getPouzivatel(Long.parseLong(vybranyPouzivatelId));
            rocnikSutaze = sutazRocnikService.getRocnikSutaze(Long.parseLong(vybranyRocnikSutazeId));
            TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
            kriteria.getPouzivatelIds().add(pouzivatel.getId());
            kriteria.setSutazRocnikId(rocnikSutaze.getId());
            List<TipNaZapasDto> tipy = tipNaZapasService.findTipyNaZapas(kriteria);
            for (TipNaZapasDto tip : tipy) {
                mapaTipovNaZapas.put(tip.getZapas().getId(), tip);
            }
            zapasyRocnika.clear();
            zapasyRocnika.addAll(rocnikSutaze.getZapasy());
            Collections.sort(zapasyRocnika, new ZapasDatumComparator());
        }

        // nadefinovane tipy na zapasy pre aktualny rocnik sutaze a pouzivatela - container
        final WebMarkupContainer tipyNaZapasDiv = new WebMarkupContainer("tipyNaZapasDiv");
        tipyNaZapasDiv.setOutputMarkupId(true);
        tipyNaZapasDiv.setOutputMarkupPlaceholderTag(true);
        add(tipyNaZapasDiv);

        // aktualne vybrany tip na zapas - container
        final WebMarkupContainer tipNaZapasDiv = new WebMarkupContainer("tipNaZapasDiv");
        tipNaZapasDiv.setOutputMarkupId(true);
        tipNaZapasDiv.setOutputMarkupPlaceholderTag(true);
        add(tipNaZapasDiv);

        // aktualne vybrany tip na zapas - model
        final CompoundPropertyModel<TipNaZapasDto> model = new CompoundPropertyModel<TipNaZapasDto>(tipNaZapas);

        // aktualne vybrany tip na zapas - form
        final Form<TipNaZapasDto> tipNaZapasForm = new Form<TipNaZapasDto>("tipNaZapasForm", model) {

            private static final long serialVersionUID = -5909910230660356711L;

            @Override
            protected void onSubmit() {
                super.onSubmit();

                PageParameters params = new PageParameters();
                params.add("pouzivatelId", pouzivatel.getId());
                params.add("rocnikId", rocnikSutaze.getId());
                // vysledna stranka
                AdminTipNaZapasPage page;

                try {
                    // spracovanie tipu na zapasu
                    tipNaZapas.setGolyDomaci(golyDomaci);
                    tipNaZapas.setGolyHostia(golyHostia);
                    tipNaZapas.setPouzivatel(pouzivatel);
                    // ked tip nema nadefinovane id, vkladame novy tip
                    if (tipNaZapas.getId() == null) {
                        tipNaZapasService.addTipNaZapas(pouzivatel.getLogin(), tipNaZapas, true);
                    // inak editujeme existujuci
                    } else {
                        tipNaZapasService.editTipNaZapas(pouzivatel.getLogin(), tipNaZapas, true);
                    }

                    page = new AdminTipNaZapasPage(params);
                    page.info(new StringResourceModel("zapas.tip.info.zmeneny",
                            this,
                            null,
                            new Object[]{
                                    tipNaZapas.getZapas().getDomaci().getKod(),
                                    tipNaZapas.getZapas().getHostia().getKod()}).getString());
                } catch (Exception e) {
                    page = new AdminTipNaZapasPage(params);
                    page.error(new StringResourceModel("zapas.tip.error.spracovat",
                            this,
                            null,
                            new Object[]{
                                    tipNaZapas.getZapas().getDomaci().getKod(),
                                    tipNaZapas.getZapas().getHostia().getKod()}).getString());
                }

                setResponsePage(page);
            }
        };
        tipNaZapasDiv.add(tipNaZapasForm);

        // rocnik sutaze - combo
        final DropDownChoice<RocnikSutazeDto> rocnikSutazeCombo = new DropDownChoice<RocnikSutazeDto>("rocnikSutaze",
                new PropertyModel<RocnikSutazeDto>(this, "rocnikSutaze"),
                sutazRocnikService.getRocnikySutaze(),
                new ChoiceRenderer<RocnikSutazeDto>() {

                    private static final long serialVersionUID = 5611756026710930341L;

                    @Override
                    public Object getDisplayValue(RocnikSutazeDto object) {
                        if (object == null) {
                            return "";
                        }

                        return object.getSutaz().getNazov()
                                + " - " + getString("sport.typ." + object.getSutaz().getSportTyp())
                                + " (ročník " + object.getRok() + ")";
                    }
                }
        ) {

            private static final long serialVersionUID = 5989124459980513721L;

            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected boolean isSelected(RocnikSutazeDto object, int index, String selected) {
                try {
                    return rocnikSutaze.getId().equals(object.getId());
                } catch (Exception e) {
                    return super.isSelected(object, index, selected);
                }
            }
        };
        rocnikSutazeCombo.setNullValid(false).setRequired(true);
        tipNaZapasForm.add(rocnikSutazeCombo);

        // onUpdate - ajax behavior
        rocnikSutazeCombo.add(new AjaxFormComponentUpdatingBehavior("onchange") {

            private static final long serialVersionUID = 5620445949571539011L;

            @Override
            protected boolean getUpdateModel() {
                return true;
            }

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                RocnikSutazeDto vybranyRocnik = rocnikSutazeCombo.getModelObject();
                if (pouzivatel != null) {
                    TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
                    kriteria.getPouzivatelIds().add(pouzivatel.getId());
                    kriteria.setSutazRocnikId(vybranyRocnik.getId());
                    List<TipNaZapasDto> tipy = tipNaZapasService.findTipyNaZapas(kriteria);

                    mapaTipovNaZapas.clear();
                    for (TipNaZapasDto tip : tipy) {
                        mapaTipovNaZapas.put(tip.getZapas().getId(), tip);
                    }

                    zapasyRocnika.clear();
                    zapasyRocnika.addAll(vybranyRocnik.getZapasy());
                    Collections.sort(zapasyRocnika, new ZapasDatumComparator());
                }

                target.add(tipyNaZapasDiv);
            }
        });

        // pouzivatel - combo
        final DropDownChoice<PouzivatelDto> pouzivatelCombo = new DropDownChoice<PouzivatelDto>("pouzivatel",
                new PropertyModel<PouzivatelDto>(this, "pouzivatel"),
                pouzivatelService.getPouzivatelia(),
                new ChoiceRenderer<PouzivatelDto>() {

                    private static final long serialVersionUID = 5611756026710930341L;

                    @Override
                    public Object getDisplayValue(PouzivatelDto object) {
                        if (object == null) {
                            return "";
                        }

                        return object.getLogin();
                    }
                }
        ) {

            private static final long serialVersionUID = 3134447109373795673L;

            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }

            @Override
            protected boolean isSelected(PouzivatelDto object, int index, String selected) {
                try {
                    return pouzivatel.getId().equals(object.getId());
                } catch (Exception e) {
                    return super.isSelected(object, index, selected);
                }
            }
        };
        pouzivatelCombo.setNullValid(false).setRequired(true);
        tipNaZapasForm.add(pouzivatelCombo);

        // onUpdate - ajax behavior
        pouzivatelCombo.add(new AjaxFormComponentUpdatingBehavior("onchange") {

            private static final long serialVersionUID = 3613836471599174451L;

            @Override
            protected boolean getUpdateModel() {
                return true;
            }

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                PouzivatelDto vybranyPouzivatel = pouzivatelCombo.getModelObject();
                if (rocnikSutaze != null) {

                    TipNaZapasKriteria kriteria = new TipNaZapasKriteria();
                    kriteria.getPouzivatelIds().add(vybranyPouzivatel.getId());
                    kriteria.setSutazRocnikId(rocnikSutaze.getId());
                    List<TipNaZapasDto> tipy = tipNaZapasService.findTipyNaZapas(kriteria);

                    mapaTipovNaZapas.clear();
                    for (TipNaZapasDto tip : tipy) {
                        mapaTipovNaZapas.put(tip.getZapas().getId(), tip);
                    }

                    zapasyRocnika.clear();
                    zapasyRocnika.addAll(rocnikSutaze.getZapasy());
                    Collections.sort(zapasyRocnika, new ZapasDatumComparator());
                }

                target.add(tipyNaZapasDiv);
            }
        });

        // zapas na ktory sa tipuje
        // domaci - container
        tipNaZapasForm.add(new VlajkaKodPanel("domaciVlajka",
                tipNaZapas.getZapas() == null ? null : tipNaZapas.getZapas().getDomaci()));
        // hostia - container
        tipNaZapasForm.add(new VlajkaKodPanel("hostiaVlajka",
                tipNaZapas.getZapas() == null ? null : tipNaZapas.getZapas().getHostia()));

        // tip na goly domaci - input
        tipNaZapasForm.add(new RequiredTextField<Integer>("golyDomaci", new PropertyModel<Integer>(this, "golyDomaci")));
        // tip na goly hostia - input
        tipNaZapasForm.add(new RequiredTextField<Integer>("golyHostia", new PropertyModel<Integer>(this, "golyHostia")));

        // nadefinovane rocniky - tabulka
        ListView<ZapasDto> tipyNaZapasView = new ListView<ZapasDto>("tipNaZapasRiadok", zapasyRocnika) {

            private static final long serialVersionUID = 1714302946474312718L;

            @Override
            protected void populateItem(ListItem<ZapasDto> item) {
                ZapasDto zapas = item.getModelObject();
                // priznak, ci sa jedna o klubovu sutaz
                boolean isKlub = zapas.getRocnikSutaze().getSutaz().isKlub();

                final Long zapasId = zapas.getId();
                TipNaZapasDto tip = mapaTipovNaZapas.get(zapasId);
                final Long tipNaZapasId = tip == null ? null : tip.getId();
                // datum
                item.add(new Label("datum", sdf.format(zapas.getDatum())));
                // domaci
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("domaciVlajka", zapas.getDomaci(), false));
                } else {
                    item.add(new VlajkaKodPanel("domaciVlajka", zapas.getDomaci()));
                }
                // hostia
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("hostiaVlajka", zapas.getHostia(), false));
                } else {
                    item.add(new VlajkaKodPanel("hostiaVlajka", zapas.getHostia()));
                }
                // goly domaci
                item.add(new Label("golyDomaci", tip == null ? null : tip.getGolyDomaci().toString()));
                // goly hostia
                item.add(new Label("golyHostia", tip == null ? null : tip.getGolyHostia().toString()));
                // zmena tipu
                item.add(new Link<String>("zmenitTipLink") {

                    private static final long serialVersionUID = 2447486668782476964L;

                    @Override
                    public void onClick() {
                        setResponsePage(new AdminTipNaZapasPage(new PageParameters()
                                .add("tipNaZapasId", tipNaZapasId == null ? 0L : tipNaZapasId)
                                .add("pouzivatelId", pouzivatel.getId())
                                .add("rocnikId", rocnikSutaze.getId())
                                .add("zapasId", zapasId)
                        ));
                    }
                });
            }
        };
        tipyNaZapasDiv.add(tipyNaZapasView);
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ADMIN;
    }
}
