package sk.di.tipovacka.web.menu;

/**
 * Enum pre grupovanie menu poloziek.
 */
public enum MenuItemEnum {

    ADMIN("Administrácia", "fa fa-fw fa-cogs"),
    HOME("Domov", "fa fa-fw fa-home"),
    TIP("Ostatné tipy", "fa fa-fw fa-question"),
    ARCHIV("Archív", "fa fa-fw fa-archive"),
    TEST("Test", "fa fa-fw fa-wrench"),
    POUZIVATEL("Používateľ", "fa fa-fw fa-user"),
    POKLADNIK("Podmienky účasti", "fa fa-fw fa-money"),
    NONE("", "");

    private String label;
    private String iconClass;

    private MenuItemEnum(String label, String iconClass) {
        this.label = label;
        this.iconClass = iconClass;
    }

    public String getLabel() {
        return label;
    }

    public String getIconClass() {
        return iconClass;
    }

    @Override
    public String toString() {
        return label;
    }
}
