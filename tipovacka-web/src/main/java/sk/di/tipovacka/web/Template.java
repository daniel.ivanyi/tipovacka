package sk.di.tipovacka.web;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.service.PouzivatelService;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.web.admin.HomeAdminPage;
import sk.di.tipovacka.web.admin.muzstva.AdminMuzstvoPage;
import sk.di.tipovacka.web.admin.pouzivatel.AdminPouzivatelPage;
import sk.di.tipovacka.web.admin.rocnik.AdminRocnikPage;
import sk.di.tipovacka.web.admin.sutaz.AdminSutazPage;
import sk.di.tipovacka.web.admin.test.AdminTestPage;
import sk.di.tipovacka.web.admin.tip.AdminTipNaZapasPage;
import sk.di.tipovacka.web.admin.zapas.AdminZapasPage;
import sk.di.tipovacka.web.auth.LoginPage;
import sk.di.tipovacka.web.home.HomePage;
import sk.di.tipovacka.web.home.podmienky.PodmienkyPage;
import sk.di.tipovacka.web.menu.MenuItemEnum;
import sk.di.tipovacka.web.menu.TwitterBootstrapNavBarPanel;
import sk.di.tipovacka.web.pokladnik.PokladnikPage;
import sk.di.tipovacka.web.tip.archiv.TipyArchivPrehladPage;
import sk.di.tipovacka.web.tip.prehlad.TipyPrehladPage;
import sk.di.tipovacka.web.utils.SecurityUtils;

/**
 * Template pre vsetkz stranky.
 * <p/>
 * User: Dano
 * Date: 17.5.2013
 */
public abstract class Template extends WebPage {

    private static final long serialVersionUID = 374271932298510431L;

    // aktualne prihlaseny pouzivatel, aby sme k nemu vedeli pristupovat aj z ostatnych stranok
    protected PouzivatelDto pouzivatel = SecurityUtils.getCurrentUser();
    protected RocnikSutazeDto vybranyRocnik;

    @SpringBean
    private PouzivatelService pouzivatelService;

    @SpringBean
    private SutazRocnikService sutazRocnikService;

    public Template(PageParameters parameters) {
        super(parameters);

        //ak pouzivatel nie je prihlaseny, redirect na login stranku
        if (!SpringSecurityAuthenticatedWebSession.get().isSignedIn()) {
            throw new RestartResponseException(LoginPage.class);
        }

        // nasetujeme vybrany rocnik podla prihlaseneho pouzivatela
        vybranyRocnik = pouzivatel.getVybranyRocnik();

        // ak je vybrany rocnik, nasetujeme ho
        String rocnikId = parameters.get("rocnikId").toString();
        if (StringUtils.isNotEmpty(rocnikId)) {
            vybranyRocnik = sutazRocnikService.getRocnikSutaze(Long.parseLong(rocnikId));
            pouzivatel.setVybranyRocnik(vybranyRocnik);
        }

        // domovska stranka
        Class page;
        if (pouzivatel.isAdmin()) {
            page = HomeAdminPage.class;
        } else {
            if (pouzivatel.isSuhlas()) {
                page = HomePage.class;
            } else {
                page = PodmienkyPage.class;
            }
        }
        // vyskladame menu
        TwitterBootstrapNavBarPanel.Builder nav = new TwitterBootstrapNavBarPanel.Builder("navBar",
                page,
                getActiveMenu());
        // ak je prihlaseny admin
        if (pouzivatel.isAdmin()) {
            nav.withMenuItemAsDropdown(MenuItemEnum.ADMIN, AdminSutazPage.class, getString("menu.admin.sutaz"))
                    .withMenuItemAsDropdown(MenuItemEnum.ADMIN, AdminRocnikPage.class, getString("menu.admin.sutaz.rocnik"))
                    .withMenuItemAsDropdown(MenuItemEnum.ADMIN, AdminZapasPage.class, getString("menu.admin.sutaz.rocnik.zapas"))
                    .withMenuItemAsDropdown(MenuItemEnum.ADMIN, AdminTipNaZapasPage.class, getString("menu.admin.sutaz.rocnik.zapas.tip"))
                    .withMenuItemAsDropdown(MenuItemEnum.ADMIN, AdminMuzstvoPage.class, getString("menu.admin.muzstvo"))
                    .withMenuItemAsDropdown(MenuItemEnum.ADMIN, AdminPouzivatelPage.class, getString("menu.admin.pouzivatel"));
            // pre testovanie
            nav.withMenuItemAsDropdown(MenuItemEnum.TEST, AdminTestPage.class, getString("menu.test"));
        // inak tiper
        }
        if (pouzivatel.isTiper()) {
            nav.withMenuItem(MenuItemEnum.TIP, TipyPrehladPage.class);
            nav.withMenuItem(MenuItemEnum.ARCHIV, TipyArchivPrehladPage.class);
        }
        if (pouzivatel.isPokladnik()) {
            nav.withMenuItem(MenuItemEnum.POKLADNIK, PokladnikPage.class);
        }
        // a pridame ho na stranku
        add(nav.build());

        // error - info hlaska
        add(new FeedbackPanel("feedback") {

            private static final long serialVersionUID = -785584817320401039L;

            @Override
            protected String getCSSClass(FeedbackMessage message) {
                String cssClass;
                switch (message.getLevel()) {
                    case FeedbackMessage.SUCCESS:
                        cssClass = "msg success";
                        break;
                    case FeedbackMessage.ERROR:
                        cssClass = "msg failure";
                        break;
                    case FeedbackMessage.WARNING:
                        cssClass = "msg warning";
                        break;
                    case FeedbackMessage.INFO:
                        cssClass = "msg notice";
                        break;
                    default:
                        cssClass = "msg notice";
                        break;
                }
                return cssClass;
            }
        });
    }

    public abstract MenuItemEnum getActiveMenu();
}
