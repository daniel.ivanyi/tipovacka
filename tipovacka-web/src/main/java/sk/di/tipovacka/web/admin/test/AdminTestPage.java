package sk.di.tipovacka.web.admin.test;

import org.apache.wicket.extensions.markup.html.form.DateTextField;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.ZapasService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.util.Date;

/**
 * Stranka pre nastavenie testovacieho rocnika
 *
 * User: Dano
 * Date: 1.6.2013
 */
public class AdminTestPage extends Template {
    private static final long serialVersionUID = 6525516720177112407L;

    @SpringBean
    private ZapasService zapasService;
    @SpringBean
    private SutazRocnikService rocnikSutazeService;

    private RocnikSutazeDto rocnikSutaze;
    private Date zaciatok;

    public AdminTestPage(PageParameters parameters) {
        super(parameters);

        Form<Void> testForm = new Form<Void>("testForm") {
            private static final long serialVersionUID = -6044275992463858699L;

            @Override
            protected void onSubmit() {
                try {
                    zapasService.initZapasyNaTest(rocnikSutaze.getId(), zaciatok);
                    info(new StringResourceModel("sutaz.rocnik.zapas.info.nastaveneNaTest",
                            this,
                            null,
                            new Object[]{
                                    rocnikSutaze.getSutaz().getNazov() + " " + rocnikSutaze.getRok()
                            }).getString());
                } catch (Exception e) {
                    error(new StringResourceModel("sutaz.rocnik.zapas.error.nastaveneNaTest",
                            this,
                            null,
                            new Object[]{
                                    rocnikSutaze.getSutaz().getNazov() + " " + rocnikSutaze.getRok()
                            }).getString());
                }
            }
        };
        add(testForm);

        // rocnik sutaze
        testForm.add(new DropDownChoice<RocnikSutazeDto>("rocnikSutaze",
                new PropertyModel<RocnikSutazeDto>(this, "rocnikSutaze"),
                rocnikSutazeService.getRocnikySutaze(),
                new ChoiceRenderer<RocnikSutazeDto>() {
                    private static final long serialVersionUID = -6821338982763787071L;

                    @Override
                    public Object getDisplayValue(RocnikSutazeDto object) {
                        if (object == null) {
                            return "";
                        }

                        return object.getSutaz().getNazov()
                                + " - " + getString("sport.typ." + object.getSutaz().getSportTyp())
                                + " (ročník " + object.getRok() + ")";
                    }
                }
        ));

        // zaciatok - input date
        testForm.add(new DateTextField("zaciatok",
                new PropertyModel<Date>(this, "zaciatok"),
                "dd.MM.yyyy HH:mm"));
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.TEST;
    }
}
