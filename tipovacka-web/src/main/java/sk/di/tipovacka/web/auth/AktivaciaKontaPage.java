package sk.di.tipovacka.web.auth;

import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.service.PouzivatelService;

/**
 * Stranka na aktivaciu konta
 *
 * User: Dano
 * Date: 28.10.2013
 */
public class AktivaciaKontaPage extends WebPage {

    private static final long serialVersionUID = -4183798625635511934L;

    @SpringBean
    private PouzivatelService pouzivatelService;

    public AktivaciaKontaPage(final PageParameters parameters) {

        // login page - link
        Link loginPageLink = new Link<String>("loginPage") {
            private static final long serialVersionUID = -342591890445611118L;

            @Override
            public void onClick() {
                setResponsePage(new LoginPage(new PageParameters()));
            }
        };
        add(loginPageLink);

        // popis aktivacie - label
        Label popis = new Label("popis", "");
        add(popis);

        try {
            Long pouivatelId = parameters.get("code").toLong();
            // dohladame pouzivatela
            PouzivatelDto pouzivatel = pouzivatelService.getPouzivatel(pouivatelId);
            if (pouzivatel == null) {
                popis.setDefaultModelObject(getString("konto.aktivacia.error"));
                loginPageLink.setVisible(false);
            } else if (EntityStatusEnum.AKTIVNY.equals(pouzivatel.getStatus())) {
                popis.setDefaultModelObject(getString("konto.aktivacia.uzAktivne"));
            } else {
                pouzivatel.setStatus(EntityStatusEnum.AKTIVNY);
                pouzivatelService.editPouzivatel("admin", pouzivatel);
                popis.setDefaultModelObject(getString("konto.aktivacia.info"));
            }
        } catch (Exception e) {
            popis.setDefaultModelObject(getString("konto.aktivacia.error"));
            loginPageLink.setVisible(false);
        }
    }
}
