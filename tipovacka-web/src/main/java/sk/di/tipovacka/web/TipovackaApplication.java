package sk.di.tipovacka.web;

import org.apache.wicket.RuntimeConfigurationType;
import org.apache.wicket.authroles.authentication.AuthenticatedWebApplication;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.spring.injection.annot.SpringComponentInjector;
import sk.di.tipovacka.web.admin.HomeAdminPage;
import sk.di.tipovacka.web.admin.muzstva.AdminMuzstvoPage;
import sk.di.tipovacka.web.admin.pouzivatel.AdminPouzivatelPage;
import sk.di.tipovacka.web.admin.rocnik.AdminRocnikPage;
import sk.di.tipovacka.web.admin.sutaz.AdminSutazPage;
import sk.di.tipovacka.web.admin.zapas.AdminZapasPage;
import sk.di.tipovacka.web.auth.AktivaciaKontaPage;
import sk.di.tipovacka.web.auth.LoginPage;
import sk.di.tipovacka.web.home.HomePage;
import sk.di.tipovacka.web.home.podmienky.PodmienkyPage;
import sk.di.tipovacka.web.home.vyber.VyberPage;
import sk.di.tipovacka.web.tip.archiv.TipyArchivDetailPage;
import sk.di.tipovacka.web.tip.archiv.TipyArchivPrehladPage;
import sk.di.tipovacka.web.tip.prehlad.TipyPrehladPage;

/**
 * Aplikacia Tipovacka, do ktorej sa treba prihlasit
 */
public class TipovackaApplication extends AuthenticatedWebApplication {
    boolean isInitialized = false;

    @Override
    protected void init() {
        if (!isInitialized) {
            super.init();
            setListeners();
            isInitialized = true;

            mountPage("konto/aktivacia", AktivaciaKontaPage.class);

            mountPage("admin/domov", HomeAdminPage.class);
            mountPage("admin/sutaz", AdminSutazPage.class);
            mountPage("admin/sutaz/rocnik", AdminRocnikPage.class);
            mountPage("admin/sutaz/rocnik/zapas", AdminZapasPage.class);
            mountPage("admin/muzstvo", AdminMuzstvoPage.class);
            mountPage("admin/pouzivatel", AdminPouzivatelPage.class);

            mountPage("domov", HomePage.class);
            mountPage("domov/sutaz/podmienky", PodmienkyPage.class);
            mountPage("domov/sutaz/vyber", VyberPage.class);
            mountPage("tipy", TipyPrehladPage.class);
            mountPage("archiv", TipyArchivPrehladPage.class);
            mountPage("archiv/detail", TipyArchivDetailPage.class);

            getDebugSettings().setAjaxDebugModeEnabled(false);
            getMarkupSettings().setStripWicketTags(true);
            getMarkupSettings().setDefaultMarkupEncoding("UTF-8");
            setExternalHtmlDirIfSystemPropertyIsPresent();
        }
    }

    private void setExternalHtmlDirIfSystemPropertyIsPresent() {
        String externalDir = System.getProperty("wicket.html.dir");
        if (externalDir != null) {
            getResourceSettings().addResourceFolder(externalDir);
        }
    }

    private void setListeners() {
        getComponentInstantiationListeners().add(new SpringComponentInjector(this));
    }

    @Override
    public RuntimeConfigurationType getConfigurationType() {
        return RuntimeConfigurationType.DEPLOYMENT;
    }

    @SuppressWarnings("unchecked")
    public Class getHomePage() {
        return LoginPage.class;
    }

    @Override
    protected Class<? extends AuthenticatedWebSession> getWebSessionClass() {
        return SpringSecurityAuthenticatedWebSession.class;
    }

    @Override
    protected Class<? extends WebPage> getSignInPageClass() {
        return LoginPage.class;

    }
}
