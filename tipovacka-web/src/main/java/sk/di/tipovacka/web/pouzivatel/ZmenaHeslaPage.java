package sk.di.tipovacka.web.pouzivatel;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.service.PouzivatelService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.menu.MenuItemEnum;

/**
 * Stranka pre zmenu hesla.
 * <p/>
 * User: Dano
 * Date: 21.5.2013
 */
public class ZmenaHeslaPage extends Template {

    private static final long serialVersionUID = -2725888442366262618L;

    @SpringBean
    PouzivatelService pouzivatelService;

    private String stareHeslo;
    private String noveHeslo;
    private String potvrdenieHesla;

    public ZmenaHeslaPage(PageParameters parameters) {
        super(parameters);

        // formular pre zmenu hesla
        Form<Void> zmenaHeslaForm = new Form<Void>("zmenaHeslaForm") {
            private static final long serialVersionUID = 8427616940062421689L;

            @Override
            protected void onSubmit() {

                ZmenaHeslaPage page = new ZmenaHeslaPage(new PageParameters());

                // overime, ci nove heslo je spravne potvrdene
                if (getNoveHeslo().equals(getPotvrdenieHesla())) {
                    try {
                        pouzivatelService.zmenaHesla(pouzivatel.getId(),
                                getStareHeslo(),
                                getNoveHeslo());
                        page.info(getString("zmena.hesla.info"));
                    } catch (Exception e) {
                        page.error(getString("zmena.hesla.error"));
                    }
                } else {
                    page.error(getString("zmena.hesla.potvrdeneHeslo"));
                }
            }
        };
        add(zmenaHeslaForm);

        // stare heslo - input password
        zmenaHeslaForm.add(new PasswordTextField("stareHeslo", new PropertyModel<String>(this, "stareHeslo")));

        // nove heslo - input password
        zmenaHeslaForm.add(new PasswordTextField("noveHeslo", new PropertyModel<String>(this, "noveHeslo")));

        // potvrdenie hesla - input password
        zmenaHeslaForm.add(new PasswordTextField("potvrdenieHesla", new PropertyModel<String>(this, "potvrdenieHesla")));

    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.POUZIVATEL;
    }

    public String getStareHeslo() {
        return stareHeslo;
    }

    public void setStareHeslo(String stareHeslo) {
        this.stareHeslo = stareHeslo;
    }

    public String getNoveHeslo() {
        return noveHeslo;
    }

    public void setNoveHeslo(String noveHeslo) {
        this.noveHeslo = noveHeslo;
    }

    public String getPotvrdenieHesla() {
        return potvrdenieHesla;
    }

    public void setPotvrdenieHesla(String potvrdenieHesla) {
        this.potvrdenieHesla = potvrdenieHesla;
    }
}
