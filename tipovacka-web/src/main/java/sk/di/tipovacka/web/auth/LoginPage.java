package sk.di.tipovacka.web.auth;

import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.authroles.authentication.AuthenticatedWebSession;
import org.apache.wicket.feedback.FeedbackMessage;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.WebPage;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.PasswordTextField;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.FeedbackPanel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.enums.RolaEnum;
import sk.di.tipovacka.api.service.PouzivatelService;
import sk.di.tipovacka.web.home.HomePage;
import sk.di.tipovacka.web.home.podmienky.PodmienkyPage;
import sk.di.tipovacka.web.admin.HomeAdminPage;
import sk.di.tipovacka.web.utils.SecurityUtils;

import javax.persistence.EntityNotFoundException;


/**
 * Prihlasovacie a registracna stranka.
 * <p/>
 * User: Dano
 * Date: 16.5.2013
 */
public final class LoginPage extends WebPage {

    private static final long serialVersionUID = -5640163110173316134L;

    private String username;
    private String password;
    private String mail;
    private PouzivatelDto pouzivatel;

    @SpringBean
    private PouzivatelService pouzivatelService;

    public LoginPage(final PageParameters parameters) {

        // formulare - container
        final WebMarkupContainer formDiv = new WebMarkupContainer("formDiv");
        formDiv.setOutputMarkupId(true);
        formDiv.setOutputMarkupPlaceholderTag(true);
        add(formDiv);

        // feedback - panel
        formDiv.add(new FeedbackPanel("feedback") {

            private static final long serialVersionUID = 7964627092496859575L;

            @Override
            protected String getCSSClass(FeedbackMessage message) {
                String cssClass;
                switch (message.getLevel()) {
                    case FeedbackMessage.SUCCESS:
                        cssClass = "msg success";
                        break;
                    case FeedbackMessage.ERROR:
                        cssClass = "msg failure";
                        break;
                    case FeedbackMessage.WARNING:
                        cssClass = "msg warning";
                        break;
                    case FeedbackMessage.INFO:
                        cssClass = "msg notice";
                        break;
                    default:
                        cssClass = "msg notice";
                        break;
                }
                return cssClass;
            }
        });

        // login form - form
        final Form loginForm = new Form("loginForm") {
            private static final long serialVersionUID = 7154702832343384399L;

            @Override
            protected void onSubmit() {
                AuthenticatedWebSession session = AuthenticatedWebSession.get();
                if (session.signIn(username, password)) {
                    PouzivatelDto pouzivatel = SecurityUtils.getCurrentUser();
                    // ak je prihlaseny admin ide na admin home
                    if (pouzivatel.isAdmin()) {
                        setResponsePage(new HomeAdminPage(new PageParameters()));
                    } else {
                        if (pouzivatel.isSuhlas()) {
                            setResponsePage(new HomePage(new PageParameters()));
                        } else {
                            setResponsePage(new PodmienkyPage(new PageParameters()));
                        }
                    }
                } else {
                    error(getString("login.failed"));
                }
            }
        };
        formDiv.add(loginForm);


        loginForm.add(new RequiredTextField<String>("username", new PropertyModel<String>(this, "username")));
        loginForm.add(new PasswordTextField("password", new PropertyModel<String>(this, "password")));

        // registracia form - form
        pouzivatel = new PouzivatelDto();
        CompoundPropertyModel<PouzivatelDto> model = new CompoundPropertyModel<PouzivatelDto>(pouzivatel);
        final Form<PouzivatelDto> registraciaForm = new Form<PouzivatelDto>("registraciaForm", model) {

            private static final long serialVersionUID = 2597397713012074966L;

            @Override
            protected void onSubmit() {
                pouzivatel.getRoly().clear();
                pouzivatel.getRoly().add(RolaEnum.TIPER);

                LoginPage page = new LoginPage(new PageParameters());
                try {
                    pouzivatelService.addPouzivatel("registracia_nova", pouzivatel);
                    page.info(new StringResourceModel("pouzivatel.info.registrovany",
                            this,
                            null,
                            new Object[]{pouzivatel.getLogin()}).getString());
                } catch (Exception e) {
                    page.error(new StringResourceModel("pouzivatel.error.registrovany",
                            this,
                            null,
                            new Object[]{pouzivatel.getLogin()}).getString());
                }
                setResponsePage(page);
            }
        };
        formDiv.add(registraciaForm.setVisible(false));

        registraciaForm.add(new RequiredTextField<String>("login"));
        registraciaForm.add(new PasswordTextField("heslo"));
        registraciaForm.add(new RequiredTextField<String>("mail"));
        registraciaForm.add(new RequiredTextField<String>("meno"));
        registraciaForm.add(new RequiredTextField<String>("priezvisko"));

        final Form<String> zabudnuteHesloForm = new Form<String>("zabudnuteHesloForm") {
            private static final long serialVersionUID = 4195440736087634845L;

            @Override
            protected void onSubmit() {
                LoginPage page = new LoginPage(new PageParameters());

                try {
                    pouzivatelService.zabudnuteHeslo(mail);
                    page.info(new StringResourceModel("pouzivatel.info.zabudnute.heslo",
                            this,
                            null,
                            new Object[]{mail}).getString());
                } catch (EntityNotFoundException e) {
                    page.error(new StringResourceModel("pouzivatel.error.nenajdeny",
                            this,
                            null,
                            new Object[]{mail}).getString());
                } catch (Exception e) {
                    page.error(new StringResourceModel("pouzivatel.error.zabudnute.heslo",
                            this,
                            null,
                            new Object[]{mail}).getString());
                }
                setResponsePage(page);
            }
        };
        formDiv.add(zabudnuteHesloForm.setVisible(false));

        zabudnuteHesloForm.add(new RequiredTextField<String>("mail", new PropertyModel<String>(this, "mail")));

        // prihlasenie - container - onclick
        add(new WebMarkupContainer("prihlasenie").add(new AjaxEventBehavior("onclick") {
            private static final long serialVersionUID = 4925174212129970343L;

            @Override
            protected void onEvent(AjaxRequestTarget target) {
                loginForm.setVisible(true);
                registraciaForm.setVisible(false);
                zabudnuteHesloForm.setVisible(false);
                target.add(formDiv);
            }
        }));

        // registracia - container - onclick
        add(new WebMarkupContainer("registracia").add(new AjaxEventBehavior("onclick") {
            private static final long serialVersionUID = 979370669028827909L;

            @Override
            protected void onEvent(AjaxRequestTarget target) {
                loginForm.setVisible(false);
                registraciaForm.setVisible(true);
                zabudnuteHesloForm.setVisible(false);
                target.add(formDiv);
            }
        }));

        // zabudnuteHeslo - container - onclick
        add(new WebMarkupContainer("zabudnuteHeslo").add(new AjaxEventBehavior("onclick") {
            private static final long serialVersionUID = 5368780654130858541L;

            @Override
            protected void onEvent(AjaxRequestTarget target) {
                loginForm.setVisible(false);
                registraciaForm.setVisible(false);
                zabudnuteHesloForm.setVisible(true);
                target.add(formDiv);
            }
        }));
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }
}

