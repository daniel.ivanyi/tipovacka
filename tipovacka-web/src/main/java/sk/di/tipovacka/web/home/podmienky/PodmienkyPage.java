package sk.di.tipovacka.web.home.podmienky;

import org.apache.wicket.Page;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.CheckBox;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.SubmitLink;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.enums.SutazTypEnum;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.SutazRocnikSuhlasService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.commons.panel.NadpisPanel;
import sk.di.tipovacka.web.home.vyber.VyberPage;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * Podmienky ucasti na tipovacke.
 *
 * User: Dano
 * Date: 16.2.2014
 */
public class PodmienkyPage extends Template {

    private static final long serialVersionUID = -5783097508428544155L;

    @SpringBean
    private SutazRocnikService sutazRocnikService;
    @SpringBean
    private SutazRocnikSuhlasService sutazRocnikSuhlasService;

    private List<RocnikSutazeDto> vybraneSutaze = new ArrayList<>();

    public PodmienkyPage(final PageParameters parameters) {
        super(parameters);

        // nadpis - panel
        add(new NadpisPanel("nadpisPanel", getString("nadpis.home.podmienky"), vybranyRocnik));

        // dohladame si aktualne aktivne rocniku sutaze
        vybraneSutaze = sutazRocnikService.getAktualnyRocnikSutaze();

        // vybrane sutaze - model
        final CompoundPropertyModel<List<RocnikSutazeDto>> model =
                new CompoundPropertyModel<List<RocnikSutazeDto>>(vybraneSutaze);

        // tip na vitaza - form, zobrazuje sa, ked sa da tipovat
        final Form<List<RocnikSutazeDto>> vybraneSutazeForm = new Form<List<RocnikSutazeDto>>("vybraneSutazeForm", model) {

            private static final long serialVersionUID = -3701389808788748066L;
        };
        add(vybraneSutazeForm);

        ListView list = new ListView<RocnikSutazeDto>("aktivneSutaze", model) {

            private static final long serialVersionUID = -4016437302762376371L;

            @Override
            protected void populateItem(ListItem<RocnikSutazeDto> item) {
                RocnikSutazeDto rocnik = item.getModelObject();

                // priznak, ci je aktualne prihlaseny pouzivatel prihlaseny k sutazi
                boolean isPrihlaseny = pouzivatel.getAktivneSutazeId().contains(rocnik.getId());

                // nasetujeme labels
                String sportTyp = rocnik.getSutaz().getSportTyp().name().toLowerCase();
                String sutazTyp = rocnik.getSutaz().getSutazTyp().getSkratka().toLowerCase();
                int rok = rocnik.getRok();
                String rocnikLogo = "/img/logo_" + sportTyp
                        + "_" + sutazTyp
                        + (SutazTypEnum.LIGA_MAJSTROV.equals(rocnik.getSutaz().getSutazTyp()) ? "" : "_" + rok)
                        + ".jpg";
                item.add(new NonCachingImage("rocnikLogo", new ContextRelativeResource(rocnikLogo)));
                item.add(new Label("rocnikNadpis", rocnik.getSutaz().getNazov()));
                item.add(new Label("rocnikRok", String.valueOf(rocnik.getRok())));
                item.add(new Label("typSport", "(" + getString("sport.typ.nadpis." + rocnik.getSutaz().getSportTyp()) + ")" ));
                // pridame container pre checkbox
                WebMarkupContainer prihlasitSaDiv = new WebMarkupContainer("prihlasitSaDiv");
                prihlasitSaDiv.setVisible(!isPrihlaseny);
                item.add(prihlasitSaDiv);
                prihlasitSaDiv.add(new CheckBox("vybranyRocnik", new PropertyModel<Boolean>(rocnik, "selected")));
                // a container pre zobrazenie, ze sme prihlaseny
                WebMarkupContainer prihlasenyDiv = new WebMarkupContainer("prihlasenyDiv");
                prihlasenyDiv.setVisible(isPrihlaseny);
                item.add(prihlasenyDiv);
            }
        };
        list.setReuseItems(true);
        vybraneSutazeForm.add(list);

        // suhlasim - link
        vybraneSutazeForm.add(new SubmitLink("suhlasimLink") {
            private static final long serialVersionUID = 7064868737814787488L;

            @Override
            public void onSubmit() {

                Page page;

                try {
                    // skontrolujeme, ci bola vybrana aspon jedna aktivna sutaz
                    boolean isVybranaSutaz = false;
                    for (RocnikSutazeDto sutaz : vybraneSutaze) {
                        if (sutaz.getSelected()) {
                            // nastavime kontrolny priznak
                            isVybranaSutaz = true;
                            // pridame suhlas
                            sutazRocnikSuhlasService.addSuhlas(pouzivatel.getLogin(),
                                    pouzivatel.getId(),
                                    sutaz.getId());
                            // pridame medzi aktivne sutaze klienta
                            pouzivatel.getAktivneSutazeId().add(sutaz.getId());
                        }
                    }
                    // ak nebola ziadna sutaz vybrana, chyba
                    if (!isVybranaSutaz) {
                        page = new PodmienkyPage(parameters);
                        page.error(getString("suhlas.error.sutaz.prazdna"));
                    } else {
                        page = new VyberPage(parameters);
                        page.info(getString("suhas.info.spracovany"));
                    }
                } catch (Exception e) {
                    page = new PodmienkyPage(parameters);
                    page.error(getString("suhlas.error.spracovat"));
                }
                setResponsePage(page);
            }
        });
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.HOME;
    }
}
