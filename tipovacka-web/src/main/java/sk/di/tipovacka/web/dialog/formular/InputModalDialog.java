package sk.di.tipovacka.web.dialog.formular;

import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.ajax.markup.html.form.AjaxSubmitLink;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;

/**
 * Modalne okno pre zadanie inputu
 *
 * User: Dano
 * Date: 30.5.2013
 */
public abstract class InputModalDialog extends Panel {

    private static final long serialVersionUID = -5216165138564673279L;

    private String input = "";
    private Label nadpisLabel;
    private Label otazkaLabel;
    private RequiredTextField inputTextField;

    public InputModalDialog(String id) {
        super(id);

        // nadpis - label
        add(nadpisLabel = new Label("nadpis", "Nadpis"));

        // vlozenie inputu - form
        Form inputForm = new Form("inputForm") {

            private static final long serialVersionUID = -8642912362851643624L;

            @Override
            protected void onSubmit() {
                // pri odoslani nerobime nic
            }
        };
        inputForm.setModel(new CompoundPropertyModel(this));
        add(inputForm);

        // input - label
        inputForm.add(otazkaLabel = new Label("otazka", "Vloz hodnotu"));

        // input - text field
        inputTextField = new RequiredTextField<String>("input");
        inputForm.add(inputTextField);

        // tlacitko - ano
        add(new AjaxLink("spracovatTlacitko") {

            private static final long serialVersionUID = 7633267815240504255L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                System.out.println("input=" + getInput());
                vykonaj(target);
            }
        });
    }

    public abstract void vykonaj(AjaxRequestTarget target);

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public void zmenOtazku(String otazka) {
        otazkaLabel.setDefaultModelObject(otazka);
    }

    public void zmenNadpis(String nadpis) {
        nadpisLabel.setDefaultModelObject(nadpis);
    }
}
