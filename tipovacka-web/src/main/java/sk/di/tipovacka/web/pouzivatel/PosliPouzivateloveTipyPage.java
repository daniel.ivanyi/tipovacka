package sk.di.tipovacka.web.pouzivatel;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.service.PouzivatelService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.admin.HomeAdminPage;
import sk.di.tipovacka.web.menu.MenuItemEnum;

/**
 * Odosle pouzivatelove tipy na zapas na jeho mail
 * <p/>
 * User: IVAN1497
 * Date: 12.2.2014
 */
public class PosliPouzivateloveTipyPage extends Template {
    private static final long serialVersionUID = -1367366504282212513L;

    @SpringBean
    private PouzivatelService pouzivatelService;

    public PosliPouzivateloveTipyPage(PageParameters parameters) {
        super(parameters);

        boolean odoslane = pouzivatelService.odoslaniePouzivatelovychTipov(pouzivatel.getId());
        if (odoslane) {
            info(new StringResourceModel("odoslanie.tipov.info",
                    this,
                    null,
                    new Object[]{pouzivatel.getMail()}).getString());
        } else {
            error(new StringResourceModel("odoslanie.tipov.error",
                    this,
                    null,
                    new Object[]{pouzivatel.getMail()}).getString());
        }
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.POUZIVATEL;
    }
}
