package sk.di.tipovacka.web.admin.pouzivatel;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.enums.RolaEnum;
import sk.di.tipovacka.api.service.PouzivatelService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.dialog.DialogAkciaEnum;
import sk.di.tipovacka.web.dialog.potvrdenie.PotvrdenieModalDialog;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Administracia pouzivatela.
 * <p>
 * User: Dano
 * Date: 19.5.2013
 */
public class AdminPouzivatelPage extends Template {

    private static final long serialVersionUID = -4730077034643540313L;

    @SpringBean
    private PouzivatelService pouzivatelService;

    private PouzivatelDto user;
    private ArrayList<String> vybraneRole = new ArrayList<>();

    public AdminPouzivatelPage(PageParameters parameters) {
        super(parameters);

        // na zaciatku si nastavime inicializacne udaje pre pouzivatela
        user = new PouzivatelDto();

        // v parametroch moze prist pouzivatelId
        String pouzivatelId = parameters.get("pouzivatelId").toString();
        if (StringUtils.isNotEmpty(pouzivatelId)) {
            user = pouzivatelService.getPouzivatel(Long.parseLong(pouzivatelId));
            for (RolaEnum r : user.getRoly()) {
                vybraneRole.add(r.name());
            }
        }

        // div pre formular na managerovanie pouzivatela
        final WebMarkupContainer userDiv = new WebMarkupContainer("userDiv");
        userDiv.setOutputMarkupPlaceholderTag(true);
        userDiv.setOutputMarkupId(true);
        add(userDiv);

        // model
        final CompoundPropertyModel<PouzivatelDto> model = new CompoundPropertyModel<PouzivatelDto>(user);
        // form
        final Form<PouzivatelDto> userForm = new Form<PouzivatelDto>("userForm", model) {

            private static final long serialVersionUID = -4817559265396068758L;

            @Override
            protected void onSubmit() {
                super.onSubmit();

                // vysledna stranka
                AdminPouzivatelPage page;
                // spracovanie pouzivatela
                try {
                    if (user.getId() == null) {
                        user.getRoly().clear();
                        for (String rola : vybraneRole) {
                            user.getRoly().add(RolaEnum.valueOf(rola));
                        }
                        Long pouzivatelId = pouzivatelService.addPouzivatel(pouzivatel.getLogin(), user);

                        page = new AdminPouzivatelPage(new PageParameters());
                        page.info(new StringResourceModel("pouzivatel.info.pridany",
                                this,
                                null,
                                new Object[]{user.getLogin()}).getString());
                    } else {
                        user.getRoly().clear();
                        for (String rola : vybraneRole) {
                            user.getRoly().add(RolaEnum.valueOf(rola));
                        }
                        pouzivatelService.editPouzivatel(pouzivatel.getLogin(), user);

                        page = new AdminPouzivatelPage(new PageParameters());
                        page.info(new StringResourceModel("pouzivatel.info.zmeneny",
                                this,
                                null,
                                new Object[]{user.getLogin()}).getString());
                    }
                } catch (Exception e) {
                    page = new AdminPouzivatelPage(new PageParameters());
                    page.error(new StringResourceModel("pouzivatel.error.spracovat",
                            this,
                            null,
                            new Object[]{user.getLogin()}).getString());
                }

                setResponsePage(page);
            }
        };
        userDiv.add(userForm);

        // login - input text
        userForm.add(new RequiredTextField<String>("login"));

        // mail - input text
        userForm.add(new RequiredTextField<String>("mail"));

        // meno - input text
        userForm.add(new RequiredTextField<String>("meno"));

        // priezvisko - input text
        userForm.add(new RequiredTextField<String>("priezvisko"));

        // rola - checkbox
        final List<String> dostupneRole = new ArrayList<>();
        for (RolaEnum r : RolaEnum.values()) {
            dostupneRole.add(r.name());
        }
        final CheckBoxMultipleChoice<String> zoznamRoly =
                new CheckBoxMultipleChoice<String>("zoznamRoly",
                        new Model(vybraneRole),
                        dostupneRole);
        userForm.add(zoznamRoly);

        // nadefinovani pouzivatelia - container
        final WebMarkupContainer useriDiv = new WebMarkupContainer("useriDiv");
        useriDiv.setOutputMarkupId(true);
        useriDiv.setOutputMarkupPlaceholderTag(true);
        add(useriDiv);

        // pridanie noveho - container onclick
        WebMarkupContainer pridajLinkDiv = new WebMarkupContainer("pridajLink");
        pridajLinkDiv.setOutputMarkupId(true);
        pridajLinkDiv.add(new AjaxEventBehavior("onclick") {
            private static final long serialVersionUID = 4290162270843398505L;

            @Override
            @SuppressWarnings("unchecked")
            protected void onEvent(AjaxRequestTarget target) {
                // vyresetujeme formular pre spravu pouzivatela
                user = new PouzivatelDto();
                model.setObject(user);
                // vyresetujeme rolu
                vybraneRole.clear();
                zoznamRoly.setModelObject(vybraneRole);
                // prekreslime
                target.add(userDiv);
            }
        });
        useriDiv.add(pridajLinkDiv);
        // nadefinovani pouzivatelia - model
        List<PouzivatelDto> users = pouzivatelService.getPouzivatelia();
        final CompoundPropertyModel<List<PouzivatelDto>> usersModel =
                new CompoundPropertyModel<List<PouzivatelDto>>(users);

        // potvrdenie vymazania - container
        final WebMarkupContainer potvrdenieModalDialogDiv = new WebMarkupContainer("potvrdenieModalDialogDiv");
        potvrdenieModalDialogDiv.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        add(potvrdenieModalDialogDiv);
        // potvrdenie vymazania - modalne okno
        final PotvrdenieModalDialog potvrdenieModalDialog = new PotvrdenieModalDialog("potvrdenieModalDialog") {

            private static final long serialVersionUID = -8940685024364201443L;

            @Override
            public void vykonaj(AjaxRequestTarget target) {
                PouzivatelDto p = pouzivatelService.getPouzivatel(getEntityId());
                try {
                    pouzivatelService.removePouzivatel(pouzivatel.getLogin(), getEntityId());
                    setResponsePage(new AdminPouzivatelPage(new PageParameters()));
                } catch (Exception e) {
                    error(new StringResourceModel("pouzivatel.error.odstranit",
                            this,
                            null,
                            new Object[]{p.getLogin()}).getString());
                }
            }
        };
        potvrdenieModalDialogDiv.add(potvrdenieModalDialog);

        // nadefinovani pouzivatelia - tabulka
        ListView<PouzivatelDto> usersView = new ListView<PouzivatelDto>("userRiadok", usersModel) {

            private static final long serialVersionUID = 149566657396950575L;

            @Override
            protected void populateItem(ListItem<PouzivatelDto> item) {
                final PouzivatelDto p = item.getModelObject();
                item.add(new WebMarkupContainer("iconStatus")
                        .add(new AttributeAppender("class",
                                p.isAktivny()
                                        ? "fa fa-fw fa-check"
                                        : "fa fa-fw fa-trash")));
                item.add(new Label("login", p.getLogin()));
                item.add(new Label("mail", p.getMail()));
                item.add(new Label("menoPriezvisko", p.getMenoPriezvisko()));
                StringBuilder sb = new StringBuilder();
                for (RolaEnum rola : p.getRoly()) {
                    if (sb.length() > 0) {
                        sb.append(" ");
                    }
                    sb.append(rola.getNazov());
                }
                item.add(new Label("rola", sb.toString()));

                WebMarkupContainer akcieDiv = new WebMarkupContainer("akcieDiv");
                item.add(akcieDiv.setVisible(p.isAktivny()));

                akcieDiv.add(new Link<String>("zmenitLink") {

                    private static final long serialVersionUID = 1041104924896631200L;

                    @Override
                    public void onClick() {
                        user = pouzivatelService.getPouzivatel(p.getId());
                        setResponsePage(new AdminPouzivatelPage(new PageParameters().add("pouzivatelId", p.getId())));
                    }
                });

                akcieDiv.add(new AjaxLink("odstranitLink") {

                    private static final long serialVersionUID = -7884677629367893428L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        // nasetujeme udaje pre potvrdenie
                        potvrdenieModalDialog.setEntityId(p.getId());
                        potvrdenieModalDialog.setAkcia(DialogAkciaEnum.VYMAZANIE);
                        potvrdenieModalDialog.zmenOtazku(new StringResourceModel("pouzivatel.vymazanie.potvrdenie",
                                this,
                                null,
                                new Object[]{p.getLogin()}).getString());
                        // a zobrazime ho
                        target.appendJavaScript("$('#potvrdenieModal').modal({show:true});");
                    }
                });

                item.add(new AjaxLink<String>("aktivovatLink") {

                    private static final long serialVersionUID = -4940130834986577346L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        try {
                            p.setStatus(EntityStatusEnum.AKTIVNY);
                            pouzivatelService.editPouzivatel(pouzivatel.getLogin(), p);
                            setResponsePage(new AdminPouzivatelPage(new PageParameters()));
                        } catch (Exception e) {
                            error(new StringResourceModel("pouzivatel.error.aktivovat",
                                    this,
                                    null,
                                    new Object[]{p.getLogin()}).getString());
                        }
                    }
                }.setVisible(!p.isAktivny()));
            }
        };
        useriDiv.add(usersView);
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ADMIN;
    }

    public PouzivatelDto getUser() {
        return user;
    }

    public void setUser(PouzivatelDto user) {
        this.user = user;
    }
}
