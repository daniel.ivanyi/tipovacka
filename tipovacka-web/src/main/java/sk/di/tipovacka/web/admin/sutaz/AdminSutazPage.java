package sk.di.tipovacka.web.admin.sutaz;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.SutazDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.enums.SportTypEnum;
import sk.di.tipovacka.api.enums.SutazTypEnum;
import sk.di.tipovacka.api.service.SutazService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.admin.rocnik.AdminRocnikPage;
import sk.di.tipovacka.web.dialog.potvrdenie.PotvrdenieModalDialog;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.util.Arrays;
import java.util.List;

/**
 * Administracia sutaze
 * <p/>
 * User: Dano
 * Date: 19.5.2013
 */
public class AdminSutazPage extends Template {

    private static final long serialVersionUID = 8964413343380932359L;

    @SpringBean
    private SutazService sutazService;

    private SutazDto sutaz = new SutazDto();

    public AdminSutazPage(PageParameters parameters) {
        super(parameters);

        String sutazId = parameters.get("sutazId").toString();
        if (StringUtils.isNotEmpty(sutazId)) {
            sutaz = sutazService.getSutaz(Long.parseLong(sutazId));
        }

        // div pre formular na managerovanie sutaze
        final WebMarkupContainer sutazDiv = new WebMarkupContainer("sutazDiv");
        sutazDiv.setOutputMarkupId(true);
        sutazDiv.setOutputMarkupPlaceholderTag(true);
        add(sutazDiv);

        // model
        final CompoundPropertyModel<SutazDto> model = new CompoundPropertyModel<SutazDto>(sutaz);
        // form
        final Form<SutazDto> sutazForm = new Form<SutazDto>("sutazForm", model) {
            private static final long serialVersionUID = 3677407767651964290L;


            @Override
            protected void onSubmit() {
                super.onSubmit();

                // vysledna stranka
                AdminSutazPage page;
                // spracovanie sutaze
                try {
                    if (sutaz.getId() == null) {
                        Long sutazId = sutazService.addSutaz(pouzivatel.getLogin(), sutaz);

                        page = new AdminSutazPage(new PageParameters());
                        page.info(new StringResourceModel("sutaz.info.pridana",
                                this,
                                null,
                                new Object[]{sutaz.getNazov()}).getString());
                    } else {
                        sutazService.editSutaz(pouzivatel.getLogin(), sutaz);

                        page = new AdminSutazPage(new PageParameters());
                        page.info(new StringResourceModel("sutaz.info.zmenena",
                                this,
                                null,
                                new Object[]{sutaz.getNazov()}).getString());
                    }
                } catch (Exception e) {
                    page = new AdminSutazPage(new PageParameters());
                    page.error(new StringResourceModel("sutaz.error.spracovat",
                            this,
                            null,
                            new Object[]{sutaz.getNazov()}).getString());
                }

                setResponsePage(page);
            }
        };
        sutazDiv.add(sutazForm);

        // nazov sutaze - input text
        sutazForm.add(new RequiredTextField<String>("nazov"));

        // typ sporty - combo
        sutazForm.add(new DropDownChoice<SportTypEnum>("sportTyp",
                Arrays.asList(SportTypEnum.values()),
                new ChoiceRenderer<SportTypEnum>() {
                    private static final long serialVersionUID = -4060436749795002689L;

                    @Override
                    public Object getDisplayValue(SportTypEnum object) {
                        if (object == null) {
                            return "";
                        }

                        return getString("sport.typ." + object.name());
                    }
                })
                .setNullValid(false)
                .setRequired(true));

        // typ sporty - combo
        sutazForm.add(new DropDownChoice<SutazTypEnum>("sutazTyp",
                Arrays.asList(SutazTypEnum.values()),
                new ChoiceRenderer<SutazTypEnum>() {

                    private static final long serialVersionUID = 3794623392287415517L;

                    @Override
                    public Object getDisplayValue(SutazTypEnum object) {
                        if (object == null) {
                            return "";
                        }

                        return getString("sutaz.typ." + object.name());
                    }
                })
                .setNullValid(false)
                .setRequired(true));

        // nadefinovane sutaze - container
        final WebMarkupContainer sutazeDiv = new WebMarkupContainer("sutazeDiv");
        sutazeDiv.setOutputMarkupId(true);
        sutazDiv.setOutputMarkupPlaceholderTag(true);

        // pridanie noveho - container onclick
        WebMarkupContainer pridajLinkDiv = new WebMarkupContainer("pridajLink");
        pridajLinkDiv.setOutputMarkupId(true);
        pridajLinkDiv.add(new AjaxEventBehavior("onclick") {
            private static final long serialVersionUID = 5450459529491277294L;

            @Override
            @SuppressWarnings("unchecked")
            protected void onEvent(AjaxRequestTarget target) {
                // vyresetujeme formular pre spravu
                sutaz = new SutazDto();
                model.setObject(sutaz);
                // prekreslime
                target.add(sutazDiv);
            }
        });
        sutazeDiv.add(pridajLinkDiv);

        // nadefinovane sutaze - model
        List<SutazDto> sutaze = sutazService.getSutaze();
        final CompoundPropertyModel<List<SutazDto>> sutazeModel =
                new CompoundPropertyModel<List<SutazDto>>(sutaze);

        // potvrdenie vymazania - container
        final WebMarkupContainer potvrdenieModalDialogDiv = new WebMarkupContainer("potvrdenieModalDialogDiv");
        potvrdenieModalDialogDiv.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        add(potvrdenieModalDialogDiv);
        // potvrdenie vymazania - modalne okno
        final PotvrdenieModalDialog potvrdenieModalDialog = new PotvrdenieModalDialog("potvrdenieModalDialog") {
            private static final long serialVersionUID = -7136907043878633261L;

            @Override
            public void vykonaj(AjaxRequestTarget target) {
                try {
                    sutazService.removeSutaz(pouzivatel.getLogin(), getEntityId());
                    setResponsePage(new AdminSutazPage(new PageParameters()));
                } catch (Exception e) {
                    error(new StringResourceModel("sutaz.error.odstranit",
                            this,
                            null,
                            new Object[]{sutaz.getNazov()}).getString());
                }
            }
        };
        potvrdenieModalDialogDiv.add(potvrdenieModalDialog);

        // nadefinovane sutaze - tabulka
        ListView<SutazDto> sutazeView = new ListView<SutazDto>("sutazRiadok", sutazeModel) {
            private static final long serialVersionUID = -8302539653454245265L;

            @Override
            protected void populateItem(ListItem<SutazDto> item) {
                final SutazDto s = item.getModelObject();
                item.add(new WebMarkupContainer("iconStatus")
                        .add(new AttributeAppender("class",
                                s.isAktivny()
                                        ? "fa fa-fw fa-check"
                                        : "fa fa-fw fa-trash")));
                item.add(new Label("nazov", s.getNazov()));
                item.add(new Label("sportType", getString("sport.typ." + s.getSportTyp().name())));
                WebMarkupContainer akcieDiv = new WebMarkupContainer("akcieDiv");
                item.add(akcieDiv.setVisible(s.isAktivny()));
                akcieDiv.add(new Link<String>("pridajRocnikLink") {
                    private static final long serialVersionUID = 7144235529223186589L;

                    @Override
                    public void onClick() {
                        PageParameters params = new PageParameters();
                        params.add("sutazId", s.getId());

                        setResponsePage(new AdminRocnikPage(params));
                    }
                });
                akcieDiv.add(new Link<String>("zmenitLink") {
                    private static final long serialVersionUID = 1193675380366015955L;

                    @Override
                    public void onClick() {
                        setResponsePage(new AdminSutazPage(new PageParameters().add("sutazId", s.getId())));
                    }
                });
                akcieDiv.add(new AjaxLink("odstranitLink") {
                    private static final long serialVersionUID = 7676814128743989693L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        // nasetujeme udaje pre potvrdenie
                        potvrdenieModalDialog.setEntityId(s.getId());
                        potvrdenieModalDialog.zmenOtazku(new StringResourceModel("sutaz.potvrdenie.vymazanie",
                                this,
                                null,
                                new Object[]{s.getNazov()}).getString());
                        // refreshneme dialog
                        target.add(potvrdenieModalDialogDiv);
                        // a zobrazime ho
                        target.appendJavaScript("$('#potvrdenieModal').modal({show:true});");
                    }
                });
                item.add(new Link<String>("aktivovatLink") {

                    private static final long serialVersionUID = 6819239457324173693L;

                    @Override
                    public void onClick() {
                        try {
                            s.setStatus(EntityStatusEnum.AKTIVNY);
                            sutazService.editSutaz(pouzivatel.getLogin(), s);
                            setResponsePage(new AdminSutazPage(new PageParameters()));
                        } catch (Exception e) {
                            error(new StringResourceModel("sutaz.error.aktivovat",
                                    this,
                                    null,
                                    new Object[]{sutaz.getNazov()}).getString());
                        }
                    }
                }.setVisible(!s.isAktivny()));
            }
        };
        sutazeDiv.add(sutazeView);
        add(sutazeDiv);
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ADMIN;
    }
}
