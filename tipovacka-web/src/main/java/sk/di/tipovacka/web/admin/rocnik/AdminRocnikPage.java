package sk.di.tipovacka.web.admin.rocnik;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.Page;
import org.apache.wicket.ajax.AjaxEventBehavior;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.*;
import org.apache.wicket.markup.html.link.ExternalLink;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.MuzstvoDto;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.dto.SutazDto;
import sk.di.tipovacka.api.enums.EntityStatusEnum;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.api.service.SutazService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.commons.panel.vlajka.VlajkaKodPanel;
import sk.di.tipovacka.web.dialog.DialogAkciaEnum;
import sk.di.tipovacka.web.dialog.potvrdenie.PotvrdenieModalDialog;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.io.IOException;
import java.util.Calendar;
import java.util.List;

/**
 * Administracia rocnika rocniky
 *
 * User: Dano
 * Date: 19.5.2013
 */
public class AdminRocnikPage extends Template {

    private static final long serialVersionUID = 9036645035998400219L;

    @SpringBean
    private SutazService sutazService;
    @SpringBean
    private SutazRocnikService rocnikSutazeService;

    private RocnikSutazeDto rocnik;

    public AdminRocnikPage(PageParameters parameters) {
        super(parameters);

        // na zaciatku si nastavime inicializacne udaje pre rocnik sutaze
        rocnik = new RocnikSutazeDto();
        rocnik.setRok(Calendar.getInstance().get(Calendar.YEAR));

        // v parametroch moze prist sutazId a rocnikSutazeId, ak ich mame, nastavime do rocnika
        String rocnikSutazeId = parameters.get("rocnikSutazeId").toString();
        if (StringUtils.isNotEmpty(rocnikSutazeId)) {
            rocnik = rocnikSutazeService.getRocnikSutaze(Long.parseLong(rocnikSutazeId));
        }
        String sutazId = parameters.get("sutazId").toString();
        if (StringUtils.isNotEmpty(sutazId)) {
            SutazDto sutaz = sutazService.getSutaz(Long.parseLong(sutazId));
            rocnik.setSutaz(sutaz);
            rocnik.setSutazId(sutaz.getId());
        }

        // div pre formular na managerovanie rocnika
        final WebMarkupContainer rocnikDiv = new WebMarkupContainer("rocnikDiv");
        rocnikDiv.setOutputMarkupId(true);
        rocnikDiv.setOutputMarkupPlaceholderTag(true);
        add(rocnikDiv);

        // model
        final CompoundPropertyModel<RocnikSutazeDto> model = new CompoundPropertyModel<RocnikSutazeDto>(rocnik);
        // form
        final Form<RocnikSutazeDto> rocnikForm = new Form<RocnikSutazeDto>("rocnikForm", model) {
            private static final long serialVersionUID = -9009552985341799607L;

            @Override
            protected void onSubmit() {
                super.onSubmit();

                // vysledna stranka
                AdminRocnikPage page;
                // spracovanie rocniky
                try {
                    if (rocnik.getId() == null) {
                        Long rocnikId = rocnikSutazeService.addSutazRocnik(pouzivatel.getLogin(), rocnik);

                        page = new AdminRocnikPage(new PageParameters());
                        page.info(new StringResourceModel("sutaz.rocnik.info.pridany",
                                this,
                                null,
                                new Object[]{rocnik.getSutaz().getNazov(), rocnik.getRok()}).getString());
                    } else {
                        rocnikSutazeService.editSutazRocnik(pouzivatel.getLogin(), rocnik);

                        page = new AdminRocnikPage(new PageParameters());
                        page.info(new StringResourceModel("sutaz.rocnik.info.zmeneny",
                                this,
                                null,
                                new Object[]{rocnik.getSutaz().getNazov(), rocnik.getRok()}).getString());
                    }
                } catch (Exception e) {
                    page = new AdminRocnikPage(new PageParameters());
                    page.error(new StringResourceModel("sutaz.rocnik.error.spracovat",
                            this,
                            null,
                            new Object[]{rocnik.getSutaz().getNazov(), rocnik.getRok()}).getString());
                }

                setResponsePage(page);
            }
        };
        rocnikDiv.add(rocnikForm);

        // sutaz - combo
        rocnikForm.add(new DropDownChoice<SutazDto>("sutaz",
                new PropertyModel<SutazDto>(model, "sutaz"),
                sutazService.getSutaze(),
                new ChoiceRenderer<SutazDto>() {
                    private static final long serialVersionUID = -1249985401495386135L;

                    @Override
                    public Object getDisplayValue(SutazDto object) {
                        if (object == null) {
                            return "";
                        }

                        return object.getNazov() + " (" + getString("sport.typ." + object.getSportTyp()) + ")";
                    }
                }
        ) {
            private static final long serialVersionUID = -7013255524967892392L;

            @Override
            protected boolean isSelected(SutazDto object, int index, String selected) {
                try {
                    return model.getObject().getSutaz().getId().equals(object.getId());
                } catch(Exception e) {
                    return super.isSelected(object, index, selected);
                }
            }
        }.setNullValid(false).setRequired(true));

        // rok - input text
        rocnikForm.add(new RequiredTextField<Integer>("rok"));

        // vyberieme z muzstiev iba tie, ktore realne sutazia v rocniku
        List<MuzstvoDto> muzstvaRocnika = rocnik.getMuzstva();

        // vitaz - combo
        final DropDownChoice<MuzstvoDto> vitazRocnikaCombo = new DropDownChoice<MuzstvoDto>("vitazRocnika",
                new PropertyModel<MuzstvoDto>(model, "vitaz"),
                muzstvaRocnika,
                new ChoiceRenderer<MuzstvoDto>() {
                    private static final long serialVersionUID = 7219457044969410138L;

                    @Override
                    public Object getDisplayValue(MuzstvoDto object) {
                        if (object == null) {
                            return "";
                        }

                        return object.getNazov() + " (" + object.getKod() + ")";
                    }
                }) {

            private static final long serialVersionUID = -2316867111271845879L;

            @Override
            protected boolean isSelected(MuzstvoDto object, int index, String selected) {
                try {
                    return model.getObject().getVitaz().getKod().equals(object.getKod());
                } catch (Exception e) {
                    return super.isSelected(object, index, selected);
                }
            }

            @Override
            protected boolean wantOnSelectionChangedNotifications() {
                return true;
            }
        };
        rocnikForm.add(vitazRocnikaCombo);

        // statistika priebehu rocnika - input text
        rocnikForm.add(new TextField<String>("statistikaUrl"));

        // aktualnost - input checkbox
        rocnikForm.add(new CheckBox("aktualny"));

        // nadefinovane rocniky - container
        final WebMarkupContainer rocnikyDiv = new WebMarkupContainer("rocnikyDiv");
        rocnikyDiv.setOutputMarkupId(true);
        rocnikyDiv.setOutputMarkupPlaceholderTag(true);
        add(rocnikyDiv);

        // pridanie noveho - container onclick
        WebMarkupContainer pridajLinkDiv = new WebMarkupContainer("pridajLink");
        pridajLinkDiv.setOutputMarkupId(true);
        pridajLinkDiv.add(new AjaxEventBehavior("onclick") {
            private static final long serialVersionUID = 1915772159460992595L;

            @Override
            @SuppressWarnings("unchecked")
            protected void onEvent(AjaxRequestTarget target) {
                // vyresetujeme formular pre spravu
                rocnik = new RocnikSutazeDto();
                model.setObject(rocnik);
                // prekreslime
                target.add(rocnikDiv);
            }
        });
        rocnikyDiv.add(pridajLinkDiv);

        // nadefinovane rocniky - model
        List<RocnikSutazeDto> rocniky = rocnikSutazeService.getRocnikySutaze();
        final CompoundPropertyModel<List<RocnikSutazeDto>> rocnikyModel =
                new CompoundPropertyModel<List<RocnikSutazeDto>>(rocniky);

        // potvrdenie vymazania - container
        final WebMarkupContainer potvrdenieModalDialogDiv = new WebMarkupContainer("potvrdenieModalDialogDiv");
        potvrdenieModalDialogDiv.setOutputMarkupId(true).setOutputMarkupPlaceholderTag(true);
        add(potvrdenieModalDialogDiv);
        // potvrdenie vymazania - modalne okno
        final PotvrdenieModalDialog potvrdenieModalDialog = new PotvrdenieModalDialog("potvrdenieModalDialog") {
            private static final long serialVersionUID = -7136907043878633261L;

            @Override
            public void vykonaj(AjaxRequestTarget target) {

                RocnikSutazeDto r = rocnikSutazeService.getRocnikSutaze(getEntityId());

                try {
                    switch (getAkcia()) {
                        case INICIALIZACIA_ZAPASOV:
                            // inicializujeme
                            try {
                                rocnikSutazeService.inicializujRocnikSutaze(pouzivatel.getLogin(), getEntityId());
                                info(new StringResourceModel("sutaz.rocnik.info.inicializovany",
                                        this,
                                        null,
                                        new Object[]{
                                                r.getSutaz().getNazov(),
                                                r.getRok()
                                        }).getString());
                            } catch (Exception e) {
                                error(new StringResourceModel("sutaz.rocnik.error.inicializovat",
                                        this,
                                        null,
                                        new Object[]{
                                                r.getSutaz().getNazov(),
                                                r.getRok()
                                        }).getString());
                            }
                            setResponsePage(new AdminRocnikPage(new PageParameters()));
                            break;
                        case VYMAZANIE:
                            rocnikSutazeService.removeRocnik(pouzivatel.getLogin(), getEntityId());
                            setResponsePage(new AdminRocnikPage(new PageParameters()));
                            break;
                    }
                } catch (Exception e) {
                    error(new StringResourceModel("sutaz.rocnik.error.odstranit",
                            this,
                            null,
                            new Object[]{
                                    r.getSutaz().getNazov(),
                                    r.getRok()
                            }).getString());
                }
            }
        };
        potvrdenieModalDialogDiv.add(potvrdenieModalDialog);

        // nadefinovane rocniky - tabulka
        ListView<RocnikSutazeDto> rocnikyView = new ListView<RocnikSutazeDto>("rocnikRiadok", rocnikyModel) {
            private static final long serialVersionUID = 4369832969326855991L;

            @Override
            protected void populateItem(ListItem<RocnikSutazeDto> item) {
                final RocnikSutazeDto r = item.getModelObject();
                item.add(new WebMarkupContainer("iconStatus")
                        .add(new AttributeAppender("class",
                                r.isAktivny()
                                        ? "fa fa-fw fa-check"
                                        : "fa fa-fw fa-trash")));
                item.add(new Label("sutaz.nazov", r.getSutaz().getNazov()
                        + " (" + getString("sport.typ." + r.getSutaz().getSportTyp()) + ")"));
                item.add(new WebMarkupContainer("iconAktualna")
                        .add(new AttributeAppender("class", r.isAktualny() ? "fa fa-fw fa-star-o" : "")));
                item.add(new Label("rok", String.valueOf(r.getRok())));
                item.add(new VlajkaKodPanel("vitazVlajka", r.getVitaz()));
                item.add(new ExternalLink("statistikaUrl",
                        StringUtils.isNotEmpty(r.getStatistikaUrl()) ? r.getStatistikaUrl() : "",
                        StringUtils.isNotEmpty(r.getStatistikaUrl()) ? "štatistiky" : ""));
                WebMarkupContainer akcieDiv = new WebMarkupContainer("akcieDiv");
                item.add(akcieDiv.setVisible(r.isAktivny()));

                // link na inicializaciu je viditelny iba vtedy, ked rocnik sutaze este nebol
                // online inicializovany a ma nadefinovanu IIHF statistiku
                boolean mozemInicializovatRocnik = !r.isInit() && StringUtils.isNotEmpty(r.getStatistikaUrl());
                WebMarkupContainer inicializujZapasySpan = new WebMarkupContainer("inicializujZapasySpan");
                akcieDiv.add(inicializujZapasySpan.setVisible(mozemInicializovatRocnik));
                inicializujZapasySpan.add(new AjaxLink<String>("inicializujZapasyLink") {
                    private static final long serialVersionUID = -6260441806381549653L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        // nasetujeme udaje pre potvrdenie
                        potvrdenieModalDialog.setEntityId(r.getId());
                        potvrdenieModalDialog.setAkcia(DialogAkciaEnum.INICIALIZACIA_ZAPASOV);
                        potvrdenieModalDialog.zmenOtazku(new StringResourceModel("sutaz.rocnik.inicializacia.potvrdenie",
                                this,
                                null,
                                new Object[]{
                                        r.getSutaz().getNazov(),
                                        r.getRok(),
                                        r.getStatistikaUrl()}).getString());
                        target.add(potvrdenieModalDialogDiv);
                        // a zobrazime ho
                        target.appendJavaScript("$('#potvrdenieModal').modal({show:true});");
                    }
                });

                // update priebehu rocnika
                WebMarkupContainer updatePriebehRocnikaSpan = new WebMarkupContainer("updatePriebehRocnikaSpan");
                akcieDiv.add(updatePriebehRocnikaSpan.setVisible(!mozemInicializovatRocnik));
                updatePriebehRocnikaSpan.add(new Link<String>("updatePriebehRocnikaLink") {
                    private static final long serialVersionUID = 6049004607849402239L;

                    @Override
                    public void onClick() {
                        Page page = new AdminRocnikPage(new PageParameters().add("rocnikSutazeId", r.getId()));
                        try {
                            rocnikSutazeService.updatePriebehRocnikaSutaze(pouzivatel.getLogin(), r.getId());
                            page.info(new StringResourceModel("sutaz.rocnik.info.update",
                                    this,
                                    null,
                                    new Object[]{
                                            r.getSutaz().getNazov(),
                                            r.getRok()
                                    }).getString());
                        } catch (IOException e) {
                            page.error(new StringResourceModel("sutaz.rocnik.error.update",
                                    this,
                                    null,
                                    new Object[]{
                                            r.getSutaz().getNazov(),
                                            r.getRok()
                                    }).getString());
                        }
                        setResponsePage(page);
                    }
                });

                akcieDiv.add(new Link<String>("zmenitLink") {

                    private static final long serialVersionUID = -7590919290596377951L;

                    @Override
                    public void onClick() {
                        rocnik = rocnikSutazeService.getRocnikSutaze(r.getId());
                        setResponsePage(new AdminRocnikPage(new PageParameters().add("rocnikSutazeId", r.getId())));
                    }
                });

                akcieDiv.add(new AjaxLink("odstranitLink") {
                    private static final long serialVersionUID = 7676814128743989693L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        // nasetujeme udaje pre potvrdenie
                        potvrdenieModalDialog.setEntityId(r.getId());
                        potvrdenieModalDialog.setAkcia(DialogAkciaEnum.VYMAZANIE);
                        potvrdenieModalDialog.zmenOtazku(new StringResourceModel("sutaz.rocnik.vymazanie.potvrdenie",
                                this,
                                null,
                                new Object[]{r.getSutaz().getNazov(), r.getRok()}).getString());
                        target.add(potvrdenieModalDialogDiv);
                        // a zobrazime ho
                        target.appendJavaScript("$('#potvrdenieModal').modal({show:true});");
                    }
                });

                item.add(new AjaxLink<String>("aktivovatLink") {

                    private static final long serialVersionUID = 6819239457324173693L;

                    @Override
                    public void onClick(AjaxRequestTarget target) {
                        try {
                            r.setStatus(EntityStatusEnum.AKTIVNY);
                            rocnikSutazeService.editSutazRocnik(pouzivatel.getLogin(), r);
                            setResponsePage(new AdminRocnikPage(new PageParameters()));
                        } catch (Exception e) {
                            error(new StringResourceModel("sutaz.rocnik.error.aktivovat",
                                    this,
                                    null,
                                    new Object[]{r.getSutaz().getNazov(), r.getRok()}).getString());
                        }
                    }
                }.setVisible(!r.isAktivny()));
            }
        };
        rocnikyDiv.add(rocnikyView);
    }

    public RocnikSutazeDto getRocnik() {
        return rocnik;
    }

    public void setRocnik(RocnikSutazeDto rocnik) {
        this.rocnik = rocnik;
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ADMIN;
    }
}
