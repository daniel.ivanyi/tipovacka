package sk.di.tipovacka.web.commons.panel.vlajka.klub;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.Image;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import org.apache.wicket.request.resource.ContextRelativeResource;
import sk.di.tipovacka.api.dto.MuzstvoDto;
import sk.di.tipovacka.impl.util.ParseUtils;

/**
 * Panel pre zobrazenie klubu
 */
public class VlajkaKlubPanel extends Panel {

    private static final long serialVersionUID = 1789045042728668782L;

    public VlajkaKlubPanel(String id, MuzstvoDto muzstvo, boolean isSeda) {
        super(id);

        // image - logo klubu
        String logoKlubu = "/img/vlajka/" + ParseUtils.getNoInterpunktString(muzstvo.getNazov()) + ".png";
        Image image = new NonCachingImage("logo", new ContextRelativeResource(logoKlubu));
        // pridame title
        image.add(new AttributeAppender("title", muzstvo.getNazov()));
        // osetrime, ci ma byt logo sede
        if (isSeda) {
            image.add(new AttributeAppender("class", "sedy"));
        }
        // a pridame na stranku
        add(image);
    }
}
