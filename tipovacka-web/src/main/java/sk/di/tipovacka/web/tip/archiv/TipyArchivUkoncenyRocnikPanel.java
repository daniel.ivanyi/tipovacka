package sk.di.tipovacka.web.tip.archiv;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.link.Link;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.request.resource.ContextRelativeResource;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.PouzivatelovTipStatistikaDto;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.service.StatistikaService;
import sk.di.tipovacka.api.service.SutazRocnikService;

import java.util.List;

/**
 * Panel ukonceneho rocnika pre prehlad.
 *
 * User: Dano
 * Date: 9.11.2013
 */
public class TipyArchivUkoncenyRocnikPanel extends Panel {
    private static final long serialVersionUID = -6302126737354098978L;

    @SpringBean
    private SutazRocnikService sutazRocnikService;
    @SpringBean
    private StatistikaService statistikaService;

    public TipyArchivUkoncenyRocnikPanel(String id, final Long rocnikId) {
        super(id);

        RocnikSutazeDto rocnik = sutazRocnikService.getRocnikSutaze(rocnikId);

        // nazov rocnika - label
        add(new Label("nazovRocnika",
                rocnik.getSutaz().getNazov() + " - " + rocnik.getRok()));

        // logo rocnika - image
        String sportTyp = rocnik.getSutaz().getSportTyp().name().toLowerCase();
        String sutazTyp = rocnik.getSutaz().getSutazTyp().getSkratka().toLowerCase();
        int rok = rocnik.getRok();
        add(new NonCachingImage("rocnikLogo",
                new ContextRelativeResource("/img/logo_" + sportTyp + "_" + sutazTyp + "_" + rok + ".jpg")));

        // natiahneme si statistiku
        final List<PouzivatelovTipStatistikaDto> zoznam = statistikaService.getStatistikaTiperov(rocnikId);

        add(new ListView<PouzivatelovTipStatistikaDto>("riadokTabulky", zoznam) {

            private static final long serialVersionUID = 4741206151399229822L;

            @Override
            protected void populateItem(ListItem<PouzivatelovTipStatistikaDto> item) {
                String predoslaHodnota = (item.getIndex() == 0
                        ? ""
                        : zoznam.get(item.getIndex() - 1).getHodnota());

                PouzivatelovTipStatistikaDto stat = item.getModelObject();
                item.add(new Label("poradie", predoslaHodnota.equals(stat.getHodnota())
                        ? ""
                        : String.valueOf(item.getIndex() + 1)));
                item.add(new Label("login", stat.getPouzivatel().getLogin()));
                item.add(new Label("body", String.valueOf(stat.getBody())));
            }
        });

        // detail na ukonceny rocnik - link
        add(new Link<String>("detailLink") {
            private static final long serialVersionUID = -912472741104836139L;

            @Override
            public void onClick() {
                PageParameters parameters = new PageParameters();
                parameters.add("archivRocnikId", rocnikId);
                setResponsePage(TipyArchivDetailPage.class, parameters);
            }
        });
    }
}
