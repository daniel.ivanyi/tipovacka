package sk.di.tipovacka.web.tip.prehlad;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.RestartResponseException;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.joda.time.DateMidnight;
import sk.di.tipovacka.api.comparator.ZapasDatumComparator;
import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;
import sk.di.tipovacka.api.dto.*;
import sk.di.tipovacka.api.enums.OrderDirectionEnum;
import sk.di.tipovacka.api.service.*;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.commons.panel.NadpisPanel;
import sk.di.tipovacka.web.commons.panel.vlajka.VlajkaKodPanel;
import sk.di.tipovacka.web.commons.panel.vlajka.klub.VlajkaKlubPanel;
import sk.di.tipovacka.web.home.vyber.VyberPage;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Stranka znazornujuca prehlad tipov.
 * <p/>
 * User: Dano
 * Date: 9.6.2013
 */
public class TipyPrehladPage extends Template {

    private static final long serialVersionUID = -6575352458226102731L;

    private final SimpleDateFormat datumFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    private final SimpleDateFormat datumShortFormat = new SimpleDateFormat("dd.MM.yyyy");
    @SpringBean
    private TipNaVitazaService tipNaVitazaService;
    @SpringBean
    private TipNaZapasService tipNaZapasService;
    @SpringBean
    private ZapasService zapasService;
    @SpringBean
    private SutazRocnikService sutazRocnikService;
    @SpringBean
    private SutazRocnikSuhlasService rocnikSuhlasService;

    // mapa tipov pouzivatela na vitaza
    private Map<String, TipNaVitazaDto> pouzitelovTipNaVitazaMapa;
    // mapa zoznamu tipov pouzivatelov na zapas podla zapasov
    private Map<Long, Map<String, TipNaZapasDto>> pouzitelovTipNaZapasMapa;
    // zoznam zacatych a odohratych zapasov
    private List<ZapasDto> zapasy;
    // mapa zapasov podla hracieho dna
    private Map<Date, List<ZapasDto>> zapasyPodlaHraciehoDna = new HashMap<>();
    // hraci den, za ktory sa maju zobrazit tipy na zapas
    private Date vybranyHraciDen;

    public TipyPrehladPage(PageParameters parameters) {
        super(parameters);

        // ak nie je vybrany ziaden rocnik, presmerujeme na stranku vyberu sutaze
        if (vybranyRocnik == null) {
            throw new RestartResponseException(VyberPage.class, parameters.add("forward", "tipy"));
        }

        // priznak, ci sa jedna o klubovu sutaz
        final boolean isKlub = vybranyRocnik.getSutaz().isKlub();

        // nadpis - panel
        add(new NadpisPanel("nadpisPanel", getString("nadpis.tipy"), vybranyRocnik));

        // nacitame si vsetky tipy na vitaza vo forme mapy
        setPouzivatelovTipNaVitazaMap();

        // zistime si kolko uzivatelov sa zucastnilo tipovania
        final List<String> tipNaVitazaPouzivatelia = new ArrayList<>(pouzitelovTipNaVitazaMapa.keySet());

        // zosortujeme
        Collections.sort(tipNaVitazaPouzivatelia, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        });

        // zoznam, ktori nesplnili podmienky ucasti
        List<String> nesplnili = rocnikSuhlasService.findAllLoginCoNezaplatili(vybranyRocnik.getId());

        // tipy na vitaza - tabulka header bunka
        add(new ListView<String>("tipNaVitazaPouzivatel", tipNaVitazaPouzivatelia) {
            private static final long serialVersionUID = -5312348080927721741L;

            @Override
            protected void populateItem(ListItem<String> item) {
                item.add(new Label("login", item.getModelObject()));
            }
        });

        final MuzstvoDto vitazRocnika = vybranyRocnik.getVitaz();
        // tipy na vitaza - label
        add(new Label("tipNaVitazaLabel", "Mužstvo").setVisible(!tipNaVitazaPouzivatelia.isEmpty()));

        // tipy na vitaza - tabulka body bunka
        add(new ListView<String>("tipNaVitazaMuzstvo", tipNaVitazaPouzivatelia) {
            private static final long serialVersionUID = 6103732532551593558L;

            @Override
            protected void populateItem(ListItem<String> item) {
                boolean zobrazitTip = !nesplnili.contains(item.getModelObject());
                final TipNaVitazaDto tip = zobrazitTip
                        ? pouzitelovTipNaVitazaMapa.get(item.getModelObject())
                        : null;
                MuzstvoDto tipnuteMuzstvo = (tip == null ? null : tip.getMuzstvo());
                boolean spravnyTyp = false;
                if (tipnuteMuzstvo != null && vitazRocnika != null) {
                    spravnyTyp = tipnuteMuzstvo.getId().equals(vitazRocnika.getId());
                }
                String tipTitle = "";
                if (tip != null) {
                    tipTitle = String.format("%s, posledná zmena %s",
                            tip.isNahodnyTip() ? "Náhodný tip" : "Tip zadaný používateľom",
                            datumFormat.format(tip.getDatum()));
                    if (tip.isNahodnyTip()) {
                        item.add(new AttributeAppender("class", "nahodnyTip"));
                    }
                }
                Label body = new Label("body", tip == null ? "" : String.valueOf(tip.getBody()));
                item.add(new AttributeAppender("title", tipTitle));
                item.add(body);
                item.add(isKlub
                        ? new VlajkaKlubPanel("vlajka", tipnuteMuzstvo, !spravnyTyp)
                        : new VlajkaKodPanel("vlajka", tipnuteMuzstvo, !spravnyTyp));
            }
        });

        // nacitame si vsetky tipy na na zapasy vo forme mapy
        setPouzivatelovTipNaZapasMap();

        // pomocou setu dostaneme jedinecne loginy
        Set<String> pouzivatelia = new HashSet<>();
        for (Long key : pouzitelovTipNaZapasMapa.keySet()) {
            pouzivatelia.addAll(pouzitelovTipNaZapasMapa.get(key).keySet());
        }
        // zistime si kolko uzivatelov sa zucastnilo tipovania
        final List<String> tipNaZapasPouzivatelia = new ArrayList<>(pouzivatelia);
        // zosortujeme
        Collections.sort(tipNaZapasPouzivatelia, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        });

        // tip na zapas - webcontainer
        final WebMarkupContainer tipNaZapasContainer = new WebMarkupContainer("tipNaZapasContainer");
        tipNaZapasContainer.setOutputMarkupId(true);
        tipNaZapasContainer.setOutputMarkupPlaceholderTag(true);
        tipNaZapasContainer.setVisible(!zapasy.isEmpty());
        add(tipNaZapasContainer);

        // tip na zapas - tabulka header bunka
        tipNaZapasContainer.add(new ListView<String>("tipNaZapasPouzivatel", tipNaZapasPouzivatelia) {
            private static final long serialVersionUID = 838891357045723430L;

            @Override
            protected void populateItem(ListItem<String> item) {
                item.add(new Label("login", item.getModelObject()));
            }
        });

        // zosortujeme zoznam zapasov
        Collections.sort(zapasy, new ZapasDatumComparator(OrderDirectionEnum.DESC));

        // tip na zapas - tabulka body riadok
        final ListView<ZapasDto> tipNaZapasView = new ListView<ZapasDto>("tipNaZapasRiadok", zapasy) {
            private static final long serialVersionUID = -8768084053796014417L;

            @Override
            protected void populateItem(ListItem<ZapasDto> item) {
                final ZapasDto zapas = item.getModelObject();
                // datum
                item.add(new Label("datum", datumFormat.format(zapas.getDatum())));
                // domaci
                boolean isDomaciPrehra = false;
                if (zapas.getDomaci() != null && zapas.getPrehra() != null && !zapas.isRemiza()) {
                    isDomaciPrehra = zapas.getDomaci().getId().equals(zapas.getPrehra().getId());
                }
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("domaci", zapas.getDomaci(), isDomaciPrehra));
                } else {
                    item.add(new VlajkaKodPanel("domaci", zapas.getDomaci(), isDomaciPrehra));
                }
                // skore
                item.add(new Label("skore", zapas.getSkore())
                        .add(new AttributeAppender("title", zapas.getPriebeh())));
                // hostia
                boolean isHostiaPrehra = false;
                if (zapas.getHostia() != null && zapas.getPrehra() != null && !zapas.isRemiza()) {
                    isHostiaPrehra = zapas.getHostia().getId().equals(zapas.getPrehra().getId());
                }
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("hostia", zapas.getHostia(), isHostiaPrehra));
                } else {
                    item.add(new VlajkaKodPanel("hostia", zapas.getHostia(), isHostiaPrehra));
                }

                // tip na zapas - tabulka body riadok bunka
                item.add(new ListView<String>("tipNaZapasGoly", tipNaZapasPouzivatelia) {
                    private static final long serialVersionUID = -1570462590599292407L;

                    @Override
                    protected void populateItem(ListItem<String> item) {
                        String login = item.getModelObject();
                        boolean zobrazitTip = !nesplnili.contains(login);
                        TipNaZapasDto tip = zobrazitTip
                                ? pouzitelovTipNaZapasMapa.get(zapas.getId()).get(login)
                                : null;

                        String tipTitle = (tip == null
                                ? ""
                                : (tip.isNahodnyTip()
                                    ? "Náhodný tip"
                                    : "Tip zadaný používateľom"));
                        String tipNaZapas = (tip == null
                                ? ""
                                : (tip.getGolyDomaci() + " : " + tip.getGolyHostia()));
                        if (tip != null && tip.isNahodnyTip()) {
                            item.add(new AttributeAppender("class", "nahodnyTip"));
                        }
                        item.add(new AttributeAppender("title", tipTitle));
                        Label tipNaZapasLabel = new Label("tip", Model.of(tipNaZapas));
                        if (StringUtils.isEmpty(tipNaZapas)) {
                            tipNaZapasLabel.add(new AttributeAppender("class", "fa fa-fw fa-remove"));
                        }
                        item.add(tipNaZapasLabel);
                        item.add(new Label("body", tip == null ? "" : String.valueOf(tip.getBody())));
                    }
                });
            }
        };
        tipNaZapasContainer.add(tipNaZapasView);

        //hraci den - combo
        List<Date> hracieDni = new ArrayList<Date>(zapasyPodlaHraciehoDna.keySet());
        Collections.sort(hracieDni, new Comparator<Date>() {
            @Override
            public int compare(Date o1, Date o2) {
                return o1.compareTo(o2) * -1;
            }
        });
        DropDownChoice<Date> hraciDenCombo = new DropDownChoice<>("hraciDen",
                new PropertyModel<Date>(this, "vybranyHraciDen"),
                hracieDni,
                new ChoiceRenderer<Date>() {
                    private static final long serialVersionUID = -2282976964858215214L;

                    @Override
                    public Object getDisplayValue(Date object) {
                        return getString("typ.hraciDen") + " " + datumShortFormat.format(object);
                    }
                }
        );
        add(hraciDenCombo);
        hraciDenCombo.add(new AjaxFormComponentUpdatingBehavior("onchange") {
            private static final long serialVersionUID = 3511757454011263161L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                if (vybranyHraciDen == null) {
                    tipNaZapasView.setList(zapasy);
                } else {
                    tipNaZapasView.setList(zapasyPodlaHraciehoDna.get(vybranyHraciDen));
                }
                target.add(tipNaZapasContainer);
            }
        });
        hraciDenCombo.setVisible(!zapasy.isEmpty());
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.TIP;
    }

    /**
     * Nasetuje mapu kde klucik je login a hodnota je tip na vitaza.
     */
    private void setPouzivatelovTipNaVitazaMap() {
        if (pouzitelovTipNaVitazaMapa == null) {
            pouzitelovTipNaVitazaMapa = new HashMap<>();
        }

        // overime, ci uz dany rocnik zacal a mozu byt zobrazene tipy na vitaza
        if (vybranyRocnik.getPrvyZapas() != null && vybranyRocnik.getPrvyZapas().getDatum().before(new Date())) {
            // pre rocnik dohladame vsetky tipy na vitaza
            List<TipNaVitazaDto> tipy = tipNaVitazaService.getTipyNaVitaza(vybranyRocnik.getId());
            for (TipNaVitazaDto tip : tipy) {
                pouzitelovTipNaVitazaMapa.put(tip.getPouzivatel().getLogin(), tip);
            }
        }
    }

    /**
     * Nacita vsetky tipy zapasov jednotlivych pouzivatelov, na ktore sa uz neda tipovat pre.
     * Nasetuje mapu, kde klucik je id zapasu a value je mapa, kde klucik je pouzivatel a value je tip na zapas.
     */
    private void setPouzivatelovTipNaZapasMap() {
        if (pouzitelovTipNaZapasMapa == null) {
            pouzitelovTipNaZapasMapa = new HashMap<>();
        }

        // nacitame si zapasy, ktore sa uz odohrali, alebo sa zacali hrat
        ZapasKriteria kriteria = new ZapasKriteria();
        kriteria.setSutazRocnikId(vybranyRocnik.getId());
        kriteria.setDatumDoPresne(new Date());
        zapasy = zapasService.findZapasy(kriteria);

        // nainicializujeme mapu dostupnych tipov na zapas
        for (ZapasDto z : zapasy) {
            pouzitelovTipNaZapasMapa.put(z.getId(), new HashMap<String, TipNaZapasDto>());
        }

        // dohladame tipy na dostupne zapasy
        List<TipNaZapasDto> tipy = tipNaZapasService
                .getTipyNaZapasPodlaZapasId(new ArrayList<>(pouzitelovTipNaZapasMapa.keySet()));
        for (TipNaZapasDto t : tipy) {
            if (pouzitelovTipNaZapasMapa.get(t.getZapas().getId()) != null) {
                pouzitelovTipNaZapasMapa.get(t.getZapas().getId()).put(t.getPouzivatel().getLogin(), t);
            }
        }

        // a este na zaver potriedime zapasy podla hracieho dna
        for (ZapasDto z : zapasy) {
            Date hraciDen = getDenHraniaZapasu(z);
            if (zapasyPodlaHraciehoDna.get(hraciDen) == null) {
                zapasyPodlaHraciehoDna.put(hraciDen, new ArrayList<ZapasDto>());
            }
            zapasyPodlaHraciehoDna.get(hraciDen).add(z);
        }
    }

    /**
     * Vrati datum zapasu bez casovej stopy.
     *
     * @param zapas zapas
     * @return      datum
     */
    private Date getDenHraniaZapasu(ZapasDto zapas) {
        return new Date(
                new DateMidnight(zapas.getDatum())
                        .toDateTime()
                        .getMillis());
    }
}
