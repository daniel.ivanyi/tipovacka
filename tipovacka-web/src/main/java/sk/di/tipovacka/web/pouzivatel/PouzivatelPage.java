package sk.di.tipovacka.web.pouzivatel;

import org.apache.wicket.markup.html.form.Form;
import org.apache.wicket.markup.html.form.RequiredTextField;
import org.apache.wicket.model.CompoundPropertyModel;
import org.apache.wicket.model.StringResourceModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.api.service.PouzivatelService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.menu.MenuItemEnum;

/**
 * Stranka pre informacie o pouzivatelovi.
 * <p/>
 * User: Dano
 * Date: 21.5.2013
 */
public class PouzivatelPage extends Template {

    private static final long serialVersionUID = -669903431962497439L;

    private PouzivatelDto user;

    @SpringBean
    private PouzivatelService pouzivatelService;

    public PouzivatelPage(PageParameters parameters) {
        super(parameters);

        user = pouzivatel;

        // model
        final CompoundPropertyModel<PouzivatelDto> model = new CompoundPropertyModel<PouzivatelDto>(user);
        // form
        final Form<PouzivatelDto> userForm = new Form<PouzivatelDto>("zmenaForm", model) {
            private static final long serialVersionUID = 684599510734444272L;

            @Override
            protected void onSubmit() {
                super.onSubmit();

                // vysledna stranka
                PouzivatelPage page;
                // spracovanie pouzivatela
                try {
                    pouzivatelService.editPouzivatel(pouzivatel.getLogin(), user);

                    page = new PouzivatelPage(new PageParameters());
                    page.info(new StringResourceModel("pouzivatel.info.zmeneny",
                            this,
                            null,
                            new Object[]{user.getLogin()}).getString());
                } catch (Exception e) {
                    page = new PouzivatelPage(new PageParameters());
                    page.error(new StringResourceModel("pouzivatel.error.spracovat",
                            this,
                            null,
                            new Object[]{user.getLogin()}).getString());
                }

                setResponsePage(page);
            }
        };
        add(userForm);

        // login - input text
        userForm.add(new RequiredTextField<String>("login"));

        // mail - input text
        userForm.add(new RequiredTextField<String>("mail"));

        // meno - input text
        userForm.add(new RequiredTextField<String>("meno"));

        // priezvisko - input text
        userForm.add(new RequiredTextField<String>("priezvisko"));

    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.POUZIVATEL;
    }
}
