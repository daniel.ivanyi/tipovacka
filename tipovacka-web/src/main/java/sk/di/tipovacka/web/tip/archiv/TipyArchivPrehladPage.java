package sk.di.tipovacka.web.tip.archiv;

import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import sk.di.tipovacka.api.dto.RocnikSutazeHlavickaDto;
import sk.di.tipovacka.api.service.SutazRocnikService;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.util.ArrayList;
import java.util.List;

/**
 * Archiv ukoncenych rocnikov.
 *
 * User: Dano
 * Date: 9.11.2013
 */
public class TipyArchivPrehladPage extends Template {

    private static final long serialVersionUID = 2975209854567965805L;

    @SpringBean
    private SutazRocnikService sutazRocnikService;

    public TipyArchivPrehladPage(PageParameters parameters) {
        super(parameters);

        // nacitame ukoncene rocniky
        List<RocnikSutazeHlavickaDto> rocniky = sutazRocnikService.getUkonceneRocniky();

        add(new ListView<RocnikSutazeHlavickaDto>("rocniky", rocniky) {

            private static final long serialVersionUID = -3297974243259321880L;

            @Override
            protected void populateItem(ListItem<RocnikSutazeHlavickaDto> item) {
                RocnikSutazeHlavickaDto rodnik = item.getModelObject();
                item.add(new TipyArchivUkoncenyRocnikPanel("rocnikArchiv",rodnik.getId() ));
            }
        });
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ARCHIV;
    }
}
