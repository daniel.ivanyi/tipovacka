package sk.di.tipovacka.web.dialog;

/**
 * Akcie, ktore chceme potvrdit v dialogu
 *
 * User: Dano
 * Date: 26.5.2013
 */
public enum DialogAkciaEnum {

    VYMAZANIE,
    INICIALIZACIA_ZAPASOV
}
