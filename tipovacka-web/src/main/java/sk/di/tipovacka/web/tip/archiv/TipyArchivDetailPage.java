package sk.di.tipovacka.web.tip.archiv;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.ChoiceRenderer;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.list.ListItem;
import org.apache.wicket.markup.html.list.ListView;
import org.apache.wicket.model.Model;
import org.apache.wicket.model.PropertyModel;
import org.apache.wicket.request.mapper.parameter.PageParameters;
import org.apache.wicket.spring.injection.annot.SpringBean;
import org.joda.time.DateMidnight;
import sk.di.tipovacka.api.comparator.ZapasDatumComparator;
import sk.di.tipovacka.api.db.kriteria.ZapasKriteria;
import sk.di.tipovacka.api.dto.*;
import sk.di.tipovacka.api.enums.OrderDirectionEnum;
import sk.di.tipovacka.api.service.*;
import sk.di.tipovacka.web.Template;
import sk.di.tipovacka.web.commons.panel.NadpisPanel;
import sk.di.tipovacka.web.commons.panel.vlajka.VlajkaKodPanel;
import sk.di.tipovacka.web.commons.panel.vlajka.klub.VlajkaKlubPanel;
import sk.di.tipovacka.web.menu.MenuItemEnum;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Detail ukonceneho rocnika.
 *
 * User: Dano
 * Date: 9.11.2013
 */
public class TipyArchivDetailPage extends Template {
    private static final long serialVersionUID = -7737754458099052869L;

    private final SimpleDateFormat datumFormat = new SimpleDateFormat("dd.MM.yyyy HH:mm");
    private final SimpleDateFormat datumShortFormat = new SimpleDateFormat("dd.MM.yyyy");

    @SpringBean
    private TipNaVitazaService tipNaVitazaService;
    @SpringBean
    private TipNaZapasService tipNaZapasService;
    @SpringBean
    private ZapasService zapasService;
    @SpringBean
    private SutazRocnikService sutazRocnikService;
    @SpringBean
    private StatistikaService statistikaService;

    // mapa tipov pouzivatela na vitaza
    private Map<String, TipNaVitazaDto> pouzitelovTipNaVitazaMapa = new HashMap<>();
    // mapa zoznamu tipov pouzivatelov na zapas podla zapasov
    private Map<Long, Map<String, TipNaZapasDto>> pouzitelovTipNaZapasMapa = new HashMap<>();
    // zoznam zacatych a odohratych zapasov
    private List<ZapasDto> zapasy = new ArrayList<>();
    // mapa zapasov podla hracieho dna
    private Map<Date, List<ZapasDto>> zapasyPodlaHraciehoDna = new HashMap<>();
    // hraci den, za ktory sa maju zobrazit tipy na zapas
    private Date vybranyHraciDen;

    public TipyArchivDetailPage(PageParameters parameters) {
        super(parameters);

        Long rocnikId = parameters.get("archivRocnikId").toLong();
        RocnikSutazeDto rocnik = sutazRocnikService.getRocnikSutaze(rocnikId);

        // priznak, ci sa jedna o klubovu sutaz
        final boolean isKlub = rocnik.getSutaz().isKlub();

        // nadpis - panel
        add(new NadpisPanel("nadpisPanel", "", rocnik));

        // nacitame si vsetky tipy na vitaza vo forme mapy
        setPouzivatelovTipNaVitazaMap(rocnikId);

//        // zistime si kolko uzivatelov sa zucastnilo tipovania
//        final List<String> tipNaVitazaPouzivatelia = new ArrayList<>(pouzitelovTipNaVitazaMapa.keySet());
//
//        // zosortujeme
//        Collections.sort(tipNaVitazaPouzivatelia, new Comparator<String>() {
//            @Override
//            public int compare(String o1, String o2) {
//                return o1.compareToIgnoreCase(o2);
//            }
//        });

        // nacitame si vsetky tipy na zapasy pre dany rocnik vo forme mapy
        setPouzivatelovTipNaZapasMap(rocnikId);

        // zistime si kolko uzivatelov sa zucastnilo tipovania
        final List<String> tipNaZapasPouzivatelia = new ArrayList<>();
        // pomocou setu dostaneme jedinecne loginy
        Set<String> pouzivatelia = new HashSet<>();
        for (Long key : pouzitelovTipNaZapasMapa.keySet()) {
            pouzivatelia.addAll(pouzitelovTipNaZapasMapa.get(key).keySet());
        }
        tipNaZapasPouzivatelia.addAll(pouzivatelia);
        // zosortujeme
        Collections.sort(tipNaZapasPouzivatelia, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                return o1.compareToIgnoreCase(o2);
            }
        });

        // tipy na vitaza - tabulka header bunka
        add(new ListView<String>("tipNaVitazaPouzivatel", tipNaZapasPouzivatelia) {
            private static final long serialVersionUID = -3419951167271062067L;

            @Override
            protected void populateItem(ListItem<String> item) {
                item.add(new Label("login", item.getModelObject()));
            }
        });

        final MuzstvoDto vitazRocnika = rocnik.getVitaz();

        // tipy na vitaza - tabulka body bunka
        add(new ListView<String>("tipNaVitazaMuzstvo", tipNaZapasPouzivatelia) {
            private static final long serialVersionUID = -924435045712624946L;

            @Override
            protected void populateItem(ListItem<String> item) {
                final TipNaVitazaDto tip = pouzitelovTipNaVitazaMapa.get(item.getModelObject());
                MuzstvoDto tipnuteMuzstvo = (tip == null ? null : tip.getMuzstvo());
                boolean spravnyTyp = false;
                if (tipnuteMuzstvo != null && vitazRocnika != null) {
                    spravnyTyp = tipnuteMuzstvo.getId().equals(vitazRocnika.getId());
                }
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("vlajka", tipnuteMuzstvo, !spravnyTyp));
                } else {
                    item.add(new VlajkaKodPanel("vlajka", tipnuteMuzstvo, !spravnyTyp));
                }
                item.add(new Label("body", tip == null ? "0" : String.valueOf(tip.getBody())));
            }
        });

        // tip na zapas - webcontainer
        final WebMarkupContainer tipNaZapasContainer = new WebMarkupContainer("tipNaZapasContainer");
        tipNaZapasContainer.setOutputMarkupId(true);
        tipNaZapasContainer.setOutputMarkupPlaceholderTag(true);
        tipNaZapasContainer.setVisible(!zapasy.isEmpty());
        add(tipNaZapasContainer);

        // tip na zapas - tabulka header bunka
        tipNaZapasContainer.add(new ListView<String>("tipNaZapasPouzivatel", tipNaZapasPouzivatelia) {
            private static final long serialVersionUID = 6800019168117668346L;

            @Override
            protected void populateItem(ListItem<String> item) {
                item.add(new Label("login", item.getModelObject()));
            }
        });

        // statistika pouzivatelov pre dany rocnik
        final Map<String, Integer> ziskaneBody = new HashMap<>();
        List<PouzivatelovTipStatistikaDto> statistika = statistikaService.getStatistikaTiperov(rocnikId);
        for (PouzivatelovTipStatistikaDto s : statistika) {
            ziskaneBody.put(s.getPouzivatel().getLogin(), s.getBody());
        }
        tipNaZapasContainer.add(new ListView<String>("statistikaPouzivatela", tipNaZapasPouzivatelia) {
            private static final long serialVersionUID = -7958808031558571025L;

            @Override
            protected void populateItem(ListItem<String> item) {
                item.add(new Label("ziskaneBody", ziskaneBody.get(item.getModelObject()).toString()));
            }
        });

        // zosortujeme zoznam zapasov
        Collections.sort(zapasy, new ZapasDatumComparator(OrderDirectionEnum.ASC));

        // tip na zapas - tabulka body riadok
        final ListView<ZapasDto> tipNaZapasView = new ListView<ZapasDto>("tipNaZapasRiadok", zapasy) {
            private static final long serialVersionUID = 51270792960302462L;

            @Override
            protected void populateItem(ListItem<ZapasDto> item) {
                final ZapasDto zapas = item.getModelObject();
                // datum
                item.add(new Label("datum", datumFormat.format(zapas.getDatum())));
                // domaci
                boolean isDomaciPrehra = false;
                if (zapas.getDomaci() != null && zapas.getPrehra() != null && !zapas.isRemiza()) {
                    isDomaciPrehra = zapas.getDomaci().getId().equals(zapas.getPrehra().getId());
                }
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("domaci", zapas.getDomaci(), isDomaciPrehra));
                } else {
                    item.add(new VlajkaKodPanel("domaci", zapas.getDomaci(), isDomaciPrehra));
                }
                // skore
                item.add(new Label("skore", zapas.getSkore())
                        .add(new AttributeAppender("title", zapas.getPriebeh())));
                // hostia
                boolean isHostiaPrehra = false;
                if (zapas.getHostia() != null && zapas.getPrehra() != null && !zapas.isRemiza()) {
                    isHostiaPrehra = zapas.getHostia().getId().equals(zapas.getPrehra().getId());
                }
                if (isKlub) {
                    item.add(new VlajkaKlubPanel("hostia", zapas.getHostia(), isHostiaPrehra));
                } else {
                    item.add(new VlajkaKodPanel("hostia", zapas.getHostia(), isHostiaPrehra));
                }

                // tip na zapas - tabulka body riadok bunka
                item.add(new ListView<String>("tipNaZapasGoly", tipNaZapasPouzivatelia) {
                    private static final long serialVersionUID = 413055009915138277L;

                    @Override
                    protected void populateItem(ListItem<String> item) {
                        String login = item.getModelObject();

                        TipNaZapasDto tip = pouzitelovTipNaZapasMapa.get(zapas.getId()).get(login);
                        String tipNaZapas = (tip == null
                                ? ""
                                : (tip.getGolyDomaci() + " : " + tip.getGolyHostia()));
                        Label tipNaZapasLabel = new Label("tip", Model.of(tipNaZapas));
                        if (StringUtils.isEmpty(tipNaZapas)) {
                            tipNaZapasLabel.add(new AttributeAppender("class", "fa fa-fw fa-remove"));
                        }
                        item.add(tipNaZapasLabel);
                        item.add(new Label("body", tip == null ? "" : String.valueOf(tip.getBody())));
                    }
                });
            }
        };
        tipNaZapasContainer.add(tipNaZapasView);

        //hraci den - combo
        List<Date> hracieDni = new ArrayList<Date>(zapasyPodlaHraciehoDna.keySet());
        Collections.sort(hracieDni);
        DropDownChoice<Date> hraciDenCombo = new DropDownChoice<Date>("hraciDen",
                new PropertyModel<Date>(this, "vybranyHraciDen"),
                hracieDni,
                new ChoiceRenderer<Date>() {
                    private static final long serialVersionUID = 966552687024706636L;

                    @Override
                    public Object getDisplayValue(Date object) {
                        return getString("typ.hraciDen") + " " + datumShortFormat.format(object);
                    }
                }
        );
        hraciDenCombo.setNullValid(true);

        tipNaZapasContainer.add(hraciDenCombo);
        hraciDenCombo.add(new AjaxFormComponentUpdatingBehavior("onchange") {
            private static final long serialVersionUID = 138942909618056396L;

            @Override
            protected void onUpdate(AjaxRequestTarget target) {
                if (vybranyHraciDen == null) {
                    tipNaZapasView.setList(zapasy);
                } else {
                    tipNaZapasView.setList(zapasyPodlaHraciehoDna.get(vybranyHraciDen));
                }
                target.add(tipNaZapasContainer);
            }
        });
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ARCHIV;
    }

    /**
     * Nasetuje mapu kde klucik je login a hodnota je tip na vitaza.
     *
     * @param rocnikId rocnik, pre ktory dotahujeme tipy na vitaza
     */
    private void setPouzivatelovTipNaVitazaMap(Long rocnikId) {
        // pre rocnik dohladame vsetky tipy na vitaza
        List<TipNaVitazaDto> tipy = tipNaVitazaService.getTipyNaVitaza(rocnikId);
        for (TipNaVitazaDto tip : tipy) {
            pouzitelovTipNaVitazaMapa.put(tip.getPouzivatel().getLogin(), tip);
        }
    }

    /**
     * Nacita vsetky tipy zapasov jednotlivych pouzivatelov.
     * Nasetuje mapu, kde klucik je id zapasu a value je mapa,
     * kde klucik je pouzivatel a value je tip na zapas.
     *
     * @param rocnikId identifikator rocnika, pre ktory sa dohladavaju tipy na zapas
     */
    private void setPouzivatelovTipNaZapasMap(Long rocnikId) {
        // nacitame si zapasy, ktore sa uz odohrali, alebo sa zacali hrat
        ZapasKriteria kriteria = new ZapasKriteria();
        kriteria.setSutazRocnikId(rocnikId);
        kriteria.setDatumDoPresne(new Date());
        zapasy = zapasService.findZapasy(kriteria);

        // nainicializujeme mapu dostupnych tipov na zapas
        for (ZapasDto z : zapasy) {
            pouzitelovTipNaZapasMapa.put(z.getId(), new HashMap<String, TipNaZapasDto>());
        }

        // dohladame tipy na dostupne zapasy
        List<TipNaZapasDto> tipy = tipNaZapasService
                .getTipyNaZapasPodlaZapasId(new ArrayList<>(pouzitelovTipNaZapasMapa.keySet()));
        for (TipNaZapasDto t : tipy) {
            if (pouzitelovTipNaZapasMapa.get(t.getZapas().getId()) != null) {
                pouzitelovTipNaZapasMapa.get(t.getZapas().getId()).put(t.getPouzivatel().getLogin(), t);
            }
        }

        // a este na zaver potriedime zapasy podla hracieho dna
        for (ZapasDto z : zapasy) {
            Date hraciDen = getDenHraniaZapasu(z);
            if (zapasyPodlaHraciehoDna.get(hraciDen) == null) {
                zapasyPodlaHraciehoDna.put(hraciDen, new ArrayList<ZapasDto>());
            }
            zapasyPodlaHraciehoDna.get(hraciDen).add(z);
        }
    }

    /**
     * Vrati datum zapasu bez casovej stopy.
     *
     * @param zapas zapas
     * @return      datum
     */
    private Date getDenHraniaZapasu(ZapasDto zapas) {
        return new Date(
                new DateMidnight(zapas.getDatum())
                        .toDateTime()
                        .getMillis());
    }
}
