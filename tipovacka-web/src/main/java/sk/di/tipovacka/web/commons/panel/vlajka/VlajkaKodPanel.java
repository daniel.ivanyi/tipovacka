package sk.di.tipovacka.web.commons.panel.vlajka;

import org.apache.commons.lang.StringUtils;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.Model;
import sk.di.tipovacka.api.dto.MuzstvoDto;

/**
 * Panel pre zobrazenie vlajky.
 * <p/>
 * User: IVAN1497
 * Date: 4.6.2013
 */
public class VlajkaKodPanel extends Panel {
    private static final long serialVersionUID = 5341866800108828781L;

    public VlajkaKodPanel(String id, MuzstvoDto muzstvo) {
        this(id, muzstvo, false);
    }

    public VlajkaKodPanel(String id, MuzstvoDto muzstvo, boolean isSeda) {
        super(id);

        String vlajka = "";
        String title = "";

        if (muzstvo != null) {
            vlajka = muzstvo.getKod().toLowerCase();
            title = muzstvo.getNazov() + " (" + muzstvo.getKod() + ")";
        }
// http://demosthenes.info/blog/532/convert-images-to-black-and-white-with-css
// http://stackoverflow.com/questions/609273/convert-an-image-to-grayscale-in-html-css
        add(new WebMarkupContainer("vlajka")
                .add(new AttributeAppender("class", (isSeda ? "flagsp_g" : "flagsp")
                        + (StringUtils.isEmpty(vlajka) ? "" : " flagsp_" + vlajka)))
                .add(new AttributeAppender("title", title)));
        add(new Label("kod", Model.of(muzstvo == null ? "NUL" : muzstvo.getKod())));
    }
}
