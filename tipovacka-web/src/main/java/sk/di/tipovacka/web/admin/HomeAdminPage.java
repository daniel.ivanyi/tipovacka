package sk.di.tipovacka.web.admin;

import org.apache.wicket.request.mapper.parameter.PageParameters;
import sk.di.tipovacka.web.menu.MenuItemEnum;
import sk.di.tipovacka.web.Template;

/**
 * Uvodna obrazovka pre administraciu
 *
 * User: Dano
 * Date: 19.5.2013
 */
public class HomeAdminPage extends Template {

    private static final long serialVersionUID = -608571762017558228L;

    public HomeAdminPage(PageParameters parameters) {
        super(parameters);
    }

    @Override
    public MenuItemEnum getActiveMenu() {
        return MenuItemEnum.ADMIN;
    }
}
