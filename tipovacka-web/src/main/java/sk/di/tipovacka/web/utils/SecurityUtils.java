package sk.di.tipovacka.web.utils;

import org.apache.wicket.injection.Injector;
import sk.di.tipovacka.api.dto.PouzivatelDto;
import sk.di.tipovacka.web.SpringSecurityAuthenticatedWebSession;

/**
 * User: Dano
 * Date: 16.5.2013
 */
public class SecurityUtils {

    private static SecurityUtils instance = new SecurityUtils();

    private SecurityUtils() {
        injectDependencies();
    }

    private void injectDependencies() {
        Injector.get().inject(this);
    }
    /**
     * Metodka, vrati aktualne prihlaseneho usera
     *
     * @return aktualne prihlaseny user
     */
    public static SpringSecurityAuthenticatedWebSession getSpringSecuritySession() {
        return SpringSecurityAuthenticatedWebSession.getSpringWicketWebSession();
    }

    /**
     * Metodka, vrati aktualne prihlaseneho usera
     *
     * @return aktualne prihlaseny user
     */
    public static PouzivatelDto getCurrentUser() {
        return SpringSecurityAuthenticatedWebSession.getSpringWicketWebSession().getPouzivatel();
    }

    /**
     * Metodka, vrati login aktualne prihlaseneho pouzivatela
     *
     * @return login prihlaseneho pouzivatela
     */
    public static String getCurrentUserLogin() {
        return getCurrentUser().getLogin();
    }
}
