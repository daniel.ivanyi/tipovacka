package sk.di.tipovacka.web.utils;

import org.apache.commons.lang.time.DateUtils;
import org.apache.log4j.Logger;

import java.util.Arrays;
import java.util.Date;

/**
 * Utilitka pre parsovanie udajov na pozadovany typ.
 * <p/>
 * User: Dano
 * Date: 2.6.2013
 */
public class ParserUtils {

    private static final Logger logger = Logger.getLogger(ParserUtils.class);

    /**
     * Vyparsuje datum zo zadaneho textu
     *
     * @param text text
     * @return java.util.Date
     */
    public static Date parseDate(String text) {

        String[] datePatterns = new String[]{
                "dd.MM.yyyy",
                "dd.MM.yyyy HH:mm"
        };

        try {
            return DateUtils.parseDate(text, datePatterns);
        } catch (Exception e) {
            logger.warn("text[" + text + "] zadany v nepodporovanom formate[" + Arrays.asList(datePatterns) + "]");
            throw new IllegalArgumentException(e);
        }
    }
}
