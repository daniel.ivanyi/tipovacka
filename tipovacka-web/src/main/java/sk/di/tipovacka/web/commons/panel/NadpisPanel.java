package sk.di.tipovacka.web.commons.panel;

import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.image.NonCachingImage;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.request.resource.ContextRelativeResource;
import sk.di.tipovacka.api.dto.RocnikSutazeDto;
import sk.di.tipovacka.api.enums.SutazTypEnum;

/**
 * Panel pre nadpis tiperovi.
 *
 * User: Dano
 * Date: 27.10.2013
 */
public class NadpisPanel extends Panel {

    private static final long serialVersionUID = 341468198257977217L;

    public NadpisPanel(String id, String nadpis, RocnikSutazeDto rocnik) {
        super(id);

        String rocnikNadpis = getString("nadpis.rocnik.prazdny");
        String rocnikLogo = "/img/unknown.jpg";

        // ak mame navoleny rocnik, dotiahneme pre neho udaje
        if (rocnik != null && rocnik.getId() != null) {
            String typSportu = getString("sport.typ.nadpis." + rocnik.getSutaz().getSportTyp());
            String aktualnyRocnikSutaze = rocnik.getSutaz().getNazov() + " " + rocnik.getRok();
            rocnikNadpis = typSportu + " / " + aktualnyRocnikSutaze;

            String sportTyp = rocnik.getSutaz().getSportTyp().name().toLowerCase();
            String sutazTyp = rocnik.getSutaz().getSutazTyp().getSkratka().toLowerCase();
            int rok = rocnik.getRok();
            if (SutazTypEnum.LIGA_MAJSTROV.equals(rocnik.getSutaz().getSutazTyp())) {
                rocnikLogo = "/img/logo_futbal_lm.jpg";
            } else {
                rocnikLogo = "/img/logo_" + sportTyp + "_" + sutazTyp + "_" + rok + ".jpg";
            }
        }

        add(new NonCachingImage("rocnikLogo", new ContextRelativeResource(rocnikLogo)));
        add(new Label("rocnikNadpis", rocnikNadpis));
        add(new Label("nadpis", nadpis));
    }
}
