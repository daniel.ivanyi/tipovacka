package sk.di.tipovacka.web.dialog.tip;

import org.apache.wicket.ajax.AjaxRequestTarget;
import org.apache.wicket.ajax.form.AjaxFormComponentUpdatingBehavior;
import org.apache.wicket.ajax.markup.html.AjaxLink;
import org.apache.wicket.behavior.AttributeAppender;
import org.apache.wicket.markup.html.WebMarkupContainer;
import org.apache.wicket.markup.html.basic.Label;
import org.apache.wicket.markup.html.form.DropDownChoice;
import org.apache.wicket.markup.html.panel.Panel;
import org.apache.wicket.model.PropertyModel;
import sk.di.tipovacka.api.dto.TipNaZapasDto;
import sk.di.tipovacka.api.dto.ZapasDto;
import sk.di.tipovacka.web.commons.panel.vlajka.VlajkaKodPanel;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.List;

/**
 * Modalny dialog pre tipovanie
 * User: Dano
 * Date: 4.6.2013
 */
public abstract class TipovanieModalDialog extends Panel {

    private ZapasDto zapas;
    private TipNaZapasDto tip;
    private Label nadpisLabel;

    private static final List<Integer> goly =
            Arrays.asList(0, 1 , 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20);

    private static final long serialVersionUID = -1107215701232555105L;

    public TipovanieModalDialog(String id) {
        super(id);

        // nadpis - label
        add(nadpisLabel = new Label("nadpis", "Potvrdenie"));

        // vybrany zapas
        if (zapas == null) {
            zapas = new ZapasDto();
        }

        // vybrany tip
        if (tip == null) {
            tip = new TipNaZapasDto();
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd.MM.yyyy HH:mm");

        add(new WebMarkupContainer("statusIcon").add(new AttributeAppender("class", zapas.getTipNaZapasStavIcon(tip))));
        add(new Label("datum", zapas.getDatum() == null ? "" : sdf.format(zapas.getDatum())));
        add(new Label("skupina", zapas.getSkupina()));
        add(new Label("domaci", zapas.getDomaci() == null ? "" : zapas.getDomaci().getNazov()));
        add(new VlajkaKodPanel("domaciVlajka", zapas.getDomaci()));

        add(new DropDownChoice<Integer>("golyDomaci", new PropertyModel<Integer>(tip, "golyDomaci"), goly)
                .add(new AjaxFormComponentUpdatingBehavior("onchange") {
                    private static final long serialVersionUID = -7632348442979365989L;

                    @Override
                    protected void onUpdate(AjaxRequestTarget target) {
                        System.out.println("tip.golyDomaci=" + getDefaultModelObject());
                    }
                }));

        add(new DropDownChoice<Integer>("golyHostia", new PropertyModel<Integer>(tip, "golyHostia"), goly)
                .add(new AjaxFormComponentUpdatingBehavior("onchange") {

                    private static final long serialVersionUID = 2982052159216104872L;

                    @Override
                    protected void onUpdate(AjaxRequestTarget target) {
                        System.out.println("tip.golyHostia=" + getDefaultModelObject());
                    }
                }));

        add(new VlajkaKodPanel("hostiaVlajka", zapas.getHostia()));
        add(new Label("hostia", zapas.getHostia() == null ? "" : zapas.getHostia().getNazov()));
        add(new Label("zapasTyp", getString("zapas.typ." + zapas.getTypZapasu())));

        // tlacitko - ano
        add(new AjaxLink<String>("anoTlacitko") {

            private static final long serialVersionUID = -637704012906035771L;

            @Override
            public void onClick(AjaxRequestTarget target) {
                tipuj(target);
            }
        });
    }

    public abstract void tipuj(AjaxRequestTarget target);

    public ZapasDto getZapas() {
        return zapas;
    }

    public void setZapas(ZapasDto zapas) {
        this.zapas = zapas;
    }

    public TipNaZapasDto getTip() {
        return tip;
    }

    public void setTip(TipNaZapasDto tip) {
        this.tip = tip;
    }

    public void zmenNadpis(String nadpis) {
        nadpisLabel.setDefaultModelObject(nadpis);
    }
}
